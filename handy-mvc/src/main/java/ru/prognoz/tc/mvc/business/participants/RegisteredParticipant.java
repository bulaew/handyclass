package ru.prognoz.tc.mvc.business.participants;

import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;

public class RegisteredParticipant implements IParticipant {

	private User user;
	private boolean removed;

	public RegisteredParticipant(CourseRegistration cr) {
		super();
		this.user = cr.getUser();
		this.removed = false;
	}
	
	@Override
	public int getType() {
		return 3;
	}

	@Override
	public User getUser() {
		return this.user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String getGuestEmail() {
		return null;
	}

	@Override
	public void setGuestEmail(String guestEmail) {
	}

	@Override
	public boolean isRemoved() {
		return removed;
	}

	@Override
	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	@Override
	public int compareTo(IParticipant o) {
		if (o == null) {
			return -1;
		}
		if (o instanceof RegisteredParticipant) {
			return -1; // зарегистрированные участники всегда выше.
		}
		if (o instanceof TeacherParticipant) {
			return 1; //Учитель всегда первый.
		}
		if (this.user == null && o.getUser() == null) { // если оба участника добавлены через емейл, то сортируем по нем.
			return 0;
		} else {
			if (this.user == null) {
				return 1;
			}
			if (o.getUser() == null) {
				return -1;
			}
			return this.user.getUsername().compareTo(o.getUser().getUsername()); // сортируем по ФИО.
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegisteredParticipant other = (RegisteredParticipant) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (user.getId() != other.user.getId())
			return false;
		return true;
	}

}
