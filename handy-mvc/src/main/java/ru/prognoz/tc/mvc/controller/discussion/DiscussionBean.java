package ru.prognoz.tc.mvc.controller.discussion;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;

@Component(value = "discussionBean")
@Scope("session")
public class DiscussionBean implements Serializable {
	private static final long serialVersionUID = -841537077294288799L;
	private boolean editCourseMode = true;
	private int countRegistrations;
	private long userOid;

	public long getUserOid() {
		return userOid;
	}

	public void setUserOid(long userOid) {
		this.userOid = userOid;
	}

	private Course course;
	/**
	 * Активное обсуждение
	 */
	private Discussion activeDiscussion = null;
	/**
	 * Обсуждение в состоянии редактирования.
	 */
	private Boolean editableDiscussion = false;

	public int getCountRegistrations() {
		return countRegistrations;
	}

	public void setCountRegistrations(int countRegistrations) {
		this.countRegistrations = countRegistrations;
	}

	public Boolean getEditableDiscussion() {
		return editableDiscussion;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void clear() {
		editCourseMode = true;
		course = null;
		activeDiscussion = null;
		editableDiscussion = false;
	}

	public boolean isEditCourseMode() {
		return editCourseMode;
	}

	public void setEditCourseMode(boolean editCourseMode) {
		this.editCourseMode = editCourseMode;
	}

	public Discussion getActiveDiscussion() {
		return activeDiscussion;
	}

	public void setActiveDiscussion(Discussion activeDiscussion) {
		this.activeDiscussion = activeDiscussion;
	}

	public Boolean isEditableDiscussion() {
		return editableDiscussion;
	}

	public void setEditableDiscussion(Boolean editableDiscussion) {
		this.editableDiscussion = editableDiscussion;
	}
}
