package ru.prognoz.tc.mvc.security;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;

/**
 * Хендлер, для обработки завершения сессии авторизованного пользователя. Используется для учета времени, проведенного пользователем на
 * сайте.
 * 
 * @author mikulich
 * 
 */
@Component
public class HandyclassAuthenticationDestroyedHandler implements ApplicationListener<SessionDestroyedEvent> {

	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {
		List<SecurityContext> lstSecurityContext = event.getSecurityContexts();
		for (SecurityContext securityContext : lstSecurityContext) {
			User user = (User) securityContext.getAuthentication().getPrincipal();
			if (user != null) {
				long totalTime = user.getTotalTime();
				HttpSession se = (HttpSession) event.getSource();
				totalTime += (se.getLastAccessedTime() - se.getCreationTime());
				user.setTotalTime(totalTime);
				UserDao userDao = HandyclassAppContext.getBean(UserDao.class);
				if (userDao != null) {
					userDao.save(user);
				}
			}
		}
	}

}
