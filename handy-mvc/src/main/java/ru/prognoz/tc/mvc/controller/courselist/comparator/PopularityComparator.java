package ru.prognoz.tc.mvc.controller.courselist.comparator;

import java.util.Comparator;

import ru.prognoz.tc.mvc.controller.courselist.CourseItem;

public class PopularityComparator implements Comparator<CourseItem> {
	@Override
	public int compare(CourseItem arg0, CourseItem arg1) {
		return arg1.getCountReg().compareTo(arg0.getCountReg());
	}
}