package ru.prognoz.tc.mvc.business.reports;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.flow.ReportModule;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryCriteria;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingCriteria;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityCriteria;
import ru.prognoz.tc.core.reports.impl.courseinfo.CourseInfoCriteria;
import ru.prognoz.tc.core.reports.impl.myachievement.MyAchievementCriteria;
import ru.prognoz.tc.core.reports.impl.mycourselearn.MyCoursesLearningCriteria;
import ru.prognoz.tc.core.reports.impl.mycourserating.MyCourseRatingCriteria;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationCriteria;
import ru.prognoz.tc.core.reports.impl.propensity.MyPropensityCriteria;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.UserDao;

@Service
public class ReportsService {

	@Autowired
	private ReportModule reportModule;
	@Autowired
	private UserDao userDao;
	@Autowired
	private CourseDao courseDao;

	public long countCourses() {
		return courseDao.countCourses();
	}

	public long countUsers() {
		return userDao.countUsers();
	}

	public List<ReportItem> getAuthorActivityItems(AuthorActivityCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.AUTHOR_ACTIVITY, criteria);
	}

	public List<ReportItem> getAllCourseRatingItems(AllCourseRatingCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.COURSE_RATING, criteria);
	}
	
	public List<ReportItem> getActualCategoryItems(ActualCategoryCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.ACTUAL_CATEGORY, criteria);
	}

	public List<ReportItem> getMyCourseRatingItems(MyCourseRatingCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.MY_COURSE_RATING, criteria);
	}
	
	public List<ReportItem> getMyCourseLearningItems(MyCoursesLearningCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.MY_COURSES_LEARNING, criteria);
	}
	
	public List<ReportItem> getMyEducationItems(MyEducationCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.MY_EDUCATION, criteria);
	}
	
	public List<ReportItem> getMyAchievementsItems(MyAchievementCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.MY_ACHIEVEMENT, criteria);
	} 
	
	public List<ReportItem> getMyPropensityItems(MyPropensityCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.MY_PROPENSITY, criteria);
	} 
	
	public List<ReportItem> getCourseInfoItems(CourseInfoCriteria criteria) throws HandyCoreException {
		return reportModule.getData(ReportType.COURSE_INFO, criteria);
	}
}
