package ru.prognoz.tc.mvc.controller.auth.login;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.auth.AuthService;
import ru.prognoz.tc.mvc.business.mail.MailService;
import ru.prognoz.tc.mvc.common.HtmlCleaner;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	private static final int MAX_LENGTH = 127;

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserDao userDao;
	@Autowired
	private AuthService authService;
	@Autowired
	private MailService mailService;

	/**
	 * Starter Page
	 */
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login(Locale locale, Model model, @ModelAttribute("LoginBean") LoginPageBean bean,
			@RequestParam(value = "show", defaultValue = "") final String show,
			@RequestParam(value = "error", defaultValue = "false") final Boolean error) {
		logger.info("Login.controller GET index page");

		ModelAndView mav = new ModelAndView("login/index");
		if (show.equals("loginDlg") || error == true) {
			bean.setShowLoginDlg(true);
		}
		if (show.equals("signupDlg")) {
			bean.setShowSignupDlg(true);
		}
		mav.addObject("LoginPageBean", bean);
		return mav;
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ModelAndView submitSignupForm(final Locale locale, @RequestParam(value = "firstName", defaultValue = "") final String firstName,
			@RequestParam(value = "lastName", defaultValue = "") final String lastName,
			@RequestParam(value = "email", defaultValue = "") final String email,
			@RequestParam(value = "password", defaultValue = "") final String password,
			@RequestParam(value = "repeat_password", defaultValue = "") final String repeat_password,
			final RedirectAttributes redirectAttributes) {

		logger.info("Login.controller POST submit Signup form");

		LoginPageBean bean = new LoginPageBean();
		ModelAndView mav = new ModelAndView("redirect:/login");
		redirectAttributes.addFlashAttribute("LoginBean", bean);

		String cleanFirstName = HtmlCleaner.cleanAll(firstName);

		bean.setFirstName(cleanFirstName);
		bean.setLastName(lastName);
		bean.setEmail(email);
		bean.setPassword(password);
		bean.setRepeatPass(repeat_password);

		ViewMessage validationMsg = validateRegistrationData(cleanFirstName, lastName, email, password, repeat_password, locale);
		// Валидация введенных данных не пройдена, возвращаемся на страницу входа и отображаем диалог регистрации.
		if (validationMsg != null) {
			bean.setSignupValidationMsg(validationMsg);
			bean.setShowSignupDlg(true);
		} else { // Валидация пройдена, переходим на диалог входа в систему.
			try {
				User createdUser = authService.createAccountInternal(cleanFirstName, lastName, email, password);
				mailService.sendVerificationMsg(createdUser, locale);
			} catch (HandyCoreException exc) {
				bean.setSignupValidationMsg(new ViewMessage("error", "", "Ошибка создания пользователя"));
			}
			bean.setShowLoginDlg(true);
		}

		return mav;
	}

	private ViewMessage validateRegistrationData(final String firstName, final String lastName, final String email, final String password,
			final String repeatPassword, final Locale locale) {

		// Пустое имя пользователя
		if (StringUtils.isEmpty(firstName)) {
			return new ViewMessage("error", "firstName", messageSource.getMessage("SignupFace.inputNameText", null, locale));
		} else if (firstName.length() > MAX_LENGTH) {
			return new ViewMessage("error", "firstName", messageSource.getMessage("SignupFace.tooLongFirstName", null, locale));
		}

		if (lastName.length() > MAX_LENGTH) {
			return new ViewMessage("error", "lastName", messageSource.getMessage("SignupFace.tooLongLastName", null, locale));
		}

		// Пустой логин
		if (StringUtils.isEmpty(email)) {
			return new ViewMessage("error", "email", messageSource.getMessage("SignupFace.wrongEmail", null, locale));
		} else if (email.length() > MAX_LENGTH) {
			return new ViewMessage("error", "email", messageSource.getMessage("SignupFace.tooLongEmail", null, locale));
		}

		// невалидный пароль
		if (StringUtils.isEmpty(password) || password.length() < 3) {
			return new ViewMessage("error", "password", messageSource.getMessage("SignupFace.smallPassword", null, locale));
		} else if (password.length() > MAX_LENGTH) {
			return new ViewMessage("error", "password", messageSource.getMessage("SignupFace.tooLongPassword", null, locale));
		}
		// Дублирование логина
		User tempUser = userDao.getByLogin(email);
		if (tempUser != null) {
			return new ViewMessage("error", "email", messageSource.getMessage("SignupFace.twiceUser", null, locale));
		}

		// Не совпадение пароля.
		if (!password.equals(repeatPassword)) {
			return new ViewMessage("error", "repeat_password", messageSource.getMessage("SignupFace.wrongRepeatedPassword", null, locale));
		}
		return null;
	}
}
