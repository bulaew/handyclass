package ru.prognoz.tc.mvc.controller.useraccount;

import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

public class UserAccountBean {
	
	private Boolean redirected = false;

	private String avatarUrl = "img/avatar/avatar_big.png";

	private String firstName = "";
	private String lastName = "";
	private String about = "";

	private String login = "";
	private String newEmail = "";

	private String currentPassword = "";
	private String newPassword = "";
	private String repeatPassword = "";

	private ViewMessage savePersonalMsg;
	private ViewMessage validEmailMsg;
	private ViewMessage saveEmailMsg;
	private ViewMessage validCurPassMsg;
	private ViewMessage validNewPassMsg;
	private ViewMessage validRepPassMsg;
	private ViewMessage savePassMsg;
	
	private String facebookName = "";
	private String vkName = "";
	private String twitterName = "";
	private String googleName = "";

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	public ViewMessage getSavePersonalMsg() {
		return savePersonalMsg;
	}

	public void setSavePersonalMsg(ViewMessage savePersonalMsg) {
		this.savePersonalMsg = savePersonalMsg;
	}

	public ViewMessage getValidEmailMsg() {
		return validEmailMsg;
	}

	public void setValidEmailMsg(ViewMessage validEmailMsg) {
		this.validEmailMsg = validEmailMsg;
	}

	public ViewMessage getSaveEmailMsg() {
		return saveEmailMsg;
	}

	public void setSaveEmailMsg(ViewMessage saveEmailMsg) {
		this.saveEmailMsg = saveEmailMsg;
	}

	public ViewMessage getValidCurPassMsg() {
		return validCurPassMsg;
	}

	public void setValidCurPassMsg(ViewMessage validCurPassMsg) {
		this.validCurPassMsg = validCurPassMsg;
	}

	public ViewMessage getValidNewPassMsg() {
		return validNewPassMsg;
	}

	public void setValidNewPassMsg(ViewMessage validNewPassMsg) {
		this.validNewPassMsg = validNewPassMsg;
	}

	public ViewMessage getValidRepPassMsg() {
		return validRepPassMsg;
	}

	public void setValidRepPassMsg(ViewMessage validRepPassMsg) {
		this.validRepPassMsg = validRepPassMsg;
	}

	public ViewMessage getSavePassMsg() {
		return savePassMsg;
	}

	public void setSavePassMsg(ViewMessage savePassMsg) {
		this.savePassMsg = savePassMsg;
	}

	public Boolean isRedirected() {
		return redirected;
	}

	public void setRedirected(Boolean redirected) {
		this.redirected = redirected;
	}

	public String getFacebookName() {
		return facebookName;
	}

	public void setFacebookName(String facebookName) {
		this.facebookName = facebookName;
	}

	public String getVkName() {
		return vkName;
	}

	public void setVkName(String vkName) {
		this.vkName = vkName;
	}

	public String getTwitterName() {
		return twitterName;
	}

	public void setTwitterName(String twitterName) {
		this.twitterName = twitterName;
	}

	public String getGoogleName() {
		return googleName;
	}

	public void setGoogleName(String googleName) {
		this.googleName = googleName;
	}
	
}
