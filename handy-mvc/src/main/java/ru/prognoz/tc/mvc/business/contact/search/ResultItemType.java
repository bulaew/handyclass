package ru.prognoz.tc.mvc.business.contact.search;

public enum ResultItemType {

	USER(1), EMAIL(2), HINT(3);

	private int type;

	private ResultItemType(int type) {
		this.type = type;
	}

	public int getIntValue() {
		return type;
	}
	
	public String getStrValue() {
		switch (type) {
		case 1:
			return "user";
		case 2:
			return "mail";
		case 3:
			return "hint";
		default:
			return "null";
		}
	}

	public static ResultItemType parseInt(int code) {
		for (ResultItemType type : ResultItemType.values()) {
			if (type.getIntValue() == code) {
				return type;
			}
		}
		return null;
	}
}
