package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.AccessTypeDao;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.course.AccessType;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;

public class EditCourseWizardUtils {
	
	public static String getStepUrl(EditCourseWizardStep step) {

		switch (step) {
		case COMMON:
			return "/main/editcourse/common";
		case CONTENT:
			return "/main/editcourse/content";
		case TEST:
			return "/main/editcourse/test";
		case PARTICIPANT:
			return "/main/editcourse/participant";
		case DISCUSSION:
			return "/main/editcourse/discussion";
		default:
			return null;
		}
	}

	public static void cleanCourseBean(EditCourseBean courseBean) {
		courseBean.setStep(EditCourseWizardStep.COMMON);
		CourseCategoryDao courseCategoryDao = HandyclassAppContext.getBean(CourseCategoryDao.class);
		courseBean.setPosibleCategories(courseCategoryDao.getAll());
		AccessTypeDao accessTypeDao = HandyclassAppContext.getBean(AccessTypeDao.class);
		List<AccessType> accTypes = accessTypeDao.getAllTypes();
		List<CourseAccessType> courseAccessTypes = new ArrayList<CourseAccessType>();
		for (AccessType type : accTypes) {
			courseAccessTypes.add(CourseAccessType.parseInt(type.getId()));
		}
		courseBean.setPosibleAccessTypes(courseAccessTypes );
		courseBean.setAttachments(new ArrayList<Attachment>());
		courseBean.setVerifyMsg(null);
		courseBean.setSlides(new ArrayList<Slide>());
		courseBean.setComponents(new HashMap<Long, List<SlideComponent>>());
		courseBean.setActiveSlide(null);
		courseBean.setChildComponents(new HashMap<Long, List<ChildComponent>>());
		courseBean.setCourse(null);
	}
}
