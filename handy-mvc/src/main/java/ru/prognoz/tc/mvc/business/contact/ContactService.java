package ru.prognoz.tc.mvc.business.contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.core.config.HintedEmailDomains;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.dao.contact.ContactDao;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.dao.contact.ContactRelevoDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.domain.contact.ContactRelevo;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.mvc.business.contact.search.HandyEmailValidator;
import ru.prognoz.tc.mvc.business.contact.search.ResultItem;
import ru.prognoz.tc.mvc.business.contact.search.ResultItemType;
import ru.prognoz.tc.mvc.controller.contacts.AddressbookRequestBody;

@Service
public class ContactService {

	private static final Logger logger = LoggerFactory.getLogger(ContactService.class);

	private static int RESULT_LIMIT = 10;

	@Autowired
	private ContactListInfoDao contactListInfoDao;
	@Autowired
	private HandyObjectDao oidDao;
	@Autowired
	private ContactDao contactDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ContactListContentDao contactListContentDao;
	@Autowired
	private ContactRelevoDao relevoDao;

	public ContactListInfo createContactList(String title) throws HandyCoreException {
		ContactListInfo group = null;
		try {
			group = contactListInfoDao.newDomainInstance();
			group.setArchive(false);
			group.setCreated(new Date());
			group.setModified(new Date());
			group.setObjectId(oidDao.generate());
			group.setTitle(title);

			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			group.setOwner(currentUser);

			contactListInfoDao.save(group);
			logger.info("Contact Service : new Contact List object stored to DB ");
		} catch (Exception exc) {
			throw new HandyCoreException(exc);
		}

		return group;
	}

	public void deleteContactList(long groupOid) throws HandyCoreException {
		ContactListInfo group = contactListInfoDao.getByOid(groupOid);
		if (group == null) {
			throw new HandyCoreException("Не верно задан ИД списка");
		}
		group.setArchive(true);
		group.setModified(new Date());
		contactListInfoDao.merge(group);
	}

	public void updateContactListTitle(long groupOid, String title) throws HandyCoreException {
		ContactListInfo group = contactListInfoDao.getByOid(groupOid);
		if (group == null) {
			throw new HandyCoreException("Не верно задан ИД списка");
		}
		group.setTitle(title);
		group.setModified(new Date());
		contactListInfoDao.merge(group);
	}

	public void findContact(long groupOid, String contactName) throws HandyCoreException {
		// TODO Auto-generated method stub

	}

	/**
	 * Поиск контакта в заданном списке.
	 * 
	 * @param user
	 *            Пользователь, в контактах которого производится поиск.
	 * @param listOid
	 *            ИД списка
	 * @param memberCriteria
	 *            критерий поиска
	 * @return Contact искомый контакт либо <code>null<code>
	 * @throws HandyCoreException
	 */
	public Contact findListMember(User user, long listOid, String memberCriteria) throws HandyCoreException {
		return null;
	}

	/**
	 * Поиск контакта по всем контактам пользователя.
	 * 
	 * @param user
	 *            Пользователь, по чьим контактам проводится поиск.
	 * @param criteria
	 *            Значение, введенное в строку поиска.
	 * @return Найденные контакты.
	 */
	public List<ResultItem> search(User user, final String criteria) throws HandyCoreException {

		List<Contact> contacts = contactDao.getByOwner(user);
		List<ResultItem> resultList = new ArrayList<ResultItem>();
		for (Contact contact : contacts) {
			if (like(contact, criteria)) {
				ContactRelevo relevo = relevoDao.getByContact(contact.getObjectId().getId());
				// создаем результирующий элемент.
				ResultItem item = new ResultItem();
				if (contact.getUser() != null) {
					item.setType(ResultItemType.USER);
					item.setAvatar(contact.getUser().getAvatarURL());
					item.setUserName(contact.getUser().getUsername());
					item.setEmail(contact.getUser().getLogin());
				} else {
					item.setType(ResultItemType.EMAIL);
					item.setEmail(contact.getEmail());
				}
				if (relevo != null) {
					item.setObjectID(contact.getObjectId().getId());
					item.setRelevo(relevo.getInviteFreq());
					resultList.add(item);
				}
			}
		}

		Collections.sort(resultList, new Comparator<ResultItem>() {

			@Override
			public int compare(ResultItem o1, ResultItem o2) {
				Long int1 = Long.valueOf(o1.getRelevo());
				Long int2 = Long.valueOf(o2.getRelevo());
				return int2.compareTo(int1); // reverse
			}
		});

		// Обрезаем ответ до RESULT_LIMIT элементов.
		if (resultList.size() > RESULT_LIMIT) {
			resultList = resultList.subList(0, RESULT_LIMIT);
		}

		// Обрезаем ответ до RESULT_LIMIT элементов.

		if (resultList.size() > RESULT_LIMIT) {
			resultList = resultList.subList(0, RESULT_LIMIT);
		}

		if (resultList.size() == 0 && !StringUtils.isEmpty(criteria)) {
			String userName = HandyEmailValidator.getUserName(criteria);
			if (!StringUtils.isEmpty(userName)) {
				appendHintDomains(userName, resultList);
			}
		}

		return resultList;
	}

	private boolean like(Contact contact, String param) {
		if (StringUtils.isEmpty(param))
			return true;

		if (contact.getUser() != null) {
			if (contact.getUser().getUsername().toUpperCase().contains(param.toUpperCase())) {
				return true;
			}
		} else {
			if (contact.getEmail().toUpperCase().contains(param.toUpperCase())) {
				return true;
			}
		}

		return false;
	}

	private void appendHintDomains(String userName, List<ResultItem> resultList) {
		HintedEmailDomains emailDomens = HandyclassAppContext.getBean(HintedEmailDomains.class);
		for (String domain : emailDomens.getEmailDomains()) {
			ResultItem item = new ResultItem();
			item.setType(ResultItemType.HINT);
			item.setEmail(userName + "@" + domain);
			resultList.add(item);
		}
	}

	/**
	 * Добавление контакта в список контактов.
	 * 
	 * @param owner
	 *            владелец контакта
	 * @param listOid
	 *            ОИД списка
	 * @param userOid
	 *            ОИД пользователя, добавляемого в контакты.
	 * @throws HandyCoreException
	 */
	public void createContact(User owner, long listOid, long userOid) throws HandyCoreException {
		// проверка входных параметров.
		User contactUser = userDao.getByOid(userOid);
		if (contactUser == null) {
			throw new HandyCoreException("Не верный ОИД пользователя");
		}
		ContactListInfo listInfo = contactListInfoDao.getByOid(listOid);
		if (listInfo == null) {
			throw new HandyCoreException("Не верный ОИД списка контактов");
		}
		// поиск существующего контакта по емейлу либо по ИД пользователя.
		Contact newContact = contactDao.search(owner, contactUser.getLogin());
		// если контакт не найден - создание нового контакта.
		if (newContact == null) {
			newContact = contactDao.newDomainInstance();
			newContact.setCreated(new Date());
			newContact.setOwner(owner);
			newContact.setObjectId(oidDao.generate());
			newContact.setUser(contactUser);
		}
		newContact.setArchive(false);
		newContact.setModified(new Date());
		contactDao.merge(newContact);

		// добавление контакта в группу.
		contactListContentDao.addContactListMember(listInfo, newContact);
		logger.info("Contact Service : new Contact object stored to DB ");
	}

	/**
	 * Добавление контакта в список контактов.
	 * 
	 * @param owner
	 *            владелец контакта
	 * @param listOid
	 *            ОИД списка
	 * @param email
	 *            емейл добавляемый в контакты.
	 * @throws HandyCoreException
	 */
	public void createContact(User owner, long listOid, String email) throws HandyCoreException {
		// проверка входных параметров.
		ContactListInfo listInfo = contactListInfoDao.getByOid(listOid);
		if (listInfo == null) {
			throw new HandyCoreException("Не верный ОИД списка контактов");
		}
		// поиск существующего контакта по емейлу либо по ИД пользователя.
		Contact newContact = contactDao.search(owner, email);
		// если контакт не найден - создание нового контакта.
		if (newContact == null) {
			newContact = contactDao.newDomainInstance();
			newContact.setCreated(new Date());
			newContact.setOwner(owner);
			newContact.setObjectId(oidDao.generate());
			newContact.setEmail(email);
		}
		newContact.setArchive(false);
		newContact.setModified(new Date());
		contactDao.merge(newContact);

		// добавление контакта в группу.
		contactListContentDao.addContactListMember(listInfo, newContact);
		logger.info("Contact Service : new Contact object stored to DB ");
	}

	public void createContacts(User owner, AddressbookRequestBody rb) throws HandyCoreException {

		for (Map.Entry<Integer, Long> entry : rb.getContacts().entrySet()) {
			Contact contact = contactDao.getByOID(entry.getValue());
			if (contact.getUser() != null) {
				createContact(owner, rb.getActiveList(), contact.getUser().getObjectId().getId());
			} else {
				createContact(owner, rb.getActiveList(), contact.getEmail());
			}
			System.out.println(entry.getKey() + "/" + entry.getValue());
		}

	}

	public void increaseRelevo(User owner, Invite invite) {

		// 1. Ищем контакт по пользователю или по емейлу.
		// 2. если не нашли - создаем.
		// 3. Ищем релевантность.
		// 4. Если не нашли - создем.
		// 5. Увеличиваем релевантность.
		Contact contact = null;
		if (invite.getUser() != null) {
			contact = contactDao.search(owner, invite.getUser().getLogin());
		} else {
			contact = contactDao.search(owner, invite.getGuestEmail());
		}
		// ---//
		if (contact == null) {
			// если контакт не найден - создание нового контакта.
			contact = contactDao.newDomainInstance();
			contact.setCreated(new Date());
			contact.setOwner(owner);
			contact.setObjectId(oidDao.generate());
			contact.setArchive(false);
			contact.setModified(new Date());
			if (invite.getUser() != null) {
				contact.setEmail(invite.getUser().getLogin());
				contact.setUser(invite.getUser());
			} else {
				contact.setEmail(invite.getGuestEmail());
			}
			contactDao.merge(contact);
		}
		// ---//
		ContactRelevo relevo = relevoDao.getByContact(contact.getObjectId().getId());
		if (relevo == null) {
			relevo = relevoDao.newDomainInstance();
			relevo.setObjectId(oidDao.generate());
			relevo.setOwner(owner);
			relevo.setContactOID(contact.getObjectId());
			relevo.setInviteFreq(0);
			relevoDao.save(relevo);
		}
		// Увеличение счетчика релевантности
		relevo.setInviteFreq(relevo.getInviteFreq() + 1);
		// сохранение.
		relevoDao.merge(relevo);
	}

	public void deleteContact(User currentUser, long listOid, long contactOid) throws HandyCoreException {
		ContactListInfo listInfo = contactListInfoDao.getByOid(listOid);
		if (listInfo == null) {
			throw new HandyCoreException("Не верный ОИД списка контактов");
		}
		// поиск существующего контакта
		Contact removedContact = contactDao.getByOID(contactOid);
		if (removedContact == null) {
			throw new HandyCoreException("Не верный ОИД контакта");
		}
		try {
			contactListContentDao.removeContactListMember(listInfo, removedContact);
			// обновляем дату модификации списка.
			listInfo.setModified(new Date());
			contactListInfoDao.merge(listInfo);
		} catch (Exception exc) {
			throw new HandyCoreException(exc.getMessage());
		}
	}

}
