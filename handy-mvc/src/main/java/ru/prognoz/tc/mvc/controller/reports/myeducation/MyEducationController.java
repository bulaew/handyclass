package ru.prognoz.tc.mvc.controller.reports.myeducation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationCriteria;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationItem;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.courselist.FakeCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
public class MyEducationController {

	private static final Logger logger = LoggerFactory.getLogger(MyEducationController.class);

	@Autowired
	private HandyclassAppCfg appConfig;
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseCategoryDao courseCategoryDao;

	@RequestMapping(value = "/reports/myEducation", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws HandyCoreException {

		logger.info("my education report page");

		ModelAndView mav = new ModelAndView("reports/myEducation/index");
		mav.addObject("bean", populateBean(locale, null, 0));
		return mav;
	}
	
	@RequestMapping(value = "/reports/myEducation/renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my education report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/myEducation/report");
		mav.addObject("bean", populateBean(locale, viewModel, 0));
		return mav;
	}
	
	@RequestMapping(value = "/reports/myEducation/loadBillboard", method = RequestMethod.POST)
	public ModelAndView loadBillboard(Locale locale) throws HandyCoreException {
		logger.info("my propensity report page: loadBillboard");

		ModelAndView mav = new ModelAndView("reports/myEducation/billboard");
		mav.addObject("bean", populateBean(locale, null, appConfig.getDefaultRowLimit()));
		return mav;
	}
	
	@RequestMapping(value = "/reports/myEducation/renderBillboard", method = RequestMethod.POST)
	public ModelAndView renderBillboard(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my propensity report page: renderBillboard");

		ModelAndView mav = new ModelAndView("reports/myEducation/billboard");
		mav.addObject("bean", populateBean(locale, viewModel, appConfig.getDefaultRowLimit()));
		return mav;
	}

	private MyEducationBean populateBean(Locale locale, ReportViewModel viewModel, int rowLimit) throws HandyCoreException {
		if (viewModel == null) {
			viewModel = new ReportViewModel();
			viewModel.setController("/reports/myEducation");
			viewModel.setSort(MyEducationItem.COLUMN_COMPLETE_ATTEMPT_DT);
			viewModel.setDirection(SortDirection.DESC);
			viewModel.setReportType(ReportType.MY_EDUCATION);
		}
		MyEducationBean bean = new MyEducationBean();
		bean.setItems(reportService.getMyEducationItems(criteriaByModel(viewModel, rowLimit)));
		bean.setViewModel(viewModel);

		List<CourseCategory> categories = new ArrayList<CourseCategory>();
		categories.add(new FakeCategory());
		categories.addAll(courseCategoryDao.getAll());
		bean.setCategories(categories);
		
		List<CourseStatus> statuses = new ArrayList<>();
		for (CourseStatus value : CourseStatus.values()) {
			statuses.add(value);
		}
		bean.setStatusList(statuses);
		return bean;
	}

	private MyEducationCriteria criteriaByModel(ReportViewModel viewModel, int rowLimit) {
		MyEducationCriteria criteria = new MyEducationCriteria();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		criteria.setFilterUser(currentUser);
		criteria.setRowCount(rowLimit);
		criteria.setOrderColumn(viewModel.getSort());
		criteria.setSortDirection(viewModel.getDirection());
		criteria.setFilterCategoryID(viewModel.getCategoryFilter());
		criteria.setFilterStatus(viewModel.getStatusFilter());
		criteria.setFindValue(viewModel.getSearch());
		return criteria;
	}

}
