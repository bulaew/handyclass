package ru.prognoz.tc.mvc.controller.reports.courseinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

public class CourseInfoBean {

	private Course currentCourse;
	private List<ReportItem> items = new ArrayList<ReportItem>();
	private ReportViewModel viewModel = new ReportViewModel();
	private Map<Long, Long> slideViewMap = new HashMap<>();
	private Long countLikes;
	private Long countViews;

	public Course getCurrentCourse() {
		return currentCourse;
	}

	public void setCurrentCourse(Course currentCourse) {
		this.currentCourse = currentCourse;
	}

	public List<ReportItem> getItems() {
		return items;
	}

	public void setItems(List<ReportItem> items) {
		this.items = items;
	}

	public ReportViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(ReportViewModel viewModel) {
		this.viewModel = viewModel;
	}

	public Map<Long, Long> getSlideViewMap() {
		return slideViewMap;
	}

	public void setSlideViewMap(Map<Long, Long> slideViewMap) {
		this.slideViewMap = slideViewMap;
	}

	public Long getCountLikes() {
		return countLikes;
	}

	public void setCountLikes(Long countLikes) {
		this.countLikes = countLikes;
	}

	public Long getCountViews() {
		return countViews;
	}

	public void setCountViews(Long countViews) {
		this.countViews = countViews;
	}

}
