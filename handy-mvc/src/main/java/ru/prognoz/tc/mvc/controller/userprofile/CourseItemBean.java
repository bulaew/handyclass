package ru.prognoz.tc.mvc.controller.userprofile;

import java.util.Locale;

import ru.prognoz.tc.domain.course.Course;

public class CourseItemBean {
	
	private Course entity;
	private boolean locked = true;
	private String availableTo = "";

	public CourseItemBean(Course entity) {
		super();
		this.entity = entity;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
	public String getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(String availableTo) {
		this.availableTo = availableTo;
	}

	public String getAvatarUrl() {
		return entity.getAvatarUrl();
	}
	
	public String getCourseTitle() {
		return entity.getFullName();
	}
	
	public String getCategoryTitle(Locale locale) {
		return entity.getCategory().getTitle(locale);
	}
	
	public Long getOid() {
		return entity.getObjectId().getId();
	}
	

}
