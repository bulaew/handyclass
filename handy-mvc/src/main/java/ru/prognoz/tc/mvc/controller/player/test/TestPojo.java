package ru.prognoz.tc.mvc.controller.player.test;

import java.util.List;

public class TestPojo {
	private Long oid;
	private List<UserAnswer> answers;

	public Long getOid() {
		return oid;
	}

	public void setOid(Long oid) {
		this.oid = oid;
	}

	public List<UserAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<UserAnswer> answers) {
		this.answers = answers;
	}
}