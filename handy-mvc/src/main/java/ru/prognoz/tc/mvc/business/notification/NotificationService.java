package ru.prognoz.tc.mvc.business.notification;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.DiscussionDao;
import ru.prognoz.tc.dao.HandyCommentDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.dao.NotificationDao;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.domain.HandyComment;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.domain.course.Notification;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.mvc.controller.courselist.mode.UserMode;

@Service
public class NotificationService {

	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private HandyObjectDao oidDao;
	@Autowired
	private InviteDao inviteDao;
	@Autowired
	private DiscussionDao discussionDao;
	@Autowired
	private HandyCommentDao commentDao;
	@Autowired
	private SlideDao slideDao;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private MessageSource messageSource;

	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

	/**
	 * Оповещение №1 (Ученик). Завершение обучение по курсу.
	 * 
	 * @param courseAttempt
	 *            Попытка, в которой произошло завершение курса.
	 */
	synchronized public void fireCompleteTraining(CourseAttempt courseAttempt) {
		Course course = courseDao.getByOid(courseAttempt.getCourseOid().getId());

		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.COMPLETE_TRAINING.getIntValue());
		notification.setPerson(courseAttempt.getUser()); // ученик.
		notification.setReceiver(courseAttempt.getUser()); // ученик.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(new Date());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireCompleteTraining ");
	}
	
	/**
	 * Оповещения №3-6 при добавлении комментария.
	 * 
	 * @param comment
	 *            Новый комментарий.
	 */
	synchronized public void fireNewComment(HandyComment comment) {
		// родительский комментарий.
		HandyComment parentComment = commentDao.getParent(comment);
		// Получение корневого предка.
		Long rootOID = commentDao.findRoot(comment);
		HandyObjectID root = oidDao.getById(rootOID);
		if (rootOID == null) {
			logger.warn("Не найден корневой объект комментария " + comment.getObjectId());
			return;
		}
		// Если это обсуждение, то оповещения №5-6, иначе №3-4.
		Discussion discussion = discussionDao.getByOid(rootOID);
		Slide slide = slideDao.getByOid(root);
		// получение курса.
		Course course = null;
		if (discussion != null) {
			course = discussion.getCourse();
		} else if (slide != null) {
			course = courseDao.getByOid(slide.getCourseOID().getId());
		}
		if (course == null) {
			logger.warn("Не найден корневой объект комментария " + comment.getObjectId());
			return;
		}
		if (discussion == null) {
			fireNewCourseComment(comment, course);
			if (parentComment != null) {
				fireCommentReply(comment, parentComment, course);
			}
		} else {
			fireNewTopicComment(comment, discussion);
			if (parentComment != null) {
				fireTopicCommentReply(comment, parentComment, discussion);
			}
		}

	}

	/**
	 * Оповещение №3: ответ на комментарий.
	 * 
	 * @param newComment
	 * @param parentComment
	 * @param course
	 */
	synchronized public void fireCommentReply(HandyComment newComment, HandyComment parentComment, Course course) {
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.COMMENT_REPLY.getIntValue());
		notification.setReceiver(parentComment.getOwner()); // получатель оповещения: пользователь, создавший родительский комментарий.
		notification.setPerson(newComment.getOwner()); // Пользователь, создавший новый комментарий.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(newComment.getCreateDate());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireCommentReply ");
	}

	/**
	 * Оповещение №4 (Учитель): пользователь оставил комментарий в вашем курсе.
	 * 
	 * @param newComment
	 * @param course
	 */
	synchronized public void fireNewCourseComment(HandyComment newComment, Course course) {
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.NEW_COMMENT.getIntValue());
		notification.setReceiver(course.getOwner()); // получатель оповещения: учитель, создавший курс.
		notification.setPerson(newComment.getOwner()); // Пользователь, создавший новый комментарий.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(newComment.getCreateDate());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireNewCourseComment ");
	}

	/**
	 * Оповещение №5: пользователь ответил вам в теме.
	 * 
	 * @param newComment
	 * @param course
	 */
	synchronized public void fireTopicCommentReply(HandyComment newComment, HandyComment parentComment, Discussion discussion) {
		Course course = discussion.getCourse();

		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.TOPIC_COMMENT_REPLY.getIntValue());
		notification.setReceiver(parentComment.getOwner()); // получатель оповещения: пользователь, создавший родительский комментарий.
		notification.setPerson(newComment.getOwner()); // Пользователь, создавший новый комментарий.
		notification.setDiscussionOID(discussion.getObjectId()); // тема, в которой был создан комментарий.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(newComment.getCreateDate());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireTopicCommentReply ");
	}

	/**
	 * Оповещение №6: пользователь оставил комментарий в вашей теме.
	 * 
	 * @param newComment
	 * @param course
	 */
	synchronized public void fireNewTopicComment(HandyComment newComment, Discussion discussion) {
		Course course = discussion.getCourse();

		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.NEW_TOPIC_COMMENT.getIntValue());
		notification.setReceiver(discussion.getCreator()); // получатель оповещения: пользователь, создавший обсуждение.
		notification.setPerson(newComment.getOwner()); // Пользователь, создавший новый комментарий.
		notification.setDiscussionOID(discussion.getObjectId()); // тема, в которой был создан комментарий.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(newComment.getCreateDate());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireNewTopicComment ");
	}
	
	/**
	 * Оповещение №8 (Учитель). Завершение обучение по вашему курсу.
	 * 
	 * @param courseAttempt
	 *            Попытка, в которой произошло завершение курса.
	 */
	synchronized public void fireCourseComplete(CourseAttempt courseAttempt) {
		Course course = courseDao.getByOid(courseAttempt.getCourseOid().getId());

		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.COMPLETE_COURSE.getIntValue());
		notification.setPerson(courseAttempt.getUser()); // ученик.
		notification.setReceiver(course.getOwner()); // учитель
		notification.setCourseOID(course.getObjectId());
		notification.setDate(new Date());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireCourseComplete ");
	}

	/**
	 * Оповещение об отмене инвайта на курс.
	 * 
	 * @param registeredUser
	 *            Зарегистрированный пользователь.
	 * @param course
	 *            Курс, на который пользователь регистрируется.
	 */
	public void fireInviteDismiss(User registeredUser, Course course) {
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.DISMISS_INVITE.getIntValue());
		notification.setReceiver(registeredUser); // получатель оповещения учитель.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(new Date());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireInviteDismiss ");
	}

	/**
	 * 
	 * @param invite
	 */
	public void fireInviteSended(Invite invite) {
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.NEW_INVITE.getIntValue());
		notification.setPerson(invite.getCourse().getOwner());
		notification.setReceiver(invite.getUser());
		notification.setCourseOID(invite.getCourse().getObjectId());
		notification.setDate(new Date());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireInviteSended");
	}
	
	/**
	 * Оаовещение №9 (учитель): Создание нового обсуждения в курсе.
	 * 
	 * @param discussion
	 *            новое обсуждение.
	 */
	synchronized public void fireNewTopic(Discussion discussion) {
		Course course = discussion.getCourse();

		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.NEW_DISCUSSION.getIntValue());
		notification.setReceiver(course.getOwner()); // получатель оповещения: учитель, создавший курс.
		notification.setPerson(discussion.getCreator()); // Пользователь, создавший новое обсуждение.
		notification.setDiscussionOID(discussion.getObjectId()); // новое обсуждение.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(discussion.getCreateDate());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireNewTopic");
	}

	/**
	 * Оповещение о регистрации на курс.
	 * 
	 * @param registeredUser
	 *            Зарегистрированный пользователь.
	 * @param course
	 *            Курс, на который пользователь регистрируется.
	 */
	public void fireAcceptCourseRegistration(User registeredUser, Course course) {
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.SIGNUP_COURSE.getIntValue());
		notification.setReceiver(course.getOwner()); // получатель оповещения учитель.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(new Date());
		notification.setPerson(registeredUser); // Пользователь, подтвердивший регистрацию.
		notificationDao.save(notification);
		logger.info(" NotificationService : fireAcceptCoursRegistration ");
	}

	/**
	 * Оповещение об отчислении с курса.
	 * 
	 * @param registeredUser
	 *            Зарегистрированный пользователь.
	 * @param course
	 *            Курс, на который пользователь регистрируется.
	 */
	synchronized public void fireCourseUnRegistration(User user, Course course) {
		// TODO: добавить нотификацию.
		Notification notification = notificationDao.newDomainInstance();
		notification.setObjectId(oidDao.generate());
		notification.setNotificationType(NotificationType.DISMISS_REGISTRATION.getIntValue());
		notification.setPerson(user);
		notification.setReceiver(course.getOwner()); // получатель оповещения учитель.
		notification.setCourseOID(course.getObjectId());
		notification.setDate(new Date());
		notificationDao.save(notification);
		logger.info(" NotificationService : fireCourseUnRegistration ");
	}

	/**
	 * Получение текст оповещения по типу нотификации
	 * 
	 * @param locale
	 *            локаль
	 * @param notification
	 *            оповещение
	 * @return Текст оповещения
	 * @throws HandyCoreException
	 *             ошибка построения текста
	 */
	public String getNotificationText(Locale locale, Notification notification) throws HandyCoreException {
		try {
			NotificationType type = NotificationType.parseInt(notification.getNotificationType());
			Course course = null;
			switch (type) {
			case COMPLETE_TRAINING: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { course.getFullName() };
				return messageSource.getMessage("NotificationService.completeTraining", args, locale);
			}
			case NEARLY_TERM:
				return "";
			case COMMENT_REPLY: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { notification.getPerson().getUsername(), course.getFullName() };
				return messageSource.getMessage("NotificationService.commentReply", args, locale);
			}
			case NEW_COMMENT: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { notification.getPerson().getUsername(), course.getFullName() };
				return messageSource.getMessage("NotificationService.newComment", args, locale);
			}
			case TOPIC_COMMENT_REPLY: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				Discussion discussion = discussionDao.getByOid(notification.getDiscussionOID().getId());
				String[] args = { notification.getPerson().getUsername(), discussion.getTopic(), course.getFullName() };
				return messageSource.getMessage("NotificationService.topicCommentReply", args, locale);
			}
			case NEW_TOPIC_COMMENT: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				Discussion discussion = discussionDao.getByOid(notification.getDiscussionOID().getId());
				String[] args = { notification.getPerson().getUsername(), discussion.getTopic(), course.getFullName() };
				return messageSource.getMessage("NotificationService.newTopicComment", args, locale);
			}
			case SIGNUP_COURSE: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { notification.getPerson().getUsername(), course.getFullName() };
				return messageSource.getMessage("NotificationService.signupCourse", args, locale);
			}
			case COMPLETE_COURSE: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { notification.getPerson().getUsername(), course.getFullName() };
				return messageSource.getMessage("NotificationService.completeCourse", args, locale);
			}
			case NEW_DISCUSSION: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				Discussion discussion = discussionDao.getByOid(notification.getDiscussionOID().getId());
				String[] args = { notification.getPerson().getUsername(), discussion.getTopic(), course.getFullName() };
				return messageSource.getMessage("NotificationService.newDiscussion", args, locale);
			}
			case NEW_INVITE: {
				course = courseDao.getByOid(notification.getCourseOID().getId());
				String[] args = { notification.getPerson().getUsername(), course.getFullName() };
				return messageSource.getMessage("NotificationService.newInvite", args, locale);
			}
			case DISMISS_REGISTRATION:
				return "Отмена регистрации";
			case DISMISS_INVITE:
				return "Отмена приглашения";
			default:
				return "";
			}
		} catch (NullPointerException exc) {
			logger.info(exc.getMessage());
			return "";
		} catch (Exception e) {
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.notificationBuildError", null, locale));
		}
	}

	public void removeNotifications(User currentUser, UserMode userMode) {
		List<Notification> list = notificationDao.getByReceiver(currentUser);
		for (Notification notification : list) {
			NotificationType notificationType = NotificationType.parseInt(notification.getNotificationType());
			if (notificationType.isApplicable(userMode)) {
				notificationDao.delete(notification);
			}
		}
	}

}
