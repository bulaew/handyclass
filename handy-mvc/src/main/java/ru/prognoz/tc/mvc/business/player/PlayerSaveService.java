package ru.prognoz.tc.mvc.business.player;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.service.RollbackedSaver;

@Service
public class PlayerSaveService {
	@Autowired
	private RollbackedSaver saveService;

	public void saveAttempt(CourseAttempt attempt) throws Exception {
		saveService.merge(attempt);
	}

	public void saveAttempt(TestAttempt attempt) throws Exception {
		saveService.merge(attempt);
	}

	public void saveAttempt(SlideAttempt attempt) throws Exception {
		saveService.merge(attempt);
	}

	public void saveAttempt(QuestionAttempt attempt) throws Exception {
		saveService.merge(attempt);
	}

	public void saveAttempt(List<QuestionAttempt> attempts) throws Exception {
		saveService.merge(attempts);
	}
}
