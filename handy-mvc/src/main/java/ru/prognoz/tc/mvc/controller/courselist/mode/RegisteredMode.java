package ru.prognoz.tc.mvc.controller.courselist.mode;

public enum RegisteredMode {
	ALL, REGISTERED, NOT_REGISTERED;
}
