package ru.prognoz.tc.mvc.business.mail;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.security.auth.login.AppConfigurationEntry;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.mvc.common.GoogleMailSender;
import ru.prognoz.tc.mvc.common.HtmlBuilder;
import ru.prognoz.tc.mvc.controller.auth.login.LoginController;

@Service
public class MailService {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private GoogleMailSender mailSender;
	@Autowired
	private HtmlBuilder htmlBuilder;
	@Autowired
	private MessageSource msg;
	@Autowired
	private HandyclassAppCfg app;
	@Autowired
	private ServletContext servletContext;
	
	public void sendVerificationMsg(User user, Locale locale) throws HandyCoreException {
		if (!StringUtils.isEmpty(user.getLogin())) {
			Map<String, Object> model = verificationMessageModel(user, locale);
			model.put("user", user);
			mailSender.sendMsg(user.getLogin(), "verification message", htmlBuilder.build("email/verification", model));
			logger.info(" MailService : sendVerificationMsg ");
		}

	}

	public void dismissInvite(Locale locale, Course course, User user, String email) {
		logger.info(" MailService : dismissInvite not implemented yet ");
	}

	public void dismissCourseRegistry(Locale locale, Course course, User user) {
		logger.info(" MailService : dismissCourseRegistry not implemented yet ");
	}

	public void sendInvite(Locale locale, Invite freshInvite) throws HandyCoreException {
		Map<String, Object> model = inviteMessageModel(locale, freshInvite);
		model.put("user", freshInvite.getGuestEmail());
		String mail = null;
		if (freshInvite.getUser() == null) {
			mail = freshInvite.getGuestEmail();
		} else {
			mail = freshInvite.getUser().getLogin();
		}
		mailSender.sendMsg(mail, "verification message", htmlBuilder.build("email/invite", model));
		logger.info(" MailService : sendVerificationMsg ");
	}

	private Map<String, Object> verificationMessageModel(User user, Locale locale) {
		Map<String, Object> mailData = new HashMap<String, Object>();
		String url = app.getHostUrl() + servletContext.getContextPath() + "/auth/verify?verificationCode=" + user.getVerificationCode();
		mailData.put("link", url);
		mailData.put("user", user);
		mailData.put("header", msg.getMessage("Mail.Invite.header", null, locale));
		mailData.put("verify", msg.getMessage("Mail.Invite.verify", null, locale));
		mailData.put("copy", msg.getMessage("Mail.Invite.copy", null, locale));
		mailData.put("error", msg.getMessage("Mail.Invite.error", null, locale));
		mailData.put("info", msg.getMessage("Mail.Invite.info", null, locale));
		mailData.put("footer", msg.getMessage("Mail.Invite.footer", null, locale));
		return mailData;
	}

	private Map<String, Object> inviteMessageModel(Locale locale, Invite invite) {
		Course course = invite.getCourse();
		Map<String, Object> mailData = new HashMap<String, Object>();
		mailData.put("Owner", invite.getCourse().getOwner().getName());
		if (invite.getUser() != null) {
			mailData.put("User", invite.getUser().getName());
		} else {
			mailData.put("User", invite.getGuestEmail());
		}
		if (course.getAviableTo() != null) {
			mailData.put("to", HcDateTime.simpleDt(course.getAviableTo()));
		} else {
			mailData.put("to", msg.getMessage("CommonEditCourseAction.indefinite", null, locale));
		}
		mailData.put("from", HcDateTime.simpleDt(course.getAviableFrom()));
		mailData.put("CourseName", invite.getCourse().getFullName());
		mailData.put("CourseDescription", invite.getCourse().getDescription());
		mailData.put("header", msg.getMessage("Mail.Registration.header", null, locale));
		mailData.put("greetings", msg.getMessage("Mail.Registration.greetings", null, locale));
		mailData.put("info", msg.getMessage("Mail.Registration.info", null, locale));
		mailData.put("toCourse", msg.getMessage("Mail.Registration.toCourse", null, locale));
		mailData.put("more", msg.getMessage("Mail.Registration.more", null, locale));
		mailData.put("from", msg.getMessage("Mail.Registration.from", null, locale));
		mailData.put("to", msg.getMessage("Mail.Registration.to", null, locale));
		mailData.put("footer", msg.getMessage("Mail.Registration.footer", null, locale));
		String url = app.getHostUrl() + servletContext.getContextPath() + "/main/coursereg/index?coid=" + invite.getCourse().getObjectId().getId();
		mailData.put("link", url);
		return mailData;
	}
}
