package ru.prognoz.tc.mvc.controller.player.course;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.AttachmentDao;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.SlideComponentDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.dao.attempt.SlideAttemptDao;
import ru.prognoz.tc.dao.attempt.TestAttemptDao;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.data.TestStatus;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.test.Test;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;
import ru.prognoz.tc.mvc.business.coursereg.CourseRegistryService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.business.player.PlayerSaveService;
import ru.prognoz.tc.mvc.controller.editcourse.ForumBean;

@Controller
@RequestMapping(value = "/main/courseplayer")
public class CoursePlayerController {

	private static final Logger logger = LoggerFactory
			.getLogger(CoursePlayerController.class);
	private static final String BEAN_ATTR_NAME = "courseplayerData";
	private static final String FORUM_BEAN_ATTR_NAME = "forumData";
	
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private AttachmentDao attachmentDao;
	@Autowired
	private CourseAccessService courseAccessService;
	@Autowired
	private SlideDao slideDao;
	@Autowired
	private TestDao testDao;
	@Autowired
	private SlideComponentDao slideComponentDao;
	@Autowired
	private CoursePlayerBean bean;
	@Autowired
	private SlideAttemptDao saDao;
	@Autowired
	private CourseAttemptDao caDao;
	@Autowired
	private TestAttemptDao taDao;
	@Autowired
	private PlayerSaveService saveService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private CourseRegistryService courseRegistryService;
	@Autowired
	private ViewCounterDao viewCounterDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView contactsView(Locale locale,
			@RequestParam(value = "coid") final long courseOid)
			throws HandyCoreException {
		logger.info("Course Player");
		User currentUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		Course course = courseDao.getByOid(courseOid);
		if (!courseAccessService.canPlayAccess(course, currentUser, locale)) {
			ModelAndView mav = new ModelAndView("redirect:/main");
			return mav;
		}
		ModelAndView mav = new ModelAndView("courseplayer/index");
		mav.addObject(BEAN_ATTR_NAME, populateBean(courseOid));
		if (bean.getActiveSlide() != null) {
			mav.addObject( FORUM_BEAN_ATTR_NAME, populateForumBean(bean.getActiveSlide().getObjectId().getId()));
		}
		return mav;
	}

	private CoursePlayerBean populateBean(long courseOid)
			throws HandyCoreException {
		bean = new CoursePlayerBean();
		Course course = courseDao.getByOid(courseOid);
		if (course == null) {
			throw new HandyCoreException("bla bla bla");
		}
		bean.setCourse(course);
		Integer countViewers = viewCounterDao.getCountViews(course
				.getObjectId());
		bean.setViewCounter(countViewers.longValue());
		User currentUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		bean.setCourseAttepmt(caDao.getOrCreate(course, currentUser));
		bean.setAttachments(attachmentDao.getByCourse(course));
		bean.setSlides(slideDao.getAllVisibleByCourse(course.getObjectId()
				.getId()));
		CourseAttempt courseAttempt = caDao.getOrCreate(course, currentUser);
		bean.setTest(testDao.getByCourse(course));
		bean.setTestAttempt(taDao.getOrCreate(bean.getTest(), currentUser));
		if (!bean.getSlides().isEmpty()) {
			Collections.sort(bean.getSlides(), new Comparator<Slide>() {
				@Override
				public int compare(Slide o1, Slide o2) {
					return Integer.valueOf(o1.getPosition()).compareTo(
							Integer.valueOf(o2.getPosition()));

				}
			});
			bean.setActiveSlide(bean.getSlides().get(0));
			bean.setComponents(new HashMap<Long, List<SlideComponent>>());
			bean.setChildComponents(new HashMap<Long, List<ChildComponent>>());
			for (Slide slide : bean.getSlides()) {
				List<SlideComponent> components = slideComponentDao
						.getVisibleBySlide(slide);
				Collections.sort(components, new Comparator<SlideComponent>() {
					@Override
					public int compare(SlideComponent o1, SlideComponent o2) {
						return Integer.valueOf(o1.getPosition()).compareTo(
								Integer.valueOf(o2.getPosition()));

					}
				});
				bean.getSlideAttempts().put(slide.getObjectId().getId(), saDao.getOrCreate(courseAttempt, slide));
				bean.getComponents().put(slide.getObjectId().getId(), components);
				for (SlideComponent parent : components) {
				    bean.getChildComponents().put(parent.getComponentOID().getId(), slideComponentDao.getChildren(parent.getComponentOID().getId()));
				}
			}
		}
		return bean;
	}

	private ForumBean populateForumBean(Long forumRootComponent) {
		return new ForumBean(forumRootComponent);
	}
	
	@RequestMapping(value = { "/page" }, method = RequestMethod.POST)
	@ResponseBody
	public String changePage(Locale locale, Model mod,
			@RequestParam(value = "oid") Long oid) {
		if (bean.getSlides() == null || bean.getSlides().isEmpty()) {
			return "false";
		}
		if (bean.getActiveSlide().getObjectId().getId() == oid) {
			return "false";
		} else {
			for (Slide slide : bean.getSlides()) {
				if (slide.getObjectId().getId() == oid) {
					onAttemptUnload();
					bean.setActiveSlide(slide);
					
					return "true";
				}
			}
		}
		return "false";
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.POST)
	@ResponseBody
	public String nextPage(Locale locale, Model mod) {
		if (bean.getSlides() == null || bean.getSlides().isEmpty()) {
			return "false";
		} else {
			try {
				int index = bean.getSlides().indexOf(bean.getActiveSlide());
				int size = bean.getSlides().size();
				if (index < size) {
					onAttemptUnload();
					bean.setActiveSlide(bean.getSlides().get(index + 1));
				}
				return "true";
			} catch (IndexOutOfBoundsException e) {
				return "false";
			}
		}
	}

	@RequestMapping(value = { "/unsubscribe" }, method = RequestMethod.GET)
	public String unsubscribe(Locale locale) throws HandyCoreException {
		User currentUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		courseRegistryService.unregistration(bean.getCourse(), currentUser,
				locale);
		return "redirect:/main";
	}

	@RequestMapping(value = { "/prev" }, method = RequestMethod.POST)
	@ResponseBody
	public String prevPage(Locale locale, Model mod) {
		if (bean.getSlides() == null || bean.getSlides().isEmpty()) {
			return "false";
		} else {
			try {
				int index = bean.getSlides().indexOf(bean.getActiveSlide());
				int size = bean.getSlides().size();
				if (index - 1 >= 0 && index < size) {
					onAttemptUnload();
					bean.setActiveSlide(bean.getSlides().get(index - 1));
				}
				return "true";
			} catch (IndexOutOfBoundsException e) {
				return "false";
			}
		}
	}

	@RequestMapping(value = { "/last" }, method = RequestMethod.POST)
	@ResponseBody
	public String lastPage(Locale locale, Model mod) {
		if (bean.getSlides() == null || bean.getSlides().isEmpty()) {
			return "false";
		} else {
			onAttemptUnload();
			bean.setActiveSlide(bean.getSlides().get(
					bean.getSlides().size() - 1));
			return "true";
		}
	}

	@RequestMapping(value = { "/first" }, method = RequestMethod.POST)
	@ResponseBody
	public String firstPage(Locale locale, Model mod) {
		if (bean.getSlides() == null || bean.getSlides().isEmpty()) {
			return "false";
		} else {
			onAttemptUnload();
			bean.setActiveSlide(bean.getSlides().get(0));
			return "true";
		}
	}

	@RequestMapping(value = { "/content" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getPageContent(Locale locale, Model mod) {
		if (bean.getActiveSlide() == null) {
			return null;
		}
		onAttemptLoad();
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView("courseplayer/page");
		model.addObject("bean", bean);
		if (bean.getActiveSlide() != null) {
			model.addObject( FORUM_BEAN_ATTR_NAME, populateForumBean(bean.getActiveSlide().getObjectId().getId()));
		}
		result.setResult(model);
		return result;
	}
	
	@RequestMapping(value = { "/content/paging" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getPaging(Locale locale, Model mod) {
		if (bean.getActiveSlide() == null) {
			return null;
		}
		onAttemptLoad();
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView("courseplayer/paging");
		model.addObject("bean", bean);
		if (bean.getActiveSlide() != null) {
			model.addObject( FORUM_BEAN_ATTR_NAME, populateForumBean(bean.getActiveSlide().getObjectId().getId()));
		}
		result.setResult(model);
		return result;
	}


	@RequestMapping(value = { "/unload" }, method = RequestMethod.POST)
	@ResponseBody
	public String unload(Locale locale, Model model) {
		if (bean.getActiveSlide() != null) {
			onAttemptUnload();
		}
		return "true";
	}

	private void onAttemptLoad() {
		SlideAttempt sa = bean.getSlideAttempts().get(
				bean.getActiveSlide().getObjectId().getId());
		if (sa.getFirstView() == null) {
			sa.setFirstView(new Date());
		}
		bean.setTimeOnSlide(System.currentTimeMillis());
		sa.setViewCounter(sa.getViewCounter() + 1);
	}

	private void onAttemptUnload() {
		SlideAttempt sa = bean.getSlideAttempts().get(
				bean.getActiveSlide().getObjectId().getId());
		long spendTime = System.currentTimeMillis() - bean.getTimeOnSlide();
		sa.setLastView(new Date());
		sa.setSpendTime(sa.getSpendTime() + spendTime);
		try {
			saveService.saveAttempt(sa);
			logger.error("SlideAttempt с OID:" + sa.getSlideOid().getId()
					+ " сохранена");
		} catch (Exception e) {
			logger.error("Не могу сохранить SlideAttempt с OID:"
					+ sa.getSlideOid().getId());
		}
		calculateCouresAttempt();
	}

	private void calculateCouresAttempt() {
		CourseAttempt ca = bean.getCourseAttepmt();
		TestAttempt ta = bean.getTestAttempt();
		Test test = bean.getTest();
		Course course = bean.getCourse();
		boolean expired = false;
		if ((course.getAviableTo() != null)
				&& (course.getAviableTo().getTime() < new Date().getTime())) { // время														// курса
																				// закончилось.
			expired = true;
		}
		updateCourseCompleteStatus(ca, bean.getSlides(),
				bean.getSlideAttempts());
		CourseStatus newStatus = null;

		if (!test.isEnable()) {
			if (ca.isComplete()) {
				newStatus = CourseStatus.COMPLETED;
			} else {
				newStatus = CourseStatus.UNCOMPLETED;
			}
		} else {
			if (expired) {
				if (ta.getStatus() == TestStatus.PASSED) {
					if (ca.isComplete()) {
						newStatus = CourseStatus.PASSED;
					} else {
						newStatus = CourseStatus.NOT_PASSED;
					}
				} else {
					newStatus = CourseStatus.NOT_PASSED;
				}
			} else {
				if (ta.getStatus() == TestStatus.PASSED) {
					if (ca.isComplete()) {
						newStatus = CourseStatus.PASSED;
					} else {
						newStatus = CourseStatus.UNCOMPLETED;
					}
				} else {
					newStatus = CourseStatus.UNCOMPLETED;
				}
			}
		}
		if (!newStatus.equals(ca.getStatus())) { // Если поменялся статус.
			ca.setStatus(newStatus);
			if (newStatus.equals(CourseStatus.PASSED)
					|| newStatus.equals(CourseStatus.COMPLETED)) {
				ca.setCompleteDate(new Date());
			}
			try {
				saveService.saveAttempt(ca);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (CourseStatus.PASSED.equals(newStatus)
					|| CourseStatus.COMPLETED.equals(newStatus)) {
				notificationService.fireCourseComplete(ca);
				notificationService.fireCompleteTraining(ca);
			}
		}
	}

	private boolean updateCourseCompleteStatus(CourseAttempt ca,
			List<Slide> slides, Map<Long, SlideAttempt> attempts) {
		if (ca.isComplete()) { // статус "пролностью просмотрен курс" не меняется.
			return true;
		}
		boolean complete = true;
		for (Slide slide : slides) {
			SlideAttempt slideAttempt = attempts.get(slide.getObjectId()
					.getId());
			if (slideAttempt == null || slideAttempt.getViewCounter() < 1) { // слайд ни разу не просмотрен.
				complete = false;
				break;
			}
		}
		if (complete) {
			ca.setComplete(complete);
			ca.setCompleteDate(new Date());
		}
		return complete;
	}
}
