package ru.prognoz.tc.mvc.common;

import java.util.Map;

public interface HtmlBuilder {

	String build(String viewName, Map<String, Object> model);
	
}
