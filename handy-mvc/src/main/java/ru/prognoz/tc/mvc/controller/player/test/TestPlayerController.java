package ru.prognoz.tc.mvc.controller.player.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.dao.attempt.QuestionAttemptDao;
import ru.prognoz.tc.dao.attempt.TestAttemptDao;
import ru.prognoz.tc.dao.test.AnswerDao;
import ru.prognoz.tc.dao.test.QuestionDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;
import ru.prognoz.tc.mvc.business.player.PlayerSaveService;

@Controller
@RequestMapping(value = "/main/testplayer")
public class TestPlayerController {

	@Autowired
	private TestPlayerBean bean;
	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private AnswerDao answerDao;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private TestDao testDao;
	@Autowired
	private TestResultCalculator calculator;
	@Autowired
	private QuestionAttemptDao qaDao;
	@Autowired
	private TestAttemptDao taDao;
	@Autowired
	private PlayerSaveService saveService;
	@Autowired
	private ViewCounterDao viewCounterDao;
	
	private static final Logger logger = LoggerFactory
			.getLogger(TestPlayerController.class);

	private void initTest(Long oid) {
		bean = new TestPlayerBean();
		User currentUser = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		Test test = testDao.getByObjectId(oid);
		bean.setTestStartedTime(System.currentTimeMillis());
		bean.setTest(test);
		bean.setCourse(courseDao.getByOid(test.getCourseOID().getId()));
		List<Question> questions = questionDao
				.getNotArchiveQuestionsByTest(oid);
		bean.setQuestions(questions);
		bean.setCountRegistrations(viewCounterDao.getCountViews(bean.getCourse().getObjectId()));
		bean.setTestAttempt(taDao.getOrCreate(test, currentUser));
		for (Question question : bean.getQuestions()) {
			bean.getQuestionsAttempts().put(question.getObjectId().getId(),
					qaDao.getOrCreate(question, currentUser));
			List<Answer> answers = answerDao.getAnswersByQuestion(question);
			Collections.shuffle(answers);
			bean.getAnswers().put(question.getObjectId().getId(), answers);
			bean.getUserAnswers().put(question.getObjectId().getId(),
					answersToUserAnswers(bean.getAnswersByQuestion(question)));
		}

		if (questions.size() != 0) {
			Collections.shuffle(bean.getQuestions());
			bean.setActiveQuestion(questions.get(0));
			bean.getQuestionsAttempts()
					.get(bean.getActiveQuestion().getObjectId().getId())
					.setShowed(true);
		}
	}

	private List<UserAnswer> answersToUserAnswers(List<Answer> answers) {
		List<UserAnswer> userAnswers = new ArrayList<UserAnswer>();
		for (Answer answer : answers) {
			UserAnswer userAnswer = new UserAnswer();
			userAnswer.setOid(answer.getObjectId().getId());
			if (answer.getQuestionType().equals("single")
					|| answer.getQuestionType().equals("alternative")
					|| answer.getQuestionType().equals("multiple")) {
				userAnswer.setValue("false");
			} else if (answer.getQuestionType().equals("open")) {
				userAnswer.setValue("");
			} else if (answer.getQuestionType().equals("sequence")) {
				userAnswer.setValue("0");
			} else if (answer.getQuestionType().equals("association")) {
				userAnswer.setValue("");
			}
			userAnswers.add(userAnswer);
		}
		return userAnswers;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView openTest(@RequestParam(value = "oid") final long oid)
			throws HandyCoreException {
		logger.debug("Open test with oid " + oid);
		initTest(oid);
		ModelAndView mav = new ModelAndView("testplayer/index");
		mav.addObject("bean", bean);
		return mav;
	}

	@RequestMapping(value = { "/question" }, method = RequestMethod.POST)
	@ResponseBody
	public String changePage(Locale locale, Model mod,
			@RequestParam(value = "oid") Long oid) {
		logger.debug("Open question " + oid);
		if (bean.getQuestions() == null || bean.getQuestions().isEmpty()) {
			return "false";
		}

		for (Question question : bean.getQuestions()) {
			if (question.getObjectId().getId() == oid) {
				bean.setActiveQuestion(question);
				return "true";
			}
		}

		return "false";
	}

	@RequestMapping(value = { "/last" }, method = RequestMethod.POST)
	@ResponseBody
	public String lastPage(Locale locale, Model mod) {
		if (bean.getQuestions() == null || bean.getQuestions().isEmpty()) {
			return "false";
		} else {
			bean.setActiveQuestion(bean.getQuestions().get(
					bean.getQuestions().size() - 1));
			return "true";
		}
	}

	@RequestMapping(value = { "/first" }, method = RequestMethod.POST)
	@ResponseBody
	public String firstPage(Locale locale, Model mod) {
		logger.debug("First question");
		if (bean.getQuestions() == null || bean.getQuestions().isEmpty()) {
			return "false";
		} else {
			bean.setActiveQuestion(bean.getQuestions().get(0));
			return "true";
		}
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.POST)
	@ResponseBody
	public String nextQuestion() {
		logger.debug("Next question");
		if (bean.getQuestions() == null || bean.getQuestions().isEmpty()) {
			return "false";
		} else {
			try {
				int index = bean.getQuestions().indexOf(
						bean.getActiveQuestion());
				int size = bean.getQuestions().size();
				if (index < size) {
					bean.setActiveQuestion(bean.getQuestions().get(index + 1));
				}
				return "true";
			} catch (IndexOutOfBoundsException e) {
				return "false";
			}
		}
	}

	@RequestMapping(value = { "/prev" }, method = RequestMethod.POST)
	@ResponseBody
	public String prevQuestion(Locale locale, Model mod) {
		logger.debug("Previous question ");
		if (bean.getQuestions() == null || bean.getQuestions().isEmpty()) {
			return "false";
		} else {
			try {
				int index = bean.getQuestions().indexOf(
						bean.getActiveQuestion());
				int size = bean.getQuestions().size();
				if (index - 1 >= 0 && index < size) {
					bean.setActiveQuestion(bean.getQuestions().get(index - 1));
				}
				return "true";
			} catch (IndexOutOfBoundsException e) {
				return "false";
			}
		}
	}

	@RequestMapping(value = { "/content" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getQuestionContent() {
		logger.debug("Get question content ");
		if (bean.getActiveQuestion() == null) {
			return null;
		}
		onAttemptLoad();
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView("testplayer/question");
		model.addObject("bean", bean);
		result.setResult(model);
		return result;
	}

	@RequestMapping(value = { "/save" }, method = RequestMethod.POST)
	@ResponseBody
	public String saveAnswer(@RequestBody TestPojo testPojo) {
		logger.debug("Save question answers");
		saveQuestion(testPojo);
		return "true";
	}

	@RequestMapping(value = { "/paging" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> paging(Locale locale, Model model) {
		logger.debug("Paging for testplayer");
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView mv = new ModelAndView("testplayer/paging");
		mv.addObject("bean", bean);
		result.setResult(mv);
		return result;
	}

	@RequestMapping(value = { "/calculate" }, method = RequestMethod.POST)
	@ResponseBody
	public String calculateResult(Locale locale, Model model) {
		logger.debug("Calculate question results");
		calculator.calculateTest(bean);
		return "{\"value\":\"true\",\"oid\":\""
				+ bean.getTest().getObjectId().getId() + "\"}";
	}

	private void saveQuestion(TestPojo pojo) {
		for (UserAnswer userAnswer : bean.getUserAnswers().get(pojo.getOid())) {
			for (UserAnswer newAnswer : pojo.getAnswers()) {
				if (userAnswer.getOid() == newAnswer.getOid()) {
					if (StringUtils.isEmpty(newAnswer.getValue())) {
						userAnswer.setValue("");
					} else {
						userAnswer.setValue(newAnswer.getValue().trim());
					}
					break;
				}
			}
		}
	}

	private void onAttemptLoad() {
		QuestionAttempt qa = bean.getQuestionsAttempts().get(
				bean.getActiveQuestion().getObjectId().getId());
		qa.setShowed(true);
	}
}
