package ru.prognoz.tc.mvc.controller.contacts;

import java.util.List;
import java.util.Locale;

import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.mvc.business.contact.ContactService;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

@Controller
public class ContactsController {

	private static final Logger logger = LoggerFactory.getLogger(ContactsController.class);

	@Autowired
	private ContactService contactService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ContactListInfoDao contactListInfoDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ContactListContentDao contactListContentDao;
	@Autowired
	private HandyObjectDao handyObjectDao;

	@RequestMapping(value = "/main/contacts", method = RequestMethod.GET)
	public String contactsView(Locale locale, Model model) {
		logger.info("Contacts page", locale);
		model.addAttribute("bean", populateViewBean(null, false));
		return "contacts/index";
	}

	/**
	 * Создание списка
	 * 
	 * @param locale
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/createList" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> createList(Locale locale) {
		logger.info("Contacts : create contact list");
		DeferredResult<String> result = new DeferredResult<String>();
		try {
			ContactListInfo newGroup = contactService.createContactList(messageSource
					.getMessage("UserGroupFace.defaultTitle", null, locale));
			result.setResult("" + newGroup.getObjectId().getId());
		} catch (HandyCoreException e) {
			result.setResult("error");
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * Удаление списка
	 * 
	 * @param locale
	 * @param groupOid
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/deleteList" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> deleteList(Locale locale, @RequestParam(value = "groupOid") final long groupOid) {
		logger.info("Contacts : delete contact list");
		DeferredResult<String> result = new DeferredResult<String>();
		try {
			contactService.deleteContactList(groupOid);
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<ContactListInfo> list = contactListInfoDao.list(currentUser);
			if (list.size() > 0) {
				result.setResult("" + list.get(0).getObjectId().getId());
			} else {
				result.setResult("-1");
			}
		} catch (HandyCoreException e) {
			result.setResult("error");
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * Изменение названия списка.
	 * 
	 * @param groupOid
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/updateListTitle" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> updateListTitle(@RequestParam(value = "groupOid") final long groupOid,
			@RequestParam(value = "groupTitle") final String groupTitle) {

		logger.info("Contacts : update contact list title");
		DeferredResult<String> result = new DeferredResult<String>();
		try {
			contactService.updateContactListTitle(groupOid, groupTitle);
			result.setResult("ok");
		} catch (HandyCoreException e) {
			result.setResult("error");
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * Поиск контакта.
	 * 
	 * @param listOid
	 *            Список пользователей в котором производится поиск контакта
	 * @param contactName
	 *            Название контакта.
	 * @return Перерисованный контент активной панели.
	 */
	@RequestMapping(value = { "/main/contacts/findContact" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> findContact(Locale locale, @RequestParam(value = "groupOid") final long listOid,
			@RequestParam(value = "contactName") final String listMemberCriteria) {

		logger.info("Contacts : find contact in list");
		ModelAndView mav = new ModelAndView("contacts/groupwell");
		ContactsViewBean bean = populateViewBean(listOid, false);
		mav.addObject("bean", bean);

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		result.setResult(mav);
		try {
			// 1. Проверяем контакты текущей группы на поиск повторений, если есть - выводим пользователю уведомление.
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Contact fContact = contactService.findListMember(currentUser, listOid, listMemberCriteria);
			if (fContact != null) {
				Object args[] = { listMemberCriteria };
				bean.setMsg(new ViewMessage("error", "", messageSource.getMessage("UserGroupFace.findDoubleUserMsg", args, locale)));
			} else {
				// 2. Ищем пользователя с таким емейлом, если есть - показываем найденного.
				User findedUser = userDao.getByLogin(listMemberCriteria);
				if (findedUser != null) {
					bean.setFindUser(findedUser);
				} else {
					// 3. Проверяем на валидность емейл. Показываем емейл, как несозданный контакт.
					if (!EmailValidator.getInstance().isValid(listMemberCriteria)) {
						bean.setMsg(new ViewMessage("error", "", messageSource.getMessage("UserGroupFace.emailValidationError", null,
								locale)));
					} else {
						bean.setFindEmail(listMemberCriteria);
					}
				}
			}
		} catch (HandyCoreException e) {
			logger.error(e.getMessage());
		}
		return result;
	}

	/**
	 * Создание контакта на основе емейла.
	 * 
	 * @param locale
	 * @param listOid
	 *            ОИД списка в который нужно добавить контакт
	 * @param email
	 *            емейл
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/addContactByUser" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> addContactByUser(Locale locale, @RequestParam(value = "groupOid") final long listOid,
			@RequestParam(value = "userOid") final long userOid) {

		logger.info("Contacts : add Contact By User");
		DeferredResult<String> result = new DeferredResult<String>();

		try {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			contactService.createContact(currentUser, listOid, userOid);
			result.setResult("ok");
		} catch (HandyCoreException exc) {
			logger.error(exc.getMessage());
			result.setResult("error");
		}
		return result;
	}

	/**
	 * Создание контакта на основе емейла.
	 * 
	 * @param locale
	 * @param listOid
	 *            ОИД списка в который нужно добавить контакт
	 * @param email
	 *            емейл
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/addContactByEmail" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> addContactByEmail(Locale locale, @RequestParam(value = "groupOid") final long listOid,
			@RequestParam(value = "email") final String email) {

		logger.info("Contacts : add Contact By Email");
		DeferredResult<String> result = new DeferredResult<String>();

		try {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			contactService.createContact(currentUser, listOid, email);
			result.setResult("ok");
		} catch (HandyCoreException exc) {
			logger.error(exc.getMessage());
			result.setResult("error");
		}
		return result;
	}

	@RequestMapping(value = { "/main/contacts/addContactByAddressbook" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> addContactByAddressbook(@RequestBody AddressbookRequestBody rb) {

		logger.info("Contacts : add Contact By Addressbook");
		DeferredResult<String> result = new DeferredResult<String>();

		try {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			contactService.createContacts(currentUser, rb);
			result.setResult("{\"ok\" : \"ok\" }");
		} catch (HandyCoreException exc) {
			logger.error(exc.getMessage());
			result.setResult("error");
		}

		return result;
	}

	@RequestMapping(value = { "/main/contacts/deleteContact" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> deleteContact(Locale locale, @RequestParam(value = "groupOid") final long listOid,
			@RequestParam(value = "contactOid") final long contactOid) {

		logger.info("Contacts : delete Contact");
		DeferredResult<String> result = new DeferredResult<String>();

		try {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			contactService.deleteContact(currentUser, listOid, contactOid);
			result.setResult("ok");
		} catch (HandyCoreException exc) {
			logger.error(exc.getMessage());
			result.setResult("error");
		}
		return result;
	}

	/**
	 * Перерисвока списка групп.
	 * 
	 * @param locale
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/renderGroups" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> renderGroups(Locale locale, @RequestParam(value = "_activeGroup") final long activeGroupOid) {
		logger.info("Contacts : render contact lists");

		ModelAndView mav = new ModelAndView("contacts/groups");
		ContactsViewBean bean = populateViewBean(activeGroupOid, false);
		mav.addObject("bean", bean);

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		result.setResult(mav);
		return result;
	}

	/**
	 * Перерисвока контента группы.
	 * 
	 * @param locale
	 * @return
	 */
	@RequestMapping(value = { "/main/contacts/renderGroupContent" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> renderGroupContent(Locale locale, @RequestParam(value = "_activeGroup") final long groupOid,
			@RequestParam(value = "editTitleMode", defaultValue = "false") final boolean editTitleMode) {
		logger.info("Contacts : render contact list content");

		ModelAndView mav = new ModelAndView("contacts/groupwell");
		ContactsViewBean bean = populateViewBean(groupOid, editTitleMode);
		mav.addObject("bean", bean);

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		result.setResult(mav);
		return result;
	}

	private ContactsViewBean populateViewBean(Long activeListOid, boolean editTitleMode) {
		if (activeListOid == null) {
			activeListOid = -1L;
		}
		ContactsViewBean viewBean = new ContactsViewBean();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<ContactListInfo> list = contactListInfoDao.list(currentUser);
		viewBean.setInfoList(list);
		if (list.size() > 0 && activeListOid == -1L) {
			viewBean.setActiveListOid(list.get(0).getObjectId().getId());
		} else {
			viewBean.setActiveListOid(activeListOid);
		}
		viewBean.setEditTitleMode(editTitleMode);
		// Заполняем контакты активного списка
		ContactListInfo activeContactList = viewBean.getActiveList();
		if (activeContactList != null) {
			viewBean.setContacts(contactListContentDao.members(activeContactList));
		}
		return viewBean;
	}
}
