package ru.prognoz.tc.mvc.controller.useraccount;

import java.util.Locale;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.resource.HandyResourseService;
import ru.prognoz.tc.mvc.common.HtmlCleaner;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

@Controller
public class UserAccountController {

	private static final Logger logger = LoggerFactory.getLogger(UserAccountController.class);

	@Autowired
	UserDao userDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HandyclassAppCfg appCfg;
	@Autowired
	private HandyResourseService resourseService;

	@RequestMapping(value = "/main/useraccount", method = RequestMethod.GET)
	public String home(Locale locale, Model model, @ModelAttribute("fileuplaodattr") UserAccountBean bean) {
		logger.info("User Account Page", locale);
		if (!bean.isRedirected()) {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (currentUser != null) {
				if (currentUser.getAvatarURL() != null) {
					bean.setAvatarUrl(currentUser.getAvatarURL());
				}
				bean.setFirstName(currentUser.getName());
				bean.setLastName(currentUser.getLastName());
				bean.setAbout(currentUser.getAbout());
				bean.setLogin(currentUser.getLogin());
				bean.setFacebookName(currentUser.getFacebookName());
				bean.setVkName(currentUser.getVkName());
				bean.setTwitterName(currentUser.getTwitterName());
				bean.setGoogleName(currentUser.getGoogleName());
			}
		}
		model.addAttribute("bean", bean);
		return "useraccount/page";
	}

	/**
	 * Сохранение персональных данных
	 * 
	 * @param model
	 * @param firstName
	 * @param lastName
	 * @param about
	 * @return
	 */
	@RequestMapping(value = { "/main/useraccount/save_personal" }, method = RequestMethod.POST)
	public Callable<String> asyncPersonalDataUpdate(final Locale locale, final Model model,
			@RequestParam(value = "firstName", defaultValue = "") final String firstName,
			@RequestParam(value = "lastName", defaultValue = "") final String lastName,
			@RequestParam(value = "about", defaultValue = "") final String about) {

		logger.info("User Account Page: update personal data");

		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				UserAccountBean bean = new UserAccountBean();
				User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				if (currentUser != null) {
					if (!firstName.isEmpty()) {
						currentUser.setName(HtmlCleaner.cleanAll(firstName));
					}
					if (!lastName.isEmpty()) {
						currentUser.setLastName(HtmlCleaner.cleanAll(lastName));
					}
					currentUser.setAbout(HtmlCleaner.cleanAll(about));
					try {
						userDao.save(currentUser);
						bean.setFirstName(currentUser.getName());
						bean.setLastName(currentUser.getLastName());
						bean.setAbout(currentUser.getAbout());
						bean.setSavePersonalMsg(new ViewMessage("success", messageSource.getMessage("UserAccountFace.messageSavePersonal", null, locale)));
					} catch (Exception e) {
						bean.setSavePersonalMsg(new ViewMessage("error", messageSource.getMessage(
								"UserAccountFace.messageFailedSavePersonal", null, locale)));
					}
				}
				model.addAttribute("bean", bean);
				return "useraccount/personalPanel";
			}
		};
	}

	@RequestMapping(value = { "/main/useraccount/updateTopmenuUserName" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> asyncUpdateTopmenuUserName() {
		logger.info("User Account Page: update topmenu user name");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult(currentUser.getUsername());
		return result;
	}

	@RequestMapping(value = { "/main/useraccount/updateTopmenuUserAvatar" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> asyncUpdateTopmenuUserAvatar() {
		logger.info("User Account Page: update topmenu user avatar");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult(currentUser.getAvatarURL());
		return result;
	}

	/**
	 * Сохранение емейла/логина
	 */
	@RequestMapping(value = { "/main/useraccount/save_email" }, method = RequestMethod.POST)
	public DeferredResult<String> asyncEmailDataUpdate(final Model model, final Locale locale,
			@RequestParam(value = "email", defaultValue = "") final String email) {

		logger.info("User Account Page: update email data");
		UserAccountBean bean = new UserAccountBean();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		model.addAttribute("bean", bean);
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("useraccount/emailPanel");

		// Пустой логин
		if (email.isEmpty()) {
			bean.setLogin(currentUser.getLogin());
			bean.setNewEmail("");
			bean.setValidEmailMsg(new ViewMessage("error", messageSource.getMessage("UserAccountFace.validationData.wrongEmail", null,
					locale)));
			return result;
		}
		// Повторяющийся логин
		User tempUser = userDao.getByLogin(email);
		if (tempUser != null) {
			bean.setLogin(currentUser.getLogin());
			bean.setNewEmail(email);
			bean.setValidEmailMsg(new ViewMessage("error", messageSource.getMessage("UserAccountFace.validationData.repeatedEmail", null,
					locale)));
			return result;
		}
		// Сохранение логина/емейла
		try {
			currentUser.setLogin(HtmlCleaner.cleanAll(email));
			userDao.save(currentUser);
			bean.setLogin(currentUser.getLogin());
			bean.setSaveEmailMsg(new ViewMessage("success", messageSource.getMessage("UserAccountFace.messageSavePersonal", null, locale)));
		} catch (Exception e) {
			bean.setSaveEmailMsg(new ViewMessage("error", messageSource.getMessage("UserAccountFace.messageFailedSavePersonal", null,
					locale)));
		}
		return result;
	}

	/**
	 * Изменение пароля
	 */
	@RequestMapping(value = { "/main/useraccount/save_password" }, method = RequestMethod.POST)
	public DeferredResult<String> asyncchangePassword(final Model model, final Locale locale,
			@RequestParam(value = "cur_pass", defaultValue = "") final String currentPass,
			@RequestParam(value = "new_pass", defaultValue = "") final String newPass,
			@RequestParam(value = "repeat_pass", defaultValue = "") final String repeatPass) {

		logger.info("User Account Page: change password");
		UserAccountBean bean = new UserAccountBean();
		bean.setCurrentPassword(currentPass);
		bean.setNewPassword(newPass);
		bean.setRepeatPassword(repeatPass);
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		model.addAttribute("bean", bean);
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("useraccount/passwordPanel");

		// неверный пароль
		if (!StringUtils.isEmpty(currentUser.getPassword()) && !bean.getCurrentPassword().equals(currentUser.getPassword())) {
			bean.setValidCurPassMsg(new ViewMessage("error", messageSource.getMessage("UserAccountFace.validationData.wrongPassword", null,
					locale)));
			return result;
		}
		// Не валидный пароль
		if (StringUtils.isEmpty(newPass) || newPass.length() < 3) {
			bean.setValidNewPassMsg(new ViewMessage("error", messageSource.getMessage("SignupFace.smallPassword", null, locale)));
			return result;
		}
		// Не совпадает с повторным
		if (!bean.getNewPassword().equals(bean.getRepeatPassword())) {
			bean.setValidRepPassMsg(new ViewMessage("error", messageSource.getMessage(
					"UserAccountFace.validationData.wrongRepeatedPassword", null, locale)));
			return result;
		}
		// Сохранение
		try {
			currentUser.setPassword(bean.getNewPassword());
			userDao.save(currentUser);
			logger.info("Изменение пароля пользователя [ID: " + currentUser.getObjectId().getId() + "]");
			bean.setSavePassMsg(new ViewMessage("success", messageSource.getMessage("UserAccountFace.messageSaveReg", null, locale)));
			bean.setCurrentPassword("");
			bean.setNewPassword("");
			bean.setRepeatPassword("");
		} catch (Exception e) {
			logger.info("Ошибка при сохранении пароля пользователя [ID: " + currentUser.getObjectId().getId() + "]");
			bean.setSavePassMsg(new ViewMessage("error", messageSource.getMessage("UserAccountFace.messageFailedSaveReg", null, locale)));
		}
		return result;
	}

	@RequestMapping(value = { "/main/useraccount/fileupload" }, method = RequestMethod.POST)
	public String fileupload(@RequestParam MultipartFile avatar,
			@RequestParam(value = "firstName", defaultValue = "") final String firstName,
			@RequestParam(value = "lastName", defaultValue = "") final String lastName,
			@RequestParam(value = "about", defaultValue = "") final String about,
			@RequestParam(value = "email", defaultValue = "") final String email,
			@RequestParam(value = "cur_pass", defaultValue = "") final String currentPass,
			@RequestParam(value = "new_pass", defaultValue = "") final String newPass,
			@RequestParam(value = "repeat_pass", defaultValue = "") final String repeatPass, final RedirectAttributes redirectAttributes) {

		logger.info("User Account Page: file upload request");

		UserAccountBean bean = new UserAccountBean();
		bean.setFirstName(firstName);
		bean.setLastName(lastName);
		bean.setAbout(about);
		bean.setNewEmail(email);
		bean.setCurrentPassword(currentPass);
		bean.setNewPassword(newPass);
		bean.setRepeatPassword(repeatPass);
		bean.setRedirected(true);
		redirectAttributes.addFlashAttribute("fileuplaodattr", bean);

		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		try {
			String url = resourseService.storeUserAvatar(currentUser, avatar);
			currentUser.setAvatarURL(url);
			userDao.save(currentUser);
			bean.setAvatarUrl(currentUser.getAvatarURL());
		} catch (HandyCoreException e1) {
			return "redirect:/main/useraccount";
		}
		return "redirect:/main/useraccount";
	}

	@RequestMapping(value = { "/main/useraccount/delete_avatar" }, method = RequestMethod.POST)
	public DeferredResult<String> asyncDeleteAvatar(final Model model, final Locale locale) {
		logger.info("User Account Page: delete avatar");
		UserAccountBean bean = new UserAccountBean();

		model.addAttribute("bean", bean);
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("useraccount/avatarPanel");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		currentUser.setAvatarURL(bean.getAvatarUrl());
		userDao.save(currentUser);

		return result;
	}

}
