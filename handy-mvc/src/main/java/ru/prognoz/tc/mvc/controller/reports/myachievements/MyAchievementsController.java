package ru.prognoz.tc.mvc.controller.reports.myachievements;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.myachievement.MyAchievementCriteria;
import ru.prognoz.tc.core.reports.impl.myachievement.MyAchievementItem;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.courselist.FakeCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
public class MyAchievementsController {

	private static final Logger logger = LoggerFactory.getLogger(MyAchievementsController.class);

	@Autowired
	private HandyclassAppCfg appConfig;
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseCategoryDao courseCategoryDao;

	@RequestMapping(value = "/reports/myAchievements", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws HandyCoreException {

		logger.info("my achievements report page");

		ModelAndView mav = new ModelAndView("reports/myAchievements/index");
		mav.addObject("bean", populateBean(locale, null, 0));
		return mav;
	}

	@RequestMapping(value = "/reports/myAchievements/renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my achievements report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/myAchievements/report");
		mav.addObject("bean", populateBean(locale, viewModel, 0));
		return mav;
	}

	@RequestMapping(value = "/reports/myAchievements/loadBillboard", method = RequestMethod.POST)
	public ModelAndView loadBillboard(Locale locale) throws HandyCoreException {
		logger.info("my achievements report page: loadBillboard");

		ModelAndView mav = new ModelAndView("reports/myAchievements/billboard");
		mav.addObject("bean", populateBean(locale, null, appConfig.getDefaultRowLimit()));
		return mav;
	}

	@RequestMapping(value = "/reports/myAchievements/renderBillboard", method = RequestMethod.POST)
	public ModelAndView renderBillboard(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my achievements report page: renderBillboard");

		ModelAndView mav = new ModelAndView("reports/myAchievements/billboard");
		mav.addObject("bean", populateBean(locale, viewModel, appConfig.getDefaultRowLimit()));
		return mav;
	}

	private MyAchievementsBean populateBean(Locale locale, ReportViewModel viewModel, int rowLimit) throws HandyCoreException {
		if (viewModel == null) {
			viewModel = new ReportViewModel();
			viewModel.setController("/reports/myAchievements");
			viewModel.setSort(MyAchievementItem.COLUMN_COURSE_TITLE);
			viewModel.setDirection(SortDirection.ASC);
			viewModel.setReportType(ReportType.MY_ACHIEVEMENT);
		}
		MyAchievementsBean bean = new MyAchievementsBean();
		bean.setItems(reportService.getMyAchievementsItems(criteriaByModel(viewModel, rowLimit)));
		bean.setViewModel(viewModel);

		List<CourseCategory> categories = new ArrayList<CourseCategory>();
		categories.add(new FakeCategory());
		categories.addAll(courseCategoryDao.getAll());
		bean.setCategories(categories);

		return bean;
	}

	private MyAchievementCriteria criteriaByModel(ReportViewModel viewModel, int rowLimit) {
		MyAchievementCriteria criteria = new MyAchievementCriteria();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		criteria.setFilterUser(currentUser);
		criteria.setRowCount(rowLimit);
		criteria.setOrderColumn(viewModel.getSort());
		criteria.setSortDirection(viewModel.getDirection());
		criteria.setFilterCategoryID(viewModel.getCategoryFilter());
		criteria.setFindValue(viewModel.getSearch());
		return criteria;
	}
}
