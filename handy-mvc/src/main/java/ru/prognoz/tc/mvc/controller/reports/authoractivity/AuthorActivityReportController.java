package ru.prognoz.tc.mvc.controller.reports.authoractivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityCriteria;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityItem;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.courselist.FakeCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
@RequestMapping(value = "/reports/authorActivity")
public class AuthorActivityReportController {

	private static final Logger logger = LoggerFactory.getLogger(AuthorActivityReportController.class);
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseCategoryDao courseCategoryDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws HandyCoreException {

		logger.info("Author activity report page");

		ModelAndView mav = new ModelAndView("reports/authorActivity/index");
		ReportViewModel defaultModel = new ReportViewModel();
		defaultModel.setController("/reports/authorActivity");
		defaultModel.setSort(AuthorActivityItem.COLUMN_COUNT_COURSES);
		defaultModel.setDirection(SortDirection.DESC);
		mav.addObject("bean", populateBean(locale, defaultModel));
		return mav;
	}

	private AuthorActivityCriteria criteriaByModel(ReportViewModel viewModel) {
		AuthorActivityCriteria authorActivityCriteria = new AuthorActivityCriteria();
		authorActivityCriteria.setRowCount(-1);
		authorActivityCriteria.setOrderColumn(viewModel.getSort());
		authorActivityCriteria.setSortDirection(viewModel.getDirection());
		authorActivityCriteria.setFilterCategoryID(viewModel.getCategoryFilter());
		authorActivityCriteria.setFindValue(viewModel.getSearch());
		return authorActivityCriteria;
	}

	@RequestMapping(value = "renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("Author activity report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/authorActivity/report");
		mav.addObject("bean", populateBean(locale, viewModel));
		return mav;
	}

	private AuthorActivityBean populateBean(Locale locale, ReportViewModel viewModel) throws HandyCoreException {
		AuthorActivityBean bean = new AuthorActivityBean();
		bean.setItems(reportService.getAuthorActivityItems(criteriaByModel(viewModel)));
		bean.setViewModel(viewModel);
		
		List<CourseCategory> categories = new ArrayList<CourseCategory>();
		categories.add(new FakeCategory());
		categories.addAll(courseCategoryDao.getAll());
		bean.setCategories(categories);
		
		return bean;
	}
}
