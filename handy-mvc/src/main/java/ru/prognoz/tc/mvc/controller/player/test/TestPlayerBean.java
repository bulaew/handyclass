package ru.prognoz.tc.mvc.controller.player.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

@Component
public class TestPlayerBean {
	private Course course;
	private List<Question> questions;
	private Question activeQuestion;
	private Map<Long, List<Answer>> answers = new HashMap<Long, List<Answer>>();
	private Map<Long, List<UserAnswer>> userAnswers = new HashMap<Long, List<UserAnswer>>();
	private Test test;
	private CourseAttempt courseAttempt;
	private TestAttempt testAttempt;
	private Map<Long, QuestionAttempt> questionsAttempts = new HashMap<Long, QuestionAttempt>();
	private long testStartedTime = 0;
	private long countRegistrations = 0;

	public long getCountRegistrations() {
		return countRegistrations;
	}

	public void setCountRegistrations(long countRegistrations) {
		this.countRegistrations = countRegistrations;
	}

	public CourseAttempt getCourseAttempt() {
		return courseAttempt;
	}

	public void setCourseAttempt(CourseAttempt courseAttempt) {
		this.courseAttempt = courseAttempt;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Map<Long, List<Answer>> getAnswers() {
		return answers;
	}

	public void setAnswers(Map<Long, List<Answer>> answers) {
		this.answers = answers;
	}

	public Question getActiveQuestion() {
		return activeQuestion;
	}

	public void setActiveQuestion(Question activeQuestion) {
		this.activeQuestion = activeQuestion;
	}

	public List<Answer> getAnswersByQuestion(Question q) {
		return getAnswersByQuestion(q.getObjectId());
	}

	public List<Answer> getShuffledAnswersByQuestion(Question q) {
		List<Answer> answers = getAnswersByQuestion(q);
		Collections.shuffle(answers);
		return answers;
	}

	public List<Answer> getAnswersByQuestion(HandyObjectID oid) {
		return getAnswersByQuestion(oid.getId());
	}

	public List<Answer> getAnswersByQuestion(Long oid) {
		return answers.get(oid);
	}

	public TestAttempt getTestAttempt() {
		return testAttempt;
	}

	public void setTestAttempt(TestAttempt testAttempt) {
		this.testAttempt = testAttempt;
	}

	public Map<Long, List<UserAnswer>> getUserAnswers() {
		return userAnswers;
	}

	public Map<Long, List<UserAnswer>> getUserAnswersByOid(Long oid) {
		return userAnswers;
	}

	public void setUserAnswers(Map<Long, List<UserAnswer>> userAnswers) {
		this.userAnswers = userAnswers;
	}

	public Map<Long, QuestionAttempt> getQuestionsAttempts() {
		return questionsAttempts;
	}

	public void setQuestionsAttempts(
			Map<Long, QuestionAttempt> questionsAttempts) {
		this.questionsAttempts = questionsAttempts;
	}

	public UserAnswer getUserAnswerByOid(Long oid) {
		for (UserAnswer answer : userAnswers.get(activeQuestion.getObjectId()
				.getId())) {
			if (answer.getOid() == oid) {
				return answer;
			}
		}
		return null;
	}

	public String getAnswerTextByOid(Long oid) {
		for (List<Answer> answers : this.answers.values()) {
			for (Answer answer : answers) {
				if (answer.getObjectId().getId() == oid) {
					return answer.getText();
				}
			}
		}
		return null;
	}

	public List<UserAnswer> sortedSequenceAnswers(Question question) {
		long oid = question.getObjectId().getId();
		Collections.sort(userAnswers.get(oid), new Comparator<UserAnswer>() {
			@Override
			public int compare(UserAnswer a1, UserAnswer a2) {
				return a1.getValue().compareTo(a2.getValue());
			}
		});
		return userAnswers.get(oid);
	}

	public boolean isAnswerEmpty(Long oid) {
		UserAnswer userAnswer = getUserAnswerByOid(oid);
		return StringUtils.isEmpty(userAnswer.getValue());
	}

	public boolean drawAnswerValue(Long oid) {
		Answer currentAnswer = null;
		for (Answer answer : answers.get(getActiveQuestion().getObjectId()
				.getId())) {
			if (answer.getObjectId().getId() == oid) {
				currentAnswer = answer;
				break;
			}
		}
		if (currentAnswer == null) {
			return false;
		}
		for (UserAnswer answer : userAnswers.get(getActiveQuestion()
				.getObjectId().getId())) {
			if (currentAnswer.getValue().equals(answer.getValue())) {
				return false;
			}
		}
		return true;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public long getTestStartedTime() {
		return testStartedTime;
	}

	public void setTestStartedTime(long testStartedTime) {
		this.testStartedTime = testStartedTime;
	}

	public boolean isQuestionShowed(Question q) {
		return questionsAttempts.get(q.getObjectId().getId()).isShowed();
	}

	public boolean isQuestionActive(Question q) {
		return q.getObjectId().getId() == getActiveQuestion().getObjectId()
				.getId();
	}
}
