package ru.prognoz.tc.mvc.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserDao userDao;
	@Autowired
	@Qualifier("messageSource")
	private MessageSource messageSource;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User loggedUser = userDao.getByLogin(username);
		if (loggedUser == null) {
			throw new UsernameNotFoundException("");
		}
		if (!loggedUser.isVerified()) {
			throw new BadCredentialsException("Account is not verified!");
		}
		return loggedUser;
	}
	
}
