package ru.prognoz.tc.mvc.controller.editcourse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;
import ru.prognoz.tc.mvc.business.participants.IParticipant;

@Component(value = "editCourseBean")
@Scope("session")
public class EditCourseBean implements Serializable {

	private static final long serialVersionUID = 6249938876095891705L;
	private EditCourseWizardStep step = EditCourseWizardStep.COMMON;
	private Course course = null;
	private Test test = null;
	private List<CourseCategory> posibleCategories;
	private List<Attachment> attachments = new ArrayList<Attachment>();
	private List<Slide> slides = new ArrayList<Slide>();
	private Slide activeSlide = null;
	private HashMap<Long, List<SlideComponent>> components = new HashMap<Long, List<SlideComponent>>();
	private HashMap<Long, List<ChildComponent>> childComponents = new HashMap<Long, List<ChildComponent>>();
	private List<CourseAccessType> posibleAccessTypes;
	private boolean participantView;
	private List<Question> questions = new ArrayList<Question>();
	private Map<Long, List<Answer>> answers = new HashMap<>();
	private boolean isCourseNew;
	private EditCourseVerificationMessage verifyMsg = null;

	public HashMap<Long, List<SlideComponent>> getComponents() {
		return components;
	}

	public void setComponents(HashMap<Long, List<SlideComponent>> components) {
		this.components = components;
	}

	public boolean isCourseNew() {
		return isCourseNew;
	}

	public void setCourseNew(boolean isCourseNew) {
		this.isCourseNew = isCourseNew;
	}

	public Map<Long, List<Answer>> getAnswers() {
		return answers;
	}

	public void setAnswers(Map<Long, List<Answer>> answers) {
		this.answers = answers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	/**
	 * Участники курса.
	 */
	private Set<IParticipant> participants = new TreeSet<IParticipant>();

	public EditCourseWizardStep getStep() {
		return step;
	}

	public void setStep(EditCourseWizardStep step) {
		this.step = step;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public List<CourseCategory> getPosibleCategories() {
		return posibleCategories;
	}

	public void setPosibleCategories(List<CourseCategory> posibleCategories) {
		this.posibleCategories = posibleCategories;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public List<Slide> getSlides() {
		return slides;
	}

	public void setSlides(List<Slide> slides) {
		this.slides = slides;
	}

	public void addSlide(Slide slide) {
		slides.add(slide);
		components.put(slide.getObjectId().getId(), new ArrayList<SlideComponent>());
	}

	public Slide getActiveSlide() {
		return activeSlide;
	}

	public void setActiveSlide(Slide activeSlide) {
		this.activeSlide = activeSlide;
	}

	public List<SlideComponent> getComponentsBySlide(Slide slide) {
		return components.get(slide.getObjectId().getId());
	}

	public void setSlideComponents(Slide slide, List<SlideComponent> slideComponents) {
		components.put(slide.getObjectId().getId(), slideComponents);
	}

	public HashMap<Long, List<ChildComponent>> getChildComponents() {
		return childComponents;
	}

	public void setChildComponents(HashMap<Long, List<ChildComponent>> childComponents) {
		this.childComponents = childComponents;
	}

	public void addComponentToSlide(Slide slide, SlideComponent component) {
		if (components.get(slide.getObjectId().getId()) == null) {
			List<SlideComponent> slideComponents = new ArrayList<SlideComponent>();
			slideComponents.add(component);
			components.put(slide.getObjectId().getId(), slideComponents);
		} else {
			components.get(slide.getObjectId().getId()).add(component);
		}
	}

	public void removeComponentFromSlide(Slide slide, Long componentOid) {
		for (SlideComponent slideComponent : components.get(slide.getObjectId().getId())) {
			if (componentOid == slideComponent.getComponentOID().getId()) {
				slideComponent.setArchive(true);
			}
		}
	}

	public List<Slide> getVisibleSlides() {
		List<Slide> visibleSlides = new ArrayList<Slide>();
		for (Slide slide : slides) {
			if (!slide.isArchive()) {
				visibleSlides.add(slide);
			}
		}
		return visibleSlides;
	}

	public boolean isParticipantView() {
		return participantView;
	}

	public void setParticipantView(boolean participantView) {
		this.participantView = participantView;
	}

	public List<SlideComponent> getVisibleComponentsBySlide(Slide slide) {
		if (slide == null) {
			if (slides.size() != 0) {
				slide = slides.get(0);
				activeSlide = slide;
			} else {
				return null;
			}
		}
		List<SlideComponent> visibleComponents = new ArrayList<SlideComponent>();
		for (SlideComponent component : components.get(slide.getObjectId().getId())) {
			if (component != null && !component.isArchive()) {
				visibleComponents.add(component);
			}
		}
		return visibleComponents;
	}

	public Set<IParticipant> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<IParticipant> participants) {
		this.participants = participants;
	}

	public void setPosibleAccessTypes(List<CourseAccessType> courseAccessTypes) {
		this.posibleAccessTypes = courseAccessTypes;
	}

	public List<CourseAccessType> getPosibleAccessTypes() {
		return posibleAccessTypes;
	}

	public List<IParticipant> getActualParticipants() {
		List<IParticipant> actualParticipants = new ArrayList<IParticipant>();
		for (IParticipant participant : participants) {
			if (!participant.isRemoved()) {
				actualParticipants.add(participant);
			}
		}
		return actualParticipants;
	}

	public List<Question> getVisibleQuestions() {
		List<Question> visibleQuestions = new ArrayList<Question>();
		for (Question question : questions) {
			if (question != null && !question.isArchive()) {
				visibleQuestions.add(question);
			}
		}
		Collections.sort(visibleQuestions, new Comparator<Question>() {
			@Override
			public int compare(Question o1, Question o2) {
				return Integer.valueOf(o1.getPosition()).compareTo(Integer.valueOf(o2.getPosition()));
			}
		});
		return visibleQuestions;
	}

	public void addAnswerToQuestion(Question question, Answer answer) {
		if (answers.get(question.getObjectId().getId()) == null) {
			List<Answer> newAnswers = new ArrayList<>();
			newAnswers.add(answer);
			answers.put(question.getObjectId().getId(), newAnswers);
		} else {
			answers.get(question.getObjectId().getId()).add(answer);
		}
	}

	public List<Answer> getVisibleAnswers(Question question) {
		List<Answer> visibleAnswers = new ArrayList<Answer>();
		if (answers.get(question.getObjectId().getId()) != null) {
			for (Answer answer : answers.get(question.getObjectId().getId())) {
				if (answer != null && !answer.isArchive()) {
					visibleAnswers.add(answer);
				}
			}
		}
		return visibleAnswers;
	}

	public List<Answer> getAnswersByQuestion(Question question) {
		return answers.get(question.getObjectId().getId());
	}

	public Answer getAnswerByOid(Long oid, Long questionOid) {
		if (answers.get(questionOid) != null) {
			for (Answer answer : answers.get(questionOid)) {
				if (answer.getObjectId().getId() == oid) {
					return answer;
				}
			}
		}
		return null;
	}

	public boolean hasActiveAnswers(Question question) {
		if (getAnswersByQuestion(question) == null) {
			return false;
		}
		for (Answer answer : getAnswersByQuestion(question)) {
			if (answer.isActive()) {
				return true;
			}
		}
		return false;
	}

	public List<Answer> sortedSequenceAnswers(Question question) {
		long oid = question.getObjectId().getId();
		if (answers.get(oid) == null) {
			return null;
		}
		Collections.sort(answers.get(oid), new Comparator<Answer>() {
			@Override
			public int compare(Answer a1, Answer a2) {
				return a1.getValue().compareTo(a2.getValue());
			}
		});
		return answers.get(oid);
	}

	public List<ChildComponent> getChildren(SlideComponent component) {
		return getChildren(component.getComponentOID().getId());
	}

	public List<ChildComponent> getChildren(long component) {
		List<ChildComponent> visibleChildren = new ArrayList<ChildComponent>();
		if (childComponents.get(component) == null) {
			return visibleChildren;
		}
		for (ChildComponent child : childComponents.get(component)) {
			if (!child.isArchive()) {
				visibleChildren.add(child);
			}
		}
		return visibleChildren;
	}

	public EditCourseVerificationMessage getVerifyMsg() {
		return verifyMsg;
	}

	public void setVerifyMsg(EditCourseVerificationMessage verifyMsg) {
		this.verifyMsg = verifyMsg;
	}

	public List<Slide> getPaging() {
		List<Slide> pagingSlides = new ArrayList<Slide>();
		List<Slide> visibleSlides = getVisibleSlides();
		if (visibleSlides.size() <= 17) {
			return visibleSlides;
		}
		int position = visibleSlides.indexOf(getActiveSlide());
		if (position == 0) {
			pagingSlides.add(visibleSlides.get(position));
			for (int i = 1; i < 17; i++) {
				pagingSlides.add(visibleSlides.get(i));
			}
		} else if (position == (visibleSlides.size() - 1)) {
			pagingSlides.add(visibleSlides.get(position));
			int size = visibleSlides.size();
			for (int i = 1; i < 16; i++) {
				pagingSlides.add(visibleSlides.get(size - i));

			}
		} else {
			pagingSlides.add(visibleSlides.get(position - 1));
			pagingSlides.add(visibleSlides.get(position));
			pagingSlides.add(visibleSlides.get(position + 1));
		}
		Collections.sort(pagingSlides, new Comparator<Slide>() {
			@Override
			public int compare(Slide o1, Slide o2) {
				return Integer.valueOf(o1.getPosition()).compareTo(Integer.valueOf(o2.getPosition()));
			};
		});
		return pagingSlides;
	}
}