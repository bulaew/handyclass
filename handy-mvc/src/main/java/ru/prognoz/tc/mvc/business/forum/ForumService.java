package ru.prognoz.tc.mvc.business.forum;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ru.prognoz.tc.dao.HandyCommentDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.domain.HandyComment;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.common.HtmlCleaner;

@Service
public class ForumService {

    private static final Logger logger = LoggerFactory
	    .getLogger(ForumService.class);

    @Autowired
    private HandyCommentDao commentDao;
    @Autowired
    private HandyObjectDao oidDao;
    @Autowired
    private NotificationService notificationService;

    public void createComment(User owner, Long parentOID, String content) {
	String clearContent = HtmlCleaner.cleanWysiwyg(content);
	if (StringUtils.isEmpty(clearContent)) {
	    return;
	}

	HandyObjectID parentObjectID = oidDao.getById(parentOID);

	HandyComment newComment = commentDao.newDomainInstatnce();
	newComment.setCreateDate(new Date());
	newComment.setModifiedDate(new Date());
	newComment.setOwner(owner);
	newComment.setParentOid(parentObjectID);
	newComment.setContent(clearContent);
	newComment.setObjectId(oidDao.generate());

	commentDao.save(newComment);
	logger.info("Forum Service : HandyComment object stored to DB ");

	if (notificationService != null) {
	    notificationService.fireNewComment(newComment);
	}

    }

    public void editComment(Long commentOID, String content) {
	String clearContent = HtmlCleaner.cleanWysiwyg(content);
	if (StringUtils.isEmpty(clearContent)) {
	    return;
	}

	HandyComment comment = commentDao.find(commentOID);
	comment.setContent(HtmlCleaner.cleanWysiwyg(content));
	comment.setModifiedDate(new Date());
	commentDao.save(comment);
	logger.info("Forum Service : HandyComment object edited ");
    }

    public void removeComment(Long commentOID) {
	commentDao.setArchived(commentOID);
	logger.info("Forum Service : HandyComment object removed ");
    }

}
