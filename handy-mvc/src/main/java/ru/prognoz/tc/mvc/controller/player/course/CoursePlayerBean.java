package ru.prognoz.tc.mvc.controller.player.course;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.test.Test;

@Component(value = "coursePlayerBean")
public class CoursePlayerBean {

	private Course course;
	private List<Attachment> attachments = new ArrayList<Attachment>();
	private Long viewCounter = 321L;
	private List<Slide> slides;
	private Slide activeSlide;
	private Test test;
	private Map<Long, List<SlideComponent>> components;
	private Map<Long, List<ChildComponent>> childComponents;
	private CourseAttempt courseAttepmt;
	private TestAttempt testAttempt;
	private Map<Long, SlideAttempt> slideAttempts = new HashMap<Long, SlideAttempt>();
	private long timeOnSlide = 0;

	public Map<Long, List<ChildComponent>> getChildComponents() {
		return childComponents;
	}

	public void setChildComponents(Map<Long, List<ChildComponent>> childComponents) {
		this.childComponents = childComponents;
	}

	public long getTimeOnSlide() {
		return timeOnSlide;
	}

	public void setTimeOnSlide(long timeOnSlide) {
		this.timeOnSlide = timeOnSlide;
	}

	public CourseAttempt getCourseAttepmt() {
		return courseAttepmt;
	}

	public void setCourseAttepmt(CourseAttempt courseAttepmt) {
		this.courseAttepmt = courseAttepmt;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public Slide getActiveSlide() {
		return activeSlide;
	}

	public void setActiveSlide(Slide activeSlide) {
		this.activeSlide = activeSlide;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Long getViewCounter() {
		return viewCounter;
	}

	public void setViewCounter(Long viewCounter) {
		this.viewCounter = viewCounter;
	}

	public TestAttempt getTestAttempt() {
		return testAttempt;
	}

	public void setTestAttempt(TestAttempt testAttempt) {
		this.testAttempt = testAttempt;
	}

	public void setSlides(List<Slide> slides) {
		this.slides = slides;
	}

	public List<Slide> getSlides() {
		return slides;
	}

	public Map<Long, List<SlideComponent>> getComponents() {
		return components;
	}

	public void setComponents(Map<Long, List<SlideComponent>> components) {
		this.components = components;
	}

	public List<SlideComponent> getActiveSlideComponents() {
		if (components == null || activeSlide == null) {
			return new ArrayList<SlideComponent>();
		}
		return components.get(activeSlide.getObjectId().getId());
	}

	public Map<Long, SlideAttempt> getSlideAttempts() {
		return slideAttempts;
	}

	public void setSlideAttempts(Map<Long, SlideAttempt> slideAttempts) {
		this.slideAttempts = slideAttempts;
	}
	
	public List<ChildComponent> getChildren(SlideComponent component) {
	    return childComponents.get(component.getComponentOID().getId());
	}
}
