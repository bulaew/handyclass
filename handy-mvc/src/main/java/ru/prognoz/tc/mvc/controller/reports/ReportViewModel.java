package ru.prognoz.tc.mvc.controller.reports;

import java.io.Serializable;

import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.data.CourseStatus;

public class ReportViewModel implements Serializable {

	private static final long serialVersionUID = -8019784775848178548L;
	private String controller = "";
	private ReportType reportType = ReportType.UNDEFINED;
	// ID категории
	private int categoryFilter = 0;
	// Статус прохождения курса
	private CourseStatus statusFilter = CourseStatus.UNDEFINED;
	// Регистраций/Просмотров/Того и другого
	private String userActivityFilter = "";
	// Параметр поиска
	private String search = "";
	// Сортировка по колонке
	private String sort = "";
	// Направление сортировки ASC и DSC
	private SortDirection direction = SortDirection.ASC;
	// Параметры периода поиска
	private String startDate = "";
	private String endDate = "";

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public int getCategoryFilter() {
		return categoryFilter;
	}

	public void setCategoryFilter(int categoryFilter) {
		this.categoryFilter = categoryFilter;
	}

	public CourseStatus getStatusFilter() {
		return statusFilter;
	}

	public void setStatusFilter(CourseStatus statusFilter) {
		this.statusFilter = statusFilter;
	}

	public String getUserActivityFilter() {
		return userActivityFilter;
	}

	public void setUserActivityFilter(String userActivityFilter) {
		this.userActivityFilter = userActivityFilter;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public SortDirection getDirection() {
		return direction;
	}

	public void setDirection(SortDirection direction) {
		this.direction = direction;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}
	
}
