package ru.prognoz.tc.mvc.controller.courselist.comparator;

import java.util.Comparator;

import ru.prognoz.tc.mvc.controller.courselist.CourseItem;

public class RatingComparator implements Comparator<CourseItem> {

	@Override
	public int compare(CourseItem o1, CourseItem o2) {
		return o2.getCountLikes().compareTo(o1.getCountLikes());
	}

}
