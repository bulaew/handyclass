package ru.prognoz.tc.mvc.controller.reports.propensity;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

public class MyPropensityBean {

	private List<ReportItem> items = new ArrayList<ReportItem>();
	private ReportViewModel viewModel = new ReportViewModel();

	public List<ReportItem> getItems() {
		return items;
	}

	public void setItems(List<ReportItem> items) {
		this.items = items;
	}

	public ReportViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(ReportViewModel viewModel) {
		this.viewModel = viewModel;
	}

}
