package ru.prognoz.tc.mvc.controller.reports.myachievements;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

public class MyAchievementsBean {

	private List<ReportItem> items = new ArrayList<ReportItem>();
	private ReportViewModel viewModel = new ReportViewModel();
	private List<CourseCategory> categories = new ArrayList<CourseCategory>();

	public List<ReportItem> getItems() {
		return items;
	}

	public void setItems(List<ReportItem> items) {
		this.items = items;
	}

	public ReportViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(ReportViewModel viewModel) {
		this.viewModel = viewModel;
	}

	public List<CourseCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<CourseCategory> categories) {
		this.categories = categories;
	}

}
