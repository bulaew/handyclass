package ru.prognoz.tc.mvc.controller.editcourse.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseBean;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardStep;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardUtils;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse/test")
public class TestEditCourseController {

    private static final Logger logger = LoggerFactory
	    .getLogger(TestEditCourseController.class);

    @Autowired
    private EditCourseBean courseBean;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String startEditing(Locale locale, Model model) {
	logger.info("Test Edit Course : start editing");
	model.addAttribute("bean", courseBean);
	return "/editcourse/test/step";
    }

    @RequestMapping(value = { "/changestep" }, method = RequestMethod.GET)
    public String onChangeStep(Locale locale, Model model,
	    @RequestParam(value = "step") final Integer newStepNumber) {
	logger.info("Test Edit Course : change step");
	courseBean.setStep(EditCourseWizardStep.parseInt(newStepNumber));
	model.addAttribute("bean", courseBean);
	return "redirect:"
		+ EditCourseWizardUtils.getStepUrl(courseBean.getStep());
    }

    @RequestMapping(value = { "/toogle" }, method = RequestMethod.POST)
    @ResponseBody
    public String toogleTest() {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	ModelAndView model = new ModelAndView("editcourse/testeditor/ui/onoff");
	courseBean.getTest().setEnable(!courseBean.getTest().isEnable());
	model.addObject("bean", courseBean);
	result.setResult(model);
	return "ok";
	// return result;
    }

    @RequestMapping(value = { "/score" }, method = RequestMethod.POST)
    @ResponseBody
    public String changeScore(@RequestParam(value = "score") int score) {
	if (score < 0) {
	    score = 0;
	} else if (score > 100) {
	    score = 100;
	}
	courseBean.getTest().setPassingScore(score);
	return score + "";
    }

    // Работа с вопросами
    @RequestMapping(value = "/question/add", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> addQuestion(
	    @RequestParam(value = "type") String type, Locale locale) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	Question q = QuestionCreator.createQuestion(type, courseBean.getTest());
	if(courseBean.getQuestions().isEmpty()){
	    q.setPosition(0);
	} else {
	    q.setPosition(courseBean.getQuestions().get(courseBean.getQuestions().size()-1).getPosition()+1);
	}
	courseBean.getQuestions().add(q);
	ModelAndView model = new ModelAndView(
		"editcourse/testeditor/questions/" + q.getQuestionType());
	if (q.getQuestionType().equals("alternative")) {
	    Answer yes = AnswerCreator.CreateAnswer(q);
	    Answer no = AnswerCreator.CreateAnswer(q);
	    if(locale.getLanguage().equals("ru")){
		    yes.setText("Да");
		    no.setText("Нет");
		    
	    } else {
        	    yes.setText("Yes");
        	    no.setText("No");
	    }
	    courseBean.addAnswerToQuestion(q, yes);
	    courseBean.addAnswerToQuestion(q, no);
	}
	model.addObject("question", q);
	model.addObject("bean", courseBean);
	result.setResult(model);
	return result;
    }

    @RequestMapping(value = "/question/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteQuestion(@RequestParam(value = "oid") Long oid) {
	for (Question question : courseBean.getQuestions()) {
	    if (question.getObjectId().getId() == oid) {
		question.setArchive(true);
	    }
	}
	return oid + "";
    }

    @RequestMapping(value = "/question/edit", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> editQuestion(
	    @RequestParam(value = "oid") Long oid) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	for (Question question : courseBean.getQuestions()) {
	    if (question.getObjectId().getId() == oid) {
		ModelAndView model = new ModelAndView(
			"editcourse/testeditor/questions/"
				+ question.getQuestionType());
		question.setActive(true);
		model.addObject("question", question);
		model.addObject("bean", courseBean);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "question/cancel", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> cancelQuestion(
	    @RequestParam(value = "oid") Long oid) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	for (Question question : courseBean.getQuestions()) {
	    if (question.getObjectId().getId() == oid) {
		ModelAndView model = new ModelAndView(
			"editcourse/testeditor/questions/"
				+ question.getQuestionType());
		question.setActive(false);
		model.addObject("question", question);
		model.addObject("bean", courseBean);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/question/save", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> saveQuestion(
	    @RequestBody QuestionPojo pojo) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	for (Question question : courseBean.getQuestions()) {
	    if (question.getObjectId().getId() == pojo.getOid()) {
		QuestionSaver.saveQuestion(question, pojo);
		QuestionSaver.saveAnswers(
			courseBean.getAnswersByQuestion(question), pojo);
		ModelAndView model = new ModelAndView(
			"editcourse/testeditor/questions/"
				+ question.getQuestionType());
		model.addObject("question", question);
		model.addObject("bean", courseBean);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    // Работа с ответами к вопросу.
    @RequestMapping(value = "/answer/save", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> saveAnswers(
	    @RequestBody QuestionPojo pojo) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	System.out.println("a");
	for (Question q : courseBean.getQuestions()) {
	    if (q.getObjectId().getId() == pojo.getOid()) {
		q.setText(pojo.getQuestion());
		QuestionSaver.saveAnswers(courseBean.getAnswersByQuestion(q),
			pojo);
		ModelAndView model = new ModelAndView(
			"editcourse/testeditor/questions/"
				+ q.getQuestionType());
		model.addObject("question", q);
		model.addObject("bean", courseBean);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/answer/add", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> addAnswer(
	    @RequestParam(value = "oid") Long oid) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	Question q = null;
	for (Question question : courseBean.getQuestions()) {
	    if (question.getObjectId().getId() == oid) {
		q = question;
		break;
	    }
	}
	if (q == null) {
	    return null;
	}
	Answer a = AnswerCreator.CreateAnswer(q);
	courseBean.addAnswerToQuestion(q, a);
	ModelAndView model = new ModelAndView("editcourse/testeditor/answers/"
		+ a.getQuestionType());
	model.addObject("answer", a);
	result.setResult(model);
	return result;
    }

    @RequestMapping(value = "/answer/edit", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> editAnswer(
	    @RequestParam(value = "oid") Long oid,
	    @RequestParam(value = "parentoid") Long parentoid) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	Answer answer = courseBean.getAnswerByOid(oid, parentoid);
	if (answer != null) {
	    answer.setActive(true);
	    ModelAndView model = new ModelAndView(
		    "editcourse/testeditor/answers/" + answer.getQuestionType());
	    model.addObject("answer", answer);
	    result.setResult(model);
	}
	return result;
    }

    @RequestMapping(value = "/answer/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteAnswer(@RequestParam(value = "oid") Long oid,
	    @RequestParam(value = "parentoid") Long parentoid) {
	Answer answer = courseBean.getAnswerByOid(oid, parentoid);
	if (answer != null) {
	    answer.setArchive(true);
	    return answer.getObjectId().getId() + "";
	} else {
	    return oid + "";
	}
    }

    @RequestMapping(value = "/answer/cancel", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> cancelAnswer(
	    @RequestParam(value = "oid") Long oid,
	    @RequestParam(value = "parentoid") Long parentoid) {
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	Answer answer = courseBean.getAnswerByOid(oid, parentoid);
	if (answer != null) {
	    answer.setActive(false);
	    ModelAndView model = new ModelAndView(
		    "editcourse/testeditor/answers/" + answer.getQuestionType());
	    model.addObject("answer", answer);
	    result.setResult(model);
	}
	return result;
    }

    @RequestMapping(value = { "/question/move" }, method = RequestMethod.POST)
    public String moveComponent(@RequestParam(value = "oid") long oid,
	    @RequestParam(value = "position") int position) {
	calculateQuestionPosition(oid, position);
	return "redirect:/main/editcourse/content";
    }

    private void calculateQuestionPosition(long id, int position) {
	LinkedList<Question> components = new LinkedList<Question>(
		courseBean.getQuestions());
	Question sortable = null;
	for (Question q : components) {
	    if (q.getObjectId().getId() == id) {
		sortable = q;
		break;
	    }
	}
	if (sortable != null) {
	    components.remove(sortable);
	    components.add(position, sortable);
	}
	for (Question component : components) {
	    component.setPosition(components.indexOf(component));
	}
	Collections.sort(courseBean.getQuestions(), new Comparator<Question>() {
	    @Override
	    public int compare(Question o1, Question o2) {
		return Integer.valueOf(o1.getPosition()).compareTo(
			Integer.valueOf(o2.getPosition()));
	    }
	});
    }
}
