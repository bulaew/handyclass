package ru.prognoz.tc.mvc.controller.userprofile;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.domain.User;

public class UserProfileBean {

	private User user;

	private List<CourseItemBean> courseItems = new ArrayList<CourseItemBean>();
	
	public UserProfileBean(User user) {
		this.user = user;
	}

	public List<CourseItemBean> getCourseItems() {
		return courseItems;
	}

	public void setCourseItems(List<CourseItemBean> courseItems) {
		this.courseItems = courseItems;
	}

	public User getUser() {
		return user;
	}
	
	

}
