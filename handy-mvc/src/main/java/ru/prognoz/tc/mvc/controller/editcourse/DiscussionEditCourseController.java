package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.mvc.controller.discussion.DiscussionBean;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse/discussion")
public class DiscussionEditCourseController {

	private static final Logger logger = LoggerFactory.getLogger(DiscussionEditCourseController.class);
	private static final String FORUM_BEAN_ATTR_NAME = "forumData";
	private static final String FORUM_CHANGED_HANDLER = "DiscussionModule.updateDiscussionListPanel();";

	@Autowired
	private DiscussionBean discussionBean;

	/**
	 * Первоначальный переход на страницу "Обсуждения".
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView startEditing(Locale locale, Model model) {
		logger.info("discussion Edit Course : start editing");

		EditCourseBean courseBean = HandyclassAppContext.getBean(EditCourseBean.class);
		discussionBean.clear();
		discussionBean.setCourse(courseBean.getCourse());
		if (courseBean.getCourse().getDiscussions().size() > 0) {
		    for(Discussion discussion:courseBean.getCourse().getDiscussions()){
			if(!discussion.isArchive()) {
			    discussionBean.setActiveDiscussion(discussion);
			    break;
			}
		    }
		}
		ModelAndView mav = new ModelAndView("/editcourse/discussion/step");
		if (discussionBean.getActiveDiscussion() != null) {
			mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(discussionBean.getActiveDiscussion().getObjectId().getId()));
		}
		return mav;
	}

	@RequestMapping(value = { "/changestep" }, method = RequestMethod.GET)
	public String onChangeStep(Locale locale, Model model, @RequestParam(value = "step") final Integer newStepNumber) {

		logger.info("discussion Edit Course : change step");

		EditCourseBean courseBean = HandyclassAppContext.getBean(EditCourseBean.class);
		courseBean.setStep(EditCourseWizardStep.parseInt(newStepNumber));
		model.addAttribute("bean", courseBean);

		return "redirect:" + EditCourseWizardUtils.getStepUrl(courseBean.getStep());
	}
	
	private ForumBean populateForumBean(Long forumRootComponent) {
		ForumBean forum = new ForumBean(forumRootComponent);
		forum.setOnChangeHandler(FORUM_CHANGED_HANDLER);
		return forum;
	}

}
