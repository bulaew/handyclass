package ru.prognoz.tc.mvc.controller.editcourse;

public enum EditCourseWizardStep {

	COMMON(1), CONTENT(2), TEST(3), PARTICIPANT(4), DISCUSSION(5);

	private int number;

	private EditCourseWizardStep(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public static EditCourseWizardStep parseInt(int code) {
		for (EditCourseWizardStep step : EditCourseWizardStep.values()) {
			if (step.getNumber() == code) {
				return step;
			}
		}
		return null;
	}

}
