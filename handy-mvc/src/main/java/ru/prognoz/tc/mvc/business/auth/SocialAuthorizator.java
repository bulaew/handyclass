package ru.prognoz.tc.mvc.business.auth;

import java.io.Serializable;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

public interface SocialAuthorizator extends Serializable {

	public String buildAuthUrl() throws IllegalArgumentException;
	
	public void validateAuthParams() throws IllegalArgumentException;
	
	public SocialNetworkProfile getSocialProfile(final String authorizationCode) throws HandyCoreException;
	
}
