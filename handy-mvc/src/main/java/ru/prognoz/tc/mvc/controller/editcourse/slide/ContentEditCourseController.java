package ru.prognoz.tc.mvc.controller.editcourse.slide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.SlideComponentDao;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.HasType;
import ru.prognoz.tc.domain.slide.Image;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.slide.Video;
import ru.prognoz.tc.mvc.business.resource.HandyResourseService;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseBean;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardStep;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardUtils;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse/content")
public class ContentEditCourseController {

    private static final Logger logger = LoggerFactory
	    .getLogger(ContentEditCourseController.class);

    @Autowired
    private EditCourseBean courseBean;

    @Autowired
    private HandyclassAppCfg appCfg;

    @Autowired
    private SlideComponentDao slideComponentDao;

    @Autowired
    private HandyObjectDao oidDao;

    @Autowired
    private SlideDao slideDao;

    @Autowired
    private HandyResourseService resourseService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String startEditing(Locale locale, Model model) {
	logger.info("content Edit Course : start editing");
	model.addAttribute("bean", courseBean);
	return "/editcourse/content/step";
    }

    @RequestMapping(value = { "/changestep" }, method = RequestMethod.GET)
    public String onChangeStep(Locale locale, Model model,
	    @RequestParam(value = "step") final Integer newStepNumber) {
	logger.info("content Edit Course : change step");
	courseBean.setStep(EditCourseWizardStep.parseInt(newStepNumber));
	model.addAttribute("bean", courseBean);
	return "redirect:"
		+ EditCourseWizardUtils.getStepUrl(courseBean.getStep());
    }

    // Работа со слайдами
    @RequestMapping(value = "/page/add", method = RequestMethod.GET)
    public String addPage() {
	Slide slide = slideDao.newDomainInstatnce();
	slide.setObjectId(oidDao.generate());
	slide.setCourseOID(courseBean.getCourse().getObjectId());
	courseBean.setActiveSlide(slide);
	if (!courseBean.getSlides().isEmpty()) {
	    Collections.sort(courseBean.getSlides(), new Comparator<Slide>() {
		@Override
		public int compare(Slide o1, Slide o2) {
		    return Integer.valueOf(o1.getPosition()).compareTo(
			    Integer.valueOf(o2.getPosition()));
		}
	    });
	    slide.setPosition(courseBean.getSlides()
		    .get(courseBean.getSlides().size() - 1).getPosition() + 1);
	} else {
	    slide.setPosition(0);
	}
	courseBean.addSlide(slide);
	return "redirect:/main/editcourse/content";
    }

    @RequestMapping(value = "/page/timer", method = RequestMethod.POST)
    @ResponseBody
    public String changePageTimer(
	    @RequestParam(value = "timer", defaultValue = "0") int timer) {
	courseBean.getActiveSlide().setTimer(timer);
	return timer + "";
    }

    @RequestMapping(value = "/page/delete/{oid}", method = RequestMethod.GET)
    public String deletePage(@PathVariable(value = "oid") Long oid) {
	for (Slide slide : courseBean.getVisibleSlides()) {
	    if (slide.getObjectId().getId() == oid) {
		slide.setArchive(true);
		int size = courseBean.getVisibleSlides().size();
		if (size == 0) {
		    courseBean.setActiveSlide(null);
		} else {
		    courseBean.setActiveSlide(courseBean.getVisibleSlides()
			    .get(0));
		}
		break;
	    }
	}
	return "redirect:/main/editcourse/content";
    }

    @RequestMapping(value = { "/page/next" }, method = RequestMethod.GET)
    public String nextPage(Locale locale, Model mod) {
	if (courseBean.getVisibleSlides() == null || courseBean.getVisibleSlides().isEmpty()) {
	    return "redirect:/main/editcourse/content";
	} else {
	    try {
		int index = courseBean.getVisibleSlides().indexOf(
			courseBean.getActiveSlide());
		int size = courseBean.getVisibleSlides().size();
		if (index+1 < size) {
		    courseBean.setActiveSlide(courseBean.getVisibleSlides().get(
			    index + 1));
		}
		return "redirect:/main/editcourse/content";
	    } catch (IndexOutOfBoundsException e) {
		return "redirect:/main/editcourse/content";
	    }
	}
    }

    @RequestMapping(value = { "/page/move" }, method = RequestMethod.POST)
    public String moveSlide(Locale locale,
	    @RequestParam(value = "pageNumber") int pageNumber,
	    @RequestParam(value = "position") int position) {
	calculatePagePosition(pageNumber, position);
	Collections.sort(courseBean.getSlides(), new Comparator<Slide>() {
	    @Override
	    public int compare(Slide o1, Slide o2) {
		return Integer.valueOf(o1.getPosition()).compareTo(
			Integer.valueOf(o2.getPosition()));
	    }
	});
	return "redirect:/main/editcourse/content";
    }

    @RequestMapping(value = { "/component/move" }, method = RequestMethod.POST)
    public String moveComponent(@RequestParam(value = "oid") long oid,
	    @RequestParam(value = "position") int position) {
	calculateComponentPosition(oid, position);
	return "redirect:/main/editcourse/content";
    }

    private void calculateComponentPosition(long id, int position) {
	LinkedList<SlideComponent> components = new LinkedList<SlideComponent>(
		courseBean.getComponentsBySlide(courseBean.getActiveSlide()));
	SlideComponent sortable = null;
	for (SlideComponent component : components) {
	    if (component.getComponentOID().getId() == id) {
		sortable = component;
		break;
	    }
	}
	if (sortable != null) {
	    components.remove(sortable);
	    components.add(position, sortable);
	}
	for (SlideComponent component : components) {
	    component.setPosition(components.indexOf(component));
	}
	Collections.sort(
		courseBean.getComponentsBySlide(courseBean.getActiveSlide()),
		new Comparator<SlideComponent>() {
		    @Override
		    public int compare(SlideComponent o1, SlideComponent o2) {
			return Integer.valueOf(o1.getPosition()).compareTo(
				Integer.valueOf(o2.getPosition()));
		    }
		});
    }

    private void calculatePagePosition(int pageNumber, int position) {
	List<Slide> slides = courseBean.getVisibleSlides();
	Slide sortable = slides.get(pageNumber - 1);
	if (sortable != null) {
	    slides.remove(sortable);
	    slides.add(position, sortable);
	}
	for (Slide s : slides) {
	    s.setPosition(slides.indexOf(s));
	}
	return;
    }

    @RequestMapping(value = { "/page/prev" }, method = RequestMethod.GET)
    public String prevPage(Locale locale, Model mod) {
	if (courseBean.getVisibleSlides() == null || courseBean.getVisibleSlides().isEmpty()) {
	    return "redirect:/main/editcourse/content";
	} else {
	    try {
		int index = courseBean.getVisibleSlides().indexOf(
			courseBean.getActiveSlide());
		int size = courseBean.getVisibleSlides().size();
		if (index - 1 >= 0 && index < size) {
		    courseBean.setActiveSlide(courseBean.getVisibleSlides().get(
			    index - 1));
		}
		return "redirect:/main/editcourse/content";
	    } catch (IndexOutOfBoundsException e) {
		return "redirect:/main/editcourse/content";
	    }
	}
    }

    @RequestMapping(value = { "/page/last" }, method = RequestMethod.GET)
    public String lastPage(Locale locale, Model mod) {
	if (courseBean.getVisibleSlides() == null || courseBean.getVisibleSlides().isEmpty()) {
	    return "redirect:/main/editcourse/content";
	} else {
	    courseBean.setActiveSlide(courseBean.getVisibleSlides().get(
		    courseBean.getVisibleSlides().size() - 1));
	    return "redirect:/main/editcourse/content";
	}
    }

    @RequestMapping(value = "/page/first", method = RequestMethod.GET)
    public String firstPage() {
	if (courseBean.getVisibleSlides() == null || courseBean.getVisibleSlides().isEmpty()) {
	    return "redirect:/main/editcourse/content";
	} else {
	    courseBean.setActiveSlide(courseBean.getVisibleSlides().get(0));
	    return "redirect:/main/editcourse/content";
	}
    }

    // Работа с компонентами
    @RequestMapping(value = "/component/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteComponent(@RequestParam(value = "oid") Long oid) {
	for (SlideComponent component : courseBean
		.getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
	    if (component.getComponentOID().getId() == oid) {
		component.setArchive(true);
		break;
	    }
	}
	return oid + "";
    }

    @RequestMapping(value = "/component/deletechild", method = RequestMethod.POST)
    @ResponseBody
    public String deleteChildComponent(@RequestParam(value = "oid") Long oid,
	    @RequestParam(value = "parent") Long parent) {
	for (ChildComponent child : courseBean.getChildComponents().get(parent)) {
	    if (child.getComponentOID().getId() == oid) {
		child.setArchive(true);
		break;
	    }
	}
	return oid + "";
    }

    @RequestMapping(value = "/component/decline", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> declineComponent(
	    @RequestParam(value = "oid") Long oid) {
	for (SlideComponent component : courseBean
		.getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
	    if (component.getComponentOID().getId() == oid) {
		component.setActive(false);
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView(
			"editcourse/slideeditor/component/"
				+ ((HasType) component).getType());
		model.addObject("component", component);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/childcomponent/decline", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> declineChildComponent(
	    @RequestParam(value = "oid") long oid,
	    @RequestParam(value = "parent") long parent) {
	for (ChildComponent component : courseBean.getChildren(parent)) {
	    if (component.getComponentOID().getId() == oid) {
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView model = new ModelAndView(
			"editcourse/slideeditor/component/standart/"
				+ ((HasType) component).getType());
		model.addObject("child", component);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/component/edit", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> editComponent(
	    @RequestParam(value = "oid") Long oid) {
	for (SlideComponent component : courseBean
		.getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
	    if (component.getComponentOID().getId() == oid) {
		component.setActive(true);
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView(
			"editcourse/slideeditor/component/"
				+ ((HasType) component).getType());
		model.addObject("component", component);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/childcomponent/edit", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> editChildComponent(
	    @RequestParam(value = "oid") Long oid,
	    @RequestParam(value = "parent") long parent) {
	for (ChildComponent child : courseBean.getChildren(parent)) {
	    if (child.getComponentOID().getId() == oid) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView(
			"editcourse/slideeditor/component/active/"
				+ ((HasType) child).getType() + "ui");
		model.addObject("component", child);
		result.setResult(model);
		return result;
	    }
	}
	return null;
    }

    @RequestMapping(value = "/component/save", method = RequestMethod.POST)
    @ResponseBody
    public String saveComponent(@RequestBody ComponentPojo pojo) {
	for (SlideComponent component : courseBean
		.getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
	    if ((component.getComponentOID().getId() + "")
		    .equals(pojo.getOid())) {
		ComponentSaver.saveComponent(pojo, component);
		return "{\"ok\":\"ok\"}";
	    }
	}
	return "{\"ok\":\"ok\"}";
    }

    @RequestMapping(value = "/component/savechild", method = RequestMethod.POST)
    @ResponseBody
    public String saveChildComponent(@RequestBody ComponentPojo pojo) {
	for (ChildComponent child : courseBean.getChildComponents().get(
		pojo.getParent())) {
	    if ((child.getComponentOID().getId() + "").equals(pojo.getOid())) {
		ComponentSaver.saveComponent(pojo, child);
		return "{\"ok\":\"ok\"}";
	    }
	}
	return "{\"ok\":\"ok\"}";
    }

    @RequestMapping(value = "/component/view/{oid}", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> getView(
	    @PathVariable(value = "oid") Long oid) {
	for (SlideComponent component : courseBean.getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
	    if (component.getComponentOID().getId() == oid) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView(
			"editcourse/slideeditor/component/"
				+ ((HasType) component).getType());
		model.addObject("component", component);
		model.addObject("bean", courseBean);
		result.setResult(model);
		return result;
	    }
	    if (courseBean.getChildren(component) != null) {
		for (ChildComponent child : courseBean.getChildren(component)) {
		    if (child.getComponentOID().getId() == oid) {
			DeferredResult<ModelAndView> result = new DeferredResult<>();
			ModelAndView model = new ModelAndView("editcourse/slideeditor/component/standart/"
					+ ((HasType) child).getType());
			model.addObject("child", child);
			result.setResult(model);
			return result;
		    }
		}
	    }
	}
	return null;
    }

    @RequestMapping(value = "/component/add", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> addComponent(
	    @RequestParam(value = "type") String type) {
	SlideComponent component = ComponentCreator.createComponent(type,
		courseBean.getActiveSlide());
	courseBean.getChildComponents().put(
		component.getComponentOID().getId(),
		new ArrayList<ChildComponent>());
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	ModelAndView model = new ModelAndView(
		"editcourse/slideeditor/component/" + type);
	model.addObject("component", component);
	result.setResult(model);
	Slide slide = courseBean.getActiveSlide();
	if (!courseBean.getComponentsBySlide(slide).isEmpty()) {
	    Collections.sort(courseBean.getComponentsBySlide(slide),
		    new Comparator<SlideComponent>() {
			@Override
			public int compare(SlideComponent o1, SlideComponent o2) {
			    return Integer.valueOf(o1.getPosition()).compareTo(
				    Integer.valueOf(o2.getPosition()));
			}
		    });
	    component.setPosition(courseBean.getComponentsBySlide(slide)
		    .get(courseBean.getComponentsBySlide(slide).size() - 1)
		    .getPosition() + 1);
	} else {
	    component.setPosition(0);
	}
	courseBean.addComponentToSlide(courseBean.getActiveSlide(), component);
	return result;
    }

    @RequestMapping(value = "/component/add/marker", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> addMarker(
	    @RequestBody ComponentPojo pojo) {
	ChildComponent component = ComponentCreator.createChildComponent(pojo,
		courseBean.getActiveSlide());
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	ModelAndView model = new ModelAndView(
		"editcourse/slideeditor/component/standart/marker");
	courseBean.getChildComponents().get(component.getParentOid().getId())
		.add(component);
	model.addObject("child", component);
	result.setResult(model);
	courseBean.addComponentToSlide(courseBean.getActiveSlide(), component);
	return result;
    }

    @RequestMapping(value = "/component/add/pointer", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ModelAndView> addPointer(
	    @RequestBody ComponentPojo pojo) {
	ChildComponent component = ComponentCreator.createChildComponent(pojo,
		courseBean.getActiveSlide());
	DeferredResult<ModelAndView> result = new DeferredResult<>();
	ModelAndView model = new ModelAndView(
		"editcourse/slideeditor/component/standart/pointer");
	courseBean.getChildComponents().get(component.getParentOid().getId())
		.add(component);
	model.addObject("child", component);
	result.setResult(model);
	courseBean.addComponentToSlide(courseBean.getActiveSlide(), component);
	return result;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@RequestParam MultipartFile file,
	    @RequestParam Long oid) {

	try {
	    String fileUrl = resourseService.storeCoursePrivateFile(
		    courseBean.getCourse(), file);
	    for (SlideComponent component : courseBean
		    .getVisibleComponentsBySlide(courseBean.getActiveSlide())) {
		if (component.getComponentOID().getId() == oid) {
		    if (((HasType) component).getType().equals("video")) {
			((Video) component).setVideoUrl(fileUrl);
		    } else if (((HasType) component).getType().equals("image")) {
			((Image) component).setUrl(fileUrl);
		    }
		    break;
		}
	    }
	} catch (HandyCoreException e1) {
	    return "redirect:/main/editcourse/content";
	}

	return "redirect:/main/editcourse/content";
    }

    @RequestMapping(value = "/page/change/{oid}", method = RequestMethod.GET)
    public String changePage(@PathVariable(value = "oid") Long oid) {
	for (Slide slide : courseBean.getSlides()) {
	    if (slide.getObjectId().getId() == oid) {
		courseBean.setActiveSlide(slide);
		break;
	    }
	}
	return "redirect:/main/editcourse/content";
    }
}
