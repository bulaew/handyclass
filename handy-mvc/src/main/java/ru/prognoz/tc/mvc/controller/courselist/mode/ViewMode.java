package ru.prognoz.tc.mvc.controller.courselist.mode;

public enum ViewMode {

	PORTRAIT(1), LANDSCAPE(2);

	private int type;

	private ViewMode(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		switch (type) {
		case 1:
			return "PORTRAIT";
		case 2:
			return "LANDSCAPE";
		default:
			return super.toString();
		}
	}

	public int getIntValue() {
		return type;

	}

	public static ViewMode parseInt(int code) {
		for (ViewMode mode : ViewMode.values()) {
			if (mode.getIntValue() == code) {
				return mode;
			}
		}
		return null;
	}

	public static ViewMode parseStr(String str) {
		for (ViewMode mode : ViewMode.values()) {
			if (mode.toString().equals(str)) {
				return mode;
			}
		}
		return null;
	}
	
}
