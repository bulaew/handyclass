package ru.prognoz.tc.mvc.controller.auth.verify;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.auth.AuthService;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;
import ru.prognoz.tc.mvc.controller.auth.login.LoginPageBean;

@Controller
public class AccountVerificationController {

	private static final Logger logger = LoggerFactory.getLogger(AccountVerificationController.class);
	@Autowired
	private AuthService authService;

	@RequestMapping(value = { "/auth/verify" }, method = RequestMethod.GET)
	public ModelAndView accountVerification(Locale locale, @RequestParam(value = "verificationCode") String verificationCode,
			final RedirectAttributes redirectAttributes) {

		logger.info("AccountVerificationController : accountVerification");
		User user = authService.accountVerification(verificationCode);

		LoginPageBean bean = new LoginPageBean();
		bean.setShowLoginDlg(true);
		ModelAndView mav = new ModelAndView("redirect:/login");
		redirectAttributes.addFlashAttribute("LoginBean", bean);

		if (user != null && user.isVerified() == true) {
			bean.setEmail(user.getLogin());
		} else {
			bean.setSignupValidationMsg(new ViewMessage("error", "", "Ошибка верификации пользователя"));
		}
		return mav;
	}
}
