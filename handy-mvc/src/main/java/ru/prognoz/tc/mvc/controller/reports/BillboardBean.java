package ru.prognoz.tc.mvc.controller.reports;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.mvc.controller.reports.general.GeneralDataBean;
import ru.prognoz.tc.mvc.controller.reports.propensity.MyPropensityBean;

public class BillboardBean {

	private GeneralDataBean generalDataBean;
	private List<ReportItem> authorActivityItems = new ArrayList<ReportItem>();
	private List<ReportItem> allCourseRatingItems = new ArrayList<>();
	private List<ReportItem> actualCategoryItems = new ArrayList<>();
	private List<ReportItem> myCourseRatingItems = new ArrayList<>();
	private List<ReportItem> myCourseLearningItems = new ArrayList<>();
	private List<ReportItem> myEducationItems = new ArrayList<>();
	private List<ReportItem> myAchievementsItems = new ArrayList<>();
	private MyPropensityBean myPropensityBean = new MyPropensityBean();
	private ReportViewModel viewModel = new ReportViewModel();

	public GeneralDataBean getGeneralDataBean() {
		return generalDataBean;
	}

	public void setGeneralDataBean(GeneralDataBean generalDataBean) {
		this.generalDataBean = generalDataBean;
	}

	public List<ReportItem> getAuthorActivityItems() {
		return authorActivityItems;
	}

	public void setAuthorActivityItems(List<ReportItem> authorActivityItems) {
		this.authorActivityItems = authorActivityItems;
	}

	public List<ReportItem> getAllCourseRatingItems() {
		return allCourseRatingItems;
	}

	public List<ReportItem> getActualCategoryItems() {
		return actualCategoryItems;
	}

	public void setActualCategoryItems(List<ReportItem> actualCategoryItems) {
		this.actualCategoryItems = actualCategoryItems;
	}

	public void setAllCourseRatingItems(List<ReportItem> allCourseRatingItems) {
		this.allCourseRatingItems = allCourseRatingItems;
	}

	public List<ReportItem> getMyCourseRatingItems() {
		return myCourseRatingItems;
	}

	public void setMyCourseRatingItems(List<ReportItem> myCourseRatingItems) {
		this.myCourseRatingItems = myCourseRatingItems;
	}

	public List<ReportItem> getMyCourseLearningItems() {
		return myCourseLearningItems;
	}

	public void setMyCourseLearningItems(List<ReportItem> myCourseLearningItems) {
		this.myCourseLearningItems = myCourseLearningItems;
	}
	
	public List<ReportItem> getMyEducationItems() {
		return myEducationItems;
	}

	public void setMyEducationItems(List<ReportItem> myEducationItems) {
		this.myEducationItems = myEducationItems;
	}
	
	public List<ReportItem> getMyAchievementsItems() {
		return myAchievementsItems;
	}

	public void setMyAchievementsItems(List<ReportItem> myAchievementsItems) {
		this.myAchievementsItems = myAchievementsItems;
	}

	public MyPropensityBean getMyPropensityBean() {
		return myPropensityBean;
	}

	public void setMyPropensityBean(MyPropensityBean myPropensityBean) {
		this.myPropensityBean = myPropensityBean;
	}

	public ReportViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(ReportViewModel viewModel) {
		this.viewModel = viewModel;
	}

}
