package ru.prognoz.tc.mvc.controller.editcourse.test;

import java.util.List;

public class QuestionPojo {
	private Long oid;
	private String question;
	private List<AnswerPojo> answers;

	public long getOid() {
		return oid;
	}

	public void setOid(long oid) {
		this.oid = oid;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<AnswerPojo> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerPojo> answers) {
		this.answers = answers;
	}

}
