package ru.prognoz.tc.mvc.controller.courselist.mode;

import java.util.Comparator;

import ru.prognoz.tc.mvc.controller.courselist.CourseItem;
import ru.prognoz.tc.mvc.controller.courselist.comparator.DateComparator;
import ru.prognoz.tc.mvc.controller.courselist.comparator.PopularityComparator;
import ru.prognoz.tc.mvc.controller.courselist.comparator.RatingComparator;

public enum SortMode {

	POPULARITY, RATING, DATE;
	
	public Comparator<CourseItem> getComparator() {
		switch (this) {
		case POPULARITY:
			return new PopularityComparator();
		case RATING:
			return new RatingComparator();
		case DATE:
			return new DateComparator();
		default:
			return null;
		}
	}
	
}
