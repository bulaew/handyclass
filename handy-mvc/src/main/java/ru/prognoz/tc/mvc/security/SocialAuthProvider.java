package ru.prognoz.tc.mvc.security;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;

public class SocialAuthProvider implements AuthenticationProvider {

	@Autowired
	private UserDao userDao;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		SocialAuthenticationToken token = (SocialAuthenticationToken) authentication;
		if (token == null) {
			throw new AuthenticationCredentialsNotFoundException("Bad authentication token");
		}
		if (token.getAccountId() == null || StringUtils.isEmpty(token.getAccountId())) {
			throw new BadCredentialsException("Bad authentication token");
		}

		User retrivedUser = null;
		switch (token.getNetwork()) {
		case GOOGLE:
			retrivedUser = userDao.getByGoogleAccount(token.getAccountId());
			break;
		case FACEBOOK:
			retrivedUser = userDao.getByFbAccount(token.getAccountId());
			break;
		case TWITTER:
			retrivedUser = userDao.getByTwitterAccount(token.getAccountId());
			break;
		case VKONTAKTE:
			retrivedUser = userDao.getByVkAccount(token.getAccountId());
			break;
		default:
			break;
		}
		if (retrivedUser == null) {
			throw new UsernameNotFoundException("");
		}
		if (!retrivedUser.isVerified()) {
			throw new BadCredentialsException("Account is not verified!");
		}

		return new SocialAuthenticationToken(token.getNetwork(), token.getAccountId(), retrivedUser);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (SocialAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
