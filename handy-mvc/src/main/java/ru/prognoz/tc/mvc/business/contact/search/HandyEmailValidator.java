package ru.prognoz.tc.mvc.business.contact.search;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class HandyEmailValidator {

	private static final String SPECIAL_CHARS = "\\p{Cntrl}\\(\\)<>@,;:'\\\\\\\"\\.\\[\\]";
	private static final String VALID_CHARS = "[^\\s" + SPECIAL_CHARS + "]";
	private static final String QUOTED_USER = "(\"[^\"]*\")";
	private static final String WORD = "((" + VALID_CHARS + "|')+|" + QUOTED_USER + ")";

	private static final String LEGAL_ASCII_REGEX = "^\\p{ASCII}+$";
	private static final String EMAIL_REGEX = "^\\s*?(.+)@(.+?)\\s*$";
	private static final String USER_REGEX = "^\\s*" + WORD + "(\\." + WORD + ")*$";

	private static final Pattern MATCH_ASCII_PATTERN = Pattern.compile(LEGAL_ASCII_REGEX);
	private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);
	private static final Pattern USER_PATTERN = Pattern.compile(USER_REGEX);

	private static final HandyEmailValidator EMAIL_VALIDATOR = new HandyEmailValidator();

	protected HandyEmailValidator() {
	}

	/**
	 * Returns the Singleton instance of this validator.
	 * 
	 * @return singleton instance of this validator.
	 */
	public static HandyEmailValidator getInstance() {
		return EMAIL_VALIDATOR;
	}

	public static String getUserName(String email) {

		if (StringUtils.isEmpty(email)) {
			return null;
		}

		// Емейл содержит невалидные символы.
		Matcher asciiMatcher = MATCH_ASCII_PATTERN.matcher(email);
		if (!asciiMatcher.matches()) {
			return null;
		}

		// Емейл полностью соответствует паттерну
		Matcher emailMatcher = EMAIL_PATTERN.matcher(email);
		if (emailMatcher.matches()) {
			email = emailMatcher.group(1); // возвращаем первую часть
		}
		
		if (email.contains("@")) {
			email = email.split("@")[0];
		}

		// Емейл соответствует патерну пользователя.
		Matcher userMatcher = USER_PATTERN.matcher(email);
		if (userMatcher.matches()) {
			return email;
		}

		return null;
	}
	
}
