package ru.prognoz.tc.mvc.controller.editcourse.slide;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import ru.prognoz.tc.domain.slide.Graffiti;
import ru.prognoz.tc.domain.slide.HasType;
import ru.prognoz.tc.domain.slide.Header;
import ru.prognoz.tc.domain.slide.Image;
import ru.prognoz.tc.domain.slide.Marker;
import ru.prognoz.tc.domain.slide.Pointer;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.slide.Text;
import ru.prognoz.tc.domain.slide.Video;
import ru.prognoz.tc.mvc.common.HtmlCleaner;

public class ComponentSaver {
    public static SlideComponent saveComponent(ComponentPojo pojo,
	    SlideComponent component) {
	String type = "";
	if (component instanceof HasType) {
	    type = ((HasType) component).getType();
	}
	if (StringUtils.isEmpty(type)) {
	    return null;
	} else if (type.equals("header")) {
	    return saveHeader((Header) component, pojo);
	} else if (type.equals("text")) {
	    return saveText((Text) component, pojo);
	} else if (type.equals("video")) {
	    return saveVideo((Video) component, pojo);
	} else if (type.equals("image")) {
	    return saveImage((Image) component, pojo);
	} else if (type.equals("graffiti")) {
	    return saveGraffiti((Graffiti) component, pojo);
	} else if (type.equals("pointer")) {
	    return savePointer((Pointer) component, pojo);
	} else if (type.equals("marker")) {
	    return saveMarker((Marker) component, pojo);
	} else {
	    return null;
	}
    }

    private static SlideComponent savePointer(Pointer component, ComponentPojo pojo) {
	component.setX(pojo.getX());
	component.setY(pojo.getY());
	component.setPointerType(pojo.getPointerType());
	return component;
    }

    private static SlideComponent saveMarker(Marker component,
	    ComponentPojo pojo) {
	component.setX(pojo.getX());
	component.setY(pojo.getY());
	component.setWidth(pojo.getWidth());
	component.setHeight(pojo.getHeight());
	return component;
    }

    private static SlideComponent saveGraffiti(Graffiti component,
	    ComponentPojo pojo) {
	component.setActive(false);
	component.setContent(pojo.getContent());
	return null;
    }

    private static SlideComponent saveImage(Image component, ComponentPojo pojo) {
	if (!StringUtils.isEmpty(pojo.getUrl())) {
	    component.setUrl(pojo.getUrl());
	}
	if (!StringUtils.isEmpty(pojo.getUrlType())) {
	    component.setUrlType(pojo.getUrlType());
	}
	component.setActive(false);
	component.setDescription(HtmlCleaner.cleanAll(pojo.getDescription()));
	component.setRotation(pojo.getRotation());
	component.setSize(pojo.getWidth());
	return component;
    }

    private static SlideComponent saveVideo(Video component, ComponentPojo pojo) {
	if (StringUtils.isEmpty(pojo.getUrl())) {
	    return component;
	}
	if (pojo.getUrl().contains("youtube.com")) {
	    component.setService("youtube");
	    String id = parseId(pojo.getUrl());
	    if (id != null) {
		component.setVideoUrl("https://www.youtube.com/embed/" + id + "?rel=0&showinfo=0&theme=light&wmode=transparent");
	    }
	} else if (pojo.getUrl().contains("vimeo.com")) {
	    component.setService("vimeo");
	    String id = parseId(pojo.getUrl());
	    if (id != null) {
		component.setVideoUrl("//player.vimeo.com/video/" + id);
	    }
	} else {
	    component.setVideoUrl(pojo.getUrl());
	}
	component.setActive(false);
	return component;
    }

    public static String parseId(String url) {
	String pattern = null;
	if (url.contains("youtube.com")) {
	    pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
	} else if (url.contains("vimeo.com")) {
	    return url.replace("http://vimeo.com/", "");
	}
	if (pattern == null) {
	    return null;
	}
	Pattern compiledPattern = Pattern.compile(pattern);
	Matcher matcher = compiledPattern.matcher(url);
	if (matcher.find()) {
	    matcher.end();
	    return matcher.group();
	}
	return null;
    }

    private static SlideComponent saveText(Text component, ComponentPojo pojo) {
	component.setActive(false);
	component.setTextContent(HtmlCleaner.cleanWysiwyg(pojo.getContent()));
	return component;
    }

    private static SlideComponent saveHeader(Header component,
	    ComponentPojo pojo) {
	component.setActive(false);
	component.setHeaderContent(HtmlCleaner.cleanAll(pojo.getContent()));
	return component;
    }
}
