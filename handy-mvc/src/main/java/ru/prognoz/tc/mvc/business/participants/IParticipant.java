package ru.prognoz.tc.mvc.business.participants;

import ru.prognoz.tc.domain.User;

public interface IParticipant extends Comparable<IParticipant> {

	/**
	 * 0 - undefined, 1 - teacher, 2 - invited, 3 - registered.
	 * 
	 * @return
	 */
	public int getType();

	public User getUser();

	public void setUser(User user);

	public String getGuestEmail();

	public void setGuestEmail(String guestEmail);

	public boolean isRemoved();

	public void setRemoved(boolean removed);

}
