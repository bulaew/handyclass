package ru.prognoz.tc.mvc.controller.contacts;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import ru.prognoz.tc.dao.contact.ContactDao;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;

@Controller
public class AddressBookController {

	private static final Logger logger = LoggerFactory.getLogger(AddressBookController.class);
	@Autowired
	private ContactListInfoDao contactListInfoDao;
	@Autowired
	private ContactDao contactDao;
	@Autowired
	private ContactListContentDao contactListContentDao; 
	@Autowired
	private MessageSource messageSource;
	

	@RequestMapping(value = { "/addressbook/getInitJSON" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> getInitJson(Locale locale) {

		logger.info(" Addressbook: build init json");

		/*
		 * Построение данных для инициализации диалога.
		 */
		StringBuilder initJSON = new StringBuilder();
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user != null) {
			List<ContactListInfo> contactLists = contactListInfoDao.list(user);
			initJSON.append("[ ");
			for (int i = 0; i < contactLists.size(); i++) {
				initJSON.append(initContactList(locale, contactLists.get(i)));
				if (i != contactLists.size() - 1) {
					initJSON.append(", ");
				}
			}
			// initJSON.append(initContactList(request, null)); // загружаем все контакты.
			initJSON.append(" ]");
		}

		DeferredResult<String> result = new DeferredResult<String>();
		//result.setResult(initJSON.toString() + ", " + initContactList(locale, null) );
		/*result.setResult("{" +
				" 'currentListData' : '" + initJSON.toString()+"',"+
				" 'totalData' : '"+ initContactList(locale, null) + "'"+
				"}");*/
		result.setResult("{" +
				" \"currentListData\" : "+ "\""+initJSON.toString()+"\", "+
				" \"totalData\" : "+ "\""+initContactList(locale, null)+"\", "+
				" \"i18nData\" : "+ "{" +
						" \"__registered\" : "+ "\""+messageSource.getMessage("AddressBookDlg.registered", null, locale)+"\", "+
						" \"__user1\" : "+ "\""+messageSource.getMessage("AddressBookDlg.user.1", null, locale)+"\", "+
						" \"__user2\" : "+ "\""+messageSource.getMessage("AddressBookDlg.user.2", null, locale)+"\", "+
						" \"__user3\" : "+ "\""+messageSource.getMessage("AddressBookDlg.user.3", null, locale)+"\", "+
						" \"__notRegistered\" : "+ "\""+messageSource.getMessage("AddressBookDlg.notRegistered", null, locale)+"\" "+
						"}" +
				"}");
		return result;
	}
	
	/*
	 * Построение списка контактов.
	 */
	private String initContactList(Locale locale, ContactListInfo contactList) {
		StringBuilder initJSON = new StringBuilder();
		String all = messageSource.getMessage("AddressBookDlg.all", null, locale);
		String selected = messageSource.getMessage("AddressBookDlg.registered", null, locale);
		String notSelected = messageSource.getMessage("AddressBookDlg.notRegistered", null, locale);
		String user1 = messageSource.getMessage("AddressBookDlg.user.1", null, locale);
		String user2 = messageSource.getMessage("AddressBookDlg.user.2", null, locale);
		String user3 = messageSource.getMessage("AddressBookDlg.user.3", null, locale);
		initJSON.append(" [ ");
		// ОИД списка.
		if (contactList == null) {
			initJSON.append("'totallist'");
		} else {
			initJSON.append("" + contactList.getObjectId().getId());
		}
		initJSON.append(", ");
		// название списка.
		if (contactList == null) {
			initJSON.append("'" + all + "'");
		} else {
			initJSON.append("'" + contactList.getTitle() + "'");
		}
		initJSON.append(", ");
		// контакты списка.
		initJSON.append(initContacts(locale, contactList));
		initJSON.append(" ]");
		return initJSON.toString();
	}
	
	/*
	 * Построение всех контактов из списка контактов.
	 */
	private String initContacts(Locale locale, ContactListInfo contactList) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<Contact> contacts = null;
		if (contactList == null) {
			contacts = contactDao.getByOwner(user);
		} else {
			contacts = contactListContentDao.members(contactList);
		}
		if (contacts.isEmpty()) {
			return "''";
		}
		StringBuilder initJSON = new StringBuilder();
		initJSON.append(" [ ");
		for (int i = 0; i < contacts.size(); i++) {
			initJSON.append(initContact(contacts.get(i)));
			if (i < contacts.size() - 1) {
				initJSON.append(",");
			}
		}
		initJSON.append(" ]");
		return initJSON.toString();
	}

	/*
	 * Построение контакта.
	 */
	private String initContact(Contact contact) {
		StringBuilder initJSON = new StringBuilder();
		initJSON.append(" [ ");
		initJSON.append("" + contact.getObjectId().getId() + "");
		initJSON.append(", ");
		if (contact.getUser() != null) {
			if (StringUtils.isEmpty(contact.getUser().getAvatarURL())) {
				initJSON.append("'" + "img/avatar/avatar_big.png" + "'");
			} else {
				initJSON.append("'" + contact.getUser().getAvatarURL() + "'");
			}
			initJSON.append(", ");
			initJSON.append("'" + contact.getUser().getUsername() + "'");
			initJSON.append(", ");
			initJSON.append("'" + contact.getUser().getLogin() + "'");
			initJSON.append(", ");
			initJSON.append("true");
		} else {
			initJSON.append("'" + "img/course/avatar_mail_small.png" + "'");
			initJSON.append(", ");
			initJSON.append("''");
			initJSON.append(", ");
			initJSON.append("'" + contact.getEmail() + "'");
			initJSON.append(", ");
			initJSON.append("false");
		}
		initJSON.append(" ]");
		return initJSON.toString();
	}

}
