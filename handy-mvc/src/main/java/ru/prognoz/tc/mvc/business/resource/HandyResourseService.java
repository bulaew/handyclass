package ru.prognoz.tc.mvc.business.resource;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.mvc.controller.editcourse.CommonEditCourseController;

@Service
public class HandyResourseService {

	public static final String PUBLIC_COURSES_DIR = "public_courses";
	public static final String PRIVATE_COURSES_DIR = "private_courses";
	public static final String PUBLIC_USERS_DIR = "public_users";
	public static final String PRIVATE_USERS_DIR = "private_users";

	private static final Logger logger = LoggerFactory.getLogger(CommonEditCourseController.class);

	@Autowired
	private HandyclassAppCfg appCfg;

	/**
	 * Сохранение на файловую сиситему сервера файла с аватаркой пользователя.
	 * 
	 * @param user
	 *            Пользователь, для которого сохраняется аватар.
	 * @param avatar
	 *            файл
	 * @return УРЛ сохраненной аватарки
	 * @throws HandyCoreException
	 *             В случае ошибки создания файла на файловой систем, либо ошибки записи файла на файловую систему.
	 */
	public String storeUserAvatar(User user, MultipartFile avatar) throws HandyCoreException {
		String orgFileName = avatar.getOriginalFilename();
		String storeFilePath = appCfg.getPublicResourseDirectory() + "/" + PUBLIC_USERS_DIR + "/" + user.getObjectId().getId() + "/"
				+ orgFileName;
		File storeFile = new File(storeFilePath);
		File parentFile = storeFile.getParentFile();
		if (!parentFile.exists() && !parentFile.mkdirs()) {
			logger.info("File uploaded failed:" + orgFileName);
			throw new HandyCoreException("File uploading failed:" + orgFileName);
		}

		try {
			avatar.transferTo(storeFile);
			logger.info("File uploaded:" + orgFileName);
		} catch (Exception e) {
			logger.info("File uploaded failed:" + orgFileName);
			throw new HandyCoreException("File uploading failed:" + orgFileName);
		}
		return buildUserAvatarPath(user, orgFileName);
	}

	public String storeCourseAvatar(Course course, MultipartFile avatar) throws HandyCoreException {
		String orgFileName = avatar.getOriginalFilename();
		String storeFilePath = appCfg.getPublicResourseDirectory() + "/" + PUBLIC_COURSES_DIR + "/" + course.getObjectId().getId() + "/"
				+ orgFileName;
		File storeFile = new File(storeFilePath);
		File parentFile = storeFile.getParentFile();
		if (!parentFile.exists() && !parentFile.mkdirs()) {
			logger.info("File uploaded failed:" + orgFileName);
			return buildDefaultCourseAvatarPath(course);
		}

		try {
			avatar.transferTo(storeFile);
			logger.info("File uploaded:" + orgFileName);
		} catch (Exception e) {
			logger.info("File uploaded failed:" + orgFileName);
			return buildDefaultCourseAvatarPath(course);
		}
		return buildCourseAvatarPath(course, orgFileName);
	}

	public String storeCoursePrivateFile(Course course, MultipartFile file) throws HandyCoreException {
		String orgFileName = file.getOriginalFilename();
		String storeFilePath = appCfg.getPublicResourseDirectory() + "/" + PRIVATE_COURSES_DIR + "/" + course.getObjectId().getId() + "/"
				+ orgFileName;
		File storeFile = new File(storeFilePath);
		File parentFile = storeFile.getParentFile();
		if (!parentFile.exists() && !parentFile.mkdirs()) {
			logger.info("File uploading failed:" + orgFileName);
			throw new HandyCoreException("File uploading failed");
		}
		try {
			file.transferTo(storeFile);
			logger.info("File uploaded:" + orgFileName);
		} catch (Exception e) {
			logger.info("File uploading failed:" + orgFileName);
			throw new HandyCoreException("File uploading failed");
		}
		return buildCoursePrivatePath(course, orgFileName);
	}

	public String getDefaultUserAvatarPath() {
		return "/img/course/avatar_big.png";
	}

	private String buildUserAvatarPath(User user, String orgFileName) {
		return "/publicdir/" + PUBLIC_USERS_DIR + "/" + user.getObjectId().getId() + "/" + orgFileName;
	}

	private String buildDefaultCourseAvatarPath(Course course) {
		return "/img/course/default/" + course.getCategory().getAvatarUrl() + ".png";
	}

	private String buildCourseAvatarPath(Course course, String orgFileName) {
		return "/publicdir/" + PUBLIC_COURSES_DIR + "/" + course.getObjectId().getId() + "/" + orgFileName;
	}

	private String buildCoursePrivatePath(Course course, String orgFileName) {
		return "/publicdir/" + PRIVATE_COURSES_DIR + "/" + course.getObjectId().getId() + "/" + orgFileName;
	}

}
