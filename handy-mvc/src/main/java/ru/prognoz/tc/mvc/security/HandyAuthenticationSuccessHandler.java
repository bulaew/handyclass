package ru.prognoz.tc.mvc.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import ru.prognoz.tc.mvc.controller.coursereg.RegistrationAfterInviteData;

public class HandyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		RegistrationAfterInviteData data = (RegistrationAfterInviteData)request.getSession().getAttribute(RegistrationAfterInviteData.ATTR_NAME);
		request.getSession().removeAttribute(RegistrationAfterInviteData.ATTR_NAME);
		if (obj instanceof UserDetails && data != null) {
			response.sendRedirect(request.getContextPath()+"/main/coursereg/index?coid="+data.getCourseOID());
            return;
		} else {
			response.sendRedirect(request.getContextPath()+"/main");
            return;
		}
		
	}

}
