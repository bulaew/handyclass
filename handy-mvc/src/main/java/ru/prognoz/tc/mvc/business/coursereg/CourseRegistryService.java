package ru.prognoz.tc.mvc.business.coursereg;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseRegistrationDao;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.dao.attempt.SlideAttemptDao;
import ru.prognoz.tc.dao.attempt.TestAttemptDao;
import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;

@Service
public class CourseRegistryService {

	private static final Logger logger = LoggerFactory.getLogger(CourseRegistryService.class);
	@Autowired
	private CourseAccessService courseAccessService;
	@Autowired
	private CourseRegistrationDao courseRegistrationDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private InviteDao inviteDao;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private CourseAttemptDao courseAttemptDao;
	@Autowired
	private SlideAttemptDao slideAttemptDao;
	@Autowired
	private TestAttemptDao testAttemptDao;

	public boolean registration(Course course, User student, Locale locale) throws HandyCoreException {
		if (course == null || student == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		try {
			if (courseAccessService.canRegistrate(course, student, locale)) {
				boolean isReg = courseRegistrationDao.isRegistered(course, student);
				if (!isReg) { // Пользователь еще не зарегистрирован.
					/*
					 * Регистрируем пользователя на курс.
					 */
					courseAttemptDao.getOrCreate(course, student); // создаем попытку по курсу

					CourseRegistration registration = courseRegistrationDao.newDomainInstance();
					registration.setCourseOid(course.getObjectId());
					registration.setUser(student);
					registration.setDate(new Date());
					courseRegistrationDao.save(registration); // регистрируем
					inviteDao.removeInvite(course, student);

					notificationService.fireAcceptCourseRegistration(student, course); // рассылаем нотификацию о регистрации.
					return true;
				}
			}
		} catch (HandyCoreException e) {
		}
		return false;
	}

	public boolean unregistration(Course course, User user, Locale locale) throws HandyCoreException {
		if (course == null || user == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		// Удаляем попытку пользователя по курсу.
		CourseAttempt courseAttempt = courseAttemptDao.get(course, user);
		if (courseAttempt == null) {
			return true;
		}
		
		// Удаляем попытки пользователя по слайдам.
		List<SlideAttempt> listSlideAttempt = slideAttemptDao.list(courseAttempt);
		for (SlideAttempt slideAttempt : listSlideAttempt) {
			slideAttemptDao.delete(slideAttempt);
		}
		// Удаляем попытку пользователя по тесту.
		TestAttempt testAttempt = testAttemptDao.get(course.getTestOid(), user);
		if (testAttempt != null) {
			testAttemptDao.delete(testAttempt);
		}
		courseAttemptDao.delete(courseAttempt);
		
		/*
		 * Удаляем регистрирацию пользователя на курс.
		 */
		CourseRegistration registry = courseRegistrationDao.getRegistry(course, user);
		if (registry != null) {
			courseRegistrationDao.delete(registry);
		}

		notificationService.fireCourseUnRegistration(user, course); // рассылаем нотификацию об отчислении.

		logger.info(" CourseRegistryService : unregistration ");
		return false;
	}
}
