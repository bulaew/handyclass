package ru.prognoz.tc.mvc.controller.editcourse.participant;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.contact.ContactDao;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.mvc.business.participants.ParticipantService;
import ru.prognoz.tc.mvc.business.participants.SearchParticipantResultBean;
import ru.prognoz.tc.mvc.controller.contacts.AddressbookRequestBody;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseBean;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardStep;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseWizardUtils;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse/participant")
public class ParticipantEditCourseController {

	private static final Logger logger = LoggerFactory.getLogger(ParticipantEditCourseController.class);

	@Autowired
	private EditCourseBean courseBean;
	@Autowired
	private ParticipantService participantService;
	@Autowired
	private ContactListInfoDao contactListInfoDao;
	@Autowired
	private ContactDao contactDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String startEditing(Locale locale, Model model) {
		logger.info("Participant Edit Course : start editing");

		return "/editcourse/participant/step";
	}

	@RequestMapping(value = { "/changestep" }, method = RequestMethod.GET)
	public String onChangeStep(Locale locale, Model model, @RequestParam(value = "step") final Integer newStepNumber) {

		logger.info("Participant Edit Course : change step");

		courseBean.setStep(EditCourseWizardStep.parseInt(newStepNumber));

		return "redirect:" + EditCourseWizardUtils.getStepUrl(courseBean.getStep());
	}

	@RequestMapping(value = { "/accesstype" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> onChangeStep(@RequestParam(value = "accesstype") final Integer accesstype) {

		logger.info("Participant Edit Course : set access type");

		courseBean.getCourse().setAccessType(CourseAccessType.parseInt(accesstype));

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/searchParticipant" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> searchParticipant(Locale locale, @RequestParam(value = "contactName") final String searchCriteria) {

		logger.info("Participant Edit Course : searchParticipant");
		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/addParticipantPanel");
		SearchParticipantResultBean searchResult = participantService.search(searchCriteria, courseBean.getParticipants(), locale);
		mav.addObject("searchParticipantResult", searchResult);
		result.setResult(mav);

		return result;
	}

	@RequestMapping(value = { "/inviteUser" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> inviteUser(Locale locale, @RequestParam(value = "userOid") final long userOid) {

		logger.info("Participant Edit Course : inviteUser");

		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/addParticipantPanel");
		SearchParticipantResultBean inviteResult = null;
		try {
			inviteResult = participantService.inviteParticipant(userOid, courseBean.getParticipants(), locale);
		} catch (HandyCoreException e) {
			e.printStackTrace();
		}
		mav.addObject("inviteParticipantResult", inviteResult);
		result.setResult(mav);

		return result;
	}

	@RequestMapping(value = { "/view" }, method = RequestMethod.GET)
	public String inviteUser(Locale locale, @RequestParam(value = "type") final String type) {
		logger.info("Participant Edit Course : change view");
		if (type.equals("list")) {
			courseBean.setParticipantView(true);
		} else if (type.equals("big")) {
			courseBean.setParticipantView(false);
		} else {
			courseBean.setParticipantView(false);
		}
		return "redirect:/main/editcourse/participant";
	}

	@RequestMapping(value = { "/inviteEmail" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> inviteEmail(Locale locale, @RequestParam(value = "email") final String email) {

		logger.info("Participant Edit Course : inviteEmail");

		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/addParticipantPanel");
		SearchParticipantResultBean inviteResult = null;
		try {
			inviteResult = participantService.inviteParticipant(email, courseBean.getParticipants(), locale);
		} catch (HandyCoreException e) {
			e.printStackTrace();
		}
		mav.addObject("inviteParticipantResult", inviteResult);
		result.setResult(mav);

		return result;
	}

	@RequestMapping(value = { "/inviteList" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> inviteList(Locale locale, @RequestParam(value = "listOid") final long listOid) {

		logger.info("Participant Edit Course : inviteList");

		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/addParticipantPanel");
		ContactListInfo listInfo = contactListInfoDao.getByOid(listOid);
		SearchParticipantResultBean inviteResult = null;
		try {
			inviteResult = participantService.inviteParticipant(listInfo, courseBean.getParticipants(), locale);
		} catch (HandyCoreException e) {
			e.printStackTrace();
		}
		mav.addObject("inviteParticipantResult", inviteResult);
		result.setResult(mav);

		return result;
	}

	@RequestMapping(value = { "/inviteAddressbook" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> inviteList(Locale locale, @RequestBody AddressbookRequestBody rb) {

		logger.info("Participant Edit Course : invite by addressbook");

		// prepare response
		DeferredResult<String> result = new DeferredResult<String>();

		try {
			for (Map.Entry<Integer, Long> entry : rb.getContacts().entrySet()) {
				Contact contact = contactDao.getByOID(entry.getValue());
				participantService.inviteParticipant(contact, courseBean.getParticipants(), locale);
			}
		} catch (HandyCoreException e) {

		}
		result.setResult("{\"ok\" : \"ok\" }");

		return result;
	}

	@RequestMapping(value = { "/removeParticipant" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> removeParticipant(Locale locale,
			@RequestParam(value = "userOid", required = false) final Long userOid,
			@RequestParam(value = "email", required = false) final String email) {

		logger.info("Participant Edit Course : removeParticipant");

		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/participantListPanel");
		result.setResult(mav);

		try {
			participantService.removeParticipant(userOid, email, courseBean.getParticipants(), locale);
		} catch (HandyCoreException e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = { "/renderListPanel" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> renderListPanel(Locale locale) {

		logger.info("Participant Edit Course : searchParticipant");
		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/participantListPanel");
		result.setResult(mav);

		return result;
	}

	@RequestMapping(value = { "/renderAddParticipantPanel" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> renderAddParticipantPanel(Locale locale) {

		logger.info("Participant Edit Course : render add participant panel");
		// prepare response
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/editcourse/participant/addParticipantPanel");
		result.setResult(mav);

		return result;
	}

}
