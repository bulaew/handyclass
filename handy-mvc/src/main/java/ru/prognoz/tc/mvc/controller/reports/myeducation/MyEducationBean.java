package ru.prognoz.tc.mvc.controller.reports.myeducation;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

public class MyEducationBean {

	private List<ReportItem> items = new ArrayList<ReportItem>();
	private ReportViewModel viewModel = new ReportViewModel();
	private List<CourseCategory> categories = new ArrayList<CourseCategory>();
	private List<CourseStatus> statusList = new ArrayList<>();

	public List<ReportItem> getItems() {
		return items;
	}

	public void setItems(List<ReportItem> items) {
		this.items = items;
	}

	public ReportViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(ReportViewModel viewModel) {
		this.viewModel = viewModel;
	}
	
	public List<CourseCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<CourseCategory> categories) {
		this.categories = categories;
	}

	public List<CourseStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<CourseStatus> statusList) {
		this.statusList = statusList;
	}
}
