package ru.prognoz.tc.mvc.controller.courselist;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.domain.course.CourseCategory;

public class CourseListBean {

	private CourseListViewModel viewModel = new CourseListViewModel(); 
	private List<CourseItem> courseItems = new ArrayList<CourseItem>();
	private List<NotificationItem> notifications = new ArrayList<NotificationItem>();
	private List<CourseCategory> categories = new ArrayList<CourseCategory>();

	public List<CourseItem> getCourseItems() {
		return courseItems;
	}

	public void setCourseItems(List<CourseItem> courseItems) {
		this.courseItems = courseItems;
	}

	public void setNotifications(List<NotificationItem> notifications) {
		this.notifications = notifications;
	}

	public List<NotificationItem> getNotifications() {
		return notifications;
	}

	public CourseListViewModel getViewModel() {
		return viewModel;
	}

	public void setViewModel(CourseListViewModel viewModel) {
		this.viewModel = viewModel;
	}

	public List<CourseCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<CourseCategory> categories) {
		this.categories = categories;
	}
	
}