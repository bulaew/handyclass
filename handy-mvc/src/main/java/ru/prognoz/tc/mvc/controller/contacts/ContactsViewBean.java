package ru.prognoz.tc.mvc.controller.contacts;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

public class ContactsViewBean {

	private List<ContactListInfo> infoList;
	private long activeListOid = -1;
	private boolean editTitleMode = false;
	/**
	 * Контакты активного списка.
	 */
	private List<Contact> contacts = new ArrayList<Contact>();
	/*
	 * Атрибуты для отображения поиска пользователя
	 */
	private ViewMessage msg;
	private User findUser;
	private String findEmail = "";

	public List<ContactListInfo> getInfoList() {
		return infoList;
	}

	public void setInfoList(List<ContactListInfo> infoList) {
		this.infoList = infoList;
	}

	public ContactListInfo getActiveList() {
		for (ContactListInfo info : infoList) {
			if (info.getObjectId().getId() == activeListOid) {
				return info;
			}
		}
		
		return null;
	}

	public void setActiveListOid(long activeListOid) {
		this.activeListOid = activeListOid;
	}

	public boolean isEditTitleMode() {
		return editTitleMode;
	}

	public void setEditTitleMode(boolean editTitleMode) {
		this.editTitleMode = editTitleMode;
	}

	public ViewMessage getMsg() {
		return msg;
	}

	public void setMsg(ViewMessage msg) {
		this.msg = msg;
	}

	public User getFindUser() {
		return findUser;
	}

	public void setFindUser(User findUser) {
		this.findUser = findUser;
	}

	public String getFindEmail() {
		return findEmail;
	}

	public void setFindEmail(String findEmail) {
		this.findEmail = findEmail;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public long countMembers(long contactListInfoOID) {
		ContactListContentDao dao = HandyclassAppContext.getBean(ContactListContentDao.class);
		return dao.countMembers(contactListInfoOID);
	}
	
}
