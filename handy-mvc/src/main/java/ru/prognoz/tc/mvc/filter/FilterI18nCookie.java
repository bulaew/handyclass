package ru.prognoz.tc.mvc.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.util.WebUtils;

public class FilterI18nCookie implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(FilterI18nCookie.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (!(request instanceof HttpServletRequest)) {
			chain.doFilter(request, response);
			return;
		}

		HttpServletRequest req = (HttpServletRequest) request;

		Cookie cookie = WebUtils.getCookie(req, CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME);
		if (cookie != null) {
			Locale locale = org.springframework.util.StringUtils.parseLocaleString(cookie.getValue());
			if (locale != null) {
				//logger.info("Locale cookie: [" + cookie.getValue() + "] == '" + locale + "'");
				request.setAttribute(CookieLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME, locale);
				LocaleContextHolder.setLocale(locale, true);
			}
		}

		chain.doFilter(request, response);

	}

	@Override
	public void destroy() {
	}

}
