package ru.prognoz.handy.impl.domain;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;

import ru.prognoz.handy.impl.domain.course.InviteImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.Role;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Invite;

@Entity
@Table(name = "hc_user")
public class UserImpl implements User {

    private static final long serialVersionUID = 8373738402145724358L;

    private long id;
    private HandyObjectID objectID;
    // Персональные данные
    private String login;
    private String password;
    private String name;
    private String lastName;
    private String middleName;
    private String about;
    // Социальные сети
    private String googleemail;
    private String googleName;
    private String facebookAccount;
    private String facebookName;
    private String twitterAccount;
    private String twitterName;
    private String vkAccount;
    private String vkName;

    private String avatarURL;

    // Данные об активности пользователя.
    private Date registrationDate;
    private Date lastVisitDate;
    private long totalTime;

    /* Spring Security fields */
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
    /**
     * Пользователь подтвержден. Подтверждение получается по средствам
     * электронной почты. При регистрации пользователя с использованием соц
     * сетей, подтверждение устанавливается автоматически при аутентификации.
     */
    private boolean verified;
    private String verificationCode;

    private Set<Role> roles = new HashSet<Role>();
    private Set<Invite> invites = new HashSet<Invite>();

    @Id
    @GenericGenerator(name = "test-increment-strategy", strategy = "increment")
    @GeneratedValue(generator = "test-increment-strategy")
    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    @OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id")
    @Override
    public HandyObjectID getObjectId() {
	return objectID;
    }

    @Override
    public void setObjectId(HandyObjectID objectID) {
	this.objectID = objectID;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    @Type(type = "text")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Type(type = "text")
    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getMiddleName() {
	return middleName;
    }

    public void setMiddleName(String middleName) {
	this.middleName = middleName;
    }

    public String getAvatarURL() {
	return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
	this.avatarURL = avatarURL;
    }

    @Type(type = "text")
    public String getAbout() {
	return about;
    }

    public void setAbout(String about) {
	this.about = about;
    }

    public Date getRegistrationDate() {
	return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
	this.registrationDate = registrationDate;
    }

    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
	return getRoles();
    }

    @Override
    @Transient
    public String getUsername() {
	StringBuffer str = new StringBuffer();
	if (name != null) {
	    str.append(name);
	    str.append(" ");
	}
	if (lastName != null) {
	    str.append(lastName);
	}
	return str.toString();
    }

    @Override
    public boolean isAccountNonExpired() {
	return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
	return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
	return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
	return enabled;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
	this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
	this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
	this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(boolean enabled) {
	this.enabled = enabled;
    }

    @ManyToMany(targetEntity = RoleImpl.class, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
    public Set<Role> getRoles() {
	return roles;
    }

    public void setRoles(Set<Role> roles) {
	this.roles = roles;
    }

    @OneToMany(targetEntity = InviteImpl.class, fetch = FetchType.EAGER, mappedBy = "user")
    public Set<Invite> getInvites() {
	return invites;
    }

    public void setInvites(Set<Invite> invites) {
	this.invites = invites;
    }

    public String getGoogleemail() {
	return googleemail;
    }

    public void setGoogleemail(String googleemail) {
	this.googleemail = googleemail;
    }

    public String getGoogleName() {
	return googleName;
    }

    public void setGoogleName(String googleName) {
	this.googleName = googleName;
    }

    public String getFacebookAccount() {
	return facebookAccount;
    }

    public void setFacebookAccount(String facebookAccount) {
	this.facebookAccount = facebookAccount;
    }

    public String getFacebookName() {
	return facebookName;
    }

    public void setFacebookName(String facebookName) {
	this.facebookName = facebookName;
    }

    public String getTwitterAccount() {
	return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
	this.twitterAccount = twitterAccount;
    }

    public String getTwitterName() {
	return twitterName;
    }

    public void setTwitterName(String twitterName) {
	this.twitterName = twitterName;
    }

    public String getVkAccount() {
	return vkAccount;
    }

    public void setVkAccount(String vkAccount) {
	this.vkAccount = vkAccount;
    }

    public String getVkName() {
	return vkName;
    }

    public void setVkName(String vkName) {
	this.vkName = vkName;
    }

    public Date getLastVisitDate() {
	return lastVisitDate;
    }

    public void setLastVisitDate(Date lastVisitDate) {
	this.lastVisitDate = lastVisitDate;
    }

    public long getTotalTime() {
	return totalTime;
    }

    public void setTotalTime(long totalTime) {
	this.totalTime = totalTime;
    }

    public boolean isVerified() {
	return verified;
    }

    public void setVerified(boolean verified) {
	this.verified = verified;
    }

    public String getVerificationCode() {
	return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
	this.verificationCode = verificationCode;
    }

}
