package ru.prognoz.handy.impl.dao.attempt;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.QuestionAttemptImpl;
import ru.prognoz.tc.dao.attempt.QuestionAttemptDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.test.Question;

@Repository
@Transactional
public class QuestionAttemptDaoImpl implements QuestionAttemptDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(QuestionAttempt questionAttempt) {
		sessionFactory.getCurrentSession().saveOrUpdate(questionAttempt);
	}

	@Override
	public void merge(QuestionAttempt questionAttempt) {
		sessionFactory.getCurrentSession().merge(questionAttempt);
	}

	@Override
	public void delete(QuestionAttempt questionAttempt) {
		sessionFactory.getCurrentSession().delete(questionAttempt);
	}

	@Override
	public QuestionAttempt getOrCreate(Question question, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from QuestionAttemptImpl where questionOid = :questionOID and user = :userID");
		q.setLong("questionOID", question.getObjectId().getId());
		q.setLong("userID", user.getId());
		QuestionAttempt attempt = (QuestionAttempt) q.uniqueResult();
		if (attempt == null) { // создаем
			attempt = new QuestionAttemptImpl();
			attempt.setQuestionOid(question.getObjectId());
			attempt.setUser(user);
			attempt.setCorrect(false);
			attempt.setShowed(false);
			sessionFactory.getCurrentSession().save(attempt);
		}
		return attempt;
	}

}
