package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.ImageImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.ImageDao;
import ru.prognoz.tc.domain.slide.Image;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class ImageDaoImpl implements ImageDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Image newDomainInstatnce(Slide slide) {
		Image image = new ImageImpl();
		image.setComponentOID(oidDao.generate());
		image.setActive(false);
		image.setArchive(false);
		image.setUrl("");
		image.setSize(100);
		image.setDescription("");
		image.setType("image");
		image.setSlideOID(slide.getObjectId());
		return image;
	}
}
