package ru.prognoz.handy.impl.dao.contact;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.contact.ContactListInfoImpl;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.ContactListInfo;

@Repository
@Transactional
public class ContactListInfoDaoImpl implements ContactListInfoDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ContactListInfo newDomainInstance() {
		return new ContactListInfoImpl();
	}

	@Override
	public void save(ContactListInfo list) {
		sessionFactory.getCurrentSession().save(list);
	}

	@Override
	public void merge(ContactListInfo list) {
		sessionFactory.getCurrentSession().merge(list);
	}

	@Override
	public ContactListInfo getByOid(long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from ContactListInfoImpl where objectId.id = :objectId and archive != true ORDER BY objectId ASC");
		q.setLong("objectId", oid);
		return (ContactListInfo) q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactListInfo> list(User owner) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from ContactListInfoImpl where owner.id = :userId and archive != true ORDER BY objectId ASC");
		q.setLong("userId", owner.getId());
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactListInfo> search(String groupTitle) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from ContactListInfoImpl where title = :groupTitle and archive != true ORDER BY objectId ASC");
		q.setString("groupTitle", groupTitle.trim());
		return q.list();
	}

}
