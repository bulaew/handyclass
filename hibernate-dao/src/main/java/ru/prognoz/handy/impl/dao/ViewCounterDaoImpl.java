package ru.prognoz.handy.impl.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.ViewCounterImpl;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.ViewCounter;

@Repository
@Transactional
public class ViewCounterDaoImpl implements ViewCounterDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public int getCountViews(HandyObjectID componentOID) {
		Session session = sessionFactory.getCurrentSession();
		Query q = session.createQuery("select count(*) from ViewCounterImpl where componentOID.id = :oid");
		q.setLong("oid", componentOID.getId());
		Long result = (Long)q.uniqueResult();
		return result == null ? 0 : result.intValue();
	}

	@Override
	public int increase(HandyObjectID componentOID, User owner) {
		if (!isViewed(componentOID, owner)) {
			ViewCounter newViewer = new ViewCounterImpl();
			newViewer.setComponentOID(componentOID);
			newViewer.setOwner(owner);
			Session s = sessionFactory.getCurrentSession();
			s.save(newViewer);
		}
		return getCountViews(componentOID);
	}

	@Override
	public boolean isViewed(HandyObjectID componentOID, User owner) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from ViewCounterImpl where componentOID.id = :oid and owner.id = :userId");
		q.setLong("oid", componentOID.getId());
		q.setLong("userId", owner.getId());
		return q.list().size() > 0;
	}

}
