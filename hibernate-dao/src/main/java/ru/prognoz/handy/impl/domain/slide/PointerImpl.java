package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import ru.prognoz.tc.domain.slide.Pointer;

@Entity
@Table(name = "sc_pointer")
public class PointerImpl extends ChildComponentImpl implements Pointer {
	private static final long serialVersionUID = -3795684975339822324L;

	private int x;
	private int y;
	private String pointerType = "up";

	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String getPointerType() {
		return pointerType;
	}

	@Override
	public void setPointerType(String type) {
		this.pointerType = type;
	}

}