package ru.prognoz.handy.impl.domain.attempt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;

@Entity
@Table(name = "question_attempt")
public class QuestionAttemptImpl implements QuestionAttempt {

	private static final long serialVersionUID = -5198703405432870248L;
	/**
	 * ID
	 */
	private long id;
	private HandyObjectID questionOID;
	private User user;
	private boolean correct;
	private boolean showed;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	public HandyObjectID getQuestionOid() {
		return questionOID;
	}

	@Override
	public void setQuestionOid(HandyObjectID questionOID) {
		this.questionOID = questionOID;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public boolean isCorrect() {
		return correct;
	}

	@Override
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	@Override
	public boolean isShowed() {
		return showed;
	}

	@Override
	public void setShowed(boolean showed) {
		this.showed = showed;
	}

}
