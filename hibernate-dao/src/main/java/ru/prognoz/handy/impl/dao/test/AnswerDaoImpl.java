package ru.prognoz.handy.impl.dao.test;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.test.AnswerImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.test.AnswerDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;

@Repository
@Transactional
public class AnswerDaoImpl implements AnswerDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Answer newDomainInstance(Question question) {
		Answer a = new AnswerImpl();
		a.setObjectId(oidDao.generate());
		a.setText("");
		a.setValue("");
		a.setQuestionOID(question.getObjectId());
		a.setQuestionType(question.getQuestionType());
		return a;
	}

	@Override
	public List<Answer> getAnswersByQuestion(Question question) {
		return getAnswersByQuestion(question.getObjectId());
	}

	@Override
	public List<Answer> getAnswersByQuestion(HandyObjectID question) {
		return getAnswersByQuestion(question.getId());
	}

	@Override
	public List<Answer> getAnswersByQuestion(Long question) {
		Query q = sessionFactory
				.getCurrentSession()
				.createQuery(
						"from AnswerImpl where questionOID = :objectId ORDER BY id ASC");
		q.setLong("objectId", question);
		@SuppressWarnings("unchecked")
		List<Answer> answers = q.list();
		return answers;
	}

	@Override
	public void merge(Answer answer) {
		sessionFactory.getCurrentSession().merge(answer);
	}
}
