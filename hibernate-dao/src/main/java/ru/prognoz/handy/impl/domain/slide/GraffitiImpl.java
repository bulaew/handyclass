package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.slide.Graffiti;

@Entity
@Table(name = "sc_graffiti")
public class GraffitiImpl extends SlideComponentImpl implements Graffiti {
	private static final long serialVersionUID = -3795684975339822324L;
	private String content;

	@Override
	@Type(type = "text")
	public String getContent() {
		return content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}
}