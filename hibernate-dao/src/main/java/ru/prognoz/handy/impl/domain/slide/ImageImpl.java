package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.slide.Image;

@Entity
@Table(name = "sc_image")
public class ImageImpl extends SlideComponentImpl implements Image {
	private static final long serialVersionUID = -3795684975339822324L;

	private String url;
	private int rotation;
	private int size;
	private String description;
	private String urlType = "local";

	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	@Type(type = "text")
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	@Override
	public int getRotation() {
		return rotation;
	}

	@Override
	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public String getUrlType() {
		return urlType;
	}

	@Override
	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}
}