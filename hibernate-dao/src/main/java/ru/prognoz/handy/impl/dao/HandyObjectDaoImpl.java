package ru.prognoz.handy.impl.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.domain.HandyObjectID;

@Repository
@Transactional
public class HandyObjectDaoImpl implements HandyObjectDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public HandyObjectID generate() {
		HandyObjectID newOID = new HandyObjectIDImpl();
		sessionFactory.getCurrentSession().save(newOID);
		return newOID;
	}

	@Override
	public HandyObjectID getById(Long id) {
		return (HandyObjectID) sessionFactory.getCurrentSession().createCriteria(HandyObjectIDImpl.class).add(Restrictions.eq("id", id)).setMaxResults(1).uniqueResult();
	}

	@Override
	@Transactional(propagation = Propagation.NEVER)
	public HandyObjectID newDomainInstance() {
		return new HandyObjectIDImpl();
	}

}
