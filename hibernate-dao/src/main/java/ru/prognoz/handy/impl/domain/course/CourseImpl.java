package ru.prognoz.handy.impl.domain.course;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.AccessTypeDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.AccessType;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.domain.course.Invite;

@Entity
@Table(name = "course")
public class CourseImpl implements Course {

	private static final long serialVersionUID = 5134086159585746921L;

	private long courseId;

	private HandyObjectID objectId;
	private CourseCategory category;
	private String shortName;
	private String fullName;
	private String description;
	private User owner;
	private Date aviableFrom;
	private Date aviableTo;
	private Date created;
	private Date modified;
	private String avatar;
	private int viewSlideTime = 5; // in seconds

	private Set<Invite> invites = new HashSet<Invite>();
	private List<Discussion> discussions = new ArrayList<Discussion>();
	/**
	 * Тип доступа к курсу.
	 */
	private AccessType accessType;
	/**
	 * Test
	 */
	private HandyObjectID test;
	private boolean archive = false;

	@Id
	@Column(name = "courseId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	@Override
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@ManyToOne(targetEntity = CourseCategoryImpl.class)
	@JoinColumn(name = "category_id")
	@Override
	public CourseCategory getCategory() {
		return category;
	}

	@Override
	public void setCategory(CourseCategory category) {
		this.category = category;
	}

	@Column(name = "short_name")
	@Type(type = "text")
	@Override
	public String getShortName() {
		return shortName;
	}

	@Override
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Column(name = "full_name")
	@Type(type = "text")
	@Override
	public String getFullName() {
		return fullName;
	}

	@Override
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name = "desc_name")
	@Type(type = "text")
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	@Override
	public User getOwner() {
		return owner;
	}

	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
	public Date getAviableFrom() {
		return aviableFrom;
	}

	@Override
	public void setAviableFrom(Date aviableFrom) {
		this.aviableFrom = aviableFrom;
	}

	@Override
	public Date getAviableTo() {
		return aviableTo;
	}

	@Override
	public void setAviableTo(Date aviableTo) {
		this.aviableTo = aviableTo;
	}

	@Override
	public Date getCreated() {
		return created;
	}

	@Override
	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public Date getModified() {
		return modified;
	}

	@Override
	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Type(type = "text")
	@Override
	public String getAvatarUrl() {
		return avatar;
	}

	@Override
	public void setAvatarUrl(String avatarUrl) {
		this.avatar = avatarUrl;
	}

	@Override
	public int getViewSlideTime() {
		return viewSlideTime;
	}

	@Override
	public void setViewSlideTime(int viewSlideTime) {
		this.viewSlideTime = viewSlideTime;
	}

	@ManyToOne(targetEntity = AccessTypeImpl.class)
	@JoinColumn(name = "access_type")
	private AccessType getAccessTypeEntity() {
		return accessType;
	}

	private void setAccessTypeEntity(AccessType accessType) {
		this.accessType = accessType;
	}

	@Transient
	@Override
	public CourseAccessType getAccessType() {
		if (getAccessTypeEntity() == null) {
			return null;
		}
		return CourseAccessType.parseInt((int) getAccessTypeEntity().getId());
	}

	@Override
	public void setAccessType(CourseAccessType accessType) {
		AccessType typeEntity = HandyclassAppContext.getBean(AccessTypeDao.class).getById((long) accessType.getIntValue());
		setAccessTypeEntity(typeEntity);
	}

	@OneToMany(targetEntity = InviteImpl.class, fetch = FetchType.EAGER, mappedBy = "course", cascade = CascadeType.ALL)
	@Override
	public Set<Invite> getInvites() {
		return invites;
	}

	@Override
	public void setInvites(Set<Invite> invites) {
		this.invites = invites;
	}

	@OneToMany(targetEntity = DiscussionImpl.class, fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.MERGE,
			CascadeType.PERSIST }, mappedBy = "course")
	@Override
	public List<Discussion> getDiscussions() {
		return discussions;
	}

	@Override
	public void setDiscussions(List<Discussion> discussions) {
		this.discussions = discussions;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "testOID")
	@Override
	public HandyObjectID getTestOid() {
		return test;
	}

	@Override
	public void setTestOid(HandyObjectID test) {
		this.test = test;
	}
}
