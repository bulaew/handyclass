package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.TextImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.TextDao;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.Text;

@Repository
@Transactional
public class TextDaoImpl implements TextDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Text newDomainInstatnce(Slide slide) {
		Text text = new TextImpl();
		text.setComponentOID(oidDao.generate());
		text.setTextContent("");
		text.setActive(false);
		text.setArchive(false);
		text.setType("text");
		text.setSlideOID(slide.getObjectId());
		return text;
	}
}
