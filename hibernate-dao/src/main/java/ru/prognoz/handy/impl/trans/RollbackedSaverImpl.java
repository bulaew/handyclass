package ru.prognoz.handy.impl.trans;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.service.RollbackedSaver;

@Repository
public class RollbackedSaverImpl implements RollbackedSaver {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void merge(List<Object> entities) throws Exception {
		for (Object entity : entities) {
			sessionFactory.getCurrentSession().merge(entity);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void merge(Object entity) throws Exception {
		sessionFactory.getCurrentSession().merge(entity);
	}
}
