package ru.prognoz.handy.impl.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.ViewCounter;

@Entity
@Table(name = "viewcounter")
public class ViewCounterImpl implements ViewCounter {

	private static final long serialVersionUID = -8798870847566987356L;

	private long id;
	private HandyObjectID componentOID;
	private User owner;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "component_oid")
	public HandyObjectID getComponentOID() {
		return componentOID;
	}

	@Override
	public void setComponentOID(HandyObjectID componentOID) {
		this.componentOID = componentOID;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	public User getOwner() {
		return owner;
	}

	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}

}
