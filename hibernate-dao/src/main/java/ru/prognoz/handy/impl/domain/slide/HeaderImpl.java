package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.slide.Header;

@Entity
@Table(name = "sc_header")
public class HeaderImpl extends SlideComponentImpl implements Header{

	private static final long serialVersionUID = -3795684975339822324L;

	private String content;

	@Override
	@Type(type = "text")
	public String getHeaderContent() {
		return content;
	}

	@Override
	public void setHeaderContent(String headerContent) {
		this.content = headerContent;
	}
}