package ru.prognoz.handy.impl.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.LikeImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.LikeDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.Like;
import ru.prognoz.tc.domain.User;

@Repository
@Transactional
public class LikeDaoImpl implements LikeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public long getCountLikes(Long componentOID) {
		Query q = sessionFactory.getCurrentSession().createQuery("select count(*) from LikeImpl where componentOID.id = :oid");
		q.setLong("oid", componentOID);
		Long count = (Long) q.uniqueResult();
		return count == null ? 0 : count.longValue();
	}

	@Override
	public Like pushLike(Long componentOID, User owner) {
		Like like = null;
		Session session = sessionFactory.getCurrentSession();
		Query q = session.createQuery("from LikeImpl where componentOID.id = :oid and owner.id = :userId");
		q.setLong("oid", componentOID);
		q.setLong("userId", owner.getId());
		like = (Like) q.uniqueResult();
		if (like == null) {
			like = new LikeImpl();
			HandyObjectID handyObjectId = oidDao.getById(componentOID);
			like.setComponentOID(handyObjectId);
			like.setOwner(owner);
			like.setDate(new Date());
			session.save(like);
		} else {
			session.delete(like);
		}
		return like;
	}

	@Override
	public boolean isLiked(Long componentOID, User owner) {
		if (owner == null) {
			return false;
		}
		Query q = sessionFactory.getCurrentSession().createQuery("from LikeImpl where componentOID.id = :oid and owner.id = :userId");
		q.setLong("oid", componentOID);
		q.setLong("userId", owner.getId());
		Like like = (Like) q.uniqueResult();
		return like != null;
	}

	@Override
	public List<Like> getComponentLikes(Long componentOID, Long limit) {
		Query q = sessionFactory.getCurrentSession().createQuery("from LikeImpl where componentOID.id = :oid order by date desc");
		q.setLong("oid", componentOID);
		if (limit != null && limit > 0) {
			q.setMaxResults(limit.intValue());
		}
		return q.list();
	}

	@Override
	public List<Like> getComponentLikes(Long componentOID) {
		Query q = sessionFactory.getCurrentSession().createQuery("from LikeImpl where componentOID.id = :oid order by date desc");
		q.setLong("oid", componentOID);
		return q.list();
	}

}
