package ru.prognoz.handy.impl.reports;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.common.WhereBuilder;
import ru.prognoz.tc.core.reports.common.WhereBuilder.ConditionParam;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityCriteria;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityItem;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;

@Repository
@Transactional
public class AuthorActivityPipeline implements ReportPipeline {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private UserDao userDao;

	@Override
	public ReportType getType() {
		return ReportType.AUTHOR_ACTIVITY;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Активность авторов\"");
		}
	}

	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		AuthorActivityCriteria criteria = (AuthorActivityCriteria) searchCriteria;
		List<ReportItem> data = new ArrayList<ReportItem>();

		WhereBuilder categoryWhere = new WhereBuilder();
		// фильтр по категориям
		if (criteria.getFilterCategoryID() > 0) {
			categoryWhere.addCondition(" course.category_id = :filterCategotyID ", "filterCategotyID", criteria.getFilterCategoryID());
		}

		//@formatter:off
		String fullSQL = 
				"SELECT tempo.id, tempo.lastname, tempo.name, tempo.middlename, tempo.counter " +
				"FROM (" +
						"SELECT hc_user.id, hc_user.lastname, hc_user.name, hc_user.middlename, count(course.owner_id) as counter " +
						"FROM "+
							"hc_user JOIN course " +
						"ON " +
							"hc_user.id = course.owner_id and course.archive != 'true' "+
						categoryWhere.getWhere()+
						"GROUP BY " +
							"hc_user.id, hc_user.lastname, hc_user.name, hc_user.middlename" +
				") tempo ";
		//@formatter:on
		// Условие поиска.
		// Добавляем условие для поиска.
		WhereBuilder searchWhere = new WhereBuilder();
		try {
			Integer findValue = Integer.parseInt(criteria.getFindValue());
			searchWhere
					.addMultiCondition(
							"(tempo.lastname LIKE :strFind OR tempo.name LIKE :strFind OR tempo.middlename LIKE :strFind OR tempo.counter = :intFind) ",
							new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"), new ConditionParam("intFind", findValue));
		} catch (NumberFormatException exc) {
			searchWhere.addCondition(" (tempo.lastname LIKE :strFind OR tempo.name LIKE :strFind OR tempo.middlename LIKE :strFind) ",
					"strFind", "%" + criteria.getFindValue() + "%");
		}
		fullSQL += searchWhere.getWhere();
		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || AuthorActivityItem.COLUMN_AUTHOR_FIO.equals(criteria.getOrderColumn())) {
			order = "ORDER BY tempo.name";
		} else if (AuthorActivityItem.COLUMN_COUNT_COURSES.equals(criteria.getOrderColumn())) {
			order = "ORDER BY tempo.counter";
		}
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSQL += order + " ASC ";
			} else {
				fullSQL += order + " DESC ";
			}
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSQL);
		// Устанавливаем параметры запроса.
		Iterator<String> keyIterator = categoryWhere.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = categoryWhere.getParams().get(key);
			query.setParameter(key, value);
		}
		keyIterator = searchWhere.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = searchWhere.getParams().get(key);
			query.setParameter(key, value);
		}

		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}

		List<Object[]> queryList = new ArrayList<Object[]>();
		try {
			queryList = query.list();
		} catch (HibernateException e) {
			throw e;
		}
		for (Object[] row : queryList) {
			BigInteger userOID = (BigInteger) row[0];
			BigInteger courseCount = (BigInteger) row[4];
			User author = userDao.getById(userOID.longValue());
			AuthorActivityItem item = new AuthorActivityItem(author.getObjectId().getId(), author.getUsername(), courseCount.intValue());
			data.add(item);
		}
		return data;
	}

}
