package ru.prognoz.handy.impl.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.HandyCommentImpl;
import ru.prognoz.tc.dao.HandyCommentDao;
import ru.prognoz.tc.domain.HandyComment;

@Repository
@Transactional
public class HandyCommentDaoImpl implements HandyCommentDao {

	private class CommentsDateComparator implements Comparator<HandyComment> {
		@Override
		public int compare(HandyComment o1, HandyComment o2) {
			return o1.getModifiedDate().compareTo(o2.getModifiedDate());
		}
	}

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public HandyComment newDomainInstatnce() {
		return new HandyCommentImpl();
	}

	@Override
	public void merge(HandyComment comment) {
		sessionFactory.getCurrentSession().merge(comment);
	}

	@Override
	public void save(HandyComment comment) {
		sessionFactory.getCurrentSession().saveOrUpdate(comment);
	}

	@Override
	public List<HandyComment> getByParent(Long parentOid) {
		if (parentOid == null) {
			return new ArrayList<HandyComment>(); 
		}
		Query q = (Query) sessionFactory.getCurrentSession().createQuery(
				"from HandyCommentImpl where parentOid = :objectId ORDER BY createDate ASC");
		q.setLong("objectId", parentOid);
		@SuppressWarnings("unchecked")
		List<HandyComment> resList = q.list();
		return resList;
	}

	@Override
	public HandyComment find(Long commentOID) {
		Query q = (Query) sessionFactory.getCurrentSession().createQuery(
				"from HandyCommentImpl where objectId = :objectId ORDER BY createDate ASC");
		q.setLong("objectId", commentOID);
		return (HandyComment) q.uniqueResult();
	}

	@Override
	public boolean setArchived(Long commentOID) {
		HandyComment comment = find(commentOID);
		comment.setModifiedDate(new Date());
		comment.setArchive(true);
		sessionFactory.getCurrentSession().saveOrUpdate(comment);
		return comment.isArchive();
	}

	@Override
	public List<HandyComment> fullList(Long parentOid, boolean includeArchived) {
		List<HandyComment> resultList = new ArrayList<HandyComment>();
		resultList.addAll(getNestedComments(parentOid));

		if (includeArchived == false) {
			Iterator<HandyComment> iterator = resultList.iterator();
			while (iterator.hasNext()) {
				HandyComment handyComment = (HandyComment) iterator.next();
				if (handyComment.isArchive()) {
					iterator.remove();
				}
			}
		}
		Comparator<HandyComment> comparator = new CommentsDateComparator();
		Collections.sort(resultList, comparator);
		return resultList;
	}

	private List<HandyComment> getNestedComments(Long parentOid) {
		List<HandyComment> resultList = new ArrayList<HandyComment>();
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from HandyCommentImpl where parentOid = :objectId ORDER BY createDate ASC");
		q.setLong("objectId", parentOid);
		@SuppressWarnings("unchecked")
		List<HandyComment> list = q.list();
		for (HandyComment handyComment : list) {
			resultList.addAll(getNestedComments(handyComment.getObjectId().getId()));
			resultList.add(handyComment);
		}
		return resultList;
	}

	@Override
	public HandyComment getParent(HandyComment comment) {
		return find(comment.getParentOid().getId());
	}

	@Override
	public Long findRoot(HandyComment comment) {
		HandyComment parent = comment;
		HandyComment previus = null;
		do {
			previus = parent;
			parent = getParent(parent);
		} while (parent != null);
		return previus.getParentOid().getId();
	}

}
