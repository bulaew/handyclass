package ru.prognoz.handy.impl.domain.course;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.course.Attachment;

@Entity
@Table(name = "attachment")
public class AttachmentImpl implements Attachment {

	private static final long serialVersionUID = 8134330370970623592L;
	private long id;
	private HandyObjectID objectId;
	private HandyObjectID courseOid;
	private String description;
	private String attachmentUrl;
	private boolean archive;

	@Override
	@Id
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	@Column(name = "ID")
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "course_oid")
	public HandyObjectID getCourseOid() {
		return courseOid;
	}

	@Override
	public void setCourseOid(HandyObjectID courseOID) {
		this.courseOid = courseOID;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getAttachmentUrl() {
		return attachmentUrl;
	}

	@Override
	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectID) {
		this.objectId = objectID;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	@Transient
	public String getFilename() {
		return getDescription().replace("." + getExtention(), "");
	}

	@Override
	@Transient
	public String getExtention() {
		String[] splitResult = getDescription().split("\\.");
		if (splitResult != null && splitResult.length > 0) {
			return splitResult[splitResult.length - 1];
		} else {
			return "none";
		}
	}

}
