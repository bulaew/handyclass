package ru.prognoz.handy.impl.dao.test;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.test.QuestionImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.test.QuestionDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

@Repository
@Transactional
public class QuestionDaoImpl implements QuestionDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Question newDomainInstance(String type, Test test) {
		return newDomainInstance(type, test.getObjectId());
	}

	@Override
	public Question newDomainInstance(String type, HandyObjectID testOid) {
		Question q = new QuestionImpl();
		q.setText("");
		q.setObjectId(oidDao.generate());
		q.setQuestionType(type);
		q.setTestOID(testOid);
		return q;
	}

	@Override
	public List<Question> getQuestionsByTest(Test test) {
		return getQuestionsByTest(test.getObjectId());
	}

	@Override
	public List<Question> getQuestionsByTest(HandyObjectID test) {
		return getQuestionsByTest(test.getId());
	}

	@Override
	public List<Question> getQuestionsByTest(Long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from QuestionImpl where testOID = :objectId ORDER BY id ASC");
		q.setLong("objectId", oid);
		List<Question> questions = q.list();
		return questions;
	}

	@Override
	public void merge(Question question) {
		sessionFactory.getCurrentSession().merge(question);
	}

	@Override
	public List<Question> getNotArchiveQuestionsByTest(Long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from QuestionImpl where testOID = :objectId AND archive = false ORDER BY id ASC");
		q.setLong("objectId", oid);
		List<Question> questions = q.list();
		return questions;
	}

	@Override
	public List<Question> getNotArchiveQuestionsByTest(Test test) {
		return getNotArchiveQuestionsByTest(test.getObjectId());
	}

	@Override
	public List<Question> getNotArchiveQuestionsByTest(HandyObjectID oid) {
		return getNotArchiveQuestionsByTest(oid.getId());
	}

}
