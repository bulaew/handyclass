package ru.prognoz.handy.impl.reports;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.common.WhereBuilder;
import ru.prognoz.tc.core.reports.common.WhereBuilder.ConditionParam;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationCriteria;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationItem;
import ru.prognoz.tc.data.CourseStatus;

@Repository
@Transactional
public class MyEducationPipeline implements ReportPipeline {
	//@formatter:off
	private static final String SQL_QUERY = "select * from (" +
			"select "+
			"course.object_id, "+
			"course.full_name as course_title,"+
			"course.aviableto, "+
			"course.category_id as course_category, "+
			"part2.status_code, "+
			"case when part2.enable = true then part2.bestresult else (case when count_slides != 0 then cast(count_viewed_slides as numeric)/count_slides*100 else 0 end) end as best_result, "+
			"case when part1.slides_spendtime != 0 then count_views/slides_spendtime*1000*60*60 else 0 end as learn_speed, "+
			"part1.start_date as start_attempt_date, "+
			"part2.completedate as complete_attempt_date, " +
			"course.archive "+
		"from "+
			"(/* Собираем статистику по попыткам по слайдам "+
			"- всего слайдов в курсе "+
			"- всего просмотренных слайдов "+
			"- кол-во просмотров слайдов в курсе "+
			"- затраченое время  "+
			"- дата начала просмотра курса "+
			"*/ "+
			"select "+ 
				"user_slide_attempts.courseoid, "+
				"count(slideid) as count_slides, "+
				"count(case when (viewcounter != 0) then viewcounter end) as count_viewed_slides, "+
				"sum(viewcounter) as count_views, "+
				"sum(spendtime) as slides_spendtime, "+
				"min(firstview) as start_date "+
			"from "+
				"(select "+ 
					"sc_slide.courseoid, "+
					"sc_slide.slideid, "+
					"slide_attempt.course_attempt_id, "+
					"slide_attempt.id as slide_attempt_id, "+
					"slide_attempt.firstview, "+
					"slide_attempt.viewcounter, "+
					"slide_attempt.spendtime "+
				"from  "+
					"sc_slide, "+
					"slide_attempt, "+
					"(select id from course_attempt where course_attempt.user_id = :filterUser) my_attempts "+
				"where  "+
					"sc_slide.oid = slide_attempt.slide_oid and sc_slide.archive != true and slide_attempt.course_attempt_id = my_attempts.id ) user_slide_attempts "+
			"group by user_slide_attempts.courseoid) part1 "+
		"left join "+
			"(select  "+
				"test.courseoid, "+
				"my_attempts.course_status, "+
				"course_status.code as status_code, "+
				"test.enable, "+
				"test_attempt.bestresult, "+
				"my_attempts.completedate "+
			"from  "+
				"test, "+
				"test_attempt, "+
				"(select course_oid, course_status, completedate, user_id from course_attempt where course_attempt.user_id = :filterUser) my_attempts "+
			"left join  "+
				"course_status "+
			"on  "+
				"course_status.id = my_attempts.course_status "+
			"where  "+
				"test.oid = test_attempt.testoid_id and test.courseoid = my_attempts.course_oid and test_attempt.user_id = my_attempts.user_id "+
			") part2 "+
		"on "+
			"part1.courseoid = part2.courseoid "+
		"left join "+
			"course "+
		"on  "+
			"course.object_id = part1.courseoid " +
		"where " +
			"course.archive != 'true' ) full_query "; 
	//@formatter:on

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ReportType getType() {
		return ReportType.MY_EDUCATION;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Моё обучение\"");
		}
	}

	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		List<ReportItem> data = new ArrayList<ReportItem>();
		if (searchCriteria == null) {
			return data;
		}

		MyEducationCriteria criteria = (MyEducationCriteria) searchCriteria;

		String fullSql = SQL_QUERY;
		WhereBuilder whereBuilder = new WhereBuilder();

		// фильтр по категории
		if (criteria.getFilterCategoryID() > 0) {
			whereBuilder.addCondition(" course_category = :filterCategory ", "filterCategory", criteria.getFilterCategoryID());
		}
		// фильтр по статусу
		if (criteria.getFilterStatus()!= null &&  !CourseStatus.UNDEFINED.equals(criteria.getFilterStatus())) {
			whereBuilder.addCondition(" status_code = :filterStatus ", "filterStatus", criteria.getFilterStatus().getIntValue());
		}
		// фильтр по периоду
		if (criteria.getFilterStartPeriod() != null && criteria.getFilterEndPeriod() != null) {
			whereBuilder.addMultiCondition(" ((start_attempt_date BETWEEN :startPeriod and :endPeriod) OR "
					+ "(complete_attempt_date BETWEEN :startPeriod and :endPeriod)) ",
					new ConditionParam("startPeriod", criteria.getFilterStartPeriod()),
					new ConditionParam("endPeriod", criteria.getFilterEndPeriod()));
		}
		// Добавляем условие для поиска.
		try {
			Double findValue = Double.parseDouble(criteria.getFindValue());
			whereBuilder
					.addMultiCondition(
							" (course_title LIKE :strFind OR best_result = :intFind OR learn_speed = :intFind) ",
							new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"),
							new ConditionParam("intFind", findValue.intValue()));
		} catch (NumberFormatException exc) {
			whereBuilder.addCondition(" (course_title LIKE :strFind) ",
					"strFind", "%" + criteria.getFindValue() + "%");
		}
		fullSql += whereBuilder.getWhere();
		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || MyEducationItem.COLUMN_COURSE_TITLE.equals(criteria.getOrderColumn())) {
			order += "ORDER BY course_title";
		} else if (MyEducationItem.COLUMN_STATUS_DESCR.equals(criteria.getOrderColumn())) {
			order += "ORDER BY status_code";
		} else if (MyEducationItem.COLUMN_BEST_RESULT.equals(criteria.getOrderColumn())) {
			order += "ORDER BY best_result";
		} else if (MyEducationItem.COLUMN_LEARN_SPEED.equals(criteria.getOrderColumn())) {
			order += "ORDER BY learn_speed";
		} else if (MyEducationItem.COLUMN_START_ATTEMPT_DT.equals(criteria.getOrderColumn())) {
			order += "ORDER BY start_attempt_date";
		} else if (MyEducationItem.COLUMN_COMPLETE_ATTEMPT_DT.equals(criteria.getOrderColumn())) {
			order += "ORDER BY complete_attempt_date";
		}
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSql += order + " ASC ";
			} else {
				fullSql += order + " DESC ";
			}
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSql);

		// Устанавливаем параметры запроса.
		query.setLong("filterUser", criteria.getFilterUser().getId());
		// Устанавливаем параметры запроса.
		Iterator<String> keyIterator = whereBuilder.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = whereBuilder.getParams().get(key);
			query.setParameter(key, value);
		}

		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}

		List<Object[]> queryList = new ArrayList<Object[]>();
		queryList = query.list();

		for (Object[] row : queryList) {
			BigInteger courseId = (BigInteger) row[0];
			String courseTitle = (String) row[1];
			Date dtEnd = (Date) row[2];
			Integer courseCategoryId = (Integer) row[3];
			Integer statusCode = (Integer) row[4];
			BigDecimal bestResult = (BigDecimal) row[5];
			BigDecimal learnSpeed = (BigDecimal) row[6];
			Date dtStart = (Date) row[7];
			Date dtComplete = (Date) row[8];

			MyEducationItem item = new MyEducationItem();
			if (courseId == null) {
				continue;
			}
			item.setCourseId(courseId.longValue());
			item.setCourseTitle(courseTitle);
			item.setCourseCategoryId(courseCategoryId.intValue());
			CourseStatus status = CourseStatus.parseInt(statusCode.intValue());
			item.setStatusCode(status);
			item.setBestResult(bestResult == null ? 0 : bestResult.doubleValue());
			item.setLearnSpeed(learnSpeed == null ? 0 : learnSpeed.doubleValue());
			item.setStartAttemptDate(dtStart);
			item.setCompleteAttemptDate(dtComplete);
			item.setCourseEndDate(dtEnd);
			data.add(item);
		}

		return data;
	}

}
