package ru.prognoz.handy.impl.domain.attempt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.UserAnswer;

@Entity
@Table(name = "user_answer")
public class UserAnswerImpl implements UserAnswer {

	private static final long serialVersionUID = 651420444402302017L;
	/**
	 * ID
	 */
	private long id;

	/**
	 * Уникальное ИД во всей системе Хендикласс.
	 */
	private HandyObjectID answerOID;
	/**
	 * Служебное поле ответа, содержит пользовательское значение ответа по данному варианту.
	 */
	private String value;
	/**
	 * пользователь.
	 */
	private User user;
	private int viewPosition;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "answer_oid")
	public HandyObjectID getAnswerOid() {
		return answerOID;
	}

	@Override
	public void setAnswerOid(HandyObjectID answerOID) {
		this.answerOID = answerOID;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int getViewPosition() {
		return viewPosition;
	}

	@Override
	public void setViewPosition(int viewPosition) {
		this.viewPosition = viewPosition;
	}

}
