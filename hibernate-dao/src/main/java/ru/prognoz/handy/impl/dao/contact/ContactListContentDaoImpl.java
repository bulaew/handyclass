package ru.prognoz.handy.impl.dao.contact;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.contact.ContactListContentImpl;
import ru.prognoz.tc.dao.contact.ContactDao;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListContent;
import ru.prognoz.tc.domain.contact.ContactListInfo;

@Repository
public class ContactListContentDaoImpl implements ContactListContentDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private ContactDao contactDao;
	
	@Override
	public ContactListContent newDomainInstance() {
		return new ContactListContentImpl();
	}
	
	@Transactional
	public void save(ContactListContent listContent) {
		sessionFactory.getCurrentSession().save(listContent);
	}
	
	@Transactional
	public void delete(ContactListContent listContent) {
		sessionFactory.getCurrentSession().delete(listContent);
	}

	@Override
	@Transactional
	public int countMembers(long contactListOID) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("select count(*) from ContactListContentImpl where listOID = :listOID");
		q.setLong("listOID", contactListOID);
		Long count = (Long) q.uniqueResult();
		return count == null ? 0 : count.intValue();
	}

	@Override
	@Transactional
	public List<Contact> members(Long listOID) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from ContactListContentImpl where listOID = :listOID");
		q.setLong("listOID", listOID);
		@SuppressWarnings("unchecked")
		List<ContactListContent> content = q.list();

		List<Contact> resultList = new ArrayList<Contact>();
		for (ContactListContent contentItem : content) {
			Contact contact = contactDao.getByOID(contentItem.getContactOID().getId());
			resultList.add(contact);
		}
		return resultList;
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public List<Contact> members(ContactListInfo contactListInfo) {
		return members(contactListInfo.getObjectId().getId());
	}

	/**
	 * Поиск в списке заданного контакта.
	 */
	@Override
	@Transactional(propagation = Propagation.NESTED)
	public Contact findContactListMember(ContactListInfo contactListInfo, Long contactOID) {
		ContactListContent fContent = __find(contactListInfo, contactOID);
		if (fContent != null) {
			return contactDao.getByOID(fContent.getContactOID().getId());
		}
		return null;
	}
	
	@Transactional
	private ContactListContent __find(ContactListInfo contactListInfo, Long contactOID) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from ContactListContentImpl where listOID = :listOID and contactOID = :contactOID");
		q.setLong("listOID", contactListInfo.getObjectId().getId());
		q.setLong("contactOID", contactOID);

		return (ContactListContent) q.uniqueResult();
	}
	
	@Transactional
	private ContactListContent __create(ContactListInfo contactListInfo, Contact contact) {
		ContactListContent newContent = new ContactListContentImpl();
		newContent.setListOID(contactListInfo.getObjectId());
		newContent.setContactOID(contact.getObjectId());
		this.save(newContent);
		return newContent;
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public boolean addContactListMember(ContactListInfo info, Contact contact) {
		if (__find(info, contact.getObjectId().getId()) != null) {
			return false;
		}
		if (__create(info, contact) != null) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public boolean removeContactListMember(ContactListInfo info, Contact contact) {
		ContactListContent listContent = __find(info, contact.getObjectId().getId());
		if (listContent == null) {
			return false;
		}
		this.delete(listContent);
		return true;
	}

}
