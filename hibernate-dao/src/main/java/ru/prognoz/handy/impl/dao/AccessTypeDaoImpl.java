package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.dao.AccessTypeDao;
import ru.prognoz.tc.domain.course.AccessType;

@Repository
@Transactional
public class AccessTypeDaoImpl implements AccessTypeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public AccessType getById(long id) {
		return (AccessType) sessionFactory.getCurrentSession().createCriteria(AccessType.class).add(Restrictions.eq("id", id))
				.setMaxResults(1).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccessType> getAllTypes() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AccessType.class);
		return criteria.list();
	}

}
