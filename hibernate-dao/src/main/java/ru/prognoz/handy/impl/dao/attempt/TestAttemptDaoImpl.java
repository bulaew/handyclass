package ru.prognoz.handy.impl.dao.attempt;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.TestAttemptImpl;
import ru.prognoz.tc.dao.attempt.TestAttemptDao;
import ru.prognoz.tc.data.TestStatus;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.test.Test;

@Repository
@Transactional
public class TestAttemptDaoImpl implements TestAttemptDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(TestAttempt testAttempt) {
		sessionFactory.getCurrentSession().saveOrUpdate(testAttempt);
	}

	@Override
	public void merge(TestAttempt testAttempt) {
		sessionFactory.getCurrentSession().merge(testAttempt);
	}

	@Override
	public void delete(TestAttempt testAttempt) {
		sessionFactory.getCurrentSession().delete(testAttempt);
	}

	@Override
	public TestAttempt get(HandyObjectID testOID, User user) {
		if (testOID == null) {
			return null;
		}
		Query q = sessionFactory.getCurrentSession().createQuery("from TestAttemptImpl where user = :userId and testOid = :testOid");
		q.setLong("userId", user.getId());
		q.setLong("testOid", testOID.getId());
		TestAttempt find = (TestAttempt) q.uniqueResult();
		return find;
	}

	@Override
	public TestAttempt getOrCreate(Test test, User user) {
		if (test == null) {
			return null;
		}
		Query q = sessionFactory.getCurrentSession().createQuery("from TestAttemptImpl where user = :userId and testOid = :testOid");
		q.setLong("userId", user.getId());
		q.setLong("testOid", test.getObjectId().getId());
		TestAttempt find = (TestAttempt) q.uniqueResult();
		if (find == null) { // создаем
			find = new TestAttemptImpl();
			find.setTestOid(test.getObjectId());
			find.setUser(user);
			find.setCountAttempts(1);
			find.setBestResult(0);
			find.setStatus(TestStatus.NOT_STARTED); // не запускался.
			find.setSpendedTime(0);
			find.setTotalTime(0);
			sessionFactory.getCurrentSession().save(find);
		}
		return find;
	}

}
