package ru.prognoz.handy.impl.dao.attempt;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.CourseAttemptImpl;
import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Test;

@Repository
@Transactional
public class CourseAttemptDaoImpl implements CourseAttemptDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private TestDao testDao;

	@Override
	public void save(CourseAttempt courseAttempt) {
		sessionFactory.getCurrentSession().saveOrUpdate(courseAttempt);
	}

	@Override
	public void merge(CourseAttempt courseAttempt) {
		sessionFactory.getCurrentSession().merge(courseAttempt);
	}

	@Override
	public void delete(CourseAttempt courseAttempt) {
		sessionFactory.getCurrentSession().delete(courseAttempt);
	}

	@Override
	public CourseAttempt get(Course course, User user) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from CourseAttemptImpl where user = :userId and courseOid = :courseOid");
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		return (CourseAttempt) q.uniqueResult();
	}

	@Override
	public CourseAttempt getOrCreate(Course course, User user) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from CourseAttemptImpl where user = :userId and courseOid = :courseOid");
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<CourseAttempt> list = q.list();
		if (list.size() > 0) {
			return list.get(0);
		} else { // создаем
			// TODO: TestImpl
			Test test = testDao.getByCourse(course);

			CourseAttempt attempt = new CourseAttemptImpl();
			attempt.setCourseOid(course.getObjectId());
			attempt.setUser(user);
			if (course.getAviableTo() != null && new Date().getTime() > course.getAviableTo().getTime()) {
				if (test != null && test.isEnable()) {
					attempt.setStatus(CourseStatus.NOT_PASSED);
				} else {
					attempt.setStatus(CourseStatus.UNCOMPLETED);
				}
			} else {
				attempt.setStatus(CourseStatus.NOT_STARTED);
			}
			s.save(attempt);
			return attempt;
		}
	}

}
