package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.HeaderImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.HeaderDao;
import ru.prognoz.tc.domain.slide.Header;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class HeaderDaoImpl implements HeaderDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Header newDomainInstatnce(Slide slide) {
		Header header = new HeaderImpl();
		header.setSlideOID(slide.getObjectId());
		header.setComponentOID(oidDao.generate());
		header.setHeaderContent("");
		header.setType("header");
		header.setActive(false);
		header.setArchive(false);
		return header;
	}
}
