package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.MarkerImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.MarkerDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.slide.Marker;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class MarkerDaoImpl implements MarkerDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Marker newDomainInstatnce(Slide slide, long parentId, int x, int y, int w, int h) {
		Marker marker = new MarkerImpl();
		marker.setSlideOID(slide.getObjectId());
		marker.setComponentOID(oidDao.generate());
		marker.setX(x);
		marker.setY(y);
		marker.setWidth(w);
		marker.setHeight(h);
		marker.setType("marker");
		marker.setActive(false);
		marker.setArchive(false);
		marker.setParentOid(oidDao.getById(parentId));
		return marker;
	}
}
