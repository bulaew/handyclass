package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.slide.Text;

@Entity
@Table(name = "sc_text")
public class TextImpl extends SlideComponentImpl implements Text {
	private static final long serialVersionUID = -3795684975339822324L;

	private String content;

	@Override
	@Type(type = "text")
	public String getTextContent() {
		return content;
	}

	@Override
	public void setTextContent(String content) {
		this.content = content;
	}
}