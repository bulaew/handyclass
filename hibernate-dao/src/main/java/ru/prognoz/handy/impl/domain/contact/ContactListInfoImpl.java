package ru.prognoz.handy.impl.domain.contact;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.ContactListInfo;

@Entity
@Table(name = "contact_list_info")
public class ContactListInfoImpl implements ContactListInfo {

	private static final long serialVersionUID = 9189343579984030286L;

	private boolean archive = false;
	private long id;
	private HandyObjectID objectId;
	private String title;
	private User owner;
	private Date created;
	private Date modified;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	@Column(name = "title")
	@Type(type = "text")
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	public User getOwner() {
		return owner;
	}

	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
	public Date getCreated() {
		return created;
	}

	@Override
	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public Date getModified() {
		return modified;
	}

	@Override
	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

}
