package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.GraffitiImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.GraffitiDao;
import ru.prognoz.tc.domain.slide.Graffiti;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class GraffitiDaoImpl implements GraffitiDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Graffiti newDomainInstatnce(Slide slide) {
		Graffiti graffiti = new GraffitiImpl();
		graffiti.setComponentOID(oidDao.generate());
		graffiti.setActive(false);
		graffiti.setArchive(false);
		graffiti.setContent("");
		graffiti.setType("graffiti");
		graffiti.setSlideOID(slide.getObjectId());
		return graffiti;
	}
}
