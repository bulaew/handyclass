package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.CourseImpl;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;

@Repository
@Transactional
public class CourseDaoImpl implements CourseDao {

	private static final long serialVersionUID = 8869190856341022463L;
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Course newDomainInstatnce() {
		return new CourseImpl();
	}

	@Override
	public Course getByOid(Long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from CourseImpl where objectId = :objectId ORDER BY objectId ASC");
		q.setLong("objectId", oid);
		return (CourseImpl) q.uniqueResult();
	}

	@Override
	public void merge(Course course) {
		sessionFactory.getCurrentSession().merge(course);
	}

	@Override
	public List<Course> getPublicCourses() {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseImpl where accessTypeEntity = :accessType and archive != true ORDER BY objectId ASC");
		q.setLong("accessType", 3);
		@SuppressWarnings("unchecked")
		List<Course> list = q.list();
		return list;
	}

	@Override
	public List<Course> getPrivateCourses() {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseImpl where accessTypeEntity = :accessType and archive != true ORDER BY objectId ASC");
		q.setLong("accessType", 1);
		@SuppressWarnings("unchecked")
		List<Course> list = q.list();
		return list;
	}

	@Override
	public List<Course> getInviteCourses() {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseImpl where accessTypeEntity = :accessType and archive != true ORDER BY objectId ASC");
		q.setLong("accessType", 2);
		@SuppressWarnings("unchecked")
		List<Course> list = q.list();
		return list;
	}

	@Override
	public List<Course> getAllCourses() {
		Query q = sessionFactory.getCurrentSession().createQuery("from CourseImpl where archive != true ORDER BY objectId ASC");
		@SuppressWarnings("unchecked")
		List<Course> list = q.list();
		return list;
	}

	@Override
	public long countCourses() {
		Query q = sessionFactory.getCurrentSession().createQuery("select count (*) from CourseImpl where archive != true");
		Long count = (Long) q.uniqueResult();
		return count;
	}

	@Override
	public List<Course> getUserOwnCourses(User user) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseImpl where owner = :ownerId ORDER BY objectId ASC");
		q.setLong("ownerId", user.getId());
		@SuppressWarnings("unchecked")
		List<Course> list = q.list();
		return list;
	}
}
