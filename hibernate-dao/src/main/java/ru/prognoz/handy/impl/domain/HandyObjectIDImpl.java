package ru.prognoz.handy.impl.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.prognoz.tc.domain.HandyObjectID;

/**
 * идентификатор объектов системы.
 * 
 * @author Denis.V.Mikulich
 * 
 */
@Entity
@Table(name = "object_id")
public class HandyObjectIDImpl implements HandyObjectID, Serializable {

	private static final long serialVersionUID = -8784726270253846574L;
	private long id;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HandyObjectIDImpl other = (HandyObjectIDImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[OID:" + id + "]";
	}

}
