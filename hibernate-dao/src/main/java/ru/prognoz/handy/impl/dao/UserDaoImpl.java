package ru.prognoz.handy.impl.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User getByLogin(String login) {
		return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("login", login.toLowerCase()))
				.setMaxResults(1).uniqueResult();
	}

	@Override
	public void save(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Override
	public User newDomainInstance() {
		return new UserImpl();
	}
	
	@Override
	public User getById(long id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from UserImpl where id = :id");
		q.setLong("id", id);
		return (UserImpl) q.uniqueResult();
	}

	@Override
	public User getByOid(long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from UserImpl where objectId = :objectId");
		q.setLong("objectId", oid);
		return (UserImpl) q.uniqueResult();
	}

	@Override
	public User getByFbAccount(String fbAccount) {
		return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("facebookAccount", fbAccount))
		.setMaxResults(1).uniqueResult();
	}
	
	@Override
	public User getByVkAccount(String vkAccount) {
		return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("vkAccount", vkAccount))
				.setMaxResults(1).uniqueResult();
	}

	@Override
	public User getByTwitterAccount(String twitterAccount) {
		return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("twitterAccount", twitterAccount))
				.setMaxResults(1).uniqueResult();
	}

	@Override
	public User getByGoogleAccount(String googleAccount) {
		return (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("googleemail", googleAccount))
				.setMaxResults(1).uniqueResult();
	}
	
	@Override
	public long countUsers() {
		Query q = sessionFactory.getCurrentSession().createQuery("select count (*) from UserImpl");
		Long count = (Long) q.uniqueResult();
		return count;
	}

	@Override
	public User getByVerifyCode(String code) {
		Query q = sessionFactory.getCurrentSession().createQuery("from UserImpl where verificationCode = :code");
		q.setString("code", code);
		return (UserImpl) q.uniqueResult();
	}

}
