package ru.prognoz.handy.impl.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.Like;
import ru.prognoz.tc.domain.User;

@Entity
@Table(name = "handylike")
public class LikeImpl implements Like {

	private static final long serialVersionUID = 5681800648095050028L;

	private long id;

	private HandyObjectID componentOID;

	private User owner;

	private Date date;

	@SuppressWarnings("unused")
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	private long getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "component_oid")
	public HandyObjectID getComponentOID() {
		return componentOID;
	}

	public void setComponentOID(HandyObjectID componentOID) {
		this.componentOID = componentOID;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
