package ru.prognoz.handy.impl.reports;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.common.WhereBuilder;
import ru.prognoz.tc.core.reports.common.WhereBuilder.ConditionParam;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.mycourselearn.MyCoursesLearningCriteria;
import ru.prognoz.tc.core.reports.impl.mycourselearn.MyCoursesLearningItem;
import ru.prognoz.tc.core.utils.HcDateTime;

@Repository
@Transactional
public class MyCourseLearningPipeline implements ReportPipeline {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ReportType getType() {
		return ReportType.MY_COURSES_LEARNING;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Обучение по моим курсам\"");
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		List<ReportItem> data = new ArrayList<ReportItem>();
		if (searchCriteria == null) {
			return data;
		}
		MyCoursesLearningCriteria criteria = (MyCoursesLearningCriteria) searchCriteria;
		//@formatter:off
		String fullSql =
				"select * from ( "+
				"select "+
				"course_oid, "+
				"course_name, "+
				"author_id, "+
				"count_registry, "+
				"learn_attempts, "+
				"complete_attempts, "+
				"(case when complete_attempts = 0 or complete_attempts is null then 0 else spend_time/complete_attempts end ) as aver_time, "+
				"(case when count_slides = 0 or count_slides is null then 0 else cast(viewcounter as numeric)/count_slides end) as quality "+
			"from "+
				"(select "+ 
					"course.courseid as course_id, "+
					"course.full_name as course_name, "+
					"course.owner_id as author_id, "+
					"sl_att.course_oid, "+
					"(select count(id) from course_registry where course_registry.course_oid = sl_att.course_oid) as count_registry, "+
					"( count (distinct CASE WHEN course_status in (2, 3, 4,5) THEN course_attempt_id END)) as learn_attempts, "+
					"( count(distinct CASE WHEN course_status in (4,5) THEN course_attempt_id END)) as complete_attempts, "+
					"( sum(CASE WHEN course_status in (4,5) THEN spendtime END)) as spend_time, "+
					"( sum(CASE WHEN course_status in (2,3,4,5) THEN viewcounter END)) as viewcounter, "+
					"( select count (sc_slide.slideid) from sc_slide where sc_slide.courseoid = sl_att.course_oid) as count_slides, " +
					"course.archive as archive "+
				"from "+
				"(select  "+
					"first.course_attempt_id, "+
					"course_attempt.course_oid, "+
					"course_attempt.course_status, "+
					"first.slide_attempt_id, "+
					"first.spendtime, "+
					"first.viewcounter "+
				"from "+
					"(select "+ 
						"slide_attempt.id as slide_attempt_id, "+
						"slide_attempt.spendtime, "+
						"slide_attempt.viewcounter, "+
						"slide_attempt.course_attempt_id "+
					"from  "+
						"slide_attempt, "+
						"sc_slide "+
					"where sc_slide.oid = slide_attempt.slide_oid and sc_slide.archive != true "+
					") first "+
				"left join course_attempt "+
				"on course_attempt.id = course_attempt_id "+
				"order by slide_attempt_id) sl_att "+
				"left join course "+
				"on course.object_id = sl_att.course_oid "+
				"group by sl_att.course_oid, course.courseid) fulldata " +
				"where archive != 'true' ) full_query ";
		//@formatter:on
		WhereBuilder whereBuilder = new WhereBuilder();
		// фильтр по категориям
		if (criteria.getFilterCategoryID() > 0) {
			whereBuilder.addCondition(" category_id = :filterCategotyID ", "filterCategotyID", criteria.getFilterCategoryID());
		}
		// фильтр по автору курса
		if (criteria.getFilterUser() != null) {
			whereBuilder.addCondition(" author_id = :filterUser ", "filterUser", criteria.getFilterUser().getId());
		}
		// Добавляем условие для поиска.
		try {
			Double findValue = Double.parseDouble(criteria.getFindValue());
			whereBuilder.addMultiCondition(
					"(course_name LIKE :strFind OR count_registry = :intFind OR learn_attempts = :intFind OR complete_attempts = :intFind "
							+ "OR aver_time = :doubleFind OR quality = :doubleFind) ",
					new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"),
					new ConditionParam("intFind", findValue), new ConditionParam("doubleFind", findValue.doubleValue()));
		} catch (NumberFormatException exc) {
			whereBuilder.addCondition(" (course_name LIKE :strFind) ", "strFind", "%" + criteria.getFindValue() + "%");
		}
		fullSql += whereBuilder.getWhere();
		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || MyCoursesLearningItem.COLUMN_COUNT_REGS.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.count_registry";
		} else if (MyCoursesLearningItem.COLUMN_COUNT_LEARN.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.learn_attempts";
		} else if (MyCoursesLearningItem.COLUMN_COUNT_COMPLETED.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.complete_attempts";
		} else if (MyCoursesLearningItem.COLUMN_COURSE_NAME.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.course_name";
		} else if (MyCoursesLearningItem.COLUMN_AVARAGE_TIME.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.aver_time";
		} else if (MyCoursesLearningItem.COLUMN_LEARN_QUALITY.equals(criteria.getOrderColumn())) {
			order += "ORDER BY full_query.quality";
		}
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSql += order + " ASC ";
			} else {
				fullSql += order + " DESC ";
			}
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSql);
		// Устанавливаем параметры запроса.

		Iterator<String> keyIterator = whereBuilder.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = whereBuilder.getParams().get(key);
			query.setParameter(key, value);
		}

		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}

		List<Object[]> queryList = new ArrayList<Object[]>();
		queryList = query.list();
		for (Object[] row : queryList) {
			MyCoursesLearningItem item = new MyCoursesLearningItem();
			BigInteger courseOID = (BigInteger) row[0];
			String courseName = (String) row[1];
			BigInteger countRegs = (BigInteger) row[3];
			BigInteger countLearn = (BigInteger) row[4];
			BigInteger countComplete = (BigInteger) row[5];
			BigDecimal avarageTime = (BigDecimal) row[6];
			BigDecimal quality = (BigDecimal) row[7];

			item.setCourseOID(courseOID.longValue());
			item.setCourseName(courseName);
			item.setCountRegs(countRegs == null ? 0 : countRegs.intValue());
			item.setCountLearn(countLearn == null ? 0 : countLearn.intValue());
			long time = 0;
			if(avarageTime != null) {
			    time = avarageTime.longValue();
			}
			item.setCountCompleted(countComplete == null ? 0 : countComplete.intValue());
			item.setAvarageLearnTime(time);
			item.setLearnQuality((quality == null) ? 0 : quality.doubleValue());
			data.add(item);
		}
		return data;
	}

}
