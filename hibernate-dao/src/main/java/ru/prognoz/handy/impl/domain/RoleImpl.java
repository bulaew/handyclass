package ru.prognoz.handy.impl.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ru.prognoz.tc.domain.Role;

@Entity
@Table(name = "hc_role")
public class RoleImpl implements Role {

	private static final long serialVersionUID = -6993319579088645310L;

	private long id;
	private String authority;

	@ManyToMany(mappedBy = "roles")
	private Set<UserImpl> users = new HashSet<UserImpl>();

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public String getAuthority() {
		return authority;
	}

}
