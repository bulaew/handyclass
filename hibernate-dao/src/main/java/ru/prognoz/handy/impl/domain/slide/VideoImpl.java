package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.slide.Video;

@Entity
@Table(name = "sc_video")
public class VideoImpl extends SlideComponentImpl implements Video {

	private static final long serialVersionUID = -3189444725880349986L;

	private String url;
	private String poster;
	private String service;
	private boolean embed;

	@Override
	@Type(type = "text")
	public String getVideoUrl() {
		return url;
	}

	@Override
	public void setVideoUrl(String url) {
		this.url = url;
	}

	@Override
	public String getService() {
		return service;
	}

	@Override
	public void setService(String service) {
		this.service = service;
	}

	@Override
	public boolean isEmbed() {
		return embed;
	}

	@Override
	public void setEmbed(boolean isEmbed) {
		this.embed = isEmbed;
	}

	@Override
	@Type(type = "text")
	public String getPoster() {
		return poster;
	}

	@Override
	public void setPoster(String poster) {
		this.poster = poster;
	}

}