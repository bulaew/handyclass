package ru.prognoz.handy.impl.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.propensity.MyPropensityCriteria;
import ru.prognoz.tc.core.reports.impl.propensity.MyPropensityItem;

@Repository
@Transactional
public class MyPropensityPipeline implements ReportPipeline {

	//@formatter:on
	private static final String SQL_QUERY = 
	"select * "+
	"from "+
		"(select "+ 
			"category_id, "+ 
			"course_category.eng_title, "+ 
			"course_category.ru_title, "+ 
			"sum(best_result*learn_speed/100) as propensity "+
		"from "+
			"(select "+ 
				"course.object_id, "+
				"course.full_name as course_title, "+
				"course.aviableto, "+
				"course.category_id as category_id, "+
				"part2.course_status as status_id, "+
				"part2.description as status_descr, "+
				"case when part2.enable = true then part2.bestresult else (case when count_slides != 0 then cast(count_viewed_slides as numeric)/count_slides*100 else 0 end) end as best_result, "+
				"case when part1.slides_spendtime != 0 then count_views/slides_spendtime*1000*60*60 else 0 end as learn_speed, "+
				"part1.start_date as start_attempt_date, "+
				"part2.completedate as complete_attempt_date "+
			"from "+
				"(/* Собираем статистику по попыткам по слайдам "+
				"- всего слайдов в курсе "+
				"- всего просмотренных слайдов "+
				"- кол-во просмотров слайдов в курсе "+
				"- затраченое время  "+
				"- дата начала просмотра курса "+
				"*/ "+
				"select "+ 
					"user_slide_attempts.courseoid, "+
					"count(slideid) as count_slides, "+
					"count(case when (viewcounter != 0) then viewcounter end) as count_viewed_slides, "+
					"sum(viewcounter) as count_views, "+
					"sum(spendtime) as slides_spendtime, "+
					"min(firstview) as start_date "+
				"from "+
					"(select "+ 
						"sc_slide.courseoid, "+
						"sc_slide.slideid, "+
						"slide_attempt.course_attempt_id, "+
						"slide_attempt.id as slide_attempt_id, "+
						"slide_attempt.firstview, "+
						"slide_attempt.viewcounter, "+
						"slide_attempt.spendtime "+
					"from  "+
						"sc_slide, "+
						"slide_attempt, "+
						"(select id from course_attempt where course_attempt.user_id = :filterUser) my_attempts "+
					"where  "+
						"sc_slide.oid = slide_attempt.slide_oid and sc_slide.archive != true and slide_attempt.course_attempt_id = my_attempts.id ) user_slide_attempts "+
				"group by user_slide_attempts.courseoid) part1 "+
			"left join "+
				"(select  "+
					"test.courseoid, "+
					"my_attempts.course_status, "+
					"course_status.description, "+
					"test.enable, "+
					"test_attempt.bestresult, "+
					"my_attempts.completedate "+
				"from  "+
					"test, "+
					"test_attempt, "+
					"(select course_oid, course_status, completedate, user_id from course_attempt where course_attempt.user_id = :filterUser) my_attempts "+
				"left join  "+
					"course_status "+
				"on  "+
					"course_status.id = my_attempts.course_status "+
				"where  "+
					"test.oid = test_attempt.testoid_id and test.courseoid = my_attempts.course_oid and test_attempt.user_id = my_attempts.user_id "+
				") part2 "+
			"on "+
				"part1.courseoid = part2.courseoid "+
			"left join "+
				"course "+
			"on  "+
				"course.object_id = part1.courseoid " +
			"where " +
				"course.archive != 'true' ) full_query "+
		"left join course_category "+
		"on full_query.category_id = course_category.id "+
		"group by category_id, course_category.id) full_data ";
		//@formatter:off	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public ReportType getType() {
		return ReportType.MY_PROPENSITY;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Мои наклонности\"");
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		List<ReportItem> data = new ArrayList<ReportItem>();
		if (searchCriteria == null) {
			return data;
		}
		MyPropensityCriteria criteria = (MyPropensityCriteria) searchCriteria;
		if (criteria.getFilterUser() == null) {
			return data;
		}
		
		String fullSql = SQL_QUERY;

		//@formatter:on

		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || MyPropensityItem.COLUMN_CATEGORY_ID.equals(criteria.getOrderColumn())) {
			order += "ORDER BY category_id";
		} else if (MyPropensityItem.COLUMN_CATEGORY_TITLE.equals(criteria.getOrderColumn())) {
			if (criteria.getLocale() != null && criteria.getLocale().getLanguage().equals("ru")) {
				order += "ORDER BY ru_title";
			} else {
				order += "ORDER BY eng_title";
			}
		} else if (MyPropensityItem.COLUMN_PROPENSITY.equals(criteria.getOrderColumn())) {
			order += "ORDER BY propensity";
		}
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSql += order + " ASC ";
			} else {
				fullSql += order + " DESC ";
			}
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSql);

		// Устанавливаем параметры запроса.
		query.setLong("filterUser", criteria.getFilterUser().getId());

		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}

		List<Object[]> queryList = new ArrayList<Object[]>();
		queryList = query.list();
		for (Object[] row : queryList) {
			Integer categoryId = (Integer) row[0];
			String categoryTitle = (String) row[1];
			String categoryRuTitle = (String) row[2];
			BigDecimal propensity = (BigDecimal) row[3];

			MyPropensityItem item = new MyPropensityItem();
			if (categoryId == null) {
				continue;
			}
			item.setCategoryId(categoryId.intValue());
			if (criteria.getLocale().getLanguage().equals("ru")) {
				item.setCategoryTitle(categoryRuTitle);
			} else {
				item.setCategoryTitle(categoryTitle);
			}
			item.setPropensity(propensity.doubleValue());
			data.add(item);
		}

		return data;
	}

}
