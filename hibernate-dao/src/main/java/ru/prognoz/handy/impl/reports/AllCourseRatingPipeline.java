package ru.prognoz.handy.impl.reports;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.common.WhereBuilder;
import ru.prognoz.tc.core.reports.common.WhereBuilder.ConditionParam;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingCriteria;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingItem;

@Repository
@Transactional
public class AllCourseRatingPipeline implements ReportPipeline {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ReportType getType() {
		return ReportType.COURSE_RATING;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Рейтинг всех курсов\"");
		}
	}

	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		AllCourseRatingCriteria criteria = (AllCourseRatingCriteria) searchCriteria;
		List<ReportItem> data = new ArrayList<ReportItem>();

		//@formatter:off
		String fullSQL = 
				"SELECT * "+ 
				"FROM 	( "+
					"SELECT "+ 
						"course.object_id, "+
						"course.owner_id, "+
						"course.category_id, "+
						"course.avatarurl, "+
						"course.full_name, "+
						"( select count(*) FROM handylike WHERE course.object_id = handylike.component_oid ) as likecounter, "+
						"( select count(*) FROM viewCounter WHERE course.object_id = viewCounter.component_oid ) as viewcounter "+		
					"FROM 	course  " +
					"WHERE	course.archive != 'true' "+
					"GROUP BY "+
						"course.courseid, course.object_id "+
					") coursesrate ";
		//@formatter:on

		WhereBuilder whereBuilder = new WhereBuilder();
		// фильтр по категориям
		if (criteria.getFilterCategoryID() > 0) {
			whereBuilder.addCondition(" category_id = :filterCategotyID ", "filterCategotyID", criteria.getFilterCategoryID());
		}
		// Добавляем условие для поиска.
		try {
			Integer findValue = Integer.parseInt(criteria.getFindValue());
			whereBuilder.addMultiCondition("(full_name LIKE :strFind OR viewcounter = :intFind OR likecounter = :intFind) ",
					new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"), new ConditionParam("intFind", findValue));
		} catch (NumberFormatException exc) {
			whereBuilder.addCondition(" (full_name LIKE :strFind) ", "strFind", "%" + criteria.getFindValue() + "%");
		}
		fullSQL += whereBuilder.getWhere();
		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || AllCourseRatingItem.COLUMN_COUNT_LIKES.equals(criteria.getOrderColumn())) {
			order = "ORDER BY likecounter";
		} else if (AllCourseRatingItem.COLUMN_COUNT_VIEWS.equals(criteria.getOrderColumn())) {
			order = "ORDER BY viewcounter";
		} else if (AllCourseRatingItem.COLUMN_COURSE_NAME.equals(criteria.getOrderColumn())) {
			order = "ORDER BY full_name";
		}
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSQL += order + " ASC ";
			} else {
				fullSQL += order + " DESC ";
			}
		}
		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSQL);
		if(criteria.getRowCount()>0){
			query.setMaxResults(criteria.getRowCount());
		}
		// Устанавливаем параметры запроса.
		Iterator<String> keyIterator = whereBuilder.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = whereBuilder.getParams().get(key);
			query.setParameter(key, value);
		}
		List<Object[]> queryList = new ArrayList<Object[]>();
		try {
			queryList = query.list();
		} catch (HibernateException e) {
			throw e;
		}
		for (Object[] row : queryList) {
			BigInteger courseOID = (BigInteger) row[0];
			String avatarUrl = (String) row[3];
			String courseName = (String) row[4];
			BigInteger courseLikes = (BigInteger) row[5];
			BigInteger courseViews = (BigInteger) row[6];
			AllCourseRatingItem item = new AllCourseRatingItem(avatarUrl, courseOID.longValue(), courseName, courseLikes.longValue(), courseViews.longValue());
			data.add(item);
		}
		
		
		return data;
	}

}
