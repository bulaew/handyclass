package ru.prognoz.handy.impl.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.CourseStatusEntity;

@Repository
@Transactional
public class CourseStatusDao {

	@Autowired
	private SessionFactory sessionFactory;

	public CourseStatusEntity getByCode(int code) {
		Query q = sessionFactory.getCurrentSession().createQuery("from CourseStatusEntity where code = :code");
		q.setLong("code", code);
		return (CourseStatusEntity) q.uniqueResult();
	}
}
