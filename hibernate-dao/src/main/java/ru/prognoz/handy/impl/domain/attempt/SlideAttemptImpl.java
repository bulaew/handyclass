package ru.prognoz.handy.impl.domain.attempt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;

@Entity
@Table(name = "slide_attempt")
public class SlideAttemptImpl implements SlideAttempt {

	private static final long serialVersionUID = 1652800423884315367L;
	private long id;

	private HandyObjectID slideOID;

	private CourseAttempt courseAttempt;

	/**
	 * Дата первого просмотра.
	 */
	private Date firstView;

	/**
	 * Дата последнего просмотра
	 */
	private Date lastView;
	/**
	 * Затраченное время
	 */
	private long spendTime;
	/**
	 * Количество просмотров
	 */
	private int viewCounter;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "slide_oid")
	public HandyObjectID getSlideOid() {
		return slideOID;
	}

	@Override
	public void setSlideOid(HandyObjectID slideOID) {
		this.slideOID = slideOID;
	}

	@Override
	@ManyToOne(targetEntity = CourseAttemptImpl.class)
	@JoinColumn(name = "course_attempt_id")
	public CourseAttempt getCourseAttempt() {
		return courseAttempt;
	}

	@Override
	public void setCourseAttempt(CourseAttempt courseAttempt) {
		this.courseAttempt = courseAttempt;
	}

	@Override
	public Date getFirstView() {
		return firstView;
	}

	@Override
	public void setFirstView(Date firstView) {
		this.firstView = firstView;
	}

	public Date getLastView() {
		return lastView;
	}

	@Override
	public void setLastView(Date lastView) {
		this.lastView = lastView;
	}

	@Override
	public long getSpendTime() {
		return spendTime;
	}

	@Override
	public void setSpendTime(long spendTime) {
		this.spendTime = spendTime;
	}

	@Override
	public int getViewCounter() {
		return viewCounter;
	}

	@Override
	public void setViewCounter(int viewCounter) {
		this.viewCounter = viewCounter;
	}

}
