package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.slide.Slide;

@Entity
@Table(name = "sc_slide")
public class SlideImpl implements Slide {

	private static final long serialVersionUID = -2259728189557367801L;

	private long slideId;
	private HandyObjectID objectId;
	private HandyObjectID courseOID;
	private int position = 0;
	private int timer = 0;
	private boolean archive = false;

	@SuppressWarnings("unused")
	@Id
	@Column(name = "slideId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	private long getSlideId() {
		return slideId;
	}

	@SuppressWarnings("unused")
	private void setSlideId(long slideId) {
		this.slideId = slideId;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "oid")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID oid) {
		this.objectId = oid;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "courseoid")
	public HandyObjectID getCourseOID() {
		return courseOID;
	}

	@Override
	public void setCourseOID(HandyObjectID courseOID) {
		this.courseOID = courseOID;
	}

	@Override
	public int getPosition() {
		return position;
	}

	@Override
	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public int getTimer() {
		return timer;
	}

	@Override
	public void setTimer(int timer) {
		this.timer = timer;
	}
}
