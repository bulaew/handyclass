package ru.prognoz.handy.impl.domain.attempt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.dao.CourseStatusDao;
import ru.prognoz.handy.impl.domain.CourseStatusEntity;
import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;

@Entity
@Table(name = "course_attempt")
public class CourseAttemptImpl implements CourseAttempt {

	private static final long serialVersionUID = 8670096789843904578L;
	private int id;
	private HandyObjectID courseOID;
	private User user;
	private boolean complete = false;
	private Date completeDate;
	private CourseStatusEntity statusEntity;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "course_oid")
	public HandyObjectID getCourseOid() {
		return courseOID;
	}

	@Override
	public void setCourseOid(HandyObjectID courseOid) {
		this.courseOID = courseOid;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public boolean isComplete() {
		return complete;
	}

	@Override
	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	@Override
	@Transient
	public CourseStatus getStatus() {
		return CourseStatus.parseInt(getStatusEntity().getCode());
	}

	@Override
	@Transient
	public void setStatus(CourseStatus status) {
		CourseStatusEntity statusEntity = HandyclassAppContext.getBean(CourseStatusDao.class).getByCode(status.getIntValue());
		setStatusEntity(statusEntity);
	}

	@ManyToOne
	@JoinColumn(name = "course_status")
	public CourseStatusEntity getStatusEntity() {
		return statusEntity;
	}

	public void setStatusEntity(CourseStatusEntity statusEntity) {
		this.statusEntity = statusEntity;
	}

	@Override
	public Date getCompleteDate() {
		return completeDate;
	}

	@Override
	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

}
