package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.VideoImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.VideoDao;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.Video;

@Repository
@Transactional
public class VideoDaoImpl implements VideoDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Video newDomainInstatnce(Slide slide) {
		Video video = new VideoImpl();
		video.setVideoUrl("");
		video.setActive(false);
		video.setArchive(false);
		video.setType("video");
		video.setSlideOID(slide.getObjectId());
		video.setComponentOID(oidDao.generate());
		return video;
	}
}
