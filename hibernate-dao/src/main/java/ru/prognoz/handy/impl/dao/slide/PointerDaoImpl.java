package ru.prognoz.handy.impl.dao.slide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.PointerImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.slide.PointerDao;
import ru.prognoz.tc.domain.slide.Pointer;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class PointerDaoImpl implements PointerDao {

	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Pointer newDomainInstatnce(Slide slide, long parentOid, int x, int y) {
		Pointer pointer = new PointerImpl();
		pointer.setSlideOID(slide.getObjectId());
		pointer.setComponentOID(oidDao.generate());
		pointer.setX(x);
		pointer.setY(y);
		pointer.setParentOid(oidDao.getById(parentOid));
		pointer.setType("pointer");
		pointer.setActive(false);
		pointer.setArchive(false);
		pointer.setPointerType("up");
		return pointer;
	}
}
