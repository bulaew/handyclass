package ru.prognoz.handy.impl.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

@Entity
@Table(name = "course_registry")
public class CourseRegistrationImpl implements CourseRegistration {

	private static final long serialVersionUID = -1521790426510825171L;
	private long id;
	/**
	 * Курс на который пользователь записался.
	 */
	private HandyObjectID courseOID;
	/**
	 * Пользователь.
	 */
	private User user;
	/**
	 * Дата записи.
	 */
	private Date date;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "course_oid")
	public HandyObjectID getCourseOid() {
		return courseOID;
	}

	@Override
	public void setCourseOid(HandyObjectID courseOID) {
		this.courseOID = courseOID;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

}
