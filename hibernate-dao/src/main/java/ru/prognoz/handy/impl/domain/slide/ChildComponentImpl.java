package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.slide.ChildComponent;

@Entity
@Table(name = "sc_child_component")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "base")
public class ChildComponentImpl extends SlideComponentImpl implements ChildComponent {

	private static final long serialVersionUID = 2113099625097530571L;

	private HandyObjectID parentOID;

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "parent_oid")
	public HandyObjectID getParentOid() {
		return parentOID;
	}

	@Override
	public void setParentOid(HandyObjectID oid) {
		this.parentOID = oid;
	}

}
