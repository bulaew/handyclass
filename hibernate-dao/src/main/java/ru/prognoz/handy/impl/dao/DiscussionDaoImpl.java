package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.DiscussionImpl;
import ru.prognoz.tc.dao.DiscussionDao;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.domain.slide.ChildComponent;

@Repository
@Transactional
public class DiscussionDaoImpl implements DiscussionDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Discussion newDomainInstatnce() {
		return new DiscussionImpl();
	}

	@Override
	public Discussion getByOid(Long oid) {
		return (Discussion) sessionFactory.getCurrentSession().createCriteria(Discussion.class).add(Restrictions.eq("objectId.id", oid))
				.setMaxResults(1).uniqueResult();
	}

	@Override
	public void merge(Discussion discussion) {
		sessionFactory.getCurrentSession().merge(discussion);
	}

	@Override
	public void save(Discussion discussion) {
		sessionFactory.getCurrentSession().save(discussion);
	}

	@Override
	public List<Discussion> getByCourseOid(long oid) {
	    Query q = sessionFactory.getCurrentSession().createQuery("from DiscussionImpl where course.objectId.id = :objectId AND archive = false");
	    q.setLong("objectId", oid);
	    List<Discussion> list = q.list();
	    return list;
	}

}
