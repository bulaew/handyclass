package ru.prognoz.handy.impl.domain.course;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;

@Entity
@Table(name = "invites")
public class InviteImpl implements Invite {

	private static final long serialVersionUID = 1776645986917232349L;
	
	private long id;
	/**
	 * Курс на который пользователя пригласили.
	 */
	private Course course;
	/**
	 * Приглашенный пользователь.
	 */
	private User user;
	/**
	 * Емейл, по кторому приглашаются пользователи, незарегистрированные в системе.
	 */
	private String guestEmail;
	/**
	 * флаг принятия приглашения.
	 */
	private boolean confirm;
	/**
	 * флаг активного приглашения.
	 */
	private boolean active;
	/**
	 * хэшкод приглашения.
	 */
	private String inviteHashCode;
	/**
	 * Оповещение и емейл уже отправлен.
	 */
	private boolean inviteMsgSended;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(targetEntity = CourseImpl.class)
	@Override
	public Course getCourse() {
		return course;
	}

	@Override
	public void setCourse(Course course) {
		this.course = course;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@Override
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "guest_email")
	@Type(type = "text")
	@Override
	public String getGuestEmail() {
		return guestEmail;
	}

	@Override
	public void setGuestEmail(String guestEmail) {
		this.guestEmail = guestEmail;
	}

	@Column(name = "confirm")
	@Override
	public boolean isConfirm() {
		return confirm;
	}

	@Override
	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	@Column(name = "active")
	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "hashCode")
	@Override
	public String getInviteHashCode() {
		return inviteHashCode;
	}

	@Override
	public void setInviteHashCode(String inviteHashCode) {
		this.inviteHashCode = inviteHashCode;
	}

	@Override
	public boolean isInviteMsgSended() {
		return inviteMsgSended;
	}

	@Override
	public void setInviteMsgSended(boolean inviteMsgSended) {
		this.inviteMsgSended = inviteMsgSended;
	}

}
