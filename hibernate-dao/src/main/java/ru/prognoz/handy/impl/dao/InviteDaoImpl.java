package ru.prognoz.handy.impl.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.InviteImpl;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;

@Repository
@Transactional
public class InviteDaoImpl implements InviteDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Invite newDomainInstatnce() {
		return new InviteImpl();
	}

	@Override
	public void save(Invite invite) {
		sessionFactory.getCurrentSession().saveOrUpdate(invite);
	}

	@Override
	public void merge(Invite invite) {
		sessionFactory.getCurrentSession().merge(invite);
	}

	@Override
	public List<User> getInvitedUsers(Course course) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from InviteImpl where course.objectId.id = :courseId and active = true ORDER BY id ASC");
		q.setLong("courseId", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<User> list = q.list();
		return list;
	}

	@Override
	public Invite getById(long id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from InviteImpl where id = :id");
		q.setLong("id", id);
		return (Invite) q.uniqueResult();
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void removeInvite(Invite invite) {
		if (invite == null) {
			return;
		}
		removeFromCollection(invite, invite.getCourse().getInvites());
		if (invite.getUser() != null) {
			removeFromCollection(invite, invite.getUser().getInvites());
		}
		delete(invite);

	}

	public void delete(Invite invite) {
		sessionFactory.getCurrentSession().delete(invite);
	}

	private void removeFromCollection(final Invite removable, Collection<Invite> invites) {
		Invite find = null;
		for (Invite invite : invites) {
			if (invite.getId() == removable.getId()) {
				find = invite;
				break;
			}
		}
		if (find != null) {
			invites.remove(find);
		}
	}

	@Override
	public boolean isInvited(Course course, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from InviteImpl where user.id = :userId and course.objectId.id = :courseOid and active = true ORDER BY id ASC");
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<Invite> list = q.list();
		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public void removeInvite(Course course, User user) {
		Invite invite = getInvite(course, user);
		if (invite == null) {
			return;
		}
		removeInvite(invite);
	}
	
	public Invite getInvite(Course course, User user) {
		String hql = "FROM InviteImpl WHERE course.objectId.id = :courseOid and user.id = :userId";
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery(hql);
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<Invite> list = q.list();
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public Invite getInvite(Course course, String email) {
		String hql = "FROM InviteImpl WHERE course.objectId.id = :courseOid and guestEmail = :guestEmail";
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery(hql);
		q.setString("guestEmail", email);
		q.setLong("courseOid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<Invite> list = q.list();
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

}
