package ru.prognoz.handy.impl.domain.contact;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;

@Entity
@Table(name = "contact")
public class ContactImpl implements Contact {

	private long id;
	private HandyObjectID objectId;
	private boolean archive = false;
	private Date created;
	private Date modified;
	private User owner;
	private User user;
	private String email;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "object_id")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner")
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "email")
	@Type(type = "text")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
