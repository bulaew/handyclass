package ru.prognoz.handy.impl.domain.test;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.InheritanceType;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Question;

@Entity
@Table(name = "QUESTION_BASE")
@Inheritance(strategy = InheritanceType.JOINED)
public class QuestionImpl implements Question, Serializable {

	private static final long serialVersionUID = -9002060233530034642L;
	private long id;
	private HandyObjectID objectId;
	private HandyObjectID testOID;
	private int position;
	private String type;
	private String text;
	private boolean archive = false;
	private boolean active = false;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	protected long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "testOID")
	public HandyObjectID getTestOID() {
		return testOID;
	}

	@Override
	public void setTestOID(HandyObjectID testOID) {
		this.testOID = testOID;
	}

	@Override
	public int getPosition() {
		return position;
	}

	@Override
	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public String getQuestionType() {
		return type;
	}

	@Override
	public void setQuestionType(String type) {
		this.type = type;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	@JoinColumn(name = "oid")
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

}
