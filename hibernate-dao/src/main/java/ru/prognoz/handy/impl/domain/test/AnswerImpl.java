package ru.prognoz.handy.impl.domain.test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Answer;

@Entity
@Table(name = "answer")
public class AnswerImpl implements Answer {

	private static final long serialVersionUID = -5508798269819450659L;

	private long id;
	private HandyObjectID objectId;
	private HandyObjectID questionOID;
	private String text;
	private String value;
	private String type;
	private boolean archive = false;
	private boolean active = false;

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	protected long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "oid")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "question_oid")
	public HandyObjectID getQuestionOID() {
		return questionOID;
	}

	public void setQuestionOID(HandyObjectID questionOID) {
		this.questionOID = questionOID;
	}

	@Column(name = "text", length = 1024)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column(name = "value", length = 1024)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	public String getQuestionType() {
		return type;
	}

	@Override
	public void setQuestionType(String type) {
		this.type = type;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

}
