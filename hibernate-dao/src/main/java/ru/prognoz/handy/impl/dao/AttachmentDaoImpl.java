package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.AttachmentImpl;
import ru.prognoz.tc.dao.AttachmentDao;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;

@Repository
@Transactional
public class AttachmentDaoImpl implements AttachmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Attachment> getByCourse(Course course) {
		Query q = (Query) sessionFactory.getCurrentSession().createQuery("from AttachmentImpl where courseOid = :objectId ORDER BY id ASC");
		q.setLong("objectId", course.getObjectId().getId());
		List<Attachment> list = q.list();
		return list;
	}

	@Override
	public Attachment newDomainInstatnce() {
		return new AttachmentImpl();
	}

	@Override
	public void merge(Attachment attachment) {
		sessionFactory.getCurrentSession().merge(attachment);
	}

}
