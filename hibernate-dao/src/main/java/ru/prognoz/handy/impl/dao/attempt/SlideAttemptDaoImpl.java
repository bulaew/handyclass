package ru.prognoz.handy.impl.dao.attempt;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.CourseAttemptImpl;
import ru.prognoz.handy.impl.domain.attempt.SlideAttemptImpl;
import ru.prognoz.tc.dao.attempt.SlideAttemptDao;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class SlideAttemptDaoImpl implements SlideAttemptDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void save(SlideAttempt slideAttempt) {
		sessionFactory.getCurrentSession().saveOrUpdate(slideAttempt);
	}

	@Override
	public void merge(SlideAttempt slideAttempt) {
		sessionFactory.getCurrentSession().merge(slideAttempt);
	}

	@Override
	public void delete(SlideAttempt slideAttempt) {
		sessionFactory.getCurrentSession().delete(slideAttempt);
	}

	@Override
	public List<SlideAttempt> list(CourseAttempt courseAttempt) {
		CourseAttemptImpl caImpl = (CourseAttemptImpl) courseAttempt;
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from SlideAttemptImpl where courseAttempt = :courseAttemptId");
		q.setLong("courseAttemptId", caImpl.getId());
		@SuppressWarnings("unchecked")
		List<SlideAttempt> list = q.list();
		return list;
	}

	@Override
	public SlideAttempt getOrCreate(CourseAttempt courseAttempt, Slide slide) {
		CourseAttemptImpl caImpl = (CourseAttemptImpl) courseAttempt;
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from SlideAttemptImpl where courseAttempt = :courseAttemptId and slideOid = :slideOid");
		q.setLong("courseAttemptId", caImpl.getId());
		q.setLong("slideOid", slide.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<SlideAttempt> list = q.list();
		if (list.size() > 0) {
			return list.get(0);
		} else { //создаем
			SlideAttempt slideAttempt = new SlideAttemptImpl();
			slideAttempt.setCourseAttempt(courseAttempt);
			slideAttempt.setSpendTime(0);
			slideAttempt.setViewCounter(0);
			slideAttempt.setSlideOid(slide.getObjectId());
			sessionFactory.getCurrentSession().save(slideAttempt);
			return slideAttempt;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SlideAttempt> getAttempts(Long slideOID) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from SlideAttemptImpl where slideOid = :slideOid");
		q.setLong("slideOid", slideOID);
		return q.list();
	}

}
