package ru.prognoz.handy.impl.domain.course;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.prognoz.tc.domain.course.AccessType;

@Entity
@Table(name = "access_type")
public class AccessTypeImpl implements AccessType {

	private static final long serialVersionUID = -6279012371388070566L;
	private long id;
	private String description;

	@Id
	@Column(name = "id")
	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "description")
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

}
