package ru.prognoz.handy.impl.reports;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.courseinfo.CourseInfoCriteria;
import ru.prognoz.tc.core.reports.impl.courseinfo.CourseInfoItem;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.User;

@Repository
@Transactional
public class CourseInfoPipeline implements ReportPipeline {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private UserDao userDao;

	@Override
	public ReportType getType() {
		return ReportType.COURSE_INFO;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Обучение по курсу\"");
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		List<ReportItem> data = new ArrayList<ReportItem>();
		if (searchCriteria == null) {
			return data;
		}
		CourseInfoCriteria criteria = (CourseInfoCriteria) searchCriteria;
		if (criteria.getFilterCourseOID() == 0) {
			return data;
		}
		
		//@formatter:off
		String fullSql =
		"select "+
			"test_data.registered_course_id, "+
			"test_data.registered_user, "+
			"(select code as course_status from course_status where course_status.id = course_attempt.course_status), "+
			"test_attempt.bestresult, "+
			"((select sum(slide_attempt.spendtime) from slide_attempt where slide_attempt.course_attempt_id = course_attempt.id)+test_attempt.totaltime) as learntime, "+
			"(select min(slide_attempt.firstview) from slide_attempt where slide_attempt.course_attempt_id = course_attempt.id), "+
			"(select max(slide_attempt.lastview) from slide_attempt where slide_attempt.course_attempt_id = course_attempt.id) "+
		"from  "+
			"(select "+ 
				"reg_data.*, "+
				"test.oid as test_oid, "+ 
				"test.courseoid, "+
				"(select courseid as registered_course_id from course where course.object_id = :courseOID) "+
			"from "+ 
				"(select user_id as registered_user  from course_registry where course_oid = :courseOID) reg_data, "+
				"test "+ 
			"where test.courseoid = :courseOID) "+
			"test_data "+
		"left join "+
			"test_attempt "+
		"on "+
			"test_attempt.testoid_id = test_data.test_oid and test_attempt.user_id = test_data.registered_user "+
		"left join "+
			"course_attempt "+
		"on "+
			"course_attempt.course_oid = :courseOID and course_attempt.user_id = test_data.registered_user ";

		//@formatter:on
				
		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSql);
		query.setParameter("courseOID", criteria.getFilterCourseOID());
		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}
		List<Object[]> queryList = new ArrayList<Object[]>();
		queryList = query.list();
		for (Object[] row : queryList) {
			CourseInfoItem item = new CourseInfoItem();
			// BigInteger courseID = (BigInteger) row[0];
			BigInteger userID = (BigInteger) row[1];
			Integer courseStatus = (Integer) row[2];
			Integer bestResult = (Integer) row[3];
			BigDecimal learnTime = (BigDecimal) row[4];
			Date dateStart = (Date) row[5];
			Date dateEnd = (Date) row[6];

			item.setUserID(userID.intValue());
			User user = userDao.getById(userID.longValue());
			item.setUserFIO(user.getUsername());
			item.setStatus(  CourseStatus.parseInt(courseStatus));
			item.setBestResult(bestResult== null ? 0 : bestResult.doubleValue());
			item.setLearnTime(learnTime == null ? 0 : learnTime.longValue());
			item.setDateStart(dateStart);
			item.setDateEnd(dateEnd);

			data.add(item);
		}
		return data;
	}
}
