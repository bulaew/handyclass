package ru.prognoz.handy.impl.dao.contact;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.contact.ContactRelevoImpl;
import ru.prognoz.tc.dao.contact.ContactRelevoDao;
import ru.prognoz.tc.domain.contact.ContactRelevo;

@Repository
@Transactional
public class ContactRelevoDaoImpl implements ContactRelevoDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ContactRelevo newDomainInstance() {
		return new ContactRelevoImpl();
	}

	@Override
	public ContactRelevo getByContact(long contactOid) {
		Session s = sessionFactory.getCurrentSession();
		Query q = s.createQuery("from ContactRelevoImpl where contactOID.id = :objectId");
		q.setLong("objectId", contactOid);
		return (ContactRelevo) q.uniqueResult();
	}

	@Override
	public void save(ContactRelevo relevo) {
		sessionFactory.getCurrentSession().saveOrUpdate(relevo);
	}

	@Override
	public void merge(ContactRelevo relevo) {
		sessionFactory.getCurrentSession().merge(relevo);
	}

}
