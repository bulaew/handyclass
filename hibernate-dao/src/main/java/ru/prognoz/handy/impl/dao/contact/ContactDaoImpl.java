package ru.prognoz.handy.impl.dao.contact;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import ru.prognoz.handy.impl.domain.contact.ContactImpl;
import ru.prognoz.tc.dao.contact.ContactDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;

@Repository
@Transactional
public class ContactDaoImpl implements ContactDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Contact getByOID(long oid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from ContactImpl where objectId.id = :oid");
		q.setLong("oid", oid);
		return (Contact) q.uniqueResult();
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public Contact search(User owner, String email) {
		Contact result = __searchByLogin(owner, email);
		if (result == null) {
			result = __searchByEmail(owner, email);
		}
		return result;
	}
	
	private Contact __searchByLogin(User owner, String login) {
		if (StringUtils.isEmpty(login)) {
			return null;
		}
		Session s = sessionFactory.getCurrentSession();

		Query q = s.createQuery("from ContactImpl where owner.id = :ownerID and user.login = :userEmail and archive != true ORDER BY id ASC");
		q.setString("userEmail", login);
		q.setLong("ownerID", owner.getId());
		return (Contact) q.uniqueResult();
	}

	private Contact __searchByEmail(User owner, String email) {
		if (StringUtils.isEmpty(email)) {
			return null;
		}
		Session s = sessionFactory.getCurrentSession();

		Query q = s.createQuery("from ContactImpl where owner.id = :ownerID and email = :guestEmail and archive != true ORDER BY id ASC");
		q.setString("guestEmail", email);
		q.setLong("ownerID", owner.getId());
		return (Contact) q.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contact> getByOwner(User owner) {
		Query q = sessionFactory.getCurrentSession().createQuery("from ContactImpl where owner.id = :ownerID and archive != true ORDER BY id ASC");
		q.setLong("ownerID", owner.getId());
		return q.list();
	}

	@Override
	public Contact newDomainInstance() {
		return new ContactImpl();
	}

	@Override
	public void save(Contact contact) {
		sessionFactory.getCurrentSession().save(contact);
	}

	@Override
	public void merge(Contact contact) {
		sessionFactory.getCurrentSession().merge(contact);
	}

}
