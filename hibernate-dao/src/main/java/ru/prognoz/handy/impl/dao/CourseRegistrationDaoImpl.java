package ru.prognoz.handy.impl.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.CourseRegistrationImpl;
import ru.prognoz.tc.dao.CourseRegistrationDao;
import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;

@Repository
@Transactional
public class CourseRegistrationDaoImpl implements CourseRegistrationDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CourseRegistration newDomainInstance() {
		return new CourseRegistrationImpl();
	}

	@Override
	public void merge(CourseRegistration registry) {
		sessionFactory.getCurrentSession().merge(registry);
	}

	@Override
	public void save(CourseRegistration registry) {
		sessionFactory.getCurrentSession().saveOrUpdate(registry);
	}
	
	@Override
	public void delete(CourseRegistration registry) {
		sessionFactory.getCurrentSession().delete(registry);
	}

	@Override
	public boolean isRegistered(Course course, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseRegistrationImpl where user.id = :userId and courseOid = :courseOid ORDER BY id ASC");
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<CourseRegistration> list = q.list();
		if (list.size() == 1) {
			return true;
		}
		return false;
	}

	@Override
	public CourseRegistration getRegistry(Course course, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from CourseRegistrationImpl where user.id = :userId and courseOid = :courseOid ORDER BY id ASC");
		q.setLong("userId", user.getId());
		q.setLong("courseOid", course.getObjectId().getId());
		return (CourseRegistration) q.uniqueResult();
	}

	@Override
	public List<CourseRegistration> getUserRegistrations(User user) {
		Query q = sessionFactory.getCurrentSession().createQuery("from CourseRegistrationImpl where user.id = :userId ORDER BY id ASC");
		q.setLong("userId", user.getId());
		@SuppressWarnings("unchecked")
		List<CourseRegistration> list = q.list();
		return list;
	}

	@Override
	public int getRegisteredCount(Course course) {
		int result = 0;
		Query q = sessionFactory.getCurrentSession().createQuery("select count(*) from CourseRegistrationImpl where courseOid = :oid");
		q.setLong("oid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<Long> likesCount = q.list();
		if (likesCount.size() > 0) {
			result = likesCount.get(0).intValue();
		}
		return result;
	}

	@Override
	public List<CourseRegistration> listRegistrations(Course course) {
		if (course == null || course.getObjectId() == null) {
			return new ArrayList<CourseRegistration>();
		}
		Query q = sessionFactory.getCurrentSession().createQuery("from CourseRegistrationImpl where courseOid = :oid");
		q.setLong("oid", course.getObjectId().getId());
		@SuppressWarnings("unchecked")
		List<CourseRegistration> list = q.list();
		return list;
	}

}
