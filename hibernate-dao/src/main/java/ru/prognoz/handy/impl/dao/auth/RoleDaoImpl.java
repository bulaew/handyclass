package ru.prognoz.handy.impl.dao.auth;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.RoleImpl;
import ru.prognoz.tc.dao.auth.RoleDao;
import ru.prognoz.tc.domain.Role;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Role newDomainInstance() {
		return new RoleImpl();
	}

	@Override
	public void save(Role role) {
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}

	@Override
	public Role getByAuthority(String authority) {
		Query q = sessionFactory.getCurrentSession().createQuery("from RoleImpl where authority = :authority");
		q.setString("authority", authority);
		return (RoleImpl) q.uniqueResult();
	}

}
