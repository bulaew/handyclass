package ru.prognoz.handy.impl.dao.slide;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.dao.SlideComponentDao;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;

@Repository
@Transactional
public class SlideComponentDaoImpl implements SlideComponentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public SlideComponent getByOid() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SlideComponent> getBySlide(Slide slide) {
		return getBySlide(slide.getObjectId().getId());
	}

	@Override
	public List<SlideComponent> getBySlide(Long slideOid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from SlideComponentImpl where slideoid = :objectId AND archive = false AND type !='pointer' AND type !='marker'");
		q.setLong("objectId", slideOid);
		List<SlideComponent> list = q.list();
		return list;
	}

	@Override
	public void merge(SlideComponent component) {
		sessionFactory.getCurrentSession().merge(component);
	}

	@Override
	public List<SlideComponent> getVisibleBySlide(Slide slide) {
		return getVisibleBySlide(slide.getObjectId().getId());
	}

	@Override
	public List<SlideComponent> getVisibleBySlide(Long slideOid) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from SlideComponentImpl where slideoid = :objectId AND archive = false AND type !='pointer' AND type !='marker'");
		q.setLong("objectId", slideOid);
		List<SlideComponent> list = q.list();
		return list;
	}

	@Override
	public List<ChildComponent> getChildren(long parentOid) {
		Query q = sessionFactory.getCurrentSession().createQuery("from ChildComponentImpl where parentOid = :objectId AND archive = false");
		q.setLong("objectId", parentOid);
		List<ChildComponent> list = q.list();
		return list;
	}
}
