package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Entity;
import javax.persistence.Table;

import ru.prognoz.tc.domain.slide.Marker;

@Entity
@Table(name = "sc_marker")
public class MarkerImpl extends ChildComponentImpl implements Marker {
	private static final long serialVersionUID = -3795684975339822324L;
	private int x;
	private int y;
	private int width;
	private int height;

	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public int getHeight() {
		return height;
	}

}