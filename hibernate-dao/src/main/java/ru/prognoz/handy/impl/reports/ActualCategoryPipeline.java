package ru.prognoz.handy.impl.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.common.WhereBuilder;
import ru.prognoz.tc.core.reports.common.WhereBuilder.ConditionParam;
import ru.prognoz.tc.core.reports.flow.ReportPipeline;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryCriteria;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryItem;

@Repository
@Transactional
public class ActualCategoryPipeline implements ReportPipeline {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ReportType getType() {
		return ReportType.ACTUAL_CATEGORY;
	}

	@Override
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException {
		try {
			return loadData(criteria);
		} catch (HibernateException e) {
			throw new HandyCoreException("Ошибка формирования данных для отчета \"Актуальные категории\"");
		}
	}

	@SuppressWarnings("unchecked")
	private List<ReportItem> loadData(SearchCriteria searchCriteria) {
		ActualCategoryCriteria criteria = (ActualCategoryCriteria) searchCriteria;
		List<ReportItem> data = new ArrayList<ReportItem>();

		//@formatter:off
String fullSql =
		"select "+
		"category_id," +
		"eng_title," +
		"ru_title," +
		"(case when(count_registry = 0 or count_registry is null) then 0 else count_complete_attempts/count_registry end) as part "+
	"from "+
		"(select "+ 
			"category_courses.category_id, "+
			"category_courses.eng_title, "+
			"category_courses.ru_title, "+
			"sum(count_complete_attempts) as count_complete_attempts, "+
			"sum(count_registry) as count_registry "+
		"from "+
			"(select all_courses.*, "+
				"(select count(course_attempt.id) from course_attempt where course_attempt.course_oid = all_courses.object_id and (course_attempt.course_status in (4,5))) as count_complete_attempts, "+
				"(select count(course_registry.id) from course_registry where course_registry.course_oid = all_courses.object_id ) as count_registry "+
			"from "+
				"(select "+ 
					"course.courseid, "+
					"course.object_id, "+
					"course.category_id, "+
					"all_category.eng_title, "+
					"all_category.ru_title, "+
					"course.full_name "+
				"from  "+
					"(select course_category.id, course_category.eng_title, course_category.ru_title from course_category) all_category, "+
					"course "+
				"where course.category_id = all_category.id and course.archive != 'true' ) all_courses) category_courses "+
		"group by category_courses.eng_title, category_courses.ru_title, category_courses.category_id)  "+
		"complete_query ";
		//@formatter:on
		WhereBuilder whereBuilder = new WhereBuilder();
		// Добавляем условие для поиска.
		try {
			Double findValue = Double.parseDouble(criteria.getFindValue());
			whereBuilder.addMultiCondition("(ru_title LIKE :strFind OR part = :doubleFind) ",
					new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"), new ConditionParam("doubleFind", findValue));

		} catch (NumberFormatException exc) {
			whereBuilder.addMultiCondition("(ru_title LIKE :strFind) ", new ConditionParam("strFind", "%" + criteria.getFindValue() + "%"));
		}
		fullSql += whereBuilder.getWhere();
		// Условия для сортировки.
		String order = "";
		if (StringUtils.isEmpty(criteria.getOrderColumn()) || ActualCategoryItem.COLUMN_CATEGORY_TITLE.equals(criteria.getOrderColumn())) {
			order += "ORDER BY ru_title";
		} else if (ActualCategoryItem.COLUMN_COMPLETE_PART.equals(criteria.getOrderColumn())) {
			order += "ORDER BY part";
		}
		// порядок сортировки
		if (!order.isEmpty()) {
			if (SortDirection.ASC.equals(criteria.getSortDirection())) {
				fullSql += order + " ASC ";
			} else {
				fullSql += order + " DESC ";
			}
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(fullSql);
		// Устанавливаем параметры запроса.
		Iterator<String> keyIterator = whereBuilder.getParams().keySet().iterator();
		while (keyIterator.hasNext()) {
			String key = keyIterator.next();
			Object value = whereBuilder.getParams().get(key);
			query.setParameter(key, value);
		}
		if (criteria.getRowCount() > 0) {
			query.setMaxResults(criteria.getRowCount());
		}
		List<Object[]> queryList = new ArrayList<Object[]>();
		try {
			queryList = query.list();
		} catch (HibernateException e) {
			throw e;
		}
		
		for (Object[] row : queryList) {
			ActualCategoryItem item = new ActualCategoryItem();
			Integer categoryID = (Integer) row[0];
			String categoryTitle = (String) row[1];
			String categoryRuTitle = (String) row[2];
			BigDecimal completePart = (BigDecimal) row[3];

			item.setCategoryId(categoryID);
			if (criteria.getLocale().getLanguage().equals("ru")) {
				item.setCategoryTitle(categoryRuTitle);
			} else {
				item.setCategoryTitle(categoryTitle);
			}
			item.setCompletePart(completePart == null ? 0 : completePart.doubleValue());
			data.add(item);
		}
		return data;
	}

}
