package ru.prognoz.handy.impl.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ru.prognoz.tc.domain.HandyComment;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

/**
 * Комментарий
 * 
 * @author mikulich
 * 
 */
@Entity
@Table(name = "comment")
public class HandyCommentImpl implements HandyComment {

	private static final long serialVersionUID = -6383183654461855662L;

	private long id;

	private HandyObjectID objectId;

	private User owner;

	private String content;

	private Date createDate;

	private Date modifiedDate;

	private HandyObjectID parentOID;

	private boolean archived;

	@Id
	@Column(name = "commentId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	public User getOwner() {
		return owner;
	}

	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
	@Type(type = "text")
	public String getContent() {
		return content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public Date getCreateDate() {
		return createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "parent_oid")
	public HandyObjectID getParentOid() {
		return parentOID;
	}

	@Override
	public void setParentOid(HandyObjectID parentOID) {
		this.parentOID = parentOID;
	}

	@Override
	public boolean isArchive() {
		return archived;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archived = archive;
	}
}
