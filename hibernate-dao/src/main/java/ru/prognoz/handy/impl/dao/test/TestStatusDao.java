package ru.prognoz.handy.impl.dao.test;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.TestStatusEntity;

@Repository
@Transactional
public class TestStatusDao {
	@Autowired
	private SessionFactory sessionFactory;

	public TestStatusEntity getById(long id) {
		return (TestStatusEntity) sessionFactory.getCurrentSession().get(TestStatusEntity.class, id);
	}
}
