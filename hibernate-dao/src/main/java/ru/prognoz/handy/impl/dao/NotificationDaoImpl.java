package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.NotificationImpl;
import ru.prognoz.tc.dao.NotificationDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Notification;

@Repository
@Transactional
public class NotificationDaoImpl implements NotificationDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Notification> getByReceiver(User receiver) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from NotificationImpl where receiver = :userId ORDER BY notificationId ASC");
		q.setLong("userId", receiver.getId());
		@SuppressWarnings("unchecked")
		List<Notification> list = q.list();
		return list;
	}

	@Override
	public Notification newDomainInstance() {
		return new NotificationImpl();
	}

	@Override
	public void save(Notification notification) {
		sessionFactory.getCurrentSession().saveOrUpdate(notification);
	}

	@Override
	public void merge(Notification notification) {
		sessionFactory.getCurrentSession().merge(notification);
	}

	@Override
	public void delete(Notification notification) {
		sessionFactory.getCurrentSession().delete(notification);
	}

}
