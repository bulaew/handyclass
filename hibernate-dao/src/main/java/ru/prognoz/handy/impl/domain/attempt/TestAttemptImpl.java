package ru.prognoz.handy.impl.domain.attempt;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.dao.test.TestStatusDao;
import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.data.TestStatus;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.TestAttempt;

@Entity
@Table(name = "test_attempt")
public class TestAttemptImpl implements TestAttempt {

	private static final long serialVersionUID = 8777994865729612291L;
	private long id;

	/**
	 * количество попыток
	 */
	private int countAttempts;
	/**
	 * результат лучшей попытки.
	 */
	private int bestResult;
	/**
	 * результат последней попытки.
	 */
	private int lastResult;
	/**
	 * статус теста после лучшей попытки. 1. тест не начат 2. тест запускался 3. тест не пройден 4. тест выполнен
	 */
	private TestStatusEntity statusEntity;
	/**
	 * затраченное время в лучшей попытке.
	 */
	private long spendedTime;
	/**
	 * Полное время на всех попытках.
	 */
	private long totalTime;

	/**
	 * Курс.
	 */
	private HandyObjectID testOID;
	/**
	 * пользователь.
	 */
	private User user;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	public HandyObjectID getTestOid() {
		return testOID;
	}

	@Override
	public void setTestOid(HandyObjectID testOID) {
		this.testOID = testOID;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	public User getUser() {
		return user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int getCountAttempts() {
		return countAttempts;
	}

	@Override
	public void setCountAttempts(int countAttempts) {
		this.countAttempts = countAttempts;	
	}

	@Override
	public int getBestResult() {
		return bestResult;
	}

	@Override
	public void setBestResult(int bestResult) {
		this.bestResult = bestResult;
	}

	@ManyToOne
	@JoinColumn(name = "test_status")
	private TestStatusEntity getStatusEntity() {
		return statusEntity;
	}

	private void setStatusEntity(TestStatusEntity statusEntity) {
		this.statusEntity = statusEntity;
	}
	
	@Override
	@Transient
	public TestStatus getStatus() {
		if (getStatusEntity() == null) {
			return null;
		}
		return TestStatus.parseInt((int) getStatusEntity().getId());
	}

	@Override
	@Transient
	public void setStatus(TestStatus testStatus) {
		TestStatusEntity statusEntity = HandyclassAppContext.getBean(TestStatusDao.class).getById((long) testStatus.getIntValue());
		setStatusEntity(statusEntity);
	}

	@Override
	public long getSpendedTime() {
		return spendedTime;
	}

	@Override
	public void setSpendedTime(long spendedTime) {
		this.spendedTime = spendedTime;
	}

	@Override
	public long getTotalTime() {
		return totalTime;
	}

	@Override
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

	@Override
	public int getLastResult() {
		return lastResult;
	}

	@Override
	public void setLastResult(int lastResult) {
		this.lastResult = lastResult;
	}

}
