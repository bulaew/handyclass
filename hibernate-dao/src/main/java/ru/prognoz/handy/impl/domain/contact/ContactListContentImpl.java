package ru.prognoz.handy.impl.domain.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.contact.ContactListContent;

@Entity
@Table(name = "contact_list_content")
public class ContactListContentImpl implements ContactListContent {
	
	private long id;
	private HandyObjectID listOID;
	private HandyObjectID contactOID;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	public HandyObjectID getListOID() {
		return listOID;
	}

	@Override
	public void setListOID(HandyObjectID listOID) {
		this.listOID = listOID;
		
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	public HandyObjectID getContactOID() {
		return contactOID;
	}

	@Override
	public void setContactOID(HandyObjectID contactOID) {
		this.contactOID = contactOID;
	}

}
