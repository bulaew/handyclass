package ru.prognoz.handy.impl.domain.slide;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.slide.SlideComponent;

@Entity
@Table(name = "sc_component")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "base")
public class SlideComponentImpl implements SlideComponent {

	private static final long serialVersionUID = -4508246751638899605L;

	private long componentId;
	private boolean archive;
	private boolean active;
	private HandyObjectID componentOid;
	private HandyObjectID slideOid;
	private int position;
	private String type;

	@Id
	@Column(name = "slideId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	protected long getComponentId() {
		return componentId;
	}

	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "oid")
	public HandyObjectID getComponentOID() {
		return componentOid;
	}

	@Override
	public void setComponentOID(HandyObjectID componentOID) {
		this.componentOid = componentOID;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "slideOID")
	public HandyObjectID getSlideOID() {
		return slideOid;
	}

	@Override
	public void setSlideOID(HandyObjectID slideOID) {
		this.slideOid = slideOID;
	}

	@Override
	public int getPosition() {
		return position;
	}

	@Override
	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getType() {
		return type;
	}
}