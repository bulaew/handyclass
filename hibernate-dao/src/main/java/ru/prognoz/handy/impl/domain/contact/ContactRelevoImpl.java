package ru.prognoz.handy.impl.domain.contact;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.ContactRelevo;

@Entity
@Table(name = "contact_relevo")
public class ContactRelevoImpl implements ContactRelevo {

	private static final long serialVersionUID = 5187008907752285285L;
	// Primary key
	private long id;
	// Object id
	private HandyObjectID objectId;
	// Владелец контактов.
	private User owner;
	// ОИД контакта
	private HandyObjectID contactOID;
	// частота использования контакта данным пользователем.
	private long inviteFreq;
	
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "owner_id")
	public User getOwner() {
		return owner;
	}

	@Override
	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
	@OneToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "contact_oid")
	public HandyObjectID getContactOID() {
		return contactOID;
	}

	@Override
	public void setContactOID(HandyObjectID contactOID) {
		this.contactOID = contactOID;
	}

	@Override
	public long getInviteFreq() {
		return inviteFreq;
	}

	@Override
	public void setInviteFreq(long inviteFreq) {
		this.inviteFreq = inviteFreq;
	}

}
