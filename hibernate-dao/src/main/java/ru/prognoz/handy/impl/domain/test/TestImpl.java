package ru.prognoz.handy.impl.domain.test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Test;

@Entity
@Table(name = "test")
public class TestImpl implements Test {

	private static final long serialVersionUID = 4881907095630058327L;

	private long testId;
	private HandyObjectID objectId;
	private HandyObjectID courseOID;
	private boolean isEnable;
	private int passingScore;
	private boolean archive = false;

	@SuppressWarnings("unused")
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	private long getTestId() {
		return testId;
	}

	@SuppressWarnings("unused")
	private void setTestId(long testId) {
		this.testId = testId;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "oid")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@Override
	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "courseOID")
	public HandyObjectID getCourseOID() {
		return courseOID;
	}

	@Override
	public void setCourseOID(HandyObjectID courseOID) {
		this.courseOID = courseOID;
	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	@Override
	public int getPassingScore() {
		return passingScore;
	}

	@Override
	public void setPassingScore(int passingScore) {
		this.passingScore = passingScore;
	}

}
