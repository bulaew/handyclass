package ru.prognoz.handy.impl.dao.test;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.test.TestImpl;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Test;

@Repository
@Transactional
public class TestDaoImpl implements TestDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HandyObjectDao oidDao;

	@Override
	public Test newDomainInstance(Course course) {
		return newDomainInstance(course.getObjectId());
	}

	@Override
	public Test newDomainInstance(HandyObjectID courseOid) {
		Test test = new TestImpl();
		test.setArchive(false);
		test.setCourseOID(courseOid);
		test.setEnable(false);
		test.setObjectId(oidDao.generate());
		test.setPassingScore(0);
		return test;
	}

	@Override
	public Test getByCourse(Course course) {
		return getByCourse(course.getObjectId());
	}

	@Override
	public Test getByCourse(Long courseOID) {
		Query q = sessionFactory.getCurrentSession().createQuery("from TestImpl where courseOID = :objectId ORDER BY id ASC");
		q.setLong("objectId", courseOID);
		return (Test) q.uniqueResult();
	}

	@Override
	public Test getByCourse(HandyObjectID courseOID) {
		return getByCourse(courseOID.getId());
	}

	@Override
	public Test getByObjectId(long OID) {
		Query q = sessionFactory.getCurrentSession().createQuery("from TestImpl where objectId = :objectId ORDER BY id ASC");
		q.setLong("objectId", OID);
		return (Test) q.uniqueResult();
	}

	@Override
	public Test getByObjectId(HandyObjectID OID) {
		return getByObjectId(OID.getId());
	}

	@Override
	public void merge(Test test) {
		sessionFactory.getCurrentSession().merge(test);
	}

}
