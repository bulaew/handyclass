package ru.prognoz.handy.impl.dao.attempt;

import java.util.Random;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.attempt.UserAnswerImpl;
import ru.prognoz.tc.dao.attempt.UserAnswerDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.UserAnswer;
import ru.prognoz.tc.domain.test.Answer;

@Repository
@Transactional
public class UserAnswerDaoImpl implements UserAnswerDao {

	private static final Random RANDOM = new Random();

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void save(UserAnswer userAnswer) {
		sessionFactory.getCurrentSession().saveOrUpdate(userAnswer);
	}

	@Override
	public void merge(UserAnswer userAnswer) {
		sessionFactory.getCurrentSession().merge(userAnswer);
	}

	@Override
	public void delete(UserAnswer userAnswer) {
		sessionFactory.getCurrentSession().delete(userAnswer);
	}

	@Override
	public UserAnswer getOrCreate(Answer answer, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery("from UserAnswerImpl where answerOid = :answerOID and user = :userID");
		q.setLong("answerOID", answer.getObjectId().getId());
		q.setLong("userID", user.getId());
		UserAnswer userAnswer = (UserAnswer) q.uniqueResult();
		if (userAnswer == null) { // создаем
			userAnswer = new UserAnswerImpl();
			userAnswer.setAnswerOid(answer.getObjectId());
			userAnswer.setUser(user);
			userAnswer.setViewPosition(RANDOM.nextInt(999999999));
			sessionFactory.getCurrentSession().save(userAnswer);
		}
		return userAnswer;
	}

	@Override
	public UserAnswer find(Answer answer, User user) {
		Query q = sessionFactory.getCurrentSession().createQuery("from UserAnswerImpl where answerOid = :answerOID and user = :userID");
		q.setLong("answerOID", answer.getObjectId().getId());
		q.setLong("userID", user.getId());
		return (UserAnswer) q.uniqueResult();
	}

}
