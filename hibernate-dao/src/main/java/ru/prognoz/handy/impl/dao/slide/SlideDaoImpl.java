package ru.prognoz.handy.impl.dao.slide;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.slide.SlideImpl;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.slide.Slide;

@Repository
@Transactional
public class SlideDaoImpl implements SlideDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Slide> getAllByCourse(Long courseOID) {
		Query q = sessionFactory.getCurrentSession().createQuery("from SlideImpl where courseOID = :objectId ORDER BY slideId ASC");
		q.setLong("objectId", courseOID);
		List<Slide> list = q.list();
		return list;
	}

	@Override
	public Slide getByOid(HandyObjectID oid) {

		return null;
	}

	@Override
	public Slide newDomainInstatnce() {
		return new SlideImpl();
	}

	@Override
	public void merge(Slide slide) {
		sessionFactory.getCurrentSession().merge(slide);
	}

	@Override
	public List<Slide> getAllVisibleByCourse(Long courseOID) {
		Query q = sessionFactory.getCurrentSession().createQuery(
				"from SlideImpl where courseOID = :objectId AND archive = false ORDER BY slideId ASC");
		q.setLong("objectId", courseOID);
		List<Slide> list = q.list();
		return list;
	}
}
