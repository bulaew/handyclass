package ru.prognoz.handy.impl.domain.course;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Notification;

@Entity
@Table(name = "Notification")
public class NotificationImpl implements Notification {

	/**
	 * ID
	 */
	private long notificationId;
	/**
	 * Уникальное ИД во всей системе Хендикласс.
	 */
	private HandyObjectID objectId;
	/**
	 * Курс, сгенерировавший оповещение.
	 */
	private HandyObjectID courseOID;
	/**
	 * обсуждение, сгенерировавший оповещение.
	 */
	private HandyObjectID discussionOID;
	/**
	 * Комментарий, сгенерировавший оповещение.
	 */
	private HandyObjectID commentOID;
	/**
	 * Лайк, сгенерировавший оповещение.
	 */
	private HandyObjectID likeOID;
	/**
	 * Тип оповещения.
	 */
	private int notificationType;
	/**
	 * Получатель оповещения.
	 */
	private User receiver;
	/**
	 * Пользователь, действие которго сгенерировало оповещение.
	 */
	private User person;
	/**
	 * Время, когда было сгенерировано оповещение.
	 */
	private Date date;

	@Id
	@Column(name = "notificationId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	private long getNotificationId() {
		return notificationId;
	}

	@SuppressWarnings("unused")
	private void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "oid")
	public HandyObjectID getObjectId() {
		return objectId;
	}

	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "receiver_id")
	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "courseOID")
	public HandyObjectID getCourseOID() {
		return courseOID;
	}

	public void setCourseOID(HandyObjectID courseOID) {
		this.courseOID = courseOID;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "discussionOID")
	public HandyObjectID getDiscussionOID() {
		return discussionOID;
	}

	public void setDiscussionOID(HandyObjectID discussionOID) {
		this.discussionOID = discussionOID;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "commentOID")
	public HandyObjectID getCommentOID() {
		return commentOID;
	}

	public void setCommentOID(HandyObjectID commentOID) {
		this.commentOID = commentOID;
	}

	@ManyToOne(targetEntity = HandyObjectIDImpl.class)
	@JoinColumn(name = "likeOID")
	public HandyObjectID getLikeOID() {
		return likeOID;
	}

	public void setLikeOID(HandyObjectID likeOID) {
		this.likeOID = likeOID;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "person_id")
	public User getPerson() {
		return person;
	}

	public void setPerson(User person) {
		this.person = person;
	}

	public int getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(int notificationType) {
		this.notificationType = notificationType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
