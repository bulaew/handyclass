package ru.prognoz.handy.impl.domain.course;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import ru.prognoz.tc.domain.course.CourseCategory;

@Entity
@Table(name = "course_category")
public class CourseCategoryImpl implements CourseCategory {

	private static final long serialVersionUID = 7537324832419158328L;
	private int id;
	private String engTitle;
	private String ruTitle;
	private String avatar;

	@Id
	@Column(name = "id")
	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Transient
	@Override
	public String getTitle(Locale locale) {
		if (locale.getLanguage().equals("ru")) {
			return getRuTitle();
		} else {
			return getEngTitle();
		}
	}

	@Column(name = "eng_title")
	@Override
	public String getEngTitle() {
		return engTitle;
	}

	@Override
	public void setEngTitle(String title) {
		this.engTitle = title;
	}

	@Column(name = "ru_title")
	@Override
	public String getRuTitle() {
		return ruTitle;
	}

	@Override
	public void setRuTitle(String ruTitle) {
		this.ruTitle = ruTitle;
	}

	@Column(name = "avatar")
	@Override
	public String getAvatarUrl() {
		return avatar;
	}

	@Override
	public void setAvatarUrl(String avatarUrl) {
		this.avatar = avatarUrl;
	}

}
