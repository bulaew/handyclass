package ru.prognoz.handy.impl.domain.course;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ru.prognoz.handy.impl.domain.HandyObjectIDImpl;
import ru.prognoz.handy.impl.domain.UserImpl;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;

@Entity
@Table(name="discussion")
public class DiscussionImpl implements Discussion {

	private static final long serialVersionUID = -8708290632560093215L;

	private long discussionId;

	private HandyObjectID objectId;

	private Course course;

	private User creator;

	private Date createDate;

	private Date modifiedDate;

	private String topic;
	
	private boolean archive = false;

	@Id
	@Column(name = "discussionId")
	@GenericGenerator(name = "test-increment-strategy", strategy = "increment")
	@GeneratedValue(generator = "test-increment-strategy")
	public long getId() {
		return discussionId;
	}

	public void setId(long id) {
		this.discussionId = id;
	}

	@OneToOne(targetEntity = HandyObjectIDImpl.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "object_id")
	@Override
	public HandyObjectID getObjectId() {
		return objectId;
	}

	@Override
	public void setObjectId(HandyObjectID objectId) {
		this.objectId = objectId;
	}

	@ManyToOne(targetEntity = CourseImpl.class)
	@JoinColumn(name = "course_id")
	@Override
	public Course getCourse() {
		return course;
	}

	@Override
	public void setCourse(Course course) {
		this.course = course;
	}

	@ManyToOne(targetEntity = UserImpl.class)
	@JoinColumn(name = "creator_id")
	@Override
	public User getCreator() {
		return creator;
	}

	@Override
	public void setCreator(User creator) {
		this.creator = creator;
	}

	@Override
	public Date getCreateDate() {
		return createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return modifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Basic(fetch = FetchType.EAGER)
	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public boolean isArchive() {
		return archive;
	}

	@Override
	public void setArchive(boolean archive) {
		this.archive = archive;
	}

}
