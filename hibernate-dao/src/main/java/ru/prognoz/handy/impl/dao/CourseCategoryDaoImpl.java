package ru.prognoz.handy.impl.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ru.prognoz.handy.impl.domain.course.CourseCategoryImpl;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.domain.course.CourseCategory;

@Repository
@Transactional
public class CourseCategoryDaoImpl implements CourseCategoryDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CourseCategory getById(int id) {
		return (CourseCategory) sessionFactory.getCurrentSession().createCriteria(CourseCategory.class).add(Restrictions.eq("id", id))
				.setMaxResults(1).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseCategory> getAll() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CourseCategoryImpl.class).addOrder(Order.asc("id"));
		return criteria.list();
	}

}
