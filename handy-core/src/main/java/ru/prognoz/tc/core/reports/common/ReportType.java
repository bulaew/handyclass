package ru.prognoz.tc.core.reports.common;

public enum ReportType {
	
	COURSE_RATING,
	MY_COURSE_RATING,
	AUTHOR_ACTIVITY,
	MY_COURSES_LEARNING,
	COURSE_INFO,
	ACTUAL_CATEGORY,
	MY_EDUCATION,
	/**
	 * Отчет "Мои успехи".
	 */
	MY_ACHIEVEMENT,
	/**
	 * Отчет "Мои наклонности".
	 */
	MY_PROPENSITY,
	USER_ACTIVITY,
	UNDEFINED

}
