package ru.prognoz.tc.core.reports.common;

import java.util.Locale;


/**
 * Критерии формирования данных для отчетов.
 * 
 * @author Denis.V.Mikulich
 * 
 */
abstract public class AbstractSearchCriteria implements SearchCriteria {

	/**
	 * Название колонки для сортировки.
	 */
	private String orderColumn = "";
	private SortDirection sortDirection = SortDirection.DESC;
	/**
	 * Локаль.
	 */
	private Locale locale = new Locale("en", "EN");
	/**
	 * Ограничитель строк отчета
	 */
	private int rowCount = 0;
	/**
	 * Ключевое слово для поиска по полям отчета.
	 */
	private String findValue = "";

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public String getFindValue() {
		return findValue;
	}

	public void setFindValue(String findValue) {
		this.findValue = findValue;
	}

	public SortDirection getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
}
