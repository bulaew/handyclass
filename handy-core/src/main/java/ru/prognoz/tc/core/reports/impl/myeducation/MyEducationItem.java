package ru.prognoz.tc.core.reports.impl.myeducation;

import java.util.Date;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.data.CourseStatus;

public class MyEducationItem implements ReportItem {

	public static final String COLUMN_COURSE_ID = "courseId";
	public static final String COLUMN_COURSE_TITLE = "courseTitle";
	public static final String COLUMN_COURSE_CATEGORY = "courseCategory";
	public static final String COLUMN_STATUS_DESCR = "statusDescr";
	public static final String COLUMN_BEST_RESULT = "bestResult";
	public static final String COLUMN_LEARN_SPEED = "learnSpeed";
	public static final String COLUMN_START_ATTEMPT_DT = "startAttemptDate";
	public static final String COLUMN_COMPLETE_ATTEMPT_DT = "completeAttemptDate";
	public static final String COLUMN_COURSE_END_DT = "courseEndDate";

	private Long courseId;
	private String courseTitle;
	private Integer courseCategoryId;
	private CourseStatus statusCode;
	private Double bestResult;
	private Double learnSpeed;
	private Date startAttemptDate;
	private Date completeAttemptDate;
	private Date courseEndDate;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public Integer getCourseCategoryId() {
		return courseCategoryId;
	}

	public void setCourseCategoryId(Integer courseCategoryId) {
		this.courseCategoryId = courseCategoryId;
	}

	public CourseStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(CourseStatus statusCode) {
		this.statusCode = statusCode;
	}

	public Double getBestResult() {
		return bestResult;
	}

	public void setBestResult(Double bestResult) {
		this.bestResult = bestResult;
	}

	public Double getLearnSpeed() {
		return learnSpeed;
	}

	public void setLearnSpeed(Double learnSpeed) {
		this.learnSpeed = learnSpeed;
	}

	public Date getStartAttemptDate() {
		return startAttemptDate;
	}

	public void setStartAttemptDate(Date startAttemptDate) {
		this.startAttemptDate = startAttemptDate;
	}

	public Date getCompleteAttemptDate() {
		return completeAttemptDate;
	}

	public void setCompleteAttemptDate(Date completeAttemptDate) {
		this.completeAttemptDate = completeAttemptDate;
	}

	public Date getCourseEndDate() {
		return courseEndDate;
	}

	public void setCourseEndDate(Date courseEndDate) {
		this.courseEndDate = courseEndDate;
	}
	
	public boolean isExpired() {
		if (courseEndDate != null) {
			Date curDt = new Date();
			long diff = courseEndDate.getTime() - curDt.getTime();
			return diff < 1000 * 60 * 60 * 24 * 3; // меньше трех дней.
		}
		return false;
	}

}
