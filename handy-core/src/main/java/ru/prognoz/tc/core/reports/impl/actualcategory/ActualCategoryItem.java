package ru.prognoz.tc.core.reports.impl.actualcategory;

import ru.prognoz.tc.core.reports.common.ReportItem;

public class ActualCategoryItem implements ReportItem {

	public static final String COLUMN_CATEGORY_ID = "categoryId";
	public static final String COLUMN_CATEGORY_TITLE = "categoryTitle";
	public static final String COLUMN_COMPLETE_PART = "completePart";

	private Integer categoryId;
	private String categoryTitle;
	private Double completePart;

	public ActualCategoryItem() {
		super();
	}
	
	public ActualCategoryItem(Integer categoryId, String categoryTitle, Double completePart) {
		super();
		this.categoryId = categoryId;
		this.categoryTitle = categoryTitle;
		this.completePart = completePart;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	public Double getCompletePart() {
		return completePart;
	}

	public void setCompletePart(Double completePart) {
		this.completePart = completePart;
	}

}
