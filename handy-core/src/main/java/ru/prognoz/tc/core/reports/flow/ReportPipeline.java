package ru.prognoz.tc.core.reports.flow;

import java.util.List;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;

/**
 * Получение данных для конкретного отчета.
 * 
 * @author Pavel Bulayeu
 * 
 */
public interface ReportPipeline {

	/**
	 * Тип обслуживаемого отчета.
	 * 
	 * @return ReportType
	 */
	public ReportType getType();

	/**
	 * Получить список строк для отчета.
	 * 
	 * @param SearchCriteria
	 *            критерии получения данных.
	 * @return List<ReportItem>
	 * @throws HandyCoreException 
	 */
	public List<ReportItem> getData(SearchCriteria criteria) throws HandyCoreException;

}
