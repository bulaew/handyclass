package ru.prognoz.tc.core.reports.common;

import java.util.HashMap;
import java.util.Map;

public class WhereBuilder {

	private Map<String, Object> params = new HashMap<String, Object>();
	private boolean and;
	private StringBuilder where;

	public WhereBuilder() {
		super();
		and = false;
		where = new StringBuilder();
	}

	public void addCondition(String condition, String paramName, Object paramValue) {
		if (paramValue == null)
			return;
		if ((paramValue instanceof String) && (paramValue.toString().isEmpty()))
			return;
		if (and) {
			where.append(" and ");
		}
		and = true;
		where.append(condition);
		params.put(paramName, paramValue);
	}
	
	public void addMultiCondition(String condition, ConditionParam...conditionParams) {
		if (and) {
			where.append(" and ");
		}
		and = true;
		where.append(condition);
		for (ConditionParam conditionParam : conditionParams) {
			params.put(conditionParam.key, conditionParam.value);
		}
	}

	public String getWhere() {
		if (where.length() == 0) {
			return "";
		}
		return "where " + where.toString();
	}

	public Map<String, Object> getParams() {
		return params;
	}
	
	public static class ConditionParam {
		public String key;
		public Object value;
		public ConditionParam(String key, Object value) {
			super();
			this.key = key;
			this.value = value;
		}
	}

}
