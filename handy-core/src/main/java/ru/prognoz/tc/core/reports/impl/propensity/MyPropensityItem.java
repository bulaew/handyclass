package ru.prognoz.tc.core.reports.impl.propensity;

import ru.prognoz.tc.core.reports.common.ReportItem;

public class MyPropensityItem implements ReportItem {

	public static final String COLUMN_CATEGORY_ID = "categoryId";
	public static final String COLUMN_CATEGORY_TITLE = "categoryTitle";
	public static final String COLUMN_PROPENSITY = "propensity";

	private Integer categoryId;
	private String categoryTitle;
	private Double propensity;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryTitle() {
		return categoryTitle;
	}

	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	public Double getPropensity() {
		return propensity;
	}

	public void setPropensity(Double propensity) {
		this.propensity = propensity;
	}

}