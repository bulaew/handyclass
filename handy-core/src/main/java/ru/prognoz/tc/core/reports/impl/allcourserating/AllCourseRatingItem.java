package ru.prognoz.tc.core.reports.impl.allcourserating;

import ru.prognoz.tc.core.reports.common.ReportItem;

public class AllCourseRatingItem implements ReportItem {

	public static final String COLUMN_COURSE_AVATAR = "course_avatar";
	public static final String COLUMN_COURSE_OID = "course_oid";
	public static final String COLUMN_COURSE_NAME = "full_name";
	public static final String COLUMN_COUNT_VIEWS = "count_views";
	public static final String COLUMN_COUNT_LIKES = "count_likes";

	private String avatarUrl;
	private long courseOID;
	private String courseName;
	private long courseViews;
	private long courseLikes;

	public AllCourseRatingItem() {
		super();
	}

	public AllCourseRatingItem(String avatarUrl, long courseOID, String courseName, long courseViews, long courseLikes) {
		super();
		this.avatarUrl = avatarUrl;
		this.courseOID = courseOID;
		this.courseName = courseName;
		this.courseViews = courseViews;
		this.courseLikes = courseLikes;
	}
	
	public long getCourseOID() {
		return courseOID;
	}

	public void setCourseOID(long courseOID) {
		this.courseOID = courseOID;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public long getCourseViews() {
		return courseViews;
	}

	public void setCourseViews(long courseViews) {
		this.courseViews = courseViews;
	}

	public long getCourseLikes() {
		return courseLikes;
	}

	public void setCourseLikes(long courseLikes) {
		this.courseLikes = courseLikes;
	}

	public String getAvatarUrl() {
	    return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
	    this.avatarUrl = avatarUrl;
	}

}
