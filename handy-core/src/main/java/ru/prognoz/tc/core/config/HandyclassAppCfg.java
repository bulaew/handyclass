package ru.prognoz.tc.core.config;

public class HandyclassAppCfg {
    
	private static int CONFIG_REPORT_ROW_LIMIT = 5;

	private String hostUrl = "http://localhost:8080";
	private String contextPath = "/app";
	private String publicResourseDirectory = "";
	// Facebook App info
	private String fbSecretKey;
	private String fbAppId;
	private String fbRedirectPath;
	// VK app info
	private String vkSecretKey;
	private String vkAppId;
	private String vkRedirectPath;
	// Twitter app info
	private String twitterConsumerKey;
	private String twitterConsumerSecret;
	private String twitterCallbackPath;
	// Google app info
	private String googleClientID;
	private String googleClientSecret;
	private String googleCallbackPath;
	// Email properties
	private String hostMailLogin;
	private String hostMailPassword;
	// Main reports page
	private Integer defaultRowLimit; // количество строк в отчете на главной странице.
	
	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public String getPublicResourseDirectory() {
		return publicResourseDirectory;
	}

	public void setPublicResourseDirectory(String publicResourseDirectory) {
		this.publicResourseDirectory = publicResourseDirectory;
	}

	public String getFbSecretKey() {
		return fbSecretKey;
	}

	public void setFbSecretKey(String fbSecretKey) {
		this.fbSecretKey = fbSecretKey;
	}

	public String getFbAppId() {
		return fbAppId;
	}

	public void setFbAppId(String fbAppId) {
		this.fbAppId = fbAppId;
	}
	
	public String getFbRedirectPath() {
		return fbRedirectPath;
	}

	public void setFbRedirectPath(String fbRedirectPath) {
		this.fbRedirectPath = fbRedirectPath;
	}

	public String getFbRedirectURL() {
		return getHostUrl()+getContextPath()+getFbRedirectPath();
	}

	public String getVkSecretKey() {
		return vkSecretKey;
	}

	public void setVkSecretKey(String vkSecretKey) {
		this.vkSecretKey = vkSecretKey;
	}

	public String getVkAppId() {
		return vkAppId;
	}

	public void setVkAppId(String vkAppId) {
		this.vkAppId = vkAppId;
	}
	
	public String getVkRedirectPath() {
		return vkRedirectPath;
	}

	public void setVkRedirectPath(String vkRedirectPath) {
		this.vkRedirectPath = vkRedirectPath;
	}

	public String getVkRedirectURL() {
		return getHostUrl()+getContextPath()+getVkRedirectPath();
	}

	
	public String getTwitterConsumerKey() {
		return twitterConsumerKey;
	}

	public void setTwitterConsumerKey(String twitterConsumerKey) {
		this.twitterConsumerKey = twitterConsumerKey;
	}

	public String getTwitterConsumerSecret() {
		return twitterConsumerSecret;
	}

	public void setTwitterConsumerSecret(String twitterConsumerSecret) {
		this.twitterConsumerSecret = twitterConsumerSecret;
	}
	
	public String getTwitterCallbackPath() {
		return twitterCallbackPath;
	}

	public void setTwitterCallbackPath(String twitterCallbackPath) {
		this.twitterCallbackPath = twitterCallbackPath;
	}

	public String getTwitterCallbackUrl() {
		return getHostUrl()+getContextPath()+getTwitterCallbackPath();
	}

	public String getGoogleCallbackUrl() {
		return getHostUrl()+getContextPath()+getGoogleCallbackPath();
	}
	
	public String getGoogleCallbackPath() {
		return googleCallbackPath;
	}

	public void setGoogleCallbackPath(String googleCallbackPath) {
		this.googleCallbackPath = googleCallbackPath;
	}

	public String getGoogleClientID() {
		return googleClientID;
	}

	public void setGoogleClientID(String googleClientID) {
		this.googleClientID = googleClientID;
	}

	public String getGoogleClientSecret() {
		return googleClientSecret;
	}

	public void setGoogleClientSecret(String googleClientSecret) {
		this.googleClientSecret = googleClientSecret;
	}

	public String getHostMailLogin() {
		return hostMailLogin;
	}

	public void setHostMailLogin(String hostMailLogin) {
		this.hostMailLogin = hostMailLogin;
	}

	public String getHostMailPassword() {
		return hostMailPassword;
	}

	public void setHostMailPassword(String hostMailPassword) {
		this.hostMailPassword = hostMailPassword;
	}

	public Integer getDefaultRowLimit() {
		if (defaultRowLimit == null) {
			return CONFIG_REPORT_ROW_LIMIT;
		}
		return defaultRowLimit;
	}

	public void setDefaultRowLimit(Integer defaultRowLimit) {
		this.defaultRowLimit = defaultRowLimit;
	}
	
}
