package ru.prognoz.tc.core.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class HcRandom {

	private static int id = 0;
	
	/**
	 * Геренирование "плохого" рандомного числа. Числа длинной 9 символов.
	 *
	 * @return
	 */
	public static Integer random()
	{
		String result = "";
		do {
			result += (new Long(System.nanoTime())).toString(); 
		} while (result.length() < 9);
		
		return NumberUtils.toInt(StringUtils.substring(result, result.length() - 9), id++);
	}

}
