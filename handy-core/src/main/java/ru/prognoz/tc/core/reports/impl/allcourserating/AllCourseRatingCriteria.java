package ru.prognoz.tc.core.reports.impl.allcourserating;

import ru.prognoz.tc.core.reports.common.AbstractSearchCriteria;

public class AllCourseRatingCriteria extends AbstractSearchCriteria {
	
	/**
	 * фильтр категорий. 0 - все категории.
	 */
	private int filterCategoryID = 0;

	public int getFilterCategoryID() {
		return filterCategoryID;
	}

	public void setFilterCategoryID(int filterCategoryID) {
		this.filterCategoryID = filterCategoryID;
	}

}
