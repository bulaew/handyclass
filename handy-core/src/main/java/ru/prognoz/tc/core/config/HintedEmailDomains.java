package ru.prognoz.tc.core.config;

import java.util.List;

public class HintedEmailDomains {

	List<String> emailDomains;

	public List<String> getEmailDomains() {
		return emailDomains;
	}

	public void setEmailDomains(List<String> emailDomains) {
		this.emailDomains = emailDomains;
	}
	
}
