package ru.prognoz.tc.core.reports.impl.mycourserating;

import ru.prognoz.tc.core.reports.common.AbstractSearchCriteria;
import ru.prognoz.tc.domain.User;

public class MyCourseRatingCriteria extends AbstractSearchCriteria {
	
	/**
	 * фильтр категорий. 0 - все категории.
	 */
	private int filterCategoryID = 0;
	private User filterUser = null;

	public int getFilterCategoryID() {
		return filterCategoryID;
	}

	public void setFilterCategoryID(int filterCategoryID) {
		this.filterCategoryID = filterCategoryID;
	}

	public User getFilterUser() {
		return filterUser;
	}

	public void setFilterUser(User filterUser) {
		this.filterUser = filterUser;
	}
	
}
