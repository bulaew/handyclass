package ru.prognoz.tc.data;

public enum TestStatus {
	/**
	 * Тест не запускался.
	 */
	NOT_STARTED(1),
	/**
	 * Тест запускался,
	 */
	STARTED(2),
	/**
	 * Тест завершен с неудовлетворительным результатом.
	 */
	NOT_PASSED(3),
	/**
	 * Тест завершен с удовлетворительным результатом.
	 */
	PASSED(4);

	private int status;

	private TestStatus(int status) {
		this.status = status;
	}

	public int getIntValue() {
		return status;

	}

	public static TestStatus parseInt(int code) {
		for (TestStatus status : TestStatus.values()) {
			if (status.getIntValue() == code) {
				return status;
			}
		}
		return null;
	}

}
