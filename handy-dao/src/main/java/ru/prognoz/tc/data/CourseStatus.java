package ru.prognoz.tc.data;

public enum CourseStatus {
	UNDEFINED(0),
	NOT_STARTED(1), // не начат
	UNCOMPLETED(2), // не завершен
	NOT_PASSED(3), // не сдан
	PASSED(4), // сдан
	COMPLETED(5); // завершен

	private int type;

	private CourseStatus(int type) {
		this.type = type;
	}

	public int getIntValue() {
		return type;
	}

	public String getI18ncode() {
		if (type == 0) {
			return "Reports.allStatuses";
		}
		return "CourseStatus." + type;
	}

	public static CourseStatus parseInt(int code) {
		for (CourseStatus type : CourseStatus.values()) {
			if (type.getIntValue() == code) {
				return type;
			}
		}
		return null;
	}
}
