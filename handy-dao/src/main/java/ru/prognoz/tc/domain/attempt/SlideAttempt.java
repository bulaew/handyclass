package ru.prognoz.tc.domain.attempt;

import java.io.Serializable;
import java.util.Date;

import ru.prognoz.tc.domain.HandyObjectID;

public interface SlideAttempt extends Serializable {

	public HandyObjectID getSlideOid();

	public void setSlideOid(HandyObjectID slideOID);

	/**
	 * Получить попытку по курсу, в котором содержится данный слайд.
	 * 
	 * @return
	 */
	public CourseAttempt getCourseAttempt();

	public void setCourseAttempt(CourseAttempt courseAttempt);

	public Date getFirstView();

	public void setFirstView(Date firstView);

	public Date getLastView();

	public void setLastView(Date lastView);

	public long getSpendTime();

	public void setSpendTime(long spendTime);

	public int getViewCounter();

	public void setViewCounter(int viewCounter);
}
