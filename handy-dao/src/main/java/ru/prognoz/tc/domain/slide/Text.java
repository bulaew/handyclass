package ru.prognoz.tc.domain.slide;

public interface Text extends HasType, SlideComponent {

	public String getTextContent();

	public void setTextContent(String headerContent);
}
