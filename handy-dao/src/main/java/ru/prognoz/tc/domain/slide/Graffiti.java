package ru.prognoz.tc.domain.slide;

public interface Graffiti extends HasType, SlideComponent {
	public String getContent();

	public void setContent(String headerContent);
}