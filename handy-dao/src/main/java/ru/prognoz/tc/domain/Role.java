package ru.prognoz.tc.domain;

import org.springframework.security.core.GrantedAuthority;

public interface Role extends GrantedAuthority {

	public long getId();

	public void setId(long id);

	public String getAuthority();

	public void setAuthority(String authority);

}
