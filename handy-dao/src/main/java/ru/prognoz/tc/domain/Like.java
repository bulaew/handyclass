package ru.prognoz.tc.domain;

import java.io.Serializable;
import java.util.Date;

public interface Like extends Serializable {
	public HandyObjectID getComponentOID();

	public void setComponentOID(HandyObjectID componentOID);

	public User getOwner();

	public void setOwner(User owner);

	public Date getDate();

	public void setDate(Date date);
}
