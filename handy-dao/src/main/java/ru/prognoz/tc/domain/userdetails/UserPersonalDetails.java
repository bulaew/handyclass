package ru.prognoz.tc.domain.userdetails;

public interface UserPersonalDetails {

	public String getLogin();

	public void setLogin(String login);

	public String getPassword();

	public void setPassword(String password);

	public String getName();

	public void setName(String name);

	public String getLastName();

	public void setLastName(String lastName);

	public String getMiddleName();

	public void setMiddleName(String middleName);

	public String getAvatarURL();

	public void setAvatarURL(String avatarURL);

	public String getAbout();

	public void setAbout(String about);
}
