package ru.prognoz.tc.domain.slide;

public interface Header extends SlideComponent, HasType {

	public String getHeaderContent();

	public void setHeaderContent(String headerContent);
}