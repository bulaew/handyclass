package ru.prognoz.tc.domain.test;

import java.io.Serializable;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface Test extends Serializable, Archivable {
	
	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public HandyObjectID getCourseOID();

	public void setCourseOID(HandyObjectID courseOID);

	public boolean isEnable();

	public void setEnable(boolean isEnable);

	public int getPassingScore();

	public void setPassingScore(int passingScore);
}
