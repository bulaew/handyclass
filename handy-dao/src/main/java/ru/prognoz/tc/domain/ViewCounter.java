package ru.prognoz.tc.domain;

import java.io.Serializable;

public interface ViewCounter extends Serializable {

	/**
	 * Уникальный идентификатор компоненты, которая просмотрена пользователем.
	 * 
	 * @return
	 */
	public HandyObjectID getComponentOID();

	public void setComponentOID(HandyObjectID componentOID);

	/**
	 * Пользователь, просмотревший компанент.
	 * 
	 * @return
	 */
	public User getOwner();

	public void setOwner(User owner);
}
