package ru.prognoz.tc.domain.contact;

import ru.prognoz.tc.domain.HandyObjectID;

/**
 * Маппер-класс. Содержит связь списка пользователей с контактами
 * 
 * @author mikulich
 * 
 */
public interface ContactListContent {

	/**
	 * ОИД списка
	 * 
	 * @return
	 */
	public HandyObjectID getListOID();

	public void setListOID(HandyObjectID listOID);

	/**
	 * ОИД контакта.
	 * @return
	 */
	public HandyObjectID getContactOID();

	public void setContactOID(HandyObjectID contactOID);

}
