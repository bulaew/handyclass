package ru.prognoz.tc.domain.attempt;

import java.io.Serializable;
import java.util.Date;

import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface CourseAttempt extends Serializable {
	
	public HandyObjectID getCourseOid();
	
	public void setCourseOid(HandyObjectID courseOid);

	public User getUser();

	public void setUser(User user);

	/**
	 * Завершен просмотр всех слайдов.
	 * 
	 * @return
	 */
	public boolean isComplete();

	public void setComplete(boolean complete);

	public CourseStatus getStatus();

	public void setStatus(CourseStatus status);

	/**
	 * Дата успешного завершения обучения. обучение успешно завершено, если статус попытки "Сдан" или "Завершено".
	 * 
	 * @return Дата
	 */
	public Date getCompleteDate();

	public void setCompleteDate(Date completeDate);

}
