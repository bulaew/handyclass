package ru.prognoz.tc.domain.slide;

import java.util.List;

public interface HasChildren {
	public List<ChildComponent> getChildren();

	public void setChildren(List<ChildComponent> children);
}
