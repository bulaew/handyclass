package ru.prognoz.tc.domain.slide;

public interface Marker extends HasType, ChildComponent {
	public int getX();

	public void setX(int x);

	public int getY();

	public void setY(int y);

	public void setWidth(int width);

	public int getWidth();

	public void setHeight(int height);

	public int getHeight();
}
