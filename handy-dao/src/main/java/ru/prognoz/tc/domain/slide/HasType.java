package ru.prognoz.tc.domain.slide;

public interface HasType {
	public String getType();

	public void setType(String type);
}
