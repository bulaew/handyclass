package ru.prognoz.tc.domain;

import java.io.Serializable;
import java.util.Date;

public interface CourseRegistration extends Serializable {

	/**
	 * Курс на который пользователь записался.
	 */
	public HandyObjectID getCourseOid();

	public void setCourseOid(HandyObjectID courseOID);
	/**
	 * Пользователь.
	 */
	public User getUser();

	public void setUser(User user);

	/**
	 * Дата записи.
	 */
	public Date getDate();

	public void setDate(Date date);

}
