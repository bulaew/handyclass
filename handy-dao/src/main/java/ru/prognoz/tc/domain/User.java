package ru.prognoz.tc.domain;

import java.util.Set;

import org.springframework.security.core.userdetails.UserDetails;

import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.domain.userdetails.UserActivityDetails;
import ru.prognoz.tc.domain.userdetails.UserPersonalDetails;
import ru.prognoz.tc.domain.userdetails.UserSocialDetatils;

public interface User extends UserDetails, UserPersonalDetails, UserActivityDetails, UserSocialDetatils {

	public long getId();

	public void setId(long id);
	
	public HandyObjectID getObjectId();
	
	public void setObjectId(HandyObjectID objectID);

	/**
	 * Установка ролей пользователя.
	 * 
	 * @return
	 */
	public Set<Role> getRoles();

	public void setRoles(Set<Role> roles);
	
	public Set<Invite> getInvites();

	public void setInvites(Set<Invite> invites);

}
