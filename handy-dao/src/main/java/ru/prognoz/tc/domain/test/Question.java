package ru.prognoz.tc.domain.test;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface Question extends Archivable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);
	
	public HandyObjectID getTestOID();

	public void setTestOID(HandyObjectID testOID);

	public int getPosition();

	public void setPosition(int position);

	public String getQuestionType();

	public void setQuestionType(String questionType);

	public String getText();

	public void setText(String text);

	public boolean isActive();
	
	public void setActive(boolean active);
}
