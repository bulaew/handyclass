package ru.prognoz.tc.domain.contact;

import java.io.Serializable;
import java.util.Date;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface ContactListInfo extends Archivable, Serializable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public String getTitle();

	public void setTitle(String title);

	public User getOwner();

	public void setOwner(User owner);

	public Date getCreated();

	public void setCreated(Date created);

	public Date getModified();

	public void setModified(Date modified);

}
