package ru.prognoz.tc.domain.attempt;

import java.io.Serializable;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface UserAnswer extends Serializable {

	public HandyObjectID getAnswerOid();

	public void setAnswerOid(HandyObjectID answerOID);

	public String getValue();

	public void setValue(String value);

	public User getUser();

	public void setUser(User user);

	public int getViewPosition();

	public void setViewPosition(int viewPosition);

}
