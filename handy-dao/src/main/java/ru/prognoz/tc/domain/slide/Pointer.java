package ru.prognoz.tc.domain.slide;

public interface Pointer extends HasType, ChildComponent {
	public int getX();

	public void setX(int x);

	public int getY();

	public void setY(int y);

	public String getPointerType();

	public void setPointerType(String type);
}
