package ru.prognoz.tc.domain;

/**
 * идентификатор объектов системы.
 * 
 * @author Denis.V.Mikulich
 * 
 */
public interface HandyObjectID {

	public long getId();

	public void setId(long id);
}
