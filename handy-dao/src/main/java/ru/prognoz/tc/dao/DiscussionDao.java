package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.course.Discussion;

public interface DiscussionDao {
	
	public Discussion newDomainInstatnce();
	
	public Discussion getByOid(Long oid);

	public void merge(Discussion discussion);

	public void save(Discussion discussion);

	public List<Discussion> getByCourseOid(long oid);

}
