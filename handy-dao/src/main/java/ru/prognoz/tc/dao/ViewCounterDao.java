package ru.prognoz.tc.dao;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface ViewCounterDao {
	
	public int getCountViews(HandyObjectID componentOID);
	
	public int increase(HandyObjectID componentOID, User owner);
	
	public boolean isViewed(HandyObjectID componentOID, User owner);

}
