package ru.prognoz.tc.dao.attempt;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.course.Course;

public interface CourseAttemptDao {

	public void save(CourseAttempt courseAttempt);

	public void merge(CourseAttempt courseAttempt);

	public void delete(CourseAttempt courseAttempt);

	public CourseAttempt get(Course course, User user);

	public CourseAttempt getOrCreate(Course course, User user);

}
