package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;

public interface CourseRegistrationDao {
	
	public CourseRegistration newDomainInstance();
	
	public boolean isRegistered(Course course, User user);
	
	public CourseRegistration getRegistry(Course course, User user);
	
	public List<CourseRegistration> getUserRegistrations(User user);
	
	public int getRegisteredCount(Course course);
	
	public List<CourseRegistration> listRegistrations(Course course);

	void merge(CourseRegistration registry);

	void save(CourseRegistration registry);
	
	void delete(CourseRegistration registry);

}
