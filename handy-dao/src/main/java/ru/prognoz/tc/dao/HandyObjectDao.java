package ru.prognoz.tc.dao;

import ru.prognoz.tc.domain.HandyObjectID;

public interface HandyObjectDao {

	/**
	 * Создание нового объекта идентификации
	 * 
	 * @return новый ОИД
	 */
	public HandyObjectID generate();
	
	public HandyObjectID newDomainInstance();
	
	public HandyObjectID getById(Long id);

}
