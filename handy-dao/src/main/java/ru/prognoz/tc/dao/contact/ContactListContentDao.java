package ru.prognoz.tc.dao.contact;

import java.util.List;

import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListContent;
import ru.prognoz.tc.domain.contact.ContactListInfo;

public interface ContactListContentDao {
	
	public ContactListContent newDomainInstance();
	
	public int countMembers(long contactListOID);
	
	public List<Contact> members(Long listOID);
	
	public List<Contact> members(ContactListInfo contactListInfo);
	
	public Contact findContactListMember(ContactListInfo contactListInfo, Long contactOID);
	
	public boolean addContactListMember(ContactListInfo info, Contact contact);
	
	public boolean removeContactListMember(ContactListInfo info, Contact contact);

}
