package ru.prognoz.tc.dao;

import java.io.Serializable;
import java.util.List;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;

public interface CourseDao extends Serializable {

	public Course newDomainInstatnce();

	public Course getByOid(Long oid);

	public void merge(Course course);

	public List<Course> getPublicCourses();

	public List<Course> getPrivateCourses();

	public List<Course> getInviteCourses();
	
	public List<Course> getAllCourses();
	
	public List<Course> getUserOwnCourses(User user);

	long countCourses();
}
