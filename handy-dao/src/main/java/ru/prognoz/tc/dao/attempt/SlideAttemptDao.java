package ru.prognoz.tc.dao.attempt;

import java.util.List;

import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.SlideAttempt;
import ru.prognoz.tc.domain.slide.Slide;

public interface SlideAttemptDao {

	public void save(SlideAttempt slideAttempt);

	public void merge(SlideAttempt slideAttempt);

	public void delete(SlideAttempt slideAttempt);

	public List<SlideAttempt> list(CourseAttempt courseAttempt);

	/**
	 * Получить/создать попытку по слайду.
	 * 
	 * @param courseAttempt
	 *            Родительский объект попытки по курсу.
	 * @param slide
	 *            Слайд, по которому создается попытка.
	 * @return Попытка по слайду.
	 */
	public SlideAttempt getOrCreate(CourseAttempt courseAttempt, Slide slide);
	
	public List<SlideAttempt> getAttempts(Long slideOID);

}
