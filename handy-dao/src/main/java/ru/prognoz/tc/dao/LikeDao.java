package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.Like;
import ru.prognoz.tc.domain.User;

public interface LikeDao {
	public long getCountLikes(Long componentOID);

	public Like pushLike(Long componentOID, User owner);

	public boolean isLiked(Long componentOID, User owner);

	public List<Like> getComponentLikes(Long componentOID, Long limit);

	public List<Like> getComponentLikes(Long componentOID);
}
