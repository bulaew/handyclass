package ru.prognoz.tc.dao.contact;

import java.util.List;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;

public interface ContactDao {
	
	public Contact newDomainInstance();
	
	public Contact getByOID(long oid);

	public Contact search(User owner, String email);

	public List<Contact> getByOwner(User owner);
	
	public void save(Contact contact);
	
	public void merge(Contact contact);
}
