package ru.prognoz.tc.dao.attempt;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.test.Test;

public interface TestAttemptDao {

	public void save(TestAttempt testAttempt);

	public void merge(TestAttempt testAttempt);

	public void delete(TestAttempt testAttempt);

	/**
	 * Получить объект попытки по тесту.
	 * 
	 * @param test
	 *            OID теста.
	 * @param user
	 *            Пользователь, для которого была создана попытка.
	 * @return возвращает попытку
	 */
	public TestAttempt get(HandyObjectID testOID, User user);

	/**
	 * Получить/создать объект попытки по тесту.
	 * 
	 * @param test
	 *            Тест
	 * @param user
	 *            Пользователь, для которого была создана попытка.
	 * @return возвращает попытку
	 */
	public TestAttempt getOrCreate(Test test, User user);

}
