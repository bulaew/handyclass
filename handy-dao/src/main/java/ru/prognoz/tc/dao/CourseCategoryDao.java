package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.course.CourseCategory;

public interface CourseCategoryDao {

	public CourseCategory getById(int id);

	public List<CourseCategory> getAll();
}
