package ru.prognoz.handy.render;

import java.util.Map;

public interface HandyRender {
	
	public String mergetTemplate(String templateLocation, Map<String, Object> model);

}
