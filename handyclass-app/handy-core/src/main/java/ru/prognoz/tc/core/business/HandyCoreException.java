package ru.prognoz.tc.core.business;

public class HandyCoreException extends Exception {

	private static final long serialVersionUID = -8501683362953939437L;

	public HandyCoreException() {
		super();
	}

	public HandyCoreException(String text, Throwable exc) {
		super(text, exc);
	}

	public HandyCoreException(String text) {
		super(text);
	}

	public HandyCoreException(Throwable exc) {
		super(exc);
	}

}
