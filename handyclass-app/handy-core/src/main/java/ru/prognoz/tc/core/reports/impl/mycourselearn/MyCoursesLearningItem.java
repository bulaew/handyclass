package ru.prognoz.tc.core.reports.impl.mycourselearn;

import java.util.Locale;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.utils.HcDateTime;

public class MyCoursesLearningItem implements ReportItem {
	public static final String COLUMN_AUTHOR_ID = "authorid";
	public static final String COLUMN_COURSE_ID = "courseid";
	public static final String COLUMN_COURSE_NAME = "courseName";
	public static final String COLUMN_COUNT_REGS = "countRegistry";
	public static final String COLUMN_COUNT_LEARN = "countLearn";
	public static final String COLUMN_COUNT_COMPLETED = "countCompleted";
	public static final String COLUMN_AVARAGE_TIME = "avarageLearnTime";
	public static final String COLUMN_LEARN_QUALITY = "learnQuality";

	/**
	 * User ID автора.
	 */
	private int courseAuthorID;
	/**
	 * Course OID
	 */
	private long courseOID;
	/**
	 * название курса.
	 */
	private String courseName;
	/**
	 * Количество зарегистрированных учеников
	 */
	private int countRegs;
	/**
	 * Количество проходивших обучение учеников
	 */
	private int countLearn;
	/**
	 * количество завершенных обучений
	 */
	private int countCompleted;
	/**
	 * Среднее время обучения
	 */
	private double avarageLearnTime;
	/**
	 * Качество обучений. Рассчитывается по формуле: качество = Сумма просмотров каждой страницы / Общее число страниц
	 */
	private double learnQuality;

	public int getCourseAuthorID() {
		return courseAuthorID;
	}

	public void setCourseAuthorID(int courseAutheorID) {
		courseAuthorID = courseAutheorID;
	}

	public long getCourseOID() {
		return courseOID;
	}

	public void setCourseOID(long courseOID) {
		this.courseOID = courseOID;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getCountRegs() {
		return countRegs;
	}

	public void setCountRegs(int countRegs) {
		this.countRegs = countRegs;
	}

	public int getCountLearn() {
		return countLearn;
	}

	public void setCountLearn(int countLearn) {
		this.countLearn = countLearn;
	}

	public int getCountCompleted() {
		return countCompleted;
	}

	public void setCountCompleted(int countCompleted) {
		this.countCompleted = countCompleted;
	}

	public double getAvarageLearnTime() {
		return avarageLearnTime;
	}

	public void setAvarageLearnTime(double avarageLearnTime) {
		this.avarageLearnTime = avarageLearnTime;
	}

	public double getLearnQuality() {
		return learnQuality;
	}

	public void setLearnQuality(double learnQuality) {
		this.learnQuality = learnQuality;
	}
	
	public String getTotalTime(Locale locale) {
		return HcDateTime.formatTime((long) avarageLearnTime, locale);
	}

}
