package ru.prognoz.tc.core.reports.impl.courseinfo;

import java.util.Date;
import java.util.Locale;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.data.CourseStatus;

public class CourseInfoItem implements ReportItem {

	public static final String COLUMN_AUTHOR_FIO = "userFIO";
	public static final String COLUMN_AUTHOR_ID = "userID";
	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_BEST_RESULT = "bestResult";
	public static final String COLUMN_LEARN_TIME = "learnTime";
	public static final String COLUMN_DATE_START = "dateStart";
	public static final String COLUMN_DATE_END = "dateEnd";

	private Integer userID;
	private String userFIO;
	private CourseStatus status;
	private Double bestResult;
	private Long learnTime;
	private Date dateStart;
	private Date dateEnd;

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getUserFIO() {
		return userFIO;
	}

	public void setUserFIO(String userFIO) {
		this.userFIO = userFIO;
	}

	public CourseStatus getStatus() {
		return status;
	}

	public void setStatus(CourseStatus status) {
		this.status = status;
	}

	public Double getBestResult() {
		return bestResult;
	}

	public void setBestResult(Double bestResult) {
		this.bestResult = bestResult;
	}

	public Long getLearnTime() {
		return learnTime;
	}

	public void setLearnTime(Long learnTime) {
		this.learnTime = learnTime;
	}

	public String getLearnTime(Locale locale) {
		return HcDateTime.formatTime(getLearnTime(), locale);
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
}
