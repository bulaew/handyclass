package ru.prognoz.tc.core.reports.impl.courseinfo;

import ru.prognoz.tc.core.reports.common.AbstractSearchCriteria;

public class CourseInfoCriteria extends AbstractSearchCriteria {

	private Long filterCourseOID;

	public Long getFilterCourseOID() {
		return filterCourseOID;
	}

	public void setFilterCourseOID(Long filterCourseOID) {
		this.filterCourseOID = filterCourseOID;
	}
}
