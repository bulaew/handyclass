package ru.prognoz.tc.core.reports.impl.myachievement;

import java.util.Date;
import java.util.Locale;

import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.utils.HcDateTime;

public class MyAchievementItem implements ReportItem {

	public static final String COLUMN_COURSE_OID = "courseOid";
	public static final String COLUMN_COURSE_TITLE = "courseTitle";
	public static final String COLUMN_SERTIFICATE_ID = "certificateId";

	public static final String COLUMN_START_ATTEMPT_DT = "startAttemptDate";
	public static final String COLUMN_COMPLETE_ATTEMPT_DT = "completeAttemptDate";
	public static final String COLUMN_TOTAL_TIME = "totalTime";

	private Long courseOid;
	private String courseTitle;
	private Long certificateId;
	private Date startAttemptDate;
	private Date completeAttemptDate;
	private Long totalTime;

	public Long getCourseOid() {
		return courseOid;
	}

	public void setCourseOid(Long courseOid) {
		this.courseOid = courseOid;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public Long getCertificateId() {
		return certificateId;
	}

	public void setCertificateId(Long certificateId) {
		this.certificateId = certificateId;
	}

	public Date getStartAttemptDate() {
		return startAttemptDate;
	}

	public void setStartAttemptDate(Date startAttemptDate) {
		this.startAttemptDate = startAttemptDate;
	}

	public Date getCompleteAttemptDate() {
		return completeAttemptDate;
	}

	public void setCompleteAttemptDate(Date completeAttemptDate) {
		this.completeAttemptDate = completeAttemptDate;
	}

	public Long getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(Long totalTime) {
		this.totalTime = totalTime;
	}
	
	public String getTotalTime(Locale locale) {
		return HcDateTime.formatTime(getTotalTime(), locale);
	}

}
