package ru.prognoz.tc.core.reports.flow;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SearchCriteria;

@Component
public class ReportModule {

	@Autowired(required = false)
	private List<ReportPipeline> pipelines;

	public List<ReportItem> getData(ReportType reportType, SearchCriteria searchCriteria) throws HandyCoreException {
		for (ReportPipeline pipeline : pipelines) {
			if (pipeline.getType().equals(reportType)) {
				return pipeline.getData(searchCriteria);
			}
		}
		return new ArrayList<ReportItem>();
	}

}
