package ru.prognoz.tc.core.reports.impl.propensity;

import ru.prognoz.tc.core.reports.common.AbstractSearchCriteria;
import ru.prognoz.tc.domain.User;

public class MyPropensityCriteria extends AbstractSearchCriteria {

	private User filterUser = null;

	public User getFilterUser() {
		return filterUser;
	}

	public void setFilterUser(User filterUser) {
		this.filterUser = filterUser;
	}
}
