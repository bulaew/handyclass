package ru.prognoz.tc.core.reports.impl.myeducation;

import java.util.Date;

import ru.prognoz.tc.core.reports.common.AbstractSearchCriteria;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.domain.User;

public class MyEducationCriteria extends AbstractSearchCriteria {

	private User filterUser = null;
	private CourseStatus filterStatus;
	private Date filterStartPeriod;
	private Date filterEndPeriod;
	private int filterCategoryID = 0;

	public User getFilterUser() {
		return filterUser;
	}

	public void setFilterUser(User filterUser) {
		this.filterUser = filterUser;
	}

	public CourseStatus getFilterStatus() {
		return filterStatus;
	}

	public void setFilterStatus(CourseStatus filterStatus) {
		this.filterStatus = filterStatus;
	}

	public Date getFilterStartPeriod() {
		return filterStartPeriod;
	}

	public void setFilterStartPeriod(Date filterStartPeriod) {
		this.filterStartPeriod = filterStartPeriod;
	}

	public Date getFilterEndPeriod() {
		return filterEndPeriod;
	}

	public void setFilterEndPeriod(Date filterEndPeriod) {
		this.filterEndPeriod = filterEndPeriod;
	}

	public int getFilterCategoryID() {
		return filterCategoryID;
	}

	public void setFilterCategoryID(int filterCategoryID) {
		this.filterCategoryID = filterCategoryID;
	}

}
