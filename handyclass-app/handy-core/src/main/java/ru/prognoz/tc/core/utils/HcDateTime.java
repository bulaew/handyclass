package ru.prognoz.tc.core.utils;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.context.MessageSource;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppContext;

public class HcDateTime {

	/**
	 * Форматирование даты.
	 * 
	 * @param date
	 * @param locale
	 * @return строка с временем в формате "07.01.2014"
	 */
	public static String simpleDt(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat dateFormat = null;
		dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		return dateFormat.format(date);
	}

	/**
	 * получение даты.
	 * 
	 * @param date
	 * @return date с временем в формате "07.01.2014"
	 * @throws HandyCoreException
	 */
	public static Date parseDate(String dt) throws HandyCoreException {
		if (dt == null) {
			return null;
		}
		SimpleDateFormat dateFormat = null;
		dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		try {
			return dateFormat.parse(dt);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Форматирование даты для времени оповещения.
	 * 
	 * @param date
	 * @param locale
	 * @return строка с временем в формате "07 янв 2014 в 08:59"
	 */
	public static String notifyDt(Date date, Locale locale) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat dateFormat = null;
		if (locale != null && locale.getLanguage().equals("ru")) {
			dateFormat = new SimpleDateFormat("dd MMM yyyy в HH:mm", locale);
		} else {
			dateFormat = new SimpleDateFormat("dd MMM yyyy 'at' HH:mm", locale);
		}
		return dateFormat.format(date);
	}

	/**
	 * Форматирование даты для тултипов.
	 * 
	 * @param date
	 * @param locale
	 * @return строка с временем в формате "07 янв 2014"
	 */
	public static String tooltipDt(Date date, Locale locale) {
		if (date == null) {
			if (locale != null && locale.getLanguage().equals("ru")) {
				return "Не определено";
			} else {
				return "Unknown";
			}
		}
		SimpleDateFormat dateFormat = null;
		if (locale != null && locale.getLanguage().equals("ru")) {
			dateFormat = new SimpleDateFormat("'до' dd MMM. yyyy", locale);
		} else {
			dateFormat = new SimpleDateFormat("'to' dd MMM. yyyy", locale);
		}
		return dateFormat.format(date);
	}

	/**
	 * Форматирование даты для названий.
	 * 
	 * @param date
	 * @param locale
	 * @return строка с временем в формате "07 янв 2014"
	 */
	public static String titleDt(Date date, Locale locale) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat dateFormat = null;
		dateFormat = new SimpleDateFormat("dd MMM. yyyy", locale);
		return dateFormat.format(date);
	}

	/**
	 * Форматирование даты для времени создания комментария.
	 * 
	 * @param date
	 * @param locale
	 * @return строка с временем в формате "07 января 2014 в 08:59"
	 */
	public static String commentDt(Date date, Locale locale) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat dateFormat = null;
		if (locale != null && locale.getLanguage().equals("ru")) {
			dateFormat = new SimpleDateFormat("dd MMMM yyyy в HH:mm", russianDateFormatSymbols);
		} else {
			dateFormat = new SimpleDateFormat("dd MMMM yyyy 'at' HH:mm", locale);
		}
		return dateFormat.format(date);
	}

	private static DateFormatSymbols russianDateFormatSymbols = new DateFormatSymbols() {

		private static final long serialVersionUID = 4142115183739888582L;

		@Override
		public String[] getMonths() {
			return new String[] { "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября",
					"ноября", "декабря" };
		}

	};

	public static String formatTime(long millis, Locale locale) {
		MessageSource msg = HandyclassAppContext.getBean(MessageSource.class);
		long time = millis / 1000;
		String seconds = Integer.toString((int) (time % 60));
		String minutes = Integer.toString((int) ((time % 3600) / 60));
		String hours = Integer.toString((int) (time / 3600));
		for (int i = 0; i < 2; i++) {
			if (seconds.length() < 2) {
				seconds = "0" + seconds;
			}
			if (minutes.length() < 2) {
				minutes = "0" + minutes;
			}
			if (hours.length() < 2) {
				hours = "0" + hours;
			}
		}
		String timeStr = "";
		if (Integer.parseInt(hours) > 0) {
			timeStr += (hours + " " + msg.getMessage("Util.hours", null, locale) + " ");
		}
		if (Integer.parseInt(minutes) > 0) {
			timeStr += (minutes + " " + msg.getMessage("Util.minutes", null, locale) + " ");
		}
		if (Integer.parseInt(seconds) > 0) {
			timeStr += (seconds + " " + msg.getMessage("Util.seconds", null, locale) + " ");
		}
		return timeStr;
	}

}
