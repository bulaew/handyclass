package ru.prognoz.tc.core.reports.common;

public enum SortDirection {
	ASC,
	DESC
}
