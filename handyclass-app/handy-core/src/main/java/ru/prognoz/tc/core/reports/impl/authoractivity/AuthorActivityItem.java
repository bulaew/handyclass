package ru.prognoz.tc.core.reports.impl.authoractivity;

import ru.prognoz.tc.core.reports.common.ReportItem;

public class AuthorActivityItem implements ReportItem {
	public static final String COLUMN_AUTHOR_ID = "authorid";
	public static final String COLUMN_AUTHOR_FIO = "author_fio";
	public static final String COLUMN_COUNT_COURSES = "count_courses";

	/**
	 * User ID автора.
	 */
	private long authorOID;
	/**
	 * ФИО автора
	 */
	private String authorFIO;
	/**
	 * Количество курсов, созданных автором.
	 */
	private int countCourses;

	public AuthorActivityItem(long authorOID, String authorFIO, int countCourses) {
		super();
		this.authorOID = authorOID;
		this.authorFIO = authorFIO;
		this.countCourses = countCourses;
	}

	public long getAuthorOID() {
		return authorOID;
	}

	public void setAuthorOID(long authorOID) {
		this.authorOID = authorOID;
	}

	public String getAuthorFIO() {
		return authorFIO;
	}

	public void setAuthorFIO(String authorFIO) {
		this.authorFIO = authorFIO;
	}

	public int getCountCourses() {
		return countCourses;
	}

	public void setCountCourses(int countCourses) {
		this.countCourses = countCourses;
	}

}