package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;

public interface InviteDao {
	
	public Invite newDomainInstatnce();
	
	public void save(Invite invite);
	
	public void merge(Invite invite);
	
	/**
	 * Список приглашенных пользователей с активным приглашением.
	 * 
	 * @param course
	 * @return
	 */
	public List<User> getInvitedUsers(Course course);
	
	public Invite getById(long id);

	public void removeInvite(Invite invite);

	public boolean isInvited(Course course, User student);

	public void removeInvite(Course course, User student);

}
