package ru.prognoz.tc.domain;

import java.io.Serializable;
import java.util.Date;

public interface HandyComment extends Serializable, Archivable {
	
	/**
	 * Уникальный ИД комментария в системе
	 * @return HandyObjectID
	 */
	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	/**
	 * Пользователь, оставивший комментарий
	 * @return User
	 */
	public User getOwner();

	public void setOwner(User owner);

	/**
	 * Контент комментария
	 * @return
	 */
	public String getContent();

	public void setContent(String content);

	public Date getCreateDate();

	public void setCreateDate(Date createDate);

	public Date getModifiedDate();

	public void setModifiedDate(Date modifiedDate);

	/**
	 * Уникальный ИД комментируемого объекта.
	 * @return HandyObjectID
	 */
	public HandyObjectID getParentOid();

	public void setParentOid(HandyObjectID parentOID);

}
