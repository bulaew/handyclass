package ru.prognoz.tc.domain.slide;

public interface Video extends HasType, SlideComponent {

	public String getVideoUrl();

	public void setVideoUrl(String headerContent);

	public String getService();

	public void setService(String service);

	public boolean isEmbed();

	public void setEmbed(boolean isEmbed);

	public String getPoster();

	public void setPoster(String poster);
}
