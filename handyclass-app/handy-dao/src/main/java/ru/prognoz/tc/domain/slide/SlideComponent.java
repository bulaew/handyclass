package ru.prognoz.tc.domain.slide;

import java.io.Serializable;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface SlideComponent extends Archivable, Serializable {

	public HandyObjectID getComponentOID();

	public void setComponentOID(HandyObjectID componentOID);

	public HandyObjectID getSlideOID();

	public void setSlideOID(HandyObjectID slideOID);

	public int getPosition();

	public void setPosition(int position);

	public boolean isActive();

	public void setActive(boolean active);

	public void setType(String type);
	
	public String getType();
}
