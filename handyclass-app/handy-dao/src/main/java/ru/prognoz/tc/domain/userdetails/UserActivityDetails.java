package ru.prognoz.tc.domain.userdetails;

import java.util.Date;

/**
 * Активность пользователя в системе
 * 
 * @author mikulich
 * 
 */
public interface UserActivityDetails {

	public Date getRegistrationDate();

	public void setRegistrationDate(Date registrationDate);

	public void setAccountNonExpired(boolean accountNonExpired);

	public void setAccountNonLocked(boolean accountNonLocked);

	public void setCredentialsNonExpired(boolean credentialsNonExpired);

	public void setEnabled(boolean enabled);

	public Date getLastVisitDate();

	public void setLastVisitDate(Date lastVisitDate);

	public long getTotalTime();

	public void setTotalTime(long totalTime);

	public boolean isVerified();

	public void setVerified(boolean verified);

	public String getVerificationCode();

	public void setVerificationCode(String verificationCode);
}
