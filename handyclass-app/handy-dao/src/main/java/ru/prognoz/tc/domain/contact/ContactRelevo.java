package ru.prognoz.tc.domain.contact;

import java.io.Serializable;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

/**
 * Таблица релевантности контактов.
 * 
 * @author Denis.V.Mikulich
 * 
 */
public interface ContactRelevo extends Serializable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	/**
	 * Владелец контактов.
	 * 
	 * @return
	 */
	public User getOwner();

	public void setOwner(User owner);

	public HandyObjectID getContactOID();

	public void setContactOID(HandyObjectID contactOID);

	/**
	 * частота использования контакта данным пользователем.
	 * 
	 * @return
	 */
	public long getInviteFreq();

	public void setInviteFreq(long inviteFreq);
}
