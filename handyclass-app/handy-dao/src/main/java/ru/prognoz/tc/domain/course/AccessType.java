package ru.prognoz.tc.domain.course;

import java.io.Serializable;

/**
 * Описание доступа к курсу.
 * 
 * @author mikulich
 * 
 */
public interface AccessType extends Serializable {

	public long getId();

	public void setId(long id);

	public String getDescription();

	public void setDescription(String description);

}
