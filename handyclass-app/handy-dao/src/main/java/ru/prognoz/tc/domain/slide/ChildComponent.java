package ru.prognoz.tc.domain.slide;

import ru.prognoz.tc.domain.HandyObjectID;

public interface ChildComponent extends SlideComponent {
	public HandyObjectID getParentOid();

	public void setParentOid(HandyObjectID oid);
}
