package ru.prognoz.tc.dao.slide;

import ru.prognoz.tc.domain.slide.Pointer;
import ru.prognoz.tc.domain.slide.Slide;

public interface PointerDao {
	public Pointer newDomainInstatnce(Slide slide, long parentId, int x, int y);
}
