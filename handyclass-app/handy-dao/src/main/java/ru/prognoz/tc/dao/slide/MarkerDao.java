package ru.prognoz.tc.dao.slide;

import ru.prognoz.tc.domain.slide.Marker;
import ru.prognoz.tc.domain.slide.Slide;

public interface MarkerDao {
	public Marker newDomainInstatnce(Slide slide, long parentId, int x, int y, int w, int h);
}
