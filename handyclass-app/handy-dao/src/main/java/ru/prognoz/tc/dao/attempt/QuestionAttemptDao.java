package ru.prognoz.tc.dao.attempt;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.test.Question;

public interface QuestionAttemptDao {
	
	public void save(QuestionAttempt questionAttempt);

	public void merge(QuestionAttempt questionAttempt);

	public void delete(QuestionAttempt questionAttempt);
	
	/**
	 * 
	 * @param question
	 * @param user
	 * @return
	 */
	public QuestionAttempt getOrCreate(Question question, User user);

}
