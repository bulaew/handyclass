package ru.prognoz.tc.dao;

import ru.prognoz.tc.domain.User;

public interface UserDao {

	public User newDomainInstance();
	
	public User getByLogin(final String login);
	
	public User getByFbAccount(final String fbAccount);
	
	public User getByVkAccount(String vkAccount);
	
	public User getByTwitterAccount(String twitterAccount);
	
	public User getByGoogleAccount(String googleAccount);
	
	public User getByOid(long oid);
	
	public User getByVerifyCode(String code);

	public void save(User user);

	long countUsers();

	User getById(long id);

}
