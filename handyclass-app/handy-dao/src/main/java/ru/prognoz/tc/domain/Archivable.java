package ru.prognoz.tc.domain;

public interface Archivable {

	/**
	 * True - архивный объект, false - не архивный объект
	 * @return 
	 */
	public boolean isArchive();

	/**
	 * Установить признак архивного объекта
	 * @param archive 
	 */
	public void setArchive(boolean archive);
}
