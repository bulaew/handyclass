package ru.prognoz.tc.domain.attempt;

import java.io.Serializable;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface QuestionAttempt extends Serializable {

	public HandyObjectID getQuestionOid();

	public void setQuestionOid(HandyObjectID questionOID);

	public User getUser();

	public void setUser(User user);

	public boolean isCorrect();

	public void setCorrect(boolean correct);

	public boolean isShowed();

	public void setShowed(boolean showed);
}
