package ru.prognoz.tc.domain.userdetails;

public interface UserSocialDetatils {

	public String getGoogleemail();

	public void setGoogleemail(String googleemail);

	public String getGoogleName();

	public void setGoogleName(String googleName);

	public String getFacebookAccount();

	public void setFacebookAccount(String facebookAccount);

	public String getFacebookName();

	public void setFacebookName(String facebookName);

	public String getTwitterAccount();

	public void setTwitterAccount(String twitterAccount);

	public String getTwitterName();

	public void setTwitterName(String twitterName);

	public String getVkAccount();

	public void setVkAccount(String vkAccount);

	public String getVkName();

	public void setVkName(String vkName);
}
