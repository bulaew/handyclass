package ru.prognoz.tc.domain.course;

import java.io.Serializable;
import java.util.Date;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface Discussion extends Serializable, Archivable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public Course getCourse();

	public void setCourse(Course course);

	public User getCreator();

	public void setCreator(User creator);

	public Date getCreateDate();

	public void setCreateDate(Date createDate);

	public Date getModifiedDate();

	public void setModifiedDate(Date modifiedDate);

	public String getTopic();

	public void setTopic(String topic);

}
