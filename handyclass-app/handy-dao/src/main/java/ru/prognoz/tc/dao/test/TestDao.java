package ru.prognoz.tc.dao.test;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Test;

public interface TestDao {

	public Test getByCourse(Course course);

	public Test getByCourse(Long courseOID);

	public Test getByObjectId(long OID);

	public Test getByObjectId(HandyObjectID OID);

	public Test newDomainInstance(Course coures);

	public Test newDomainInstance(HandyObjectID courseOid);

	public Test getByCourse(HandyObjectID courseOID);

	public void merge(Test test);

}
