package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.HandyComment;
import ru.prognoz.tc.domain.HandyObjectID;

public interface HandyCommentDao {
	
	public HandyComment newDomainInstatnce();
	
	public List<HandyComment> getByParent(final Long parentOid);
	
	/**
	 * Поиск комментария по его OID.
	 * 
	 * @param oid
	 * @return искомый комментарий или null.
	 */
	public HandyComment find(final Long commentOID);
	
	public boolean setArchived(final Long commentOID);
	
	public List<HandyComment> fullList(final Long parentOid, boolean includeArchived);
	
	public HandyComment getParent(final HandyComment comment);
	
	public Long findRoot(final HandyComment comment);

	void merge(HandyComment comment);

	void save(HandyComment comment);

}
