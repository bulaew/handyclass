package ru.prognoz.tc.dao.slide;

import java.util.List;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.slide.Slide;

public interface SlideDao {
	public void merge(Slide slide);

	public Slide newDomainInstatnce();

	public List<Slide> getAllVisibleByCourse(Long courseOID);
	
	public List<Slide> getAllByCourse(Long courseOID);

	public Slide getByOid(HandyObjectID oid);
}
