package ru.prognoz.tc.domain.attempt;

import java.io.Serializable;

import ru.prognoz.tc.data.TestStatus;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface TestAttempt extends Serializable {

	public HandyObjectID getTestOid();

	public void setTestOid(HandyObjectID testOID);

	public User getUser();

	public void setUser(User user);

	public int getCountAttempts();

	public void setCountAttempts(int countAttempts);

	public int getBestResult();

	public void setBestResult(int bestResult);
	
	public int getLastResult();

	public void setLastResult(int lastResult);

	public TestStatus getStatus();

	public void setStatus(TestStatus testStatus);

	public long getSpendedTime();

	public void setSpendedTime(long spendedTime);

	public long getTotalTime();

	public void setTotalTime(long totalTime);
	

}
