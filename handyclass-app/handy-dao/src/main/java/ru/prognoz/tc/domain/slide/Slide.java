package ru.prognoz.tc.domain.slide;

import java.io.Serializable;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface Slide extends Archivable, Serializable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID oid);

	public HandyObjectID getCourseOID();

	public void setCourseOID(HandyObjectID courseOID);

	public int getPosition();

	public void setPosition(int position);

	public int getTimer();

	public void setTimer(int timer);
}
