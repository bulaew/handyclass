package ru.prognoz.tc.domain.course;

import java.io.Serializable;

import ru.prognoz.tc.domain.User;

public interface Invite extends Serializable {

	public long getId();

	public void setId(long id);

	/**
	 * Курс на который пользователя пригласили.
	 */
	public Course getCourse();

	public void setCourse(Course course);

	/**
	 * Приглашенный пользователь.
	 */
	public User getUser();

	public void setUser(User user);

	/**
	 * Емейл, по кторому приглашаются пользователи, незарегистрированные в системе.
	 */
	public String getGuestEmail();

	public void setGuestEmail(String guestEmail);

	/**
	 * флаг принятия приглашения.
	 */
	public boolean isConfirm();

	public void setConfirm(boolean confirm);

	/**
	 * флаг активного приглашения.
	 */
	public boolean isActive();

	public void setActive(boolean active);

	/**
	 * хэшкод приглашения.
	 */
	public String getInviteHashCode();

	public void setInviteHashCode(String inviteHashCode);

	/**
	 * Оповещение и емейл уже отправлен.
	 */
	public boolean isInviteMsgSended();

	public void setInviteMsgSended(boolean inviteMsgSended);

}
