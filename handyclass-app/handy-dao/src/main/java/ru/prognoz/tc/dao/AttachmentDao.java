package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;

public interface AttachmentDao {

	public Attachment newDomainInstatnce();

	public List<Attachment> getByCourse(Course course);

	public void merge(Attachment attachment);
}
