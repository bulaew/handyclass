package ru.prognoz.tc.dao.contact;

import ru.prognoz.tc.domain.contact.ContactRelevo;

public interface ContactRelevoDao {
	
	public ContactRelevo newDomainInstance();
	
	public ContactRelevo getByContact(long contactOid);

	public void save(ContactRelevo relevo);
	
	public void merge(ContactRelevo relevo);

}
