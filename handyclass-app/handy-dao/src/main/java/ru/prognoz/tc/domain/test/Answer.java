package ru.prognoz.tc.domain.test;

import java.io.Serializable;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface Answer extends Serializable, Archivable {

	public boolean isActive();
	
	public void setActive(boolean active);
	
	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public HandyObjectID getQuestionOID();

	public void setQuestionOID(HandyObjectID questionOID);

	public String getText();

	public void setText(String text);

	public String getValue();

	public void setValue(String value);

	public String getQuestionType();

	public void setQuestionType(String type);
}
