package ru.prognoz.tc.domain.slide;


public interface Loadable {
	public boolean isLoaded();

	public void setLoaded(boolean isLoaded);
}
