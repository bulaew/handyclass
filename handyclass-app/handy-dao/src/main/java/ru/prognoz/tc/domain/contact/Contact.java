package ru.prognoz.tc.domain.contact;

import java.util.Date;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface Contact {
	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public boolean isArchive();

	public void setArchive(boolean archive);

	public Date getCreated();

	public void setCreated(Date created);

	public Date getModified();

	public void setModified(Date modified);

	public User getOwner();

	public void setOwner(User owner);

	public User getUser();

	public void setUser(User user);

	public String getEmail();

	public void setEmail(String email);
}
