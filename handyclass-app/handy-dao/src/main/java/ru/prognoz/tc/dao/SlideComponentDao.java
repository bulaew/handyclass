package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;

public interface SlideComponentDao {
	public SlideComponent getByOid();

	public List<SlideComponent> getVisibleBySlide(Slide slide);

	public List<SlideComponent> getVisibleBySlide(Long slideOid);

	public List<SlideComponent> getBySlide(Slide slide);

	public List<SlideComponent> getBySlide(Long slideOid);

	void merge(SlideComponent component);

	public List<ChildComponent> getChildren(long parentOid);

}
