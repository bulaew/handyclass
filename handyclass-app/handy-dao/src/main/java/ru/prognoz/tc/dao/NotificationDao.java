package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Notification;

public interface NotificationDao {

	public Notification newDomainInstance();
	
	public void save(Notification notification);
	
	public void merge(Notification notification);

	public List<Notification> getByReceiver(User receiver);

	public void delete(Notification notification);
}
