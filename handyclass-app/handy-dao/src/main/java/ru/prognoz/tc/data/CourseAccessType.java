package ru.prognoz.tc.data;

public enum CourseAccessType {

	PRIVATE(1), INVITE(2), PUBLIC(3);

	private int type;

	private CourseAccessType(int type) {
		this.type = type;
	}

	public int getIntValue() {
		return type;
	}
	
	public String getI18ncode() {
		return "AccessType."+type;
	}
	
	public static CourseAccessType parseInt(Long code) {
		return parseInt(code.intValue());
	}

	public static CourseAccessType parseInt(int code) {
		for (CourseAccessType type : CourseAccessType.values()) {
			if (type.getIntValue() == code) {
				return type;
			}
		}
		return null;
	}

}
