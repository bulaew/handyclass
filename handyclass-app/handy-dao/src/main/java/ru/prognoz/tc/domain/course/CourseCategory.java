package ru.prognoz.tc.domain.course;

import java.io.Serializable;
import java.util.Locale;

/**
 * Описание категорий курса.
 * 
 * @author mikulich
 * 
 */
public interface CourseCategory extends Serializable {

	public int getId();

	public void setId(int id);

	/**
	 * Транзитивное поле, возвращающее название категории по текущей локали.
	 * 
	 * @param locale
	 * @return
	 */
	public String getTitle(Locale locale);

	public String getEngTitle();

	public void setEngTitle(String title);

	public String getRuTitle();

	public void setRuTitle(String ruTitle);

	public String getAvatarUrl();

	public void setAvatarUrl(String avatarUrl);
}
