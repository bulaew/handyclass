package ru.prognoz.tc.dao;

import java.util.List;

import ru.prognoz.tc.domain.course.AccessType;

public interface AccessTypeDao {

	public AccessType getById(long id);

	public List<AccessType> getAllTypes();

}
