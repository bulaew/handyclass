package ru.prognoz.tc.dao.contact;

import java.util.List;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.ContactListInfo;

public interface ContactListInfoDao {

	public ContactListInfo newDomainInstance();

	public void save(ContactListInfo list);
	
	public void merge(ContactListInfo list);

	public ContactListInfo getByOid(long oid);

	/**
	 * Все списки пользователя.
	 * 
	 * @param owner
	 * @return
	 */
	public List<ContactListInfo> list(User owner);

	/**
	 * Поиск по названию списка (по полному совпадению)
	 * 
	 * @param groupTitle
	 * @return
	 */
	public List<ContactListInfo> search(String groupTitle);

}
