package ru.prognoz.tc.dao.test;

import java.util.List;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;

public interface AnswerDao {
	public Answer newDomainInstance(Question question);

	public List<Answer> getAnswersByQuestion(Question question);

	public List<Answer> getAnswersByQuestion(HandyObjectID question);

	public List<Answer> getAnswersByQuestion(Long question);

	public void merge(Answer answer);
}


