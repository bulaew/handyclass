package ru.prognoz.tc.domain;

public interface Privilege {

	public long getId();

	public void setId(long id);

	public String getName();

	public void setName(String name);

}
