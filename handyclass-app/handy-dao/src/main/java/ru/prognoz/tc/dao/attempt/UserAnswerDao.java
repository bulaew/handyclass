package ru.prognoz.tc.dao.attempt;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.UserAnswer;
import ru.prognoz.tc.domain.test.Answer;

public interface UserAnswerDao {
	
	public void save(UserAnswer userAnswer);

	public void merge(UserAnswer userAnswer);

	public void delete(UserAnswer userAnswer);
	
	public UserAnswer getOrCreate(Answer answer, User user);
	
	public UserAnswer find(Answer answer, User user);

}
