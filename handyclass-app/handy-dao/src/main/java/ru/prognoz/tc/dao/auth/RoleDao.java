package ru.prognoz.tc.dao.auth;

import ru.prognoz.tc.domain.Role;

public interface RoleDao {
	
	public Role newDomainInstance();
	
	public void save(Role role);
	
	public Role getByAuthority(String authority);

}
