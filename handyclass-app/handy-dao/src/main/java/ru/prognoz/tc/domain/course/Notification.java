package ru.prognoz.tc.domain.course;

import java.util.Date;


import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface Notification {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public User getReceiver();

	public void setReceiver(User receiver);

	public HandyObjectID getCourseOID();

	public void setCourseOID(HandyObjectID courseOID);

	public HandyObjectID getDiscussionOID();

	public void setDiscussionOID(HandyObjectID discussionOID);

	public HandyObjectID getCommentOID();

	public void setCommentOID(HandyObjectID commentOID);

	public HandyObjectID getLikeOID();

	public void setLikeOID(HandyObjectID likeOID);

	public User getPerson();

	public void setPerson(User person);

	public int getNotificationType();

	public void setNotificationType(int notificationType);

	public Date getDate();

	public void setDate(Date date);
}
