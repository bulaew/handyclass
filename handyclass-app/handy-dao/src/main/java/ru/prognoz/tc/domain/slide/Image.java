package ru.prognoz.tc.domain.slide;

public interface Image extends HasType, SlideComponent {

	public String getUrlType();

	public void setUrlType(String urlType);

	public void setRotation(int rotation);

	public int getRotation();

	public void setSize(int size);

	public int getSize();

	public String getUrl();

	public void setUrl(String headerContent);

	public String getDescription();

	public void setDescription(String description);

}
