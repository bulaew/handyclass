package ru.prognoz.tc.service;

import java.util.List;

public interface RollbackedSaver {
	
	public void merge(List<Object> entities) throws Exception;
	
	public void merge(Object entity) throws Exception;

}
