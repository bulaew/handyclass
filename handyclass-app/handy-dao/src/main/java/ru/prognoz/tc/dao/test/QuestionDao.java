package ru.prognoz.tc.dao.test;

import java.util.List;

import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

public interface QuestionDao {

	public List<Question> getQuestionsByTest(Test test);

	public List<Question> getQuestionsByTest(HandyObjectID test);

	public List<Question> getQuestionsByTest(Long test);

	public Question newDomainInstance(String type, Test test);

	public Question newDomainInstance(String type, HandyObjectID testOid);

	public void merge(Question question);

	public List<Question> getNotArchiveQuestionsByTest(Long oid);

	public List<Question> getNotArchiveQuestionsByTest(Test test);

	public List<Question> getNotArchiveQuestionsByTest(HandyObjectID oid);
}
