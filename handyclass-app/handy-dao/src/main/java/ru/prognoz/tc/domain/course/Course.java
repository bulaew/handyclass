package ru.prognoz.tc.domain.course;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.domain.User;

public interface Course extends Archivable, Serializable {

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectId);

	public CourseCategory getCategory();

	public void setCategory(CourseCategory category);

	public String getShortName();

	public void setShortName(String shortName);

	public String getFullName();

	public void setFullName(String fullName);

	public String getDescription();

	public void setDescription(String description);

	public User getOwner();

	public void setOwner(User owner);

	public Date getAviableFrom();

	public void setAviableFrom(Date aviableFrom);

	public Date getAviableTo();

	public void setAviableTo(Date aviableTo);

	public Date getCreated();

	public void setCreated(Date created);

	public Date getModified();

	public void setModified(Date modified);

	public String getAvatarUrl();

	public void setAvatarUrl(String avatarUrl);

	public int getViewSlideTime();

	public void setViewSlideTime(int viewSlideTime);

	public CourseAccessType getAccessType();

	public void setAccessType(CourseAccessType accessType);

	public Set<Invite> getInvites();

	public void setInvites(Set<Invite> invites);

	public List<Discussion> getDiscussions();

	public void setDiscussions(List<Discussion> discussions);

	public HandyObjectID getTestOid();

	public void setTestOid(HandyObjectID test);
}
