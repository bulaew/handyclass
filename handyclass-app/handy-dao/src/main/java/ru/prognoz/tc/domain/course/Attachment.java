package ru.prognoz.tc.domain.course;

import java.io.Serializable;

import ru.prognoz.tc.domain.Archivable;
import ru.prognoz.tc.domain.HandyObjectID;

public interface Attachment extends Serializable, Archivable {

	public long getId();

	public void setId(long id);

	public HandyObjectID getCourseOid();

	public void setCourseOid(HandyObjectID courseOID);

	public String getDescription();

	public void setDescription(String description);

	public String getAttachmentUrl();

	public void setAttachmentUrl(String attachmentUrl);

	public HandyObjectID getObjectId();

	public void setObjectId(HandyObjectID objectID);
	
	public String getFilename();
	
	public String getExtention();

}
