package ru.prognoz.tc.mvc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;
import ru.prognoz.tc.mvc.business.resource.HandyResourseService;

public class HandyAccessFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(HandyAccessFilter.class);
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (!(request instanceof HttpServletRequest)) {
			chain.doFilter(request, response);
			return;
		}
		HttpServletRequest req = (HttpServletRequest) request;
		String contextPath = req.getContextPath();
		String requestURI = req.getRequestURI();
		String url = requestURI.replaceFirst(contextPath, "");
		String[] parts = url.split("/");
		if (parts.length > 4 && parts[2].equals(HandyResourseService.PRIVATE_COURSES_DIR)) {
			CourseAccessService accessService = HandyclassAppContext.getBean(CourseAccessService.class);
			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			if (accessService != null && obj != null && obj instanceof User) {
				User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				try {
					Long courseOid = Long.parseLong(parts[3]);
					CourseDao courseDao = HandyclassAppContext.getBean(CourseDao.class);
					boolean result = accessService.canPlayAccess(courseDao.getByOid(courseOid), currentUser, LocaleContextHolder.getLocale());
					if (result == false) {
						logger.info("private resourses ["+url+"] external accessing attempt was blocked!");
						return;
					}
				} catch (Exception e) {
					logger.info("private resourses ["+url+"] external accessing attempt was blocked!");
					return;
				}
			} else {
				logger.info("private resourses ["+url+"] external accessing attempt was blocked!");
				return;
			}
		}
		
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
