package ru.prognoz.tc.mvc.controller.coursereg;

import ru.prognoz.tc.domain.User;

public class RegistrationAfterInviteData {
	
	public static final String ATTR_NAME = "RegistrationAfterInviteData";
	
	private String guestEmail;
	private User invitedUser;
	private Long courseOID;

	public String getGuestEmail() {
		return guestEmail;
	}

	public void setGuestEmail(String userEmail) {
		this.guestEmail = userEmail;
	}

	public Long getCourseOID() {
		return courseOID;
	}

	public void setCourseOID(Long courseOID) {
		this.courseOID = courseOID;
	}

	public User getInvitedUser() {
		return invitedUser;
	}

	public void setInvitedUser(User invitedUser) {
		this.invitedUser = invitedUser;
	}

}
