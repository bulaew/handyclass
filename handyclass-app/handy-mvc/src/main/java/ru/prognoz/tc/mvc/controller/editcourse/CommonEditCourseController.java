package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.dao.AttachmentDao;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.resource.HandyResourseService;
import ru.prognoz.tc.service.RollbackedSaver;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse/common")
public class CommonEditCourseController {

    private static final Logger logger = LoggerFactory
	    .getLogger(CommonEditCourseController.class);

    @Autowired
    private EditCourseBean courseBean;
    @Autowired
    private CourseCategoryDao categoryDao;
    @Autowired
    private AttachmentDao attachmentDao;
    @Autowired
    private HandyObjectDao objectDao;
    @Autowired
    private HandyclassAppCfg appCfg;
    @Autowired
    private RollbackedSaver rollbackSaver;
    @Autowired
    private HandyResourseService resourseService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String startEditing(Locale locale, Model model) {
	logger.info("Common Edit Course : start editing");

	// model.addAttribute("bean", courseBean);

	return "/editcourse/common/step";
    }

    @RequestMapping(value = { "/changestep" }, method = RequestMethod.GET)
    public String onChangeStep(Locale locale, Model model,
	    @RequestParam(value = "step") final Integer newStepNumber) {

	logger.info("Common Edit Course : change step");

	courseBean.setStep(EditCourseWizardStep.parseInt(newStepNumber));
	model.addAttribute("bean", courseBean);

	return "redirect:"
		+ EditCourseWizardUtils.getStepUrl(courseBean.getStep());
    }

    @RequestMapping(value = { "/saveFieldsData" }, method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<String> onSaveFieldsData(
	    @RequestParam(value = "course_name") final String fullName,
	    @RequestParam(value = "course_category") final Integer selectedCategoryId,
	    @RequestParam(value = "course_desc") final String courseDescription,
	    @RequestParam(value = "course_fromDate") final String courseFromDate,
	    @RequestParam(value = "course_toDate") final String courseToDate)
	    throws HandyCoreException {

	logger.info("Common Edit Course : save fields data");

	courseBean.getCourse().setFullName(fullName);
	courseBean.getCourse().setCategory(categoryDao.getById(selectedCategoryId));
	courseBean.getCourse().setDescription(courseDescription);
	courseBean.getCourse().setAviableFrom(HcDateTime.parseDate(courseFromDate));
	courseBean.getCourse().setAviableTo(HcDateTime.parseDate(courseToDate));

	DeferredResult<String> result = new DeferredResult<String>();
	result.setResult("ok");
	return result;
    }

    @RequestMapping(value = { "/saveCategory" }, method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<String> onSaveCategory(
	    @RequestParam(value = "category") final Integer category)
	    throws HandyCoreException {
	logger.info("Common Edit Course : save fields data");
	courseBean.getCourse().setCategory(categoryDao.getById(category));
	courseBean.getCourse().setAvatarUrl(
		courseBean.getCourse().getCategory().getAvatarUrl());
	DeferredResult<String> result = new DeferredResult<String>();
	result.setResult("ok");
	return result;
    }

    @RequestMapping(value = { "/avatarupload" }, method = RequestMethod.POST)
    public String avatarupload(@RequestParam MultipartFile avatar) {
	logger.info("Common Edit Course: avatar upload request");

	try {
	    String url = resourseService.storeCourseAvatar(courseBean.getCourse(), avatar);
	    courseBean.getCourse().setAvatarUrl(url);
	} catch (HandyCoreException e1) {
	}
	return "redirect:/main/editcourse/common";
    }

    @RequestMapping(value = { "/categoryavatar" }, method = RequestMethod.POST)
    @ResponseBody
    public String avatarByCategory(@RequestParam int category) {
		logger.info("Common Edit Course: avatar upload request");
		Course course = courseBean.getCourse();
		if(course.getAvatarUrl().contains("/publicdir/")){
		    return "false";
		} else {
		    CourseCategory courseCategory = categoryDao.getById(category);
		    course.setAvatarUrl(courseCategory.getAvatarUrl());
		    course.setCategory(courseCategory);
		    return "true";
		}
	}

    @RequestMapping(value = { "/cleardates" }, method = RequestMethod.GET)
	public String clearDates(Locale locale) {
		logger.info("Common Edit Course: avatar upload request");
		Course course = courseBean.getCourse();
		course.setAviableFrom(new Date());
		course.setAviableTo(null);
		return "redirect:/main/editcourse/common";
	}


    @RequestMapping(value = { "/deleteAvatar" }, method = RequestMethod.GET)
    public String deleteAvatar() {
	logger.info("Common Edit Course : delete avatar");

	courseBean.getCourse().setAvatarUrl("/img/course/default/biology.png");

	DeferredResult<String> result = new DeferredResult<String>();
	result.setResult("ok");
	return "redirect:/main/editcourse/common";
    }

    @RequestMapping(value = { "/attachupload" }, method = RequestMethod.POST)
    public String attachupload(@RequestParam MultipartFile attach_file) {
	logger.info("Common Edit Course: attach upload request");

	try {
	    String url = resourseService.storeCoursePrivateFile(
		    courseBean.getCourse(), attach_file);
	    Attachment attachment = attachmentDao.newDomainInstatnce();
	    attachment.setObjectId(objectDao.generate());
	    attachment.setCourseOid(courseBean.getCourse().getObjectId());
	    attachment.setAttachmentUrl(url);
	    attachment.setDescription(attach_file.getOriginalFilename());
	    courseBean.getAttachments().add(attachment);
	} catch (HandyCoreException e1) {
	}
	return "redirect:/main/editcourse/common";
    }

    @RequestMapping(value = { "/deleteAttachment" }, method = RequestMethod.POST)
    public DeferredResult<String> deleteAttachment(
	    @RequestParam(value = "attachmentOid") final Integer attachmentOid) {
	logger.info("Common Edit Course : delete attachment");

	List<Attachment> attachments = courseBean.getAttachments();
	for (Attachment attachment : attachments) {
	    if (attachment.getObjectId().getId() == attachmentOid) {
		attachment.setArchive(true);
	    }
	}

	DeferredResult<String> result = new DeferredResult<String>();
	result.setResult("editcourse/common/attachments");
	return result;
    }

    @RequestMapping(value = { "/deleteCourse" }, method = RequestMethod.GET)
    public String deleteCourse() {
	courseBean.getCourse().setArchive(true);
	try {
	    rollbackSaver.merge(courseBean.getCourse());
	} catch (Exception e) {
	    e.printStackTrace();
	    return "redirect:/main/";
	}
	return "redirect:/main/";
    }
}
