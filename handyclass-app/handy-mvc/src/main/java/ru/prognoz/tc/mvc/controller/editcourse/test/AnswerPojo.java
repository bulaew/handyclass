package ru.prognoz.tc.mvc.controller.editcourse.test;

public class AnswerPojo {
	private Long oid;
	private String value;
	private String text;

	public Long getOid() {
		return oid;
	}

	public void setOid(Long oid) {
		this.oid = oid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
