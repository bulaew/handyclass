package ru.prognoz.tc.mvc.controller.editcourse.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.test.QuestionDao;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

public class QuestionCreator {

	@Autowired
	private QuestionDao questionDao;

	public static Question createQuestion(String type, Test test) {
		if (StringUtils.isEmpty(type)) {
			return null;
		} else {
			return HandyclassAppContext.getBean(QuestionDao.class).newDomainInstance(type, test);
		}
	}
}
