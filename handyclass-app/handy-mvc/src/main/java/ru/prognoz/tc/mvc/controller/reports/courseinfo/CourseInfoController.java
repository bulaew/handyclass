package ru.prognoz.tc.mvc.controller.reports.courseinfo;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.courseinfo.CourseInfoCriteria;
import ru.prognoz.tc.core.reports.impl.myeducation.MyEducationItem;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.LikeDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.mvc.business.course.slide.SlideService;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
public class CourseInfoController {

	private static final Logger logger = LoggerFactory.getLogger(CourseInfoController.class);

	@Autowired
	private HandyclassAppCfg appConfig;
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private SlideDao slideDao;
	@Autowired
	private SlideService slideService;
	@Autowired
	private LikeDao likeDao;
	@Autowired
	private ViewCounterDao viewCounterDao;

	@RequestMapping(value = "/reports/courseInfo", method = RequestMethod.GET)
	public ModelAndView index(Locale locale, @RequestParam Long coid) throws HandyCoreException {

		logger.info("course info report page");

		ModelAndView mav = new ModelAndView("reports/courseInfo/index");
		mav.addObject("bean", populateBean(locale, null, coid, 0));
		return mav;
	}

	private CourseInfoBean populateBean(Locale locale, ReportViewModel viewModel, Long courseOID, int rowLimit) throws HandyCoreException {
		if (viewModel == null) {
			viewModel = new ReportViewModel();
			viewModel.setController("/reports/courseInfo");
			viewModel.setSort(MyEducationItem.COLUMN_COMPLETE_ATTEMPT_DT);
			viewModel.setDirection(SortDirection.DESC);
			viewModel.setReportType(ReportType.MY_EDUCATION);
		}
		CourseInfoBean bean = new CourseInfoBean();
		bean.setItems(reportService.getCourseInfoItems(criteriaByModel(courseOID, viewModel, rowLimit)));
		bean.setViewModel(viewModel);

		Course currentCourse = courseDao.getByOid(courseOID);
		bean.setCurrentCourse(currentCourse);

		Map<Long, Long> slideCounter = new HashMap<Long, Long>();
		List<Slide> slides = slideDao.getAllVisibleByCourse(courseOID);
		for (Slide slide : slides) {
			slideCounter.put(slide.getObjectId().getId(), slideService.countPlaybacks(slide.getObjectId().getId()));
		}
		bean.setSlideViewMap(slideCounter);

		bean.setCountLikes(likeDao.getCountLikes(courseOID));
		Integer views = viewCounterDao.getCountViews(currentCourse.getObjectId());
		bean.setCountViews(views.longValue());
		return bean;
	}

	private CourseInfoCriteria criteriaByModel(Long courseOID, ReportViewModel viewModel, int rowLimit) {
		CourseInfoCriteria criteria = new CourseInfoCriteria();
		criteria.setFilterCourseOID(courseOID);
		criteria.setRowCount(rowLimit);
		criteria.setOrderColumn(viewModel.getSort());
		criteria.setSortDirection(viewModel.getDirection());
		return criteria;
	}

}
