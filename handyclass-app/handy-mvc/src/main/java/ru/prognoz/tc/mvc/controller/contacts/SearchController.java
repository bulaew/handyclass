package ru.prognoz.tc.mvc.controller.contacts;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.contact.ContactService;

@Controller
public class SearchController {
	
	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired 
	private ContactService contactService;
	
	/**
	 * Перерисвока списка групп.
	 * 
	 * @param locale
	 * @return
	 */
	@RequestMapping(value = { "/search/onSearch" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> renderSearchUl(Locale locale, @RequestParam(value = "searchCriteria") final String searchCriteria) {
		logger.info("Search : render search <UL>");

		ModelAndView mav = new ModelAndView("contacts/search/searchUl");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		try {
			mav.addObject("searchResult", contactService.search(currentUser, searchCriteria));
		} catch (HandyCoreException e) {
		}

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		result.setResult(mav);
		return result;
	}
	
}
