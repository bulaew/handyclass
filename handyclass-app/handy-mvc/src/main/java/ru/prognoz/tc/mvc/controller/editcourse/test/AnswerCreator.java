package ru.prognoz.tc.mvc.controller.editcourse.test;

import org.springframework.beans.factory.annotation.Autowired;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.test.AnswerDao;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;

public class AnswerCreator {
	@Autowired
	private AnswerDao answerDao;

	public static Answer CreateAnswer(Question question) {
		if (question == null) {
			return null;
		} else {
			return HandyclassAppContext.getBean(AnswerDao.class).newDomainInstance(question);
		}
	}
}
