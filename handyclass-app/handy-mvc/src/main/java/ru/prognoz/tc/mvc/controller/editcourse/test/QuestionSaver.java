package ru.prognoz.tc.mvc.controller.editcourse.test;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;

import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.mvc.common.HtmlCleaner;

public class QuestionSaver {
	public static void saveQuestion(Question q, QuestionPojo qp) {
		q.setActive(false);
		q.setText(qp.getQuestion());
	}

	public static void saveAnswers(List<Answer> answers, QuestionPojo qp) {
		if (answers == null) {
			return;
		}
		for (Answer answer : answers) {
			for (AnswerPojo ap : qp.getAnswers()) {
				if (answer.getObjectId().getId() == ap.getOid()) {
					answer.setActive(false);
					if (!StringUtils.isEmpty(ap.getText())) {
						answer.setText(HtmlCleaner.cleanWysiwyg(ap.getText()));
					}
					if (!StringUtils.isEmpty(ap.getValue())) {
						answer.setValue(HtmlCleaner.cleanWysiwyg(ap.getValue()));
					}
				}
			}
		}
	}
}
