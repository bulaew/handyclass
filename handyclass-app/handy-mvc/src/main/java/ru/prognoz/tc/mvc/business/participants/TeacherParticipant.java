package ru.prognoz.tc.mvc.business.participants;

import ru.prognoz.tc.domain.User;

public class TeacherParticipant implements IParticipant {

	private User user;

	public TeacherParticipant(User user) {
		super();
		this.user = user;
	}
	
	@Override
	public int getType() {
		return 1;
	}

	@Override
	public User getUser() {
		return this.user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String getGuestEmail() {
		return null;
	}

	@Override
	public void setGuestEmail(String guestEmail) {
	}

	@Override
	public boolean isRemoved() {
		return false;
	}

	@Override
	public void setRemoved(boolean removed) {
	}

	@Override
	public int compareTo(IParticipant o) {
		return -1; // Учитель всегда на первом месте.
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.getLogin().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeacherParticipant other = (TeacherParticipant) obj;
		if (user != null && other.user != null && user.getId() == other.user.getId()) {
			return true;
		} else {
			return false;
		}
	}

}
