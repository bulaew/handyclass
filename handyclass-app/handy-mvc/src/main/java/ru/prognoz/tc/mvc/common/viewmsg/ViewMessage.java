package ru.prognoz.tc.mvc.common.viewmsg;

public class ViewMessage {

	private String clazz;
	private String subject;
	private String description;
	private String forField = "";
	
	public ViewMessage(String clazz, String subject, String forField, String description) {
		super();
		this.clazz = clazz;
		this.subject = subject;
		this.description = description;
	}
	
	public ViewMessage(String clazz, String description) {
		super();
		this.clazz = clazz;
		this.subject = "";
		this.description = description;
	}
	
	public ViewMessage(String clazz, String forField, String description) {
		super();
		this.clazz = clazz;
		this.subject = "";
		this.description = description;
		this.forField = forField;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getForField() {
		return forField;
	}

	public void setForField(String forField) {
		this.forField = forField;
	}
	
}
