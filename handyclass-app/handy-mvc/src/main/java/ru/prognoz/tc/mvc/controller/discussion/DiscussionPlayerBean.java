package ru.prognoz.tc.mvc.controller.discussion;

import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;

public class DiscussionPlayerBean {
	private boolean editCourseMode = true;
	private int countRegistrations;
	private Course course;

	private Discussion activeDiscussion = null;

	private Boolean editableDiscussion = false;

	public int getCountRegistrations() {
		return countRegistrations;
	}

	public void setCountRegistrations(int countRegistrations) {
		this.countRegistrations = countRegistrations;
	}

	public Boolean getEditableDiscussion() {
		return editableDiscussion;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void clear() {
		editCourseMode = true;
		course = null;
		activeDiscussion = null;
		editableDiscussion = false;
	}

	public boolean isEditCourseMode() {
		return editCourseMode;
	}

	public void setEditCourseMode(boolean editCourseMode) {
		this.editCourseMode = editCourseMode;
	}

	public Discussion getActiveDiscussion() {
		return activeDiscussion;
	}

	public void setActiveDiscussion(Discussion activeDiscussion) {
		this.activeDiscussion = activeDiscussion;
	}

	public Boolean isEditableDiscussion() {
		return editableDiscussion;
	}

	public void setEditableDiscussion(Boolean editableDiscussion) {
		this.editableDiscussion = editableDiscussion;
	}
}
