package ru.prognoz.tc.mvc.controller.courselist.mode;

public enum UserMode {

	TEACHER(1), PUPIL(2);

	private int type;

	private UserMode(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		switch (type) {
		case 1:
			return "TEACHER";
		case 2:
			return "PUPIL";
		default:
			return super.toString();
		}
	}

	public int getIntValue() {
		return type;

	}

	public static UserMode parseInt(int code) {
		for (UserMode mode : UserMode.values()) {
			if (mode.getIntValue() == code) {
				return mode;
			}
		}
		return null;
	}

	public static UserMode parseStr(String str) {
		for (UserMode mode : UserMode.values()) {
			if (mode.toString().equals(str)) {
				return mode;
			}
		}
		return null;
	}

}
