package ru.prognoz.tc.mvc.business.auth;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.mvc.security.SocialNetwork;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

@Component
@Scope(value = "session")
public class FacebookAuthorizator implements SocialAuthorizator {

	private static final long serialVersionUID = -3752958586711273898L;

	@Autowired
	private HandyclassAppCfg appCfg;

	private FacebookConnectionFactory connectionFactory;

	/**
	 * Построение сслыки для авторизации. До построения должны быть проинициализированы параметры: fbAppId, fbSecretKey, fbRedirectURL.
	 * 
	 * @return УРЛ для авторизации.
	 * @throws IllegalArgumentException
	 *             если параметры не инициализированы.
	 */
	@Override
	public String buildAuthUrl() throws IllegalArgumentException {
		OAuth2Operations oauthOperations = getConnectionFactory().getOAuthOperations();
		OAuth2Parameters params = new OAuth2Parameters();
		params.setRedirectUri(appCfg.getFbRedirectURL());
		String authorizeUrl = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, params);

		return authorizeUrl;
	}

	/**
	 * Валидация параметров авторизации.
	 */
	@Override
	public void validateAuthParams() throws IllegalArgumentException {
		if (StringUtils.isEmpty(appCfg.getFbAppId()) || StringUtils.isEmpty(appCfg.getFbSecretKey())
				|| StringUtils.isEmpty(appCfg.getFbRedirectURL())) {
			throw new IllegalArgumentException("Facebook authorization arguments exception.");
		}
	}

	@Override
	public SocialNetworkProfile getSocialProfile(String authorizationCode) {
		UserProfile profile = getUserinfo(authorizationCode);
		SocialNetworkProfile result = new SocialNetworkProfile();
		result.setNetwork(SocialNetwork.FACEBOOK);
		result.setAccountId(profile.getUsername());
		result.setFirstName(profile.getFirstName());
		result.setLastName(profile.getLastName());
		return result;
	}

	/**
	 * Получение информации о пользователе.
	 * 
	 * @param authorizationCode
	 *            код авторизации, полученный от системы авторизации фейсбука.
	 * @return UserProfile
	 */
	private UserProfile getUserinfo(String authorizationCode) {
		OAuth2Operations oauthOperations = getConnectionFactory().getOAuthOperations();
		AccessGrant accessGrant = oauthOperations.exchangeForAccess(authorizationCode, appCfg.getFbRedirectURL(), null);
		Connection<Facebook> connection = getConnectionFactory().createConnection(accessGrant);
		return connection.fetchUserProfile();
	}

	private FacebookConnectionFactory getConnectionFactory() throws IllegalArgumentException {
		if (connectionFactory == null) {
			validateAuthParams();
			connectionFactory = new FacebookConnectionFactory(appCfg.getFbAppId(), appCfg.getFbSecretKey());
		}
		return connectionFactory;
	}

}
