package ru.prognoz.tc.mvc.business.participants;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.dao.contact.ContactListContentDao;
import ru.prognoz.tc.dao.contact.ContactListInfoDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.contact.Contact;
import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

@Service
public class ParticipantService {

	private static final Logger logger = LoggerFactory.getLogger(ParticipantService.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ContactListInfoDao contactListInfoDao;
	@Autowired
	private ContactListContentDao contactListContentDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private InviteDao inviteDao;

	/**
	 * <p>
	 * Поиск участника по заданной критерии.
	 * </p>
	 * Сначала поиск проводится среди участников курса для выявления дубликатов. Затем поиск проводится среди пользователей, списков
	 * пользователей. Если поиск результатов не дал, то считается что участник задается при помощи емейла.
	 * 
	 * @param criteria
	 *            String критерий поиска.
	 * @param listParticipants
	 *            Set<> участнику курса.
	 * @param locale
	 *            Locale локаль системы, для интернационализации сообщений.
	 * @return SearchParticipantResultBean Бин с результатами поиска.
	 */
	public SearchParticipantResultBean search(String criteria, Set<IParticipant> listParticipants, Locale locale) {
		// Если пользователь задал емейл адрес, то:
		// 1. Проверяем приглашения на предмет повторений, если есть - выводим пользователю уведомление.
		// 2. Ищем группы с таким названием.
		// 3. Ищем пользователя с таким емейлом, если есть - показываем найденного.
		// 4. делаем валидацию емейла.
		// 5. Показываем емейл, как несозданный контакт.

		logger.info("Поиск участника курса");
		SearchParticipantResultBean searchResult = new SearchParticipantResultBean();

		// 1.
		IParticipant participant = findDuplicate(criteria, listParticipants);
		if (participant != null) { // найден повтор.
			if (!participant.isRemoved()) { // Если нашли неудаленного участника - выводим предупреждение.

				searchResult.setMsg(new ViewMessage("error", messageSource.getMessage("ParticipantEditCourseAction.messageEmailInvalid",
						null, locale)));
				return searchResult;
			} else {
				searchResult.setParticipant(participant);
				return searchResult;
			}
		}

		// 2.
		List<ContactListInfo> groups = contactListInfoDao.search(criteria);
		if (groups.size() > 0) {
			searchResult.setContactList(groups.get(0));
			return searchResult;
		}
		// 3.
		User user = userDao.getByLogin(criteria);
		if (user != null) {
			Invite newInvite = inviteDao.newDomainInstatnce();
			newInvite.setUser(user);
			InvitedParticipant newParticipant = new InvitedParticipant(newInvite);
			searchResult.setParticipant(newParticipant);
			return searchResult;
		}
		// 4.
		if (!EmailValidator.getInstance().isValid(criteria)) {
			searchResult.setMsg(new ViewMessage("error", messageSource.getMessage("ParticipantEditCourseAction.messageEmailInvalid", null,
					locale)));
			return searchResult;
		} else { // 5.
			Invite newInvite = inviteDao.newDomainInstatnce();
			newInvite.setGuestEmail(criteria);
			InvitedParticipant newParticipant = new InvitedParticipant(newInvite);
			searchResult.setParticipant(newParticipant);
			return searchResult;
		}
	}

	/**
	 * Добавление <b>пользователя</b> как участника курса.
	 * 
	 * @param userOid
	 *            ИД пользователя
	 * @param listParticipants
	 *            список участников курса
	 * @param locale
	 *            локаль
	 * @return SearchParticipantResultBean результат добавления.
	 * @throws HandyCoreException
	 */
	public SearchParticipantResultBean inviteParticipant(long userOid, Set<IParticipant> listParticipants, Locale locale)
			throws HandyCoreException {
		logger.info("Добавление пользователя как участника курса ");
		SearchParticipantResultBean result = new SearchParticipantResultBean();
		User user = userDao.getByOid(userOid);
		if (user == null) {
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		IParticipant newParticipant = inviteByUser(user, listParticipants);
		if (newParticipant == null) {
			return result;
		}
		listParticipants.add(newParticipant);
		result.setParticipant(newParticipant);
		result.setMsg(new ViewMessage("success", messageSource.getMessage("ParticipantEditCourseAction.messageUserInvited", null, locale)));
		return result;
	}

	/**
	 * Добавление участника по заданному <b>емейлу</b>
	 * 
	 * @param email
	 *            емейл участника
	 * @param listParticipants
	 *            список участников
	 * @param locale
	 *            локаль
	 * @return SearchParticipantResultBean результат добавления.
	 * @throws HandyCoreException
	 */
	public SearchParticipantResultBean inviteParticipant(String email, Set<IParticipant> listParticipants, Locale locale)
			throws HandyCoreException {
		logger.info("Добавление участника курса по его емейлу");
		SearchParticipantResultBean result = new SearchParticipantResultBean();
		if (StringUtils.isEmpty(email)) {
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		IParticipant newParticipant = inviteByEmail(email, listParticipants);
		if (newParticipant == null) {
			return result;
		}
		listParticipants.add(newParticipant);
		result.setParticipant(newParticipant);
		result.setMsg(new ViewMessage("success", messageSource.getMessage("ParticipantEditCourseAction.messageUserInvited", null, locale)));
		return result;
	}

	/**
	 * Добавление участников из <b>списка контактов</b>
	 * 
	 * @param listInfo
	 *            Список контактов.
	 * @param listParticipants
	 *            список участников
	 * @param locale
	 *            локаль
	 * @return SearchParticipantResultBean результат добавления.
	 * @throws HandyCoreException
	 */
	public SearchParticipantResultBean inviteParticipant(ContactListInfo listInfo, Set<IParticipant> listParticipants, Locale locale)
			throws HandyCoreException {
		SearchParticipantResultBean result = new SearchParticipantResultBean();
		if (listInfo == null) {
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		List<Contact> contacts = contactListContentDao.members(listInfo);
		for (Contact contact : contacts) {
			inviteParticipant(contact, listParticipants, locale);
		}
		result.setContactList(listInfo);
		result.setMsg(new ViewMessage("success", messageSource.getMessage("ParticipantEditCourseAction.messageUserInvited", null, locale)));
		return new SearchParticipantResultBean();
	}

	/**
	 * Добавление <b>контакта</b> в участники курса
	 * 
	 * @param contact
	 *            Контакт
	 * @param listParticipants
	 *            список участников
	 * @param locale
	 *            локаль
	 * @return SearchParticipantResultBean результат добавления.
	 * @throws HandyCoreException
	 */
	public SearchParticipantResultBean inviteParticipant(Contact contact, Set<IParticipant> listParticipants, Locale locale)
			throws HandyCoreException {
		logger.info("добавление контакта в участники курса");
		SearchParticipantResultBean result = new SearchParticipantResultBean();
		IParticipant newParticipant;
		if (contact.getUser() != null) {
			newParticipant = inviteByUser(contact.getUser(), listParticipants);
		} else {
			newParticipant = inviteByEmail(contact.getEmail(), listParticipants);
		}
		if (newParticipant == null) {
			return result;
		}
		listParticipants.add(newParticipant);
		result.setParticipant(newParticipant);
		result.setMsg(new ViewMessage("success", messageSource.getMessage("ParticipantEditCourseAction.messageUserInvited", null, locale)));
		return new SearchParticipantResultBean();
	}

	public void removeParticipant(final Long userOid, String email, Set<IParticipant> listParticipants, Locale locale)
			throws HandyCoreException {
		if (userOid != null) {
			User user = userDao.getByOid(userOid);
			if (user == null) {
				throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
			}
			email = user.getLogin();
		}
		IParticipant fParticipant = findDuplicate(email, listParticipants);
		if (fParticipant == null) {
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		fParticipant.setRemoved(true);
	}

	private IParticipant inviteByUser(User user, Set<IParticipant> listParticipants) throws HandyCoreException {
		Invite invite = inviteDao.newDomainInstatnce();
		invite.setUser(user);
		return invite(new InvitedParticipant(invite), listParticipants);
	}

	private IParticipant inviteByEmail(String email, Set<IParticipant> listParticipants) throws HandyCoreException {
		Invite invite = inviteDao.newDomainInstatnce();
		invite.setGuestEmail(email);
		return invite(new InvitedParticipant(invite), listParticipants);
	}

	private IParticipant invite(InvitedParticipant invitedParticipant, Set<IParticipant> listParticipants) throws HandyCoreException {
		String email;
		if (invitedParticipant.getUser() != null) {
			email = invitedParticipant.getUser().getLogin();
		} else {
			email = invitedParticipant.getGuestEmail();
		}
		IParticipant fParticipant = findDuplicate(email, listParticipants);
		if (fParticipant == null) { // если такого емейла еще нет в участниках
			return invitedParticipant;
		} else {
			if (fParticipant.isRemoved()) {
				fParticipant.setRemoved(false);
				return fParticipant;
			}
		}
		// Иначе выдаем ошибку. Повторное добавление участника.
		throw new HandyCoreException("Duplicated invite");
	}

	/*
	 * @return true если такой емейл уже добавлен.
	 */
	private IParticipant findDuplicate(String searchCriteria, Set<IParticipant> listParticipants) {

		for (IParticipant participant : listParticipants) {
			if (participant.getUser() != null && participant.getUser().getLogin().equals(searchCriteria)) {
				return participant;
			}
			if (participant.getGuestEmail() != null && participant.getGuestEmail().equals(searchCriteria)) {
				return participant;
			}
		}
		return null;
	}
}
