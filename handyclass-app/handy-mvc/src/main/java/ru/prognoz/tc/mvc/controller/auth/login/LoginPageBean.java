package ru.prognoz.tc.mvc.controller.auth.login;

import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

public class LoginPageBean {

	private boolean showLoginDlg = false;
	private boolean showSignupDlg = false;
	private ViewMessage signupValidationMsg = null;
	private String firstName = "";
	private String lastName = "";
	private String email = "";
	private String password = "";
	private String repeatPass = "";

	public boolean isShowLoginDlg() {
		return showLoginDlg;
	}

	public void setShowLoginDlg(boolean showLoginDlg) {
		this.showLoginDlg = showLoginDlg;
	}

	public boolean isShowSignupDlg() {
		return showSignupDlg;
	}

	public void setShowSignupDlg(boolean showSingupDlg) {
		this.showSignupDlg = showSingupDlg;
	}

	public ViewMessage getSignupValidationMsg() {
		return signupValidationMsg;
	}

	public void setSignupValidationMsg(ViewMessage signupValidationMsg) {
		this.signupValidationMsg = signupValidationMsg;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepeatPass() {
		return repeatPass;
	}

	public void setRepeatPass(String repeatPass) {
		this.repeatPass = repeatPass;
	}

}
