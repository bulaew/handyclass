package ru.prognoz.tc.mvc.controller.courselist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.CourseRegistrationDao;
import ru.prognoz.tc.dao.LikeDao;
import ru.prognoz.tc.dao.NotificationDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.domain.course.Notification;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.business.notification.NotificationType;
import ru.prognoz.tc.mvc.controller.courselist.mode.NotifyMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.RegisteredMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.SortMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.UserMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.ViewMode;

/**
 * Страница списка курсов.
 * 
 * @author mikulich
 * 
 */
@Controller
public class CourseListController {

	private class DateComporator implements Comparator<Notification> {
		@Override
		public int compare(Notification o1, Notification o2) {
			return o1.getDate().compareTo(o2.getDate());
		}
	}

	@Autowired
	private CourseDao courseDao;
	@Autowired
	private CourseCategoryDao courseCategoryDao;
	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private CourseAccessService courseAccessService;
	@Autowired
	private CourseRegistrationDao courseRegistrationDao;
	@Autowired
	private CourseAttemptDao courseAttemptDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private LikeDao likeDao;
	@Autowired
	private HandyclassAppCfg appCfg;
	@Autowired
	private ViewCounterDao viewCounterDao;

	private static final Logger logger = LoggerFactory.getLogger(CourseListController.class);
	private static final String BEAN_ATTR_NAME = "courselistData";

	@RequestMapping(value = {"/main","/"}, method = RequestMethod.GET)
	public ModelAndView userView(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String inputJson) {
		logger.info("Course List", locale);

		CourseListViewModel viewModel = json2model(inputJson);

		CourseListBean bean = populateBean(locale, viewModel);
		ModelAndView mav = new ModelAndView("course/courselist");
		mav.addObject(BEAN_ATTR_NAME, bean);

		response.addCookie(model2cookie(bean.getViewModel(), request));
		return mav;
	}

	private CourseListViewModel json2model(String json) {
		CourseListViewModel viewModel = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			viewModel = mapper.readValue(json, CourseListViewModel.class);
		} catch (Exception e) {
			viewModel = new CourseListViewModel();
		}
		return viewModel;
	}

	private Cookie model2cookie(CourseListViewModel vm, HttpServletRequest request) {
		String json = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(vm);
		} catch (Exception e) {
			logger.error("Проблема с созданием json для куков");
		}

		Cookie cookie = new Cookie("viewModel", json);
		cookie.setMaxAge(60 * 60 * 24 * 14); // две недели живут куки
		cookie.setPath(request.getContextPath());
		return cookie;
	}

	/**
	 * При закрытии страницы читаем жсон с клиента и сохраняем в куки.
	 * 
	 * @param locale
	 * @param response
	 * @param viewModel
	 * @return
	 */
	@RequestMapping(value = { "/main/saveViewModel" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<CourseListViewModel> saveViewModel(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@RequestBody CourseListViewModel viewModel) {
		logger.info("Course List: saveViewModel");

		response.addCookie(model2cookie(viewModel, request));

		DeferredResult<CourseListViewModel> result = new DeferredResult<CourseListViewModel>();
		result.setResult(viewModel);
		return result;

	}

	@RequestMapping(value = "/main/courselist/removeNotifies", method = RequestMethod.POST)
	public ModelAndView removeNotifies(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: removeNotifies");
		CourseListViewModel viewModel = json2model(json);
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		notificationService.removeNotifications(currentUser, viewModel.getUserMode());
		CourseListBean bean = populateBean(locale, viewModel);
		ModelAndView mav = new ModelAndView("course/notifyPanel");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}

	// Переключатель между режимами ученика/учителя.

	@RequestMapping(value = "/main/courselist/renderUserModeUl", method = RequestMethod.POST)
	public ModelAndView renderUserModeUl(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderUserModeUl");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/usermodeUL");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}

	@RequestMapping(value = "/main/courselist/changeUserMode", method = RequestMethod.POST)
	public void changeUserMode(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody UserMode userMode) {
		logger.info("Course List: changeUserMode");

		CourseListViewModel vm = json2model(json);
		vm.setUserMode(userMode);
		response.addCookie(model2cookie(vm, request));
	}

	// Переключатель между режимами портрет/список

	@RequestMapping(value = "/main/courselist/renderViewModeUl", method = RequestMethod.POST)
	public ModelAndView renderViewModeUl(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderViewModeUl");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/viewmodeUL");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}
	
	@RequestMapping(value = "/main/courselist/changeViewMode", method = RequestMethod.POST)
	public void changeViewMode(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody ViewMode viewMode) {
		logger.info("Course List: changeViewMode");

		CourseListViewModel vm = json2model(json);
		vm.setViewMode(viewMode);
		response.addCookie(model2cookie(vm, request));
	}

	// Панель оповещений
	
	@RequestMapping(value = "/main/courselist/renderNotifyPanel", method = RequestMethod.POST)
	public ModelAndView renderNotifyPanel(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderNotifyPanel");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/notifyPanel");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}
	
	@RequestMapping(value = "/main/courselist/switchNotifyMode", method = RequestMethod.POST)
	public void switchNotifyMode(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody NotifyMode notifyMode) {
		logger.info("Course List: switchNotifyMode");
		CourseListViewModel vm = json2model(json);
		vm.setNotifyMode(notifyMode);
		response.addCookie(model2cookie(vm, request));
	}

	// Панель курсов
	
	@RequestMapping(value = "/main/courselist/renderCoursesPanel", method = RequestMethod.POST)
	public ModelAndView renderCoursesPanel(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderCoursesPanel");
		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/coursesPanel");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}

	// фильтр категорий 
	
	@RequestMapping(value = "/main/courselist/renderCategoryFilter", method = RequestMethod.POST)
	public ModelAndView renderCategoryFilter(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderCategoryFilter");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/categorySelector");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}
	
	@RequestMapping(value = "/main/courselist/changeCategoryFilter", method = RequestMethod.POST)
	public void changeCategoryFilter(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody Integer newCategoryValue) {
		logger.info("Course List: changeCategoryFilter");

		CourseListViewModel vm = json2model(json);
		vm.setCategoryValue(newCategoryValue);
		response.addCookie(model2cookie(vm, request));
	}

	// фильтр сортировок
	
	@RequestMapping(value = "/main/courselist/renderSortingFilter", method = RequestMethod.POST)
	public ModelAndView renderSortingFilter(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderSortingFilter");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/sortingSelector");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}
	
	@RequestMapping(value = "/main/courselist/changeSortingFilter", method = RequestMethod.POST)
	public void changeSortingFilter(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody SortMode sortMode) {
		logger.info("Course List: changeSortingFilter");

		CourseListViewModel vm = json2model(json);
		vm.setSortMode(sortMode);
		response.addCookie(model2cookie(vm, request));
	}

	// фильтр регистрации
	
	@RequestMapping(value = "/main/courselist/renderRegFilter", method = RequestMethod.POST)
	public ModelAndView renderRegFilter(Locale locale, @CookieValue(value = "viewModel", defaultValue = "") String json) {
		logger.info("Course List: renderRegFilter");

		CourseListBean bean = populateBean(locale, json2model(json));
		ModelAndView mav = new ModelAndView("course/registeredSelector");
		mav.addObject(BEAN_ATTR_NAME, bean);
		return mav;
	}
	
	@RequestMapping(value = "/main/courselist/changeRegFilter", method = RequestMethod.POST)
	public void changeRegFilter(Locale locale, HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "viewModel", defaultValue = "") String json, @RequestBody RegisteredMode regMode) {
		logger.info("Course List: changeRegFilter");

		CourseListViewModel vm = json2model(json);
		vm.setRegMode(regMode);
		response.addCookie(model2cookie(vm, request));
	}

	private CourseListBean populateBean(Locale locale, CourseListViewModel viewModel) {
		CourseListBean bean = new CourseListBean();
		if (viewModel == null) {
			viewModel = new CourseListViewModel();
		}
		bean.setViewModel(viewModel);
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		bean.setCourseItems(populateCourses(currentUser, viewModel, locale));
		bean.setNotifications(populateNotifications(currentUser, viewModel, locale));

		List<CourseCategory> categories = new ArrayList<CourseCategory>();
		categories.add(new FakeCategory());
		categories.addAll(courseCategoryDao.getAll());
		bean.setCategories(categories);

		return bean;
	}

	private List<CourseItem> populateCourses(User currentUser, CourseListViewModel viewModel, Locale locale) {
		List<Course> courses = courseDao.getAllCourses();
		List<CourseItem> courseItems = new ArrayList<CourseItem>();
		List<CourseRegistration> regs = courseRegistrationDao.getUserRegistrations(currentUser);
		for (Course course : courses) {
			// фильтрация по режиму пользователя:
			if (viewModel.getUserMode().equals(UserMode.TEACHER)) {
				if (course.getOwner().getId() != currentUser.getId()) {
					continue;
				}
			} else {
				boolean can = false;
				try {
					can = courseAccessService.canViewAccess(course, currentUser, locale);
				} catch (HandyCoreException e) {
				}
				if (!can) {
					continue;
				}
			}
			// фильтрация по категории:
			if (viewModel.getCategoryValue() != 0) {
				if (course.getCategory().getId() != viewModel.getCategoryValue()) {
					continue;
				}
			}
			// фильтрация по фильтру регистрации:
			if (viewModel.getRegMode().equals(RegisteredMode.REGISTERED) && !isContain(course, regs)) {
				continue;
			}
			if (viewModel.getRegMode().equals(RegisteredMode.NOT_REGISTERED) && isContain(course, regs)) {
				continue;
			}
			// фильтрация завершенных курсов:
			if (UserMode.PUPIL.equals(viewModel.getUserMode())) {
				if (course.getAviableTo() != null && course.getAviableTo().before(new Date())) {
					continue;
				}
			}
			CourseItem item = courseToCourseItem(course, currentUser, locale);
			if (item != null) {
				courseItems.add(item);
			}
		}

		Collections.sort(courseItems, viewModel.getSortMode().getComparator());
		return courseItems;
	}

	private boolean isContain(Course course, List<CourseRegistration> regs) {
		boolean result = false;
		for (CourseRegistration courseRegistration : regs) {
			if (courseRegistration.getCourseOid().getId() == course.getObjectId().getId()) {
				return true;
			}
		}
		return result;
	}

	private CourseItem courseToCourseItem(Course course, User currentUser, Locale locale) {
		try {
			CourseItem item = new CourseItem(course);
			CourseAttempt courseAttempt = courseAttemptDao.get(course, currentUser);
			if (courseAttempt != null) {
				item.setFinished(courseAttemptDao.get(course, currentUser).isComplete());
			}
			List<CourseRegistration> regs = courseRegistrationDao.getUserRegistrations(currentUser);
			item.setDescription(course.getDescription());
			item.setRegistered(isContain(course, regs));
			item.setCountReg(Integer.valueOf(viewCounterDao.getCountViews(course.getObjectId())));
			item.setCountLikes(likeDao.getCountLikes(course.getObjectId().getId()));
			if (course.getAviableTo() != null) {
				item.setAvailableTo(HcDateTime.tooltipDt(course.getAviableTo(), locale));
				Date curDt = new Date();
				long diff = course.getAviableTo().getTime() - curDt.getTime();
				item.setExpired(diff < 1000 * 60 * 60 * 24 * 3); // меньше трех дней.
			} else {
				item.setAvailableTo(messageSource.getMessage("CommonEditCourseAction.indefinite", null, locale));
			}

			return item;
		} catch (NullPointerException e) {
			logger.error("Can't convert course to courseitem, user oid is" + course.getOwner().getObjectId());
			return null;
		}
	}

	private List<NotificationItem> populateNotifications(User currentUser, CourseListViewModel viewModel, Locale locale) {
		List<Notification> notifications = notificationDao.getByReceiver(currentUser);
		Collections.sort(notifications, Collections.reverseOrder(new DateComporator()));
		List<NotificationItem> resultList = new ArrayList<NotificationItem>();
		for (Notification notification : notifications) {
			NotificationType notificationType = NotificationType.parseInt(notification.getNotificationType());
			if (notificationType.isApplicable(viewModel.getUserMode())) {
				NotificationItem item = new NotificationItem();
				if (notification.getPerson() != null) {
					item.setUserName(notification.getPerson().getUsername());
					item.setUserOid(notification.getPerson().getObjectId().getId() + "");
				}
				item.setDate(HcDateTime.notifyDt(notification.getDate(), locale));
				try {
					item.setMessage(notificationService.getNotificationText(locale, notification));
				} catch (HandyCoreException e) {
					item.setMessage(e.getMessage());
				}
				resultList.add(item);
			}
			if (NotifyMode.HIDE.equals(viewModel.getNotifyMode()) && resultList.size() >= 5) {
				break;
			}
		}

		return resultList;
	}
}
