package ru.prognoz.tc.mvc.controller.contacts;

import java.io.Serializable;
import java.util.Map;

public class AddressbookRequestBody implements Serializable {

	private static final long serialVersionUID = 968632210995523052L;
	private Map<Integer, Long> contacts;
	private Long activeList;

	public Map<Integer, Long> getContacts() {
		return contacts;
	}

	public void setContacts(Map<Integer, Long> contacts) {
		this.contacts = contacts;
	}

	public Long getActiveList() {
		return activeList;
	}

	public void setActiveList(Long activeList) {
		this.activeList = activeList;
	}

}
