package ru.prognoz.tc.mvc.common;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;

@Component
public class GoogleMailSender {

	@Autowired
	private HandyclassAppCfg cfg;

	public void sendMsg(String emailAddress, String subject, String htmlContent) throws HandyCoreException {
		SendProcess process = new SendProcess();
		process.init(emailAddress, subject, htmlContent);
		process.start();
	}

	private class SendProcess extends Thread {
		private String emailAddress;
		private String subject;
		private String htmlContent;

		public void init(String emailAddress, String subject, String htmlContent) throws HandyCoreException {
			if (StringUtils.isEmpty(emailAddress) || StringUtils.isEmpty(subject) || StringUtils.isEmpty(htmlContent)) {
				throw new HandyCoreException("не проинициализирован параметр при отправке емейла.");
			}
			this.emailAddress = emailAddress;
			this.subject = subject;
			this.htmlContent = htmlContent;

		}

		@Override
		public void run() {
			try {
				HtmlEmail email = new HtmlEmail();
				email.setHostName("smtp.googlemail.com");
				email.setAuthenticator(new DefaultAuthenticator(cfg.getHostMailLogin(), cfg.getHostMailPassword()));
				email.setSslSmtpPort("465");
				email.setSSLOnConnect(true);
				email.setCharset("UTF-8");
				email.setFrom("handyclasses@gmail.com");
				email.setSubject(subject);
				email.setHtmlMsg(htmlContent);
				email.addTo(emailAddress);
				email.send();
			} catch (EmailException e) {
				throw new IllegalThreadStateException(e.getMessage());
			}
		}
	}

}
