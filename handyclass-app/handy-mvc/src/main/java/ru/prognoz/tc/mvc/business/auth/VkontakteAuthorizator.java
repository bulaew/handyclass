package ru.prognoz.tc.mvc.business.auth;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.social.connect.Connection;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.social.vkontakte.api.VKontakte;
import org.springframework.social.vkontakte.api.VKontakteProfile;
import org.springframework.social.vkontakte.connect.VKontakteConnectionFactory;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.mvc.security.SocialNetwork;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

@Component
@Scope(value = "session")
public class VkontakteAuthorizator implements SocialAuthorizator {

	private static final long serialVersionUID = 6425905955505532543L;

	@Autowired
	private HandyclassAppCfg appCfg;

	private VKontakteConnectionFactory connectionFactory;

	public String buildAuthUrl() throws IllegalArgumentException {
		OAuth2Operations oauthOperations = getConnectionFactory().getOAuthOperations();
		OAuth2Parameters params = new OAuth2Parameters();
		params.setRedirectUri(appCfg.getVkRedirectURL());
		String authorizeUrl = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, params);
		return authorizeUrl;
	}

	public void validateAuthParams() throws IllegalArgumentException {
		if (StringUtils.isEmpty(appCfg.getVkAppId()) || StringUtils.isEmpty(appCfg.getVkSecretKey())
				|| StringUtils.isEmpty(appCfg.getVkRedirectURL())) {
			throw new IllegalArgumentException("VKontakte authorization arguments exception.");
		}
	}

	@Override
	public SocialNetworkProfile getSocialProfile(String authorizationCode) throws HandyCoreException {
		VKontakteProfile profile = getUserinfo(authorizationCode);
		SocialNetworkProfile result = new SocialNetworkProfile();
		result.setNetwork(SocialNetwork.VKONTAKTE);
		result.setAccountId(profile.getUid());
		result.setFirstName(profile.getFirstName());
		result.setLastName(profile.getLastName());
		return result;
	}

	public VKontakteConnectionFactory getConnectionFactory() {
		if (connectionFactory == null) {
			validateAuthParams();
			connectionFactory = new VKontakteConnectionFactory(appCfg.getVkAppId(), appCfg.getVkSecretKey());
		}
		return connectionFactory;
	}

	/**
	 * Получение информации о пользователе.
	 * 
	 * @param authorizationCode
	 *            код авторизации, полученный от системы авторизации vkontakte.
	 * @return UserProfile
	 * @throws CoreModuleException
	 */
	private VKontakteProfile getUserinfo(String authorizationCode) throws HandyCoreException {
		VKontakteProfile profile = null;
		try {
			OAuth2Operations oauthOperations = getConnectionFactory().getOAuthOperations();
			AccessGrant accessGrant = oauthOperations.exchangeForAccess(authorizationCode, appCfg.getVkRedirectURL(), null);
			Connection<VKontakte> connection = getConnectionFactory().createConnection(accessGrant);
			VKontakte vkApi = connection.getApi();
			profile = vkApi.usersOperations().getProfile();
		} catch (Exception e) {
			throw new HandyCoreException("error receiving user info");
		}
		return profile;
	}

}
