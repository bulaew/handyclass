package ru.prognoz.tc.mvc.business.auth;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.mvc.security.SocialNetwork;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow.Builder;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;

/**
 * вход в гугл-консоль: <url>https://code.google.com/apis/console/b/0/?noredirect#project:850864967234:overview</url>
 * 
 * @author mikulich
 * 
 */
@Component
@Scope(value = "session")
public class GoogleAuthorizator implements SocialAuthorizator {

	private static final long serialVersionUID = -5751244781180371569L;
	@Autowired
	private HandyclassAppCfg appCfg;
	/**
	 * Список приложений к которым требуется доступ.
	 */
	private List<String> scopes = Arrays.asList("https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile");

	private static final HttpTransport TRANSPORT = new NetHttpTransport();
	private static final JacksonFactory JSON_FACTORY = new JacksonFactory();
	private GoogleAuthorizationCodeFlow authorizationFlow;
	private String lastAuthCode = "";
	private GoogleCredential credential;

	public void validateAuthParams() throws IllegalArgumentException {
		if (StringUtils.isEmpty(appCfg.getGoogleCallbackUrl()) || StringUtils.isEmpty(appCfg.getGoogleClientID())
				|| StringUtils.isEmpty(appCfg.getGoogleClientSecret()) || scopes == null) {
			throw new IllegalArgumentException("Google authorization arguments exception.");
		}
	}

	/**
	 * Построение УРЛ на который следует перенаправить приложение для авторизации. Параметры : callbackUrl, clientID, clientSecret, scopes
	 * должны быть инициализированы.
	 * 
	 * @return String УРЛ авторизации.
	 */
	public String buildAuthUrl() throws IllegalArgumentException {
		GoogleAuthorizationCodeRequestUrl url = getAuthorizationFlow().newAuthorizationUrl();
		url.setRedirectUri(appCfg.getGoogleCallbackUrl());
		return url.build();
	}

	@Override
	public SocialNetworkProfile getSocialProfile(String authorizationCode) throws HandyCoreException {
		try {
			Userinfo userinfo = getUserInfo(authorizationCode);
			SocialNetworkProfile result = new SocialNetworkProfile();
			result.setNetwork(SocialNetwork.GOOGLE);
			result.setAccountId(userinfo.getEmail());
			result.setFirstName(userinfo.getGivenName());
			result.setLastName(userinfo.getFamilyName());
			return result;
		} catch (Exception e) {
			throw new HandyCoreException(e);
		}
	}

	private GoogleAuthorizationCodeFlow getAuthorizationFlow() throws IllegalArgumentException {
		if (authorizationFlow == null) {
			validateAuthParams();
			GoogleAuthorizationCodeFlow.Builder builder = new Builder(TRANSPORT, JSON_FACTORY, appCfg.getGoogleClientID(),
					appCfg.getGoogleClientSecret(), scopes);
			builder.setAccessType("offline");
			builder.setApprovalPrompt("auto");
			authorizationFlow = builder.build();
		}
		return authorizationFlow;
	}

	/**
	 * Получение информации о пользователе Google.
	 * 
	 * @param authorizationCode
	 *            код авторизации полученный от системы авторизации Гугл.
	 * @return Userinfo информация о пользователе.
	 * @throws IOException
	 * @throws IllegalArgumentException
	 *             в случае если параметры callbackUrl, clientID, clientSecret, scopes не инициализированы.
	 */
	private Userinfo getUserInfo(String authorizationCode) throws IllegalArgumentException, IOException {
		if (!lastAuthCode.equals(authorizationCode)) {
			GoogleTokenResponse tokenResp = getAuthorizationFlow().newTokenRequest(authorizationCode)
					.setRedirectUri(appCfg.getGoogleCallbackUrl()).execute();
			credential = new GoogleCredential().setAccessToken(tokenResp.getAccessToken());
			lastAuthCode = authorizationCode;
		}
		Oauth2 userInfoService = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();
		return userInfoService.userinfo().get().execute();
	}
}
