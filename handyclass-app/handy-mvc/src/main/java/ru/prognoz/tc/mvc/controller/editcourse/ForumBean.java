package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.List;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.HandyCommentDao;
import ru.prognoz.tc.domain.HandyComment;

public class ForumBean {

	private String forumId = "testforum";
	private Long rootOid;
	private String onChangeHandler;

	public ForumBean(Long rootOid) {
		super();
		this.rootOid = rootOid;
	}

	public String getForumId() {
		return forumId;
	}

	public void setForumId(String forumId) {
		this.forumId = forumId;
	}

	public Long getRootOid() {
		return rootOid;
	}

	public void setRootOid(Long rootOid) {
		this.rootOid = rootOid;
	}
	
	public String getOnChangeHandler() {
		return onChangeHandler;
	}

	public void setOnChangeHandler(String onChangeHandler) {
		this.onChangeHandler = onChangeHandler;
	}

	public List<HandyComment> getComments(Long parentOid) {
		HandyCommentDao commentsDao = HandyclassAppContext.getBean(HandyCommentDao.class);
		return commentsDao.getByParent(parentOid);
	}
	
	public Integer getCountComments(Long parentOid) {
		HandyCommentDao commentsDao = HandyclassAppContext.getBean(HandyCommentDao.class);
		List<HandyComment> comments = commentsDao.fullList(parentOid, false);
		return comments.size();
	}
	
	public HandyComment getLastComment(Long parentOid) {
		HandyCommentDao commentsDao = HandyclassAppContext.getBean(HandyCommentDao.class);
		List<HandyComment> comments = commentsDao.fullList(parentOid, false);
		if (comments.isEmpty()) {
			return null;
		} else {
			return comments.get( comments.size()-1 );
		}
	}

}
