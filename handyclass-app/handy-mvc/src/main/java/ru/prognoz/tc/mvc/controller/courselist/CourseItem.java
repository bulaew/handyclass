package ru.prognoz.tc.mvc.controller.courselist;

import java.util.Date;

import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.CourseCategory;

public class CourseItem {

	private Course entity;

	private int countReg;
	private long countLikes;
	private String description;
	private boolean isExpired = false;
	private boolean isFinished = false;
	private boolean isRegistered = false;
	private String availableTo = "";

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CourseItem(Course course) {
		this.entity = course;
	}

	public String getImage() {
		return entity.getAvatarUrl();
	}

	public String getName() {
		return entity.getFullName();
	}

	public long getOid() {
		return entity.getObjectId().getId();
	}

	public String getOwnerName() {
		return entity.getOwner().getUsername();
	}

	public long getOwnerOid() {
		return entity.getOwner().getObjectId().getId();
	}

	public CourseCategory getCategory() {
		return entity.getCategory();
	}

	public Integer getCountReg() {
		return countReg;
	}

	public void setCountReg(int countReg) {
		this.countReg = countReg;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public String getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(String availableTo) {
		this.availableTo = availableTo;
	}

	public Date getCreatedDate() {
		return entity.getCreated();
	}

	public Long getCountLikes() {
		return this.countLikes;
	}

	public void setCountLikes(long countLikes) {
		this.countLikes = countLikes;
	}

}
