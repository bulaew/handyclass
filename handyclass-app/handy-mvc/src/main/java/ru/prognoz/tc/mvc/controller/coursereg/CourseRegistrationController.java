package ru.prognoz.tc.mvc.controller.coursereg;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.AttachmentDao;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;
import ru.prognoz.tc.mvc.business.coursereg.CourseRegistryService;

@Controller
@RequestMapping(value = "/main/coursereg/")
public class CourseRegistrationController {

	private static final Logger logger = LoggerFactory.getLogger(CourseRegistrationController.class);
	private static final String BEAN_ATTR_NAME = "courseregData";

	@Autowired
	private CourseDao courseDao;
	@Autowired
	private AttachmentDao attachmentDao;
	@Autowired
	private CourseAccessService courseAccessService;
	@Autowired
	private ViewCounterDao viewCounterDao;
	@Autowired
	private CourseRegistryService courseRegistryService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public ModelAndView contactsView(Locale locale, @RequestParam(value = "coid") final long courseOid,
			@RequestParam(value = "invcode", required = false) final Long inviteCode) throws HandyCoreException {
		logger.info("Course registration page", locale);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication instanceof AnonymousAuthenticationToken)) { // Гость
			ModelAndView mav = new ModelAndView("coursereg/index");
			mav.addObject(BEAN_ATTR_NAME, populateBean(courseOid));
			return mav;
		}

		User currentUser = (User) authentication.getPrincipal();
		Course course = courseDao.getByOid(courseOid);
		if(course==null || course.isArchive()) {
			ModelAndView mav = new ModelAndView("redirect:/main");
			return mav;
		}
		// у пользователя нет прав на просмотр инфо по курсу.
		if (!courseAccessService.canViewAccess(course, currentUser, locale)) {
			ModelAndView mav = new ModelAndView("redirect:/main");
			return mav;
		}
		// Увеличиваем кол-во просмотров курса.
		viewCounterDao.increase(course.getObjectId(), currentUser);

		// Переход на плеер курса.
		if (courseAccessService.canPlayAccess(course, currentUser, locale)) {
			ModelAndView mav = new ModelAndView("redirect:/main/courseplayer?coid="+courseOid);
			return mav;
		}
		ModelAndView mav = new ModelAndView("coursereg/index");
		mav.addObject(BEAN_ATTR_NAME, populateBean(courseOid));
		return mav;
	}

	@RequestMapping(value = "acceptInvite", method = RequestMethod.GET)
	public ModelAndView acceptInvite(HttpServletRequest request, Locale locale, Model model,
			@RequestParam(value = "coid") final long courseOid, @RequestParam(value = "invcode", required = false) final Long inviteCode)
			throws HandyCoreException {
		logger.info("Course registration page: accept invite");

		ModelAndView mav = new ModelAndView("redirect:/login?show=signupDlg");

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication instanceof AnonymousAuthenticationToken)) { // Гость
			RegistrationAfterInviteData data = new RegistrationAfterInviteData();
			data.setCourseOID(courseOid);
			request.getSession().setAttribute(RegistrationAfterInviteData.ATTR_NAME, data);
			mav.setViewName("redirect:/login?show=loginDlg");
		}
		return mav;
	}
	
	@RequestMapping(value = "acceptRegistration", method = RequestMethod.GET)
	public ModelAndView acceptRegistration(HttpServletRequest request, Locale locale,
			@RequestParam(value = "coid") final long courseOid)
			throws HandyCoreException {
		logger.info("Course registration page: accept registration");

		ModelAndView mav = new ModelAndView();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication instanceof AnonymousAuthenticationToken)) { // Гость
			mav.setViewName("redirect:/main/coursereg/index?coid="+courseOid);
		}
		// Если не гость, пытаемся зарегистрировать его.
		User currentUser = (User) authentication.getPrincipal();
		Course course = courseDao.getByOid(courseOid);
		Boolean regResult = courseRegistryService.registration(course, currentUser, locale);
		if (!regResult) {
			mav.setViewName("redirect:/main");
		} else {
			mav.setViewName("redirect:/main/coursereg/index?coid="+courseOid);
		}
		return mav;
	}

	private CourseRegBean populateBean(long courseOid) throws HandyCoreException {
		CourseRegBean bean = new CourseRegBean();
		Course course = courseDao.getByOid(courseOid);
		if (course == null) {
			throw new HandyCoreException("bla bla bla");
		}
		bean.setCourse(course);
		bean.setAttachments(attachmentDao.getByCourse(course));
		Integer viewCounter = viewCounterDao.getCountViews(course.getObjectId());
		bean.setViewCounter(viewCounter.longValue());
		return bean;
	}

}
