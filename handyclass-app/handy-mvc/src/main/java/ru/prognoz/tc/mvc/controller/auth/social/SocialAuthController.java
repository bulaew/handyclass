package ru.prognoz.tc.mvc.controller.auth.social;

import java.io.Serializable;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.business.auth.AuthService;
import ru.prognoz.tc.mvc.business.auth.AuthenticationType;
import ru.prognoz.tc.mvc.business.auth.FacebookAuthorizator;
import ru.prognoz.tc.mvc.business.auth.GoogleAuthorizator;
import ru.prognoz.tc.mvc.business.auth.TwitterAuthorizator;
import ru.prognoz.tc.mvc.business.auth.VkontakteAuthorizator;
import ru.prognoz.tc.mvc.security.SocialAuthenticationToken;
import ru.prognoz.tc.mvc.security.SocialNetwork;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

@Controller
@Scope(value = "session")
public class SocialAuthController implements Serializable {

	private static final long serialVersionUID = 6121506278598210165L;
	@Autowired
	@Qualifier("authenticationManager")
	protected AuthenticationManager authenticationManager;
	@Autowired
	private FacebookAuthorizator fbAuthorizator;
	@Autowired
	private VkontakteAuthorizator vkAuthorizator;
	@Autowired
	private TwitterAuthorizator twitterAuthorizator;
	@Autowired
	private GoogleAuthorizator googleAuthorizator;
	@Autowired
	private UserDao userDao;
	@Autowired
	private AuthService authService;

	private static final Logger logger = LoggerFactory.getLogger(SocialAuthController.class);
	private static final String SESSION_AUTH_TYPE = "authentication_type";

	private boolean loginWithSocial(SocialNetwork network, String socialAccount, HttpServletRequest request) throws AuthenticationException {
		SocialAuthenticationToken token = new SocialAuthenticationToken(network, socialAccount);
		token.setDetails(new WebAuthenticationDetails(request));
		Authentication authenticatedUser = authenticationManager.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
		return true;
	}

	/**
	 * Обработка ответа от соц. сетей после запроса на авторизацию.
	 * 
	 * @param locale текущая локаль
	 * @param request
	 * @param profile SocialNetworkProfile данные о пользователе в соц. сети
	 * @return ModelAndView
	 * @throws HandyCoreException
	 */
	private ModelAndView socialCallback(Locale locale, HttpServletRequest request, SocialNetworkProfile profile) throws HandyCoreException {

		ModelAndView mav = new ModelAndView("redirect:/main");
		// Получаем тип авторизации
		AuthenticationType authType = (AuthenticationType) request.getSession().getAttribute(SESSION_AUTH_TYPE);
		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		if (authType == null) {
			return mav;
		}
		if (authType == AuthenticationType.REGISTRATION) {
			try {
				loginWithSocial(profile.getNetwork(), profile.getAccountId(), request);
				mav.setViewName("redirect:/main"); // переход на главную страницу
				return mav;
			} catch (UsernameNotFoundException exc) {
				authService.createAccountExternal(profile);
				loginWithSocial(profile.getNetwork(), profile.getAccountId(), request);
				mav.setViewName("redirect:/main/useraccount"); // отправляем пользователя заполнять профиль.
				return mav;
			}
		}
		if (authType == AuthenticationType.BINDING) {
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			populateUserDetails(currentUser, profile);
			userDao.save(currentUser);
			mav.setViewName("redirect:/main/useraccount");
			return mav;
		}
		if (authType == AuthenticationType.LOGIN) {
			try {
				loginWithSocial(profile.getNetwork(), profile.getAccountId(), request);
				mav.setViewName("redirect:/main"); // переход на главную страницу
				return mav;
			} catch (UsernameNotFoundException exc) {
				mav.setViewName("redirect:/login");
				return mav;
			}
		}
		return mav;
	}

	private void populateUserDetails(User user, SocialNetworkProfile profile) {
		String username = profile.getFirstName();
		if (!StringUtils.isEmpty(profile.getLastName())) {
			username += " " + profile.getLastName();
		}
		switch (profile.getNetwork()) {
		case GOOGLE:
			user.setGoogleemail(profile.getAccountId());
			user.setGoogleName(username);
			break;
		case FACEBOOK:
			user.setFacebookAccount(profile.getAccountId());
			user.setFacebookName(username);
			break;
		case TWITTER:
			user.setTwitterAccount(profile.getAccountId());
			user.setTwitterName(username);
			break;
		case VKONTAKTE:
			user.setVkAccount(profile.getAccountId());
			user.setVkName(username);
			break;
		default:
			break;
		}
	}

	/*
	 * ФЕЙСБУК
	 */
	@RequestMapping(value = { "/auth/facebooksignup" }, method = RequestMethod.GET)
	public String facebooksignup(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : facebooksignup");
		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.REGISTRATION);

		return "redirect:" + fbAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/facebookauth" }, method = RequestMethod.GET)
	public String facebookauth(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : facebookauth");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.LOGIN);

		return "redirect:" + fbAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/facebookbind" }, method = RequestMethod.GET)
	public String facebookbind(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : facebookbind");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.BINDING);

		return "redirect:" + fbAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/facebookcallback" }, method = RequestMethod.GET)
	public ModelAndView facebookcallback(Locale locale, HttpServletRequest request, @RequestParam(value = "code") String authCode)
			throws HandyCoreException {

		logger.info("SocialAuthController : facebookcallback");
		SocialNetworkProfile profile = fbAuthorizator.getSocialProfile(authCode);
		return socialCallback(locale, request, profile);
	}

	/*
	 * ВКОНТАКТЕ
	 */
	@RequestMapping(value = { "/auth/vkontaktesignup" }, method = RequestMethod.GET)
	public String vkontaktesignup(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : vkontaktesignup");
		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.REGISTRATION);

		return "redirect:" + vkAuthorizator.buildAuthUrl();
	}
	
	@RequestMapping(value = { "/auth/vkontakteauth" }, method = RequestMethod.GET)
	public String vkontakteauth(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : vkontakteauth");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.LOGIN);

		return "redirect:" + vkAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/vkontaktebind" }, method = RequestMethod.GET)
	public String vkontaktebind(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : vkontaktebind");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.BINDING);

		return "redirect:" + vkAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/vkontaktecallback" }, method = RequestMethod.GET)
	public ModelAndView vkontaktecallback(Locale locale, HttpServletRequest request, @RequestParam(value = "code") String authCode)
			throws HandyCoreException {

		logger.info("SocialAuthController : vkontaktecallback");

		SocialNetworkProfile profile = vkAuthorizator.getSocialProfile(authCode);
		return socialCallback(locale, request, profile);
	}

	/*
	 * ТВИТТЕР
	 */
	@RequestMapping(value = { "/auth/twittersignup" }, method = RequestMethod.GET)
	public String twittersignup(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : twittersignup");
		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.REGISTRATION);

		return "redirect:" + twitterAuthorizator.buildAuthUrl();
	}
	
	@RequestMapping(value = { "/auth/twitterauth" }, method = RequestMethod.GET)
	public String twitterauth(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : twitterauth");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.LOGIN);

		return "redirect:" + twitterAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/twitterbind" }, method = RequestMethod.GET)
	public String twitterbind(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : twitterbind");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.BINDING);

		return "redirect:" + twitterAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/twittercallback" }, method = RequestMethod.GET)
	public ModelAndView twittercallback(Locale locale, HttpServletRequest request, @RequestParam(value = "oauth_verifier") String authCode)
			throws HandyCoreException {

		logger.info("SocialAuthController : twittercallback");

		SocialNetworkProfile profile = twitterAuthorizator.getSocialProfile(authCode);
		return socialCallback(locale, request, profile);
	}

	/*
	 * ГУГЛ
	 */
	@RequestMapping(value = { "/auth/googlesignup" }, method = RequestMethod.GET)
	public String googlesignup(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : googlesignup");
		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.REGISTRATION);

		return "redirect:" + googleAuthorizator.buildAuthUrl();
	}
	
	@RequestMapping(value = { "/auth/googleauth" }, method = RequestMethod.GET)
	public String googleauth(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : googleauth");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.LOGIN);

		return "redirect:" + googleAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/googlebind" }, method = RequestMethod.GET)
	public String googlebind(Locale locale, HttpServletRequest request) {

		logger.info("SocialAuthController : googlebind");

		request.getSession().removeAttribute(SESSION_AUTH_TYPE);
		request.getSession().setAttribute(SESSION_AUTH_TYPE, AuthenticationType.BINDING);

		return "redirect:" + googleAuthorizator.buildAuthUrl();
	}

	@RequestMapping(value = { "/auth/googlecallback" }, method = RequestMethod.GET)
	public ModelAndView googlecallback(Locale locale, HttpServletRequest request, @RequestParam(value = "code") String authCode)
			throws HandyCoreException {

		logger.info("SocialAuthController : googlecallback");

		SocialNetworkProfile profile = googleAuthorizator.getSocialProfile(authCode);
		return socialCallback(locale, request, profile);
	}

}
