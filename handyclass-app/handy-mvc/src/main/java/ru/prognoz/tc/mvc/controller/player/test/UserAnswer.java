package ru.prognoz.tc.mvc.controller.player.test;

public class UserAnswer {
	private long oid;
	private String value;

	public long getOid() {
		return oid;
	}

	public void setOid(long oid) {
		this.oid = oid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
