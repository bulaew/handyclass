package ru.prognoz.tc.mvc.controller.reports.propensity;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.reports.common.ReportItem;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.propensity.MyPropensityCriteria;
import ru.prognoz.tc.core.reports.impl.propensity.MyPropensityItem;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
public class MyPropensityController {

	private static final Logger logger = LoggerFactory.getLogger(MyPropensityController.class);

	@Autowired
	private HandyclassAppCfg appConfig;
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseCategoryDao courseCategoryDao;

	@RequestMapping(value = "/reports/myPropensity", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws HandyCoreException {

		logger.info("my propensity report page");

		ModelAndView mav = new ModelAndView("reports/myPropensity/index");
		
		mav.addObject("bean", populateBean(locale, null, 0));
		return mav;
	}

	@RequestMapping(value = "/reports/myPropensity/renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my propensity report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/myPropensity/report");
		mav.addObject("bean", populateBean(locale, viewModel, 0));
		return mav;
	}
	
	@RequestMapping(value = "/reports/myPropensity/loadBillboard", method = RequestMethod.POST)
	public ModelAndView loadBillboard(Locale locale) throws HandyCoreException {
		logger.info("my propensity report page: loadBillboard");

		ModelAndView mav = new ModelAndView("reports/myPropensity/billboard");
		mav.addObject("bean", populateBean(locale, null, appConfig.getDefaultRowLimit()));
		return mav;
	}
	
	@RequestMapping(value = "/reports/myPropensity/renderBillboard", method = RequestMethod.POST)
	public ModelAndView renderBillboard(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("my propensity report page: renderBillboard");

		ModelAndView mav = new ModelAndView("reports/myPropensity/billboard");
		mav.addObject("bean", populateBean(locale, viewModel, appConfig.getDefaultRowLimit()));
		return mav;
	}

	private MyPropensityBean populateBean(Locale locale, ReportViewModel viewModel, int rowLimit) throws HandyCoreException {
		if (viewModel == null) {
			viewModel = new ReportViewModel();
			viewModel.setController("/reports/myPropensity");
			viewModel.setSort(MyPropensityItem.COLUMN_PROPENSITY);
			viewModel.setDirection(SortDirection.DESC);
			viewModel.setReportType(ReportType.MY_PROPENSITY);
		}
		MyPropensityBean bean = new MyPropensityBean();
		bean.setItems(reportService.getMyPropensityItems(criteriaByModel(viewModel, rowLimit)));
		List<CourseCategory> all = courseCategoryDao.getAll();
		for(ReportItem item:bean.getItems()) {
		    String title = ((MyPropensityItem)item).getCategoryTitle();
		    for(CourseCategory category: all) {
			if(title.equals(category.getEngTitle())){
			    title=category.getTitle(locale);
			    break;
			}
		    }
		    ((MyPropensityItem)item).setCategoryTitle(title);
		}
		bean.setViewModel(viewModel);
		return bean;
	}

	private MyPropensityCriteria criteriaByModel(ReportViewModel viewModel, int rowLimit) {
		MyPropensityCriteria criteria = new MyPropensityCriteria();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		criteria.setFilterUser(currentUser);
		criteria.setOrderColumn(viewModel.getSort());
		criteria.setSortDirection(viewModel.getDirection());
		criteria.setFindValue(viewModel.getSearch());
		criteria.setRowCount(rowLimit);
		return criteria;
	}

}
