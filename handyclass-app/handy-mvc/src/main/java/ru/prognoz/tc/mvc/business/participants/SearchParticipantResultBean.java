package ru.prognoz.tc.mvc.business.participants;

import ru.prognoz.tc.domain.contact.ContactListInfo;
import ru.prognoz.tc.mvc.common.viewmsg.ViewMessage;

public class SearchParticipantResultBean {
	private IParticipant participant;
	private ContactListInfo contactList;
	private ViewMessage msg;

	public IParticipant getParticipant() {
		return participant;
	}

	public void setParticipant(IParticipant participant) {
		this.participant = participant;
	}

	public ViewMessage getMsg() {
		return msg;
	}

	public void setMsg(ViewMessage msg) {
		this.msg = msg;
	}

	public ContactListInfo getContactList() {
		return contactList;
	}

	public void setContactList(ContactListInfo contactList) {
		this.contactList = contactList;
	}
	
}
