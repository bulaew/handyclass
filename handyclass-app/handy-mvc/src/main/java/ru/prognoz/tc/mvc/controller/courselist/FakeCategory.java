package ru.prognoz.tc.mvc.controller.courselist;

import java.util.Locale;

import org.springframework.context.MessageSource;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.domain.course.CourseCategory;

public class FakeCategory implements CourseCategory {

	@Override
	public int getId() {
		return -1;
	}

	@Override
	public void setId(int id) {
	}

	@Override
	public String getTitle(Locale locale) {
		MessageSource messageSource = HandyclassAppContext.getBean(MessageSource.class);
		return messageSource.getMessage("CourseListFace.allCategories", null, locale);
	}

	@Override
	public String getEngTitle() {
		return null;
	}

	@Override
	public void setEngTitle(String title) {
	}

	@Override
	public String getRuTitle() {
		return null;
	}

	@Override
	public void setRuTitle(String ruTitle) {
	}

	@Override
	public String getAvatarUrl() {
		return null;
	}

	@Override
	public void setAvatarUrl(String avatarUrl) {
	}

}
