package ru.prognoz.tc.mvc.business.auth;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.utils.HcRandom;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.dao.auth.RoleDao;
import ru.prognoz.tc.domain.Role;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

@Service
public class AuthService {

	private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private HandyObjectDao objectDao;

	public User createAccountInternal(final String firstName, final String lastName, final String email, final String password)
			throws HandyCoreException {
		User user = null;
		try {
			Role role = roleDao.getByAuthority("ROLE_USER");
			user = userDao.newDomainInstance();
			Set<Role> roles = new HashSet<Role>();
			roles.add(role);
			user.setRoles(roles);
			user.setVerified(false);
			user.setVerificationCode("" + HcRandom.random());
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setCredentialsNonExpired(true);
			user.setEnabled(true);
			user.setTotalTime(0);
			user.setObjectId(objectDao.generate());
			user.setName(firstName);
			user.setLastName(lastName);
			user.setLogin(email);
			user.setPassword(password);

			user.setAvatarURL("/img/avatar/avatar_big.png");

			userDao.save(user);

			logger.info("Создание нового пользователя: " + email);
		} catch (Exception exc) {
			throw new HandyCoreException("Ошибка создания нового пользователя");
		}
		return user;

	}

	public User createAccountExternal(final SocialNetworkProfile profile) throws HandyCoreException {
		User user = null;
		try {
			Role role = roleDao.getByAuthority("ROLE_USER");
			user = userDao.newDomainInstance();
			Set<Role> roles = new HashSet<Role>();
			roles.add(role);
			user.setRoles(roles);
			user.setVerified(true);
			user.setVerificationCode("" + HcRandom.random());
			user.setAccountNonExpired(true);
			user.setAccountNonLocked(true);
			user.setCredentialsNonExpired(true);
			user.setEnabled(true);
			user.setTotalTime(0);
			user.setObjectId(objectDao.generate());
			user.setName(profile.getFirstName());
			user.setLastName(profile.getLastName());
			switch (profile.getNetwork()) {
			case FACEBOOK:
				user.setFacebookAccount(profile.getAccountId());
				user.setFacebookName(profile.getFirstName()+" "+profile.getLastName());
				break;
			case GOOGLE:
				user.setGoogleemail(profile.getAccountId());
				user.setGoogleName(profile.getFirstName()+" "+profile.getLastName());
				break;
			case TWITTER:
				user.setTwitterAccount(profile.getAccountId());
				user.setTwitterName(profile.getFirstName()+" "+profile.getLastName());
				break;
			case VKONTAKTE:
				user.setVkAccount(profile.getAccountId());
				user.setVkName(profile.getFirstName()+" "+profile.getLastName());
				break;
			default:
				break;
			}

			user.setAvatarURL("/img/avatar/avatar_big.png");

			userDao.save(user);

			logger.info("Создание нового пользователя: " + profile.getAccountId());
		} catch (Exception exc) {
			throw new HandyCoreException("Ошибка создания нового пользователя");
		}
		return user;

	}

	public User accountVerification(String verificationCode) {
		User user = userDao.getByVerifyCode(verificationCode);
		if (user != null) {
			user.setVerified(true);
			userDao.save(user);
			return user;
		}
		return null;
	}

}
