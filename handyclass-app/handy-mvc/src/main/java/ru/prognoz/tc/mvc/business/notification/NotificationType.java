package ru.prognoz.tc.mvc.business.notification;

import ru.prognoz.tc.mvc.controller.courselist.mode.UserMode;

/**
 * Тип оповещения
 * 
 * @author Denis.V.Mikulich
 * 
 */
public enum NotificationType {

	/**
	 * Завершение обучения по курсу (ученик).
	 */
	COMPLETE_TRAINING(1),
	/**
	 * Подходит срок обучения по курсу (ученик).
	 */
	NEARLY_TERM(2),
	/**
	 * Ответ на комментарий (ученик/учитель)
	 */
	COMMENT_REPLY(3),
	/**
	 * Пользователь оставил комментарий в вашем курсе (учитель)
	 */
	NEW_COMMENT(4),
	/**
	 * Пользователь ответил ВАМ в теме (ученик/учитель).
	 */
	TOPIC_COMMENT_REPLY(5),
	/**
	 * В вашей теме новое сообщение (ученик/учитель).
	 */
	NEW_TOPIC_COMMENT(6),
	/**
	 * Запись на ваш курс (учитель)
	 */
	SIGNUP_COURSE(7),
	/**
	 * Завершение обучение по вашему курсу (учитель)
	 */
	COMPLETE_COURSE(8),
	/**
	 * Создание темы в обсуждениях вашего курса (учитель)
	 */
	NEW_DISCUSSION(9),
	/**
	 * Приглашение на курс (ученик).
	 */
	NEW_INVITE(10),
	/**
	 * Отчисление с курса.
	 */
	DISMISS_REGISTRATION(11),
	/**
	 * Отмена приглашения.
	 */
	DISMISS_INVITE(12);
	
	private int type;

	private NotificationType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		switch (type) {
		case 1:
			return "Завершение обучения по курсу";
		case 2:
			return "Подходит срок обучения по курсу";
		case 3:
			return "Ответ на комментарий";
		case 4:
			return "Пользователь оставил комментарий в вашем курсе";
		case 5:
			return "Пользователь ответил ВАМ в теме";
		case 6:
			return "В вашей теме новое сообщение";
		case 7:
			return "Запись на ваш курс";
		case 8:
			return "Завершение обучение по вашему курсу";
		case 9:
			return "Создание темы в обсуждениях вашего курса";
		case 10:
			return "Приглашение на курс";
		case 11:
			return "Отчисление с курса";
		case 12:
			return "Отмена приглашения";	
		default:
			return super.toString();
		}
	}

	public int getIntValue() {
		return type;

	}

	public static NotificationType parseInt(int code) {
		for (NotificationType type : NotificationType.values()) {
			if (type.getIntValue() == code) {
				return type;
			}
		}
		return null;
	}

	/**
	 * Проверка, предназначено ли оповещение для данного режима.
	 * 
	 * @param mode
	 * @return boolean
	 */
	public boolean isApplicable(UserMode mode) {
		switch (this) {
		case COMPLETE_TRAINING:
			return UserMode.PUPIL.equals(mode);
		case NEARLY_TERM:
			return UserMode.PUPIL.equals(mode);
		case COMMENT_REPLY:
			return true;
		case NEW_COMMENT:
			return UserMode.TEACHER.equals(mode);
		case TOPIC_COMMENT_REPLY:
			return true;
		case NEW_TOPIC_COMMENT:
			return true;
		case SIGNUP_COURSE:
			return UserMode.TEACHER.equals(mode);
		case COMPLETE_COURSE:
			return UserMode.TEACHER.equals(mode);
		case NEW_DISCUSSION:
			return UserMode.TEACHER.equals(mode);
		case NEW_INVITE:
			return UserMode.PUPIL.equals(mode);
		case DISMISS_REGISTRATION:
			return UserMode.PUPIL.equals(mode);
		case DISMISS_INVITE:
			return UserMode.PUPIL.equals(mode);
		default:
			return false;
		}
	}
}
