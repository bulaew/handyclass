package ru.prognoz.tc.mvc.controller.courselist.mode;

public enum NotifyMode {

	SHOW, HIDE;

}
