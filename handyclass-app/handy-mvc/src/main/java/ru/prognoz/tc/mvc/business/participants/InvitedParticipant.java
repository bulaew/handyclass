package ru.prognoz.tc.mvc.business.participants;

import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Invite;

public class InvitedParticipant implements IParticipant {

	private User user;
	private String guestEmail;
	private long inviteID;
	private boolean removed;

	public InvitedParticipant(Invite invite) {
		super();
		this.user = invite.getUser();
		this.guestEmail = invite.getGuestEmail();
		this.inviteID = invite.getId();
		this.removed = false;
	}
	
	@Override
	public int getType() {
		return 2;
	}

	@Override
	public User getUser() {
		return this.user;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String getGuestEmail() {
		return this.guestEmail;
	}

	@Override
	public void setGuestEmail(String guestEmail) {
		this.guestEmail = guestEmail;
	}

	public long getInviteID() {
		return inviteID;
	}

	public void setInviteID(long inviteID) {
		this.inviteID = inviteID;
	}

	@Override
	public int compareTo(IParticipant o) {
		if (o == null) {
			return -1;
		}
		if (o instanceof RegisteredParticipant) {
			return 1; // зарегистрированные участники всегда выше.
		}
		if (o instanceof TeacherParticipant) {
			return 1;
		}
		if (this.user == null && o.getUser() == null) { // если оба участника добавлены через емейл, то сортируем по нем.
			if (guestEmail != null && o.getGuestEmail() != null) {
				return guestEmail.compareTo(o.getGuestEmail());
			}
		} else {
			if (this.user == null) {
				return 1;
			}
			if (o.getUser() == null) {
				return -1;
			}
			return this.user.getUsername().compareTo(o.getUser().getUsername()); // сортируем по ФИО.
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guestEmail == null) ? 0 : guestEmail.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvitedParticipant other = (InvitedParticipant) obj;
		if (guestEmail == null) {
			if (other.guestEmail != null)
				return false;
		} else if (!guestEmail.equals(other.guestEmail))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (user.getId() != other.user.getId())
			return false;
		return true;
	}

	@Override
	public boolean isRemoved() {
		return removed;
	}

	@Override
	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

}
