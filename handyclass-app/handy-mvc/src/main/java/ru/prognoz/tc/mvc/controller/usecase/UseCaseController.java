package ru.prognoz.tc.mvc.controller.usecase;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.domain.HandyObjectID;
import ru.prognoz.tc.mvc.controller.courselist.CourseListController;

@Controller
public class UseCaseController {

	private static final Logger logger = LoggerFactory.getLogger(CourseListController.class);

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HandyObjectDao oidDao;
	@Autowired
	private HandyclassAppCfg appCfg;

	/**
	 * Маппинг нескольких УРЛов на один контроллер.
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
//	@RequestMapping(value = { "/usecase/", "/usecase", "/usecase/index" }, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Use Case : standart sync request", locale);

		model.addAttribute("country", locale.getCountry());
		model.addAttribute("language", locale.getLanguage());
		model.addAttribute("i18nparam", messageSource.getMessage("HelloFromController", null, locale));

		HandyObjectID newOID = oidDao.generate();
		model.addAttribute("newHandyObject", newOID.getId());

		return "/usecase/index";
	}

//	@RequestMapping(value = { "/usecasedeferred" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> asyncUpdateDaoPanel(final HttpServletResponse response) {
		logger.info("Use Case : Deferred result async request");
		DeferredResult<String> result = new DeferredResult<String>();
		HandyObjectID newOID = oidDao.generate();
		result.setResult(newOID.getId() + ""); // Отправляем на сервер новое значение в текстовом виде.
		return result;
	}

//	@RequestMapping(value = { "/usecasecallable" }, method = RequestMethod.POST)
	public Callable<String> asyncUpdate(final Model model) {
		logger.info("Use Case : Callable async request");
		return new Callable<String>() {
			@Override
			public String call() throws Exception {
				HandyObjectID newOID = oidDao.generate();
				model.addAttribute("newHandyObject", newOID.getId());
				return "/usecase/daoTest"; // Отправляем на сервер всё представление.
			}
		};
	}

//	@RequestMapping(value = { "/usecase/fileupload" }, method = RequestMethod.POST)
	public String fileupload(@RequestParam MultipartFile file) {
		logger.info("Use Case : file upload request");

		String orgFileName = file.getOriginalFilename();
		String fileDest = appCfg.getPublicResourseDirectory() + orgFileName;
		File dest = new File(fileDest);
		try {
			file.transferTo(dest);
			logger.info("File uploaded:" + orgFileName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("File uploaded failed:" + orgFileName);
		}

		return "redirect:/usecase/index";
	}

}
