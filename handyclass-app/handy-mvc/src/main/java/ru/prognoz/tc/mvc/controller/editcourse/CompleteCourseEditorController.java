package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.prognoz.tc.mvc.business.course.save.CourseSaveService;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse")
public class CompleteCourseEditorController {

	private static final Logger logger = LoggerFactory.getLogger(CompleteCourseEditorController.class);
	@Autowired
	private EditCourseBean courseBean;
	@Autowired
	private CourseSaveService courseSaveService;

	@RequestMapping(value = "/complete", method = RequestMethod.GET)
	public String completeEditing(Locale locale) {

		EditCourseVerificationMessage msg = courseSaveService.verifyCourse(courseBean);
		if (msg != null) {
			courseBean.setVerifyMsg(msg);
			courseBean.setStep(EditCourseWizardStep.COMMON);
			return "redirect:" + EditCourseWizardUtils.getStepUrl(courseBean.getStep());
		}
		
		try {
			courseSaveService.storeCourse(courseBean);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return "redirect:/main/editcourse/discussion";
		}

		try {
			courseSaveService.storeParticipants(courseBean, locale);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		EditCourseWizardUtils.cleanCourseBean(courseBean);
		return "redirect:/main";
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public String cancelEditing() {

		logger.info("Cancel Course Editing ");

		EditCourseWizardUtils.cleanCourseBean(courseBean);

		return "redirect:/main";
	}

}
