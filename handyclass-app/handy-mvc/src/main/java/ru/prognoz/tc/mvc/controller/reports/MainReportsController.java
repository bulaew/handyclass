package ru.prognoz.tc.mvc.controller.reports;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryCriteria;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryItem;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingItem;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityCriteria;
import ru.prognoz.tc.core.reports.impl.authoractivity.AuthorActivityItem;
import ru.prognoz.tc.mvc.business.mail.MailService;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.reports.general.GeneralDataBean;

@Controller
@RequestMapping(value = "/reports/main/")
public class MainReportsController {

	private static final Logger logger = LoggerFactory.getLogger(MainReportsController.class);
	@Autowired
	private MailService mailService;
	@Autowired
	private ReportsService reportService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws Exception {

		logger.info("Main reports page");

		ModelAndView mav = new ModelAndView("reports/index");
		BillboardBean bean = populateBillboardBean(locale, null);
		mav.addObject("bean", bean);
		return mav;
	}

	private BillboardBean populateBillboardBean(Locale locale, ReportViewModel viewModel) throws Exception {

		if (viewModel == null) {
			viewModel = new ReportViewModel();
			viewModel.setController("/reports/main/");
			viewModel.setSort(AllCourseRatingItem.COLUMN_COUNT_VIEWS);
			viewModel.setDirection(SortDirection.DESC);
		}
		BillboardBean bean = new BillboardBean();
		bean.setViewModel(viewModel);
		// General data:
		GeneralDataBean generalDataBean = new GeneralDataBean();
		generalDataBean.setCountCourses(reportService.countCourses());
		generalDataBean.setCountUsers(reportService.countUsers());
		bean.setGeneralDataBean(generalDataBean);
		// Активность авторов
		AuthorActivityCriteria authorActivityCriteria = new AuthorActivityCriteria();
		authorActivityCriteria.setRowCount(3);
		authorActivityCriteria.setOrderColumn(AuthorActivityItem.COLUMN_AUTHOR_FIO);
		authorActivityCriteria.setSortDirection(SortDirection.DESC);
		bean.setAuthorActivityItems(reportService.getAuthorActivityItems(authorActivityCriteria));

		// Актуальные категории
		ActualCategoryCriteria actualCategoryCriteria = new ActualCategoryCriteria();
		actualCategoryCriteria.setLocale(locale);
		actualCategoryCriteria.setRowCount(3);
		actualCategoryCriteria.setOrderColumn(ActualCategoryItem.COLUMN_COMPLETE_PART);
		actualCategoryCriteria.setSortDirection(SortDirection.DESC);
		bean.setActualCategoryItems(reportService.getActualCategoryItems(actualCategoryCriteria));
		return bean;
	}

}
