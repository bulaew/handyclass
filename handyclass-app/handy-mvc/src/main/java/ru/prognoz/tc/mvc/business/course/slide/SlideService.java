package ru.prognoz.tc.mvc.business.course.slide;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.dao.attempt.SlideAttemptDao;
import ru.prognoz.tc.domain.attempt.SlideAttempt;

@Service
public class SlideService {

	@Autowired
	private SlideAttemptDao slideAttemptDao;

	/**
	 * Счетчик просмотров(проигрываний) слайда.
	 * 
	 * @param slideOID
	 *            ОИД слайда.
	 * @return количество проигрываний.
	 */
	public Long countPlaybacks(Long slideOID) {
		List<SlideAttempt> attempts = slideAttemptDao.getAttempts(slideOID);
		Long counter = 0L;
		for (SlideAttempt slideAttempt : attempts) {
			counter += slideAttempt.getViewCounter();
		}
		return counter;
	}

}
