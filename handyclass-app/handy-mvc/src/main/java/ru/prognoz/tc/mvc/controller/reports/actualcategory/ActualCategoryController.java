package ru.prognoz.tc.mvc.controller.reports.actualcategory;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryCriteria;
import ru.prognoz.tc.core.reports.impl.actualcategory.ActualCategoryItem;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
@RequestMapping(value = "/reports/actualCategory")
public class ActualCategoryController {

	private static final Logger logger = LoggerFactory.getLogger(ActualCategoryController.class);

	@Autowired
	private ReportsService reportService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(Locale locale) throws HandyCoreException {

		logger.info("Actual category report page");

		ModelAndView mav = new ModelAndView("reports/actualCategory/index");
		ReportViewModel defaultModel = new ReportViewModel();
		defaultModel.setController("/reports/actualCategory");
		defaultModel.setSort(ActualCategoryItem.COLUMN_COMPLETE_PART);
		defaultModel.setDirection(SortDirection.DESC);
		mav.addObject("bean", populateBean(locale, defaultModel));
		return mav;
	}
	
	@RequestMapping(value = "renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("Actual category report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/actualCategory/report");
		mav.addObject("bean", populateBean(locale, viewModel));
		return mav;
	}

	private ActualCategoryBean populateBean(Locale locale, ReportViewModel viewModel) throws HandyCoreException {
		ActualCategoryBean bean = new ActualCategoryBean();
		bean.setItems(reportService.getActualCategoryItems(criteriaByModel(locale, viewModel)));
		bean.setViewModel(viewModel);

		return bean;
	}

	private ActualCategoryCriteria criteriaByModel(Locale locale, ReportViewModel viewModel) {
		ActualCategoryCriteria actualCategoryCriteria = new ActualCategoryCriteria();
		actualCategoryCriteria.setLocale(locale);
		actualCategoryCriteria.setRowCount(-1);
		actualCategoryCriteria.setOrderColumn(viewModel.getSort());
		actualCategoryCriteria.setSortDirection(viewModel.getDirection());
		actualCategoryCriteria.setFindValue(viewModel.getSearch());
		return actualCategoryCriteria;
	}
}
