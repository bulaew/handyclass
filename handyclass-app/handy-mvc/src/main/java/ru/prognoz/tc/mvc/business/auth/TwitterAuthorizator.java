package ru.prognoz.tc.mvc.business.auth;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.social.oauth1.AuthorizedRequestToken;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Parameters;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.mvc.security.SocialNetwork;
import ru.prognoz.tc.mvc.security.SocialNetworkProfile;

@Component
@Scope(value = "session")
public class TwitterAuthorizator implements SocialAuthorizator {

	private static final long serialVersionUID = -8401340939453861288L;
	@Autowired
	private HandyclassAppCfg appCfg;
	private OAuthToken requestToken;
	private TwitterConnectionFactory conFactory;

	public String buildAuthUrl() throws IllegalArgumentException {
		OAuth1Operations operations = getConnectionFactory().getOAuthOperations();
		requestToken = operations.fetchRequestToken(appCfg.getTwitterCallbackUrl(), null);
		OAuth1Parameters params = new OAuth1Parameters();
		return operations.buildAuthorizeUrl(requestToken.getValue(), params);
	}

	public void validateAuthParams() throws IllegalArgumentException {
		if (StringUtils.isEmpty(appCfg.getTwitterCallbackUrl()) || StringUtils.isEmpty(appCfg.getTwitterConsumerKey())
				|| StringUtils.isEmpty(appCfg.getTwitterConsumerSecret())) {
			throw new IllegalArgumentException("Twetter authorization arguments exception.");
		}
	}

	@Override
	public SocialNetworkProfile getSocialProfile(String authorizationCode) throws HandyCoreException {
		try {
			OAuth1Operations operations = getConnectionFactory().getOAuthOperations();
			OAuth1Parameters params = new OAuth1Parameters();
			params.add("force_login", "true");
			OAuthToken accessToken = operations.exchangeForAccessToken(new AuthorizedRequestToken(requestToken, authorizationCode), null);
			Twitter twitter = new TwitterTemplate(appCfg.getTwitterConsumerKey(), appCfg.getTwitterConsumerSecret(),
					accessToken.getValue(), accessToken.getSecret());
			TwitterProfile profile = twitter.userOperations().getUserProfile();
			SocialNetworkProfile result = new SocialNetworkProfile();
			result.setNetwork(SocialNetwork.TWITTER);
			result.setAccountId(profile.getId() + "");
			result.setFirstName(profile.getName());
			result.setLastName("");
			return result;
		} catch (Exception exc) {
			throw new HandyCoreException(exc);
		}
	}

	private TwitterConnectionFactory getConnectionFactory() {
		if (conFactory == null) {
			validateAuthParams();
			conFactory = new TwitterConnectionFactory(appCfg.getTwitterConsumerKey(), appCfg.getTwitterConsumerSecret());
		}
		return conFactory;
	}

}
