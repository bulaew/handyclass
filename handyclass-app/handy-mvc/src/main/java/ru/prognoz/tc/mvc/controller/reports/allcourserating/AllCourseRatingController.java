package ru.prognoz.tc.mvc.controller.reports.allcourserating;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.config.HandyclassAppCfg;
import ru.prognoz.tc.core.reports.common.ReportType;
import ru.prognoz.tc.core.reports.common.SortDirection;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingCriteria;
import ru.prognoz.tc.core.reports.impl.allcourserating.AllCourseRatingItem;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.domain.course.CourseCategory;
import ru.prognoz.tc.mvc.business.reports.ReportsService;
import ru.prognoz.tc.mvc.controller.courselist.FakeCategory;
import ru.prognoz.tc.mvc.controller.reports.ReportViewModel;

@Controller
public class AllCourseRatingController {

	private static final Logger logger = LoggerFactory.getLogger(AllCourseRatingController.class);

	@Autowired
	private HandyclassAppCfg appConfig;
	@Autowired
	private ReportsService reportService;
	@Autowired
	private CourseCategoryDao courseCategoryDao;

	@RequestMapping(value = "/reports/courseRating", method = RequestMethod.GET)
	public ModelAndView index(Locale locale, @RequestParam(value="catId", defaultValue="0") Integer categoryId) throws HandyCoreException {

		logger.info("Overall course rating report page");

		ModelAndView mav = new ModelAndView("reports/allCourseRating/index");
		
		mav.addObject("bean", populateBean(locale, getDefaultVM(categoryId), 0));
		return mav;
	}
	
	@RequestMapping(value = "/reports/courseRating/renderReport", method = RequestMethod.POST)
	public ModelAndView renderReport(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("Overall course rating report page: renderReport");

		ModelAndView mav = new ModelAndView("reports/allCourseRating/report");
		mav.addObject("bean", populateBean(locale, viewModel, 0));
		return mav;
	}
	
	@RequestMapping(value = "/reports/courseRating/loadBillboard", method = RequestMethod.POST)
	public ModelAndView loadBillboard(Locale locale) throws HandyCoreException {
		logger.info("Overall course rating report page: loadBillboard");

		ModelAndView mav = new ModelAndView("reports/allCourseRating/billboard");
		mav.addObject("bean", populateBean(locale, getDefaultVM(0), appConfig.getDefaultRowLimit()));
		return mav;
	}

	@RequestMapping(value = "/reports/courseRating/renderBillboard", method = RequestMethod.POST)
	public ModelAndView renderBillboard(Locale locale, @RequestBody ReportViewModel viewModel) throws HandyCoreException {
		logger.info("Overall course rating report page: renderBillboard");

		ModelAndView mav = new ModelAndView("reports/allCourseRating/billboard");
		mav.addObject("bean", populateBean(locale, viewModel, appConfig.getDefaultRowLimit()));
		return mav;
	}

	private ReportViewModel getDefaultVM(int categoryId) {
		ReportViewModel defaultModel = new ReportViewModel();
		defaultModel.setController("/reports/courseRating");
		defaultModel.setSort(AllCourseRatingItem.COLUMN_COUNT_LIKES);
		defaultModel.setDirection(SortDirection.DESC);
		defaultModel.setCategoryFilter(categoryId);
		defaultModel.setReportType(ReportType.COURSE_RATING);
		return defaultModel;
	}
	
	private AllCourseRatingBean populateBean(Locale locale, ReportViewModel viewModel, int rowLimit) throws HandyCoreException {
		AllCourseRatingBean bean = new AllCourseRatingBean();
		bean.setItems(reportService.getAllCourseRatingItems(criteriaByModel(viewModel, rowLimit)));
		bean.setViewModel(viewModel);

		List<CourseCategory> categories = new ArrayList<CourseCategory>();
		categories.add(new FakeCategory());
		categories.addAll(courseCategoryDao.getAll());
		bean.setCategories(categories);

		return bean;
	}

	private AllCourseRatingCriteria criteriaByModel(ReportViewModel viewModel, int rowLimit) {
		AllCourseRatingCriteria allCourseRatingCriteria = new AllCourseRatingCriteria();
		allCourseRatingCriteria.setRowCount(rowLimit);
		allCourseRatingCriteria.setOrderColumn(viewModel.getSort());
		allCourseRatingCriteria.setSortDirection(viewModel.getDirection());
		allCourseRatingCriteria.setFilterCategoryID(viewModel.getCategoryFilter());
		allCourseRatingCriteria.setFindValue(viewModel.getSearch());
		return allCourseRatingCriteria;
	}
}
