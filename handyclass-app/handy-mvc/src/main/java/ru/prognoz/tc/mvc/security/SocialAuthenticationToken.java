package ru.prognoz.tc.mvc.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import ru.prognoz.tc.domain.User;

public class SocialAuthenticationToken extends AbstractAuthenticationToken {

	private static final long serialVersionUID = 7332730917011704503L;

	private SocialNetwork network;
	private String accountId;
	private User retrivedUser = null;

	public SocialAuthenticationToken(SocialNetwork network, String accountId) {
		super(null);
		setAuthenticated(false);
		this.network = network;
		this.accountId = accountId;
	}

	public SocialAuthenticationToken(SocialNetwork network, String accountId, User user) {
		super(user.getAuthorities());
		setAuthenticated(true);
		this.network = network;
		this.accountId = accountId;
		this.retrivedUser = user;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return this.retrivedUser;
	}

	public void setRetrivedUser(User retrivedUser) {
		this.retrivedUser = retrivedUser;
	}

	public SocialNetwork getNetwork() {
		return network;
	}

	public void setNetwork(SocialNetwork network) {
		this.network = network;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

}
