package ru.prognoz.tc.mvc.controller.courselist;

import java.io.Serializable;

import ru.prognoz.tc.mvc.controller.courselist.mode.NotifyMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.RegisteredMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.SortMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.UserMode;
import ru.prognoz.tc.mvc.controller.courselist.mode.ViewMode;

public class CourseListViewModel implements Serializable {

	private static final long serialVersionUID = 2871574113451005677L;

	private ViewMode viewMode = ViewMode.PORTRAIT;
	private UserMode userMode = UserMode.PUPIL;
	private NotifyMode notifyMode = NotifyMode.HIDE;
	private int categoryValue = 0;
	private SortMode sortMode = SortMode.DATE;
	private RegisteredMode regMode = RegisteredMode.ALL;

	public ViewMode getViewMode() {
		return viewMode;
	}

	public void setViewMode(ViewMode viewMode) {
		this.viewMode = viewMode;
	}

	public UserMode getUserMode() {
		return userMode;
	}

	public void setUserMode(UserMode userMode) {
		this.userMode = userMode;
	}

	public NotifyMode getNotifyMode() {
		return notifyMode;
	}

	public void setNotifyMode(NotifyMode notifyMode) {
		this.notifyMode = notifyMode;
	}

	public int getCategoryValue() {
		return categoryValue;
	}

	public void setCategoryValue(int categoryValue) {
		this.categoryValue = categoryValue;
	}

	public SortMode getSortMode() {
		return sortMode;
	}

	public void setSortMode(SortMode sortMode) {
		this.sortMode = sortMode;
	}

	public RegisteredMode getRegMode() {
		return regMode;
	}

	public void setRegMode(RegisteredMode regMode) {
		this.regMode = regMode;
	}

}
