package ru.prognoz.tc.mvc.business.course.access;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseRegistrationDao;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;

@Service
public class CourseAccessService {

	private static final Logger logger = LoggerFactory.getLogger(CourseAccessService.class);
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private InviteDao inviteDao;
	@Autowired
	private CourseRegistrationDao courseRegistrationDao;

	
	/**
	 * Проверка доступа к просмотру информации о курсе.
	 * @param course
	 * @param student
	 * @param locale
	 * @return
	 * @throws HandyCoreException
	 */
	public boolean canViewAccess(final Course course, final User student, final Locale locale) throws HandyCoreException {
		if (course == null || student == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		User author = course.getOwner();
		/*
		 * Если пользователь является автором курса, то он всегда имеет доступ к курсу.
		 */
		if (student.getId() == author.getId()) {
			return true;
		}
		/*
		 * К приватным курсам доступ закрыт.
		 */
		if (CourseAccessType.PRIVATE.equals(course.getAccessType())) {
			return false;
		}
		/*
		 * К курсам по приглашению доступ имеют только те кого пригласили, либо те кто уже зарегистрировался на курс.
		 */
		if (CourseAccessType.INVITE.equals(course.getAccessType())) {
			if (inviteDao.isInvited(course, student)) {
				return true;
			}
			if (courseRegistrationDao.isRegistered(course, student)) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Проверка доступа пользователя к плееру заданного курса.
	 * @param course курс, к которому пытаются получить доступ.
	 * @param student Пользователь, который пытается получить доступ к курсу. (может быть <code>null</code>)
	 * @param locale текущая локаль для формирования сообщений.
	 * @return boolean <code>true</> - если доступ разрешен, <code>false</> - если доступ запрещен. 
	 * @throws HandyCoreException
	 */
	public boolean canPlayAccess(final Course course, final User student, final Locale locale) throws HandyCoreException {
		if (course == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		if (student == null) return false;
		
		User author = course.getOwner();
		/*
		 * Если пользователь является автором курса, то он всегда имеет доступ к курсу.
		 */
		if (student.getId() == author.getId()) {
			return true;
		}
		return courseRegistrationDao.isRegistered(course, student);
	}
	
	public boolean canPlayTest(final Course course, final User student, final Locale locale) throws HandyCoreException {
		// TODO can play test
		if (course == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		if (student == null) return false;
		
		User author = course.getOwner();
		/*
		 * Если пользователь является автором курса, то он всегда имеет доступ к курсу.
		 */
		if (student.getId() == author.getId()) {
			return true;
		}
		return courseRegistrationDao.isRegistered(course, student);
	}
	
	public boolean canRegistrate(final Course course, final User student, final Locale locale) throws HandyCoreException {
		if (course == null || student == null) {
			logger.error("Ошибка входных данных");
			throw new HandyCoreException(messageSource.getMessage("ErrorMessage.businessLogicError", null, locale));
		}
		User author = course.getOwner();
		/*
		 * Если пользователь является автором курса, то его нельзя регистрировать
		 */
		if (student.getId() == author.getId()) {
			return false;
		}
		// Если пользователь уже зарегистрирован, то его нельзя регистрировать
		if (courseRegistrationDao.isRegistered(course, student)) {
			return false;
		}
		// На публичный курс можно регистрировать всех
		if (CourseAccessType.PUBLIC.equals(course.getAccessType())) {
			return true;
		}
		// На курсы по приглашениям, проверяем приглашение.
		if (CourseAccessType.INVITE.equals(course.getAccessType())) {
			if (inviteDao.isInvited(course, student)) {
				return true;
			}
			return false;
		}
		// На приватные курсы никого не регистрируем
		if (CourseAccessType.PRIVATE.equals(course.getAccessType())) {
			return false;
		}
		return false;
	}
	
	

}
