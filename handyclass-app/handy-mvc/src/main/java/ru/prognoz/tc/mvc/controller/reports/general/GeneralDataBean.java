package ru.prognoz.tc.mvc.controller.reports.general;

public class GeneralDataBean {

	private long countCourses = 0;
	private long countUsers = 0;

	public long getCountCourses() {
		return countCourses;
	}

	public void setCountCourses(long countCourses) {
		this.countCourses = countCourses;
	}

	public long getCountUsers() {
		return countUsers;
	}

	public void setCountUsers(long countUsers) {
		this.countUsers = countUsers;
	}

}
