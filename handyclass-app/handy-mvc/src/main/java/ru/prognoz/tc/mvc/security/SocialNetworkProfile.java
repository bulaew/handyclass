package ru.prognoz.tc.mvc.security;

public class SocialNetworkProfile {

	private SocialNetwork network;
	private String accountId;
	private String firstName;
	private String lastName;

	public SocialNetwork getNetwork() {
		return network;
	}

	public void setNetwork(SocialNetwork network) {
		this.network = network;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
