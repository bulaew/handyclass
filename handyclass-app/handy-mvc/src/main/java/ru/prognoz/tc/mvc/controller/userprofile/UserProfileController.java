package ru.prognoz.tc.mvc.controller.userprofile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.utils.HcDateTime;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.UserDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.mvc.business.course.access.CourseAccessService;

@Controller
public class UserProfileController {

	private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

	@Autowired
	private UserDao userDao;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CourseAccessService courseAccessService;

	@RequestMapping(value = "/userprofile/{id}", method = RequestMethod.GET)
	public String home(Locale locale, Model model, @PathVariable("id") Long id) throws HandyCoreException {
		logger.info("User Profile Page." + " User oid is " + id);
		User user = userDao.getByOid(id);
		if (user == null) {
			model.addAttribute("message", "Пользователя с таким OID не существует");
			return "error";
		}
		model.addAttribute("bean", populateBean(locale, user));
		return "userprofile/page";
	}
	
	private UserProfileBean populateBean(Locale locale, User user) throws HandyCoreException {
		UserProfileBean bean = new UserProfileBean(user);
		bean.setCourseItems(populateCourses(locale, user));
		return bean;
	}
	
	private List<CourseItemBean> populateCourses(Locale locale, User user) throws HandyCoreException {
		List<Course> courses = courseDao.getUserOwnCourses(user);
		List<CourseItemBean> items = new ArrayList<CourseItemBean>();
		for (Course course : courses) {
			if (course.getAccessType() == CourseAccessType.PRIVATE) {
				continue;
			}
			CourseItemBean item = new CourseItemBean(course);
			item.setLocked(false);
			if (course.getAviableTo() != null) {
				if (course.getAviableTo().before(new Date())) { // Курс закончился.
					item.setLocked(true);
				}
				item.setAvailableTo(HcDateTime.tooltipDt(course.getAviableTo(), locale));
			} else {
				item.setAvailableTo("");
			}
			User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (!courseAccessService.canViewAccess(course, currentUser, locale)) {
				item.setLocked(true);
			}
			
			items.add(item);
		}
		
		return items;
	}
}
