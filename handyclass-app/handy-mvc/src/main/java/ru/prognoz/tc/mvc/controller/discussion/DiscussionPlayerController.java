package ru.prognoz.tc.mvc.controller.discussion;

import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.DiscussionDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.mvc.business.forum.ForumService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.controller.editcourse.ForumBean;

@Controller
@Scope("request")
@RequestMapping(value = "/main/player/discussion")
public class DiscussionPlayerController {

	private static final Logger logger = LoggerFactory.getLogger(DiscussionPlayerController.class);
	private static final String FORUM_BEAN_ATTR_NAME = "forumData";

	@Autowired
	private CourseDao courseDao;
	@Autowired
	private DiscussionDao discussionDao;
	@Autowired
	private HandyObjectDao oidDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ForumService forumService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private ViewCounterDao viewCounterDao;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(Locale locale, @RequestParam(value = "coid") final long courseOid) throws HandyCoreException {
		logger.info("Player discussion page");
		ModelAndView modelAndView = new ModelAndView("discussion/player/index");
		modelAndView.addObject("bean", init(courseOid));
		return modelAndView;
	}

	private DiscussionPlayerBean init(long courseOid) {
		DiscussionPlayerBean bean = new DiscussionPlayerBean();
		Course course = courseDao.getByOid(courseOid);
		int views = viewCounterDao.getCountViews(course.getObjectId());
		bean.setCountRegistrations(views);
		bean.setCourse(course);
		return bean;
	}

	@RequestMapping(value = { "/topic/save" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> saveTopic(Locale locale, @RequestParam(value = "oid") long oid, @RequestParam(value = "name") String name) {
		logger.info("Discussion Player: Rename Topic");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Discussion discussion = discussionDao.getByOid(oid);
		if (discussion.getCreator().getObjectId().getId() == currentUser.getObjectId().getId()) {
			discussion.setTopic(name);
			discussionDao.merge(discussion);
		}
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult(discussion.getObjectId().getId() + "");
		return result;
	}

	@RequestMapping(value = { "/topic/add" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> addTopic(Locale locale, @RequestParam(value = "course") long course) {
		logger.info("Discussion Player: Add Topic");
		Discussion discussion = discussionDao.newDomainInstatnce();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		discussion.setObjectId(oidDao.generate());
		discussion.setCreator(currentUser);
		discussion.setCreateDate(new Date());
		discussion.setModifiedDate(new Date());
		discussion.setCourse(courseDao.getByOid(course));
		discussion.setTopic(messageSource.getMessage("DiscussionFace.newTopicTitle", null, locale));
		discussionDao.save(discussion);
		notificationService.fireNewTopic(discussion);
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult(discussion.getObjectId().getId() + "");
		return result;
	}

	@RequestMapping(value = { "/topic/delete" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> deleteTopic(Locale locale, @RequestParam(value = "oid") long oid) {
		logger.info("Discussion Player: delete Topic");
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Discussion discussion = discussionDao.getByOid(oid);
		if (discussion.getCreator().getObjectId().getId() == currentUser.getObjectId().getId()) {
			discussion.setArchive(true);
			discussionDao.merge(discussion);
		}
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult(discussion.getObjectId().getId() + "");
		return result;
	}

	@RequestMapping(value = { "view/all" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getTopics(Locale locale, @RequestParam(value = "oid") long oid) {
		logger.info("Discussion Edit Course : updateActiveDiscussionPanel");
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView modelAndView = new ModelAndView("/discussion/player/discussionsList");
		result.setResult(modelAndView);
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		modelAndView.addObject("discussions", discussionDao.getByCourseOid(oid));
		modelAndView.addObject("user", currentUser.getObjectId().getId());
		modelAndView.addObject(FORUM_BEAN_ATTR_NAME, new ForumBean(-1L));
		return result;
	}

	@RequestMapping(value = { "view/topic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getTopic(Locale locale, @RequestParam(value = "oid") long oid) {
		logger.info("Discussion Edit Course : view topic");
		Discussion discussion = discussionDao.getByOid(oid);
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView modelAndView = new ModelAndView("/discussion/player/active/header");
		result.setResult(modelAndView);
		modelAndView.addObject("active", discussion);
		modelAndView.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(discussion.getObjectId().getId(), ""));
		return result;
	}

	@RequestMapping(value = { "/topic/edit" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<ModelAndView> getActiveTopic(Locale locale, @RequestParam(value = "oid") long oid) {
		logger.info("Discussion Edit Course : activate topic");
		Discussion discussion = discussionDao.getByOid(oid);
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ModelAndView modelAndView = null;
		if (discussion.getCreator().getObjectId().getId() == currentUser.getObjectId().getId()) {
			modelAndView = new ModelAndView("/discussion/player/active/activeHeader");
		} else {
			modelAndView = new ModelAndView("/discussion/player/active/header");
		}
		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		result.setResult(modelAndView);
		modelAndView.addObject("active", discussion);
		modelAndView.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(discussion.getObjectId().getId(), ""));
		return result;
	}
	
	private ForumBean populateForumBean(Long forumRootComponent, String forumHandler) {
		ForumBean forum = new ForumBean(forumRootComponent);
		forum.setOnChangeHandler(forumHandler);
		return forum;
	}
}
