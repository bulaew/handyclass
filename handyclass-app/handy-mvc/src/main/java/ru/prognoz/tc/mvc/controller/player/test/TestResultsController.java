package ru.prognoz.tc.mvc.controller.player.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.ViewCounterDao;
import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.dao.attempt.QuestionAttemptDao;
import ru.prognoz.tc.dao.attempt.TestAttemptDao;
import ru.prognoz.tc.dao.test.QuestionDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

@Controller
@RequestMapping(value = "/main/testresult/")
public class TestResultsController {

	@Autowired
	private TestResultsBean bean;
	@Autowired
	private TestDao testDao;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private TestAttemptDao taDao;
	@Autowired
	private CourseAttemptDao caDao;
	@Autowired
	private QuestionAttemptDao qaDao;
	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private ViewCounterDao viewCounterDao;

	private static final Logger logger = LoggerFactory
			.getLogger(TestResultsController.class);

	private void init(Long oid) {
		bean = new TestResultsBean();
		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		Test test = testDao.getByObjectId(oid);
		Course course = courseDao.getByOid(test.getCourseOID().getId());
		int views = viewCounterDao.getCountViews(course.getObjectId());
		TestAttempt ta = taDao.getOrCreate(test, user);
		CourseAttempt ca = caDao.getOrCreate(course, user);
		List<QuestionAttempt> qa = new ArrayList<QuestionAttempt>();
		List<Question> questions = questionDao.getQuestionsByTest(test);
		for (Question question : questions) {
			qa.add(qaDao.getOrCreate(question, user));
		}
		bean.setCountRegistrations(views);
		bean.setCourse(course);
		bean.setTest(test);
		bean.setCa(ca);
		bean.setQa(qa);
		bean.setTa(ta);
		bean.setQuestions(questions);
	}

	@RequestMapping(value = { "/{oid}" }, method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView changePage(Locale locale, Model mod,
			@PathVariable(value = "oid") Long oid) {
		logger.debug("Open test with oid " + oid);
		init(oid);
		ModelAndView mav = new ModelAndView("testplayer/result/index");
		mav.addObject("bean", bean);
		return mav;
	}
}
