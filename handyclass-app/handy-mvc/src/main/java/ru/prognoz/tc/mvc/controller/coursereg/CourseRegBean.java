package ru.prognoz.tc.mvc.controller.coursereg;

import java.util.ArrayList;
import java.util.List;

import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;

public class CourseRegBean {
	
	private Course course;
	private List<Attachment> attachments = new ArrayList<Attachment>();
	private Long viewCounter = 321L;
	private Invite invite;

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Long getViewCounter() {
		return viewCounter;
	}

	public void setViewCounter(Long viewCounter) {
		this.viewCounter = viewCounter;
	}

	public Invite getInvite() {
		return invite;
	}

	public void setInvite(Invite invite) {
		this.invite = invite;
	}
	
}
