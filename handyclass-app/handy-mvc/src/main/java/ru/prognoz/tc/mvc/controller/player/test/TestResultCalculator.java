package ru.prognoz.tc.mvc.controller.player.test;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.dao.attempt.CourseAttemptDao;
import ru.prognoz.tc.data.CourseStatus;
import ru.prognoz.tc.data.TestStatus;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.business.player.PlayerSaveService;
import ru.prognoz.tc.mvc.controller.player.course.CoursePlayerController;

@Service
public class TestResultCalculator {

	private static final Logger logger = LoggerFactory
			.getLogger(CoursePlayerController.class);

	@Autowired
	private PlayerSaveService saveService;
	@Autowired
	private CourseAttemptDao caDao;
	@Autowired
	private NotificationService notificationService;

	public void calculateTest(TestPlayerBean bean) {
		float result = 0F;
		float increment = 100 / bean.getQuestions().size();
		for (Question question : bean.getQuestions()) {
			QuestionAttempt qa = bean.getQuestionsAttempts().get(
					question.getObjectId().getId());
			if (isAnswersRight(question, bean)) {
				qa.setCorrect(true);
				result += increment;
			} else {
				qa.setCorrect(false);
			}
			try {
				saveService.saveAttempt(qa);
				logger.error("QuestionAttempt с OID:"
						+ qa.getQuestionOid().getId() + " сохранена :)");
			} catch (Exception e) {
				logger.error("QuestionAttempt с OID:"
						+ qa.getQuestionOid().getId() + "не сохранена :(");
			}
		}
		double ceilResult = Math.ceil(result);
		updateTestAttempt(bean, ceilResult);
	}

	private void updateTestAttempt(TestPlayerBean bean, double ceilResult) {
		TestAttempt ta = bean.getTestAttempt();
		ta.setCountAttempts(ta.getCountAttempts() + 1);
		ta.setLastResult((int) ceilResult);
		if (ta.getBestResult() < ta.getLastResult()) {
			ta.setBestResult(ta.getLastResult());
		}
		if (ta.getStatus() != TestStatus.PASSED) {
			if (ta.getBestResult() > bean.getTest().getPassingScore()) {
				ta.setStatus(TestStatus.PASSED);
			} else {
				ta.setStatus(TestStatus.NOT_PASSED);
			}
		}
		long spendedTime = System.currentTimeMillis()
				- bean.getTestStartedTime();
		ta.setTotalTime(spendedTime + ta.getTotalTime());
		ta.setSpendedTime(spendedTime);
		if (spendedTime < ta.getSpendedTime()) {
			ta.setSpendedTime(spendedTime);
		}
		try {
			saveService.saveAttempt(ta);
			logger.error("TestAttempt для теста " + ta.getTestOid().getId()
					+ " сохранена :)");
		} catch (Exception e) {
			logger.error("TestAttempt для теста " + ta.getTestOid().getId()
					+ "не сохранена :(");
		}
		calculateCourseAttempt(bean);
	}

	private boolean isAnswersRight(Question question, TestPlayerBean bean) {
		List<Answer> answers = bean.getAnswers().get(
				(question.getObjectId().getId()));
		List<UserAnswer> userAnswers = bean.getUserAnswers().get(
				question.getObjectId().getId());
		for (Answer answer : answers) {
			long oid = answer.getObjectId().getId();
			for (UserAnswer userAnswer : userAnswers) {
				if (oid == userAnswer.getOid()) {
					String answerValue = "";
					if (question.getQuestionType().equals("open")) {
						answerValue = answer.getText().toLowerCase();
					} else {
						answerValue = answer.getValue().toLowerCase();
					}
					if (userAnswer.getValue().toLowerCase().equals(answerValue)) {
						continue;
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	private void calculateCourseAttempt(TestPlayerBean bean) {
		CourseAttempt ca = caDao.getOrCreate(bean.getCourse(),
				(User) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal());
		TestAttempt ta = bean.getTestAttempt();
		Test test = bean.getTest();
		Course course = bean.getCourse();
		boolean expired = false;
		if ((course.getAviableTo() != null)
				&& (course.getAviableTo().getTime() < new Date().getTime())) { // время курса закончилось. expired = true;
		}
		CourseStatus newStatus = null;
		if (!test.isEnable()) {
			if (ca.isComplete()) {
				newStatus = CourseStatus.COMPLETED;
			} else {
				newStatus = CourseStatus.UNCOMPLETED;
			}
		} else {
			if (expired) {
				if (ta.getStatus() == TestStatus.PASSED) {
					if (ca.isComplete()) {
						newStatus = CourseStatus.PASSED;
					} else {
						newStatus = CourseStatus.NOT_PASSED;
					}
				} else {
					newStatus = CourseStatus.NOT_PASSED;
				}
			} else {
				if (ta.getStatus() == TestStatus.PASSED) {
					if (ca.isComplete()) {
						newStatus = CourseStatus.PASSED;
					} else {
						newStatus = CourseStatus.UNCOMPLETED;
					}
				} else {
					newStatus = CourseStatus.UNCOMPLETED;
				}
			}
		}
		if (!newStatus.equals(ca.getStatus())) { // Если поменялся статус.
			ca.setStatus(newStatus);
			if (newStatus.equals(CourseStatus.PASSED) || newStatus.equals(CourseStatus.COMPLETED)) {
				ca.setCompleteDate(new Date());
			}
			try {
				saveService.saveAttempt(ca);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (CourseStatus.PASSED.equals(newStatus)
					|| CourseStatus.COMPLETED.equals(newStatus)) {
				notificationService.fireCourseComplete(ca);
				notificationService.fireCompleteTraining(ca);
			}
		}
	}
}
