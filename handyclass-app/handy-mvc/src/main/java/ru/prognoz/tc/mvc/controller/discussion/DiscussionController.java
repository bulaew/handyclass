package ru.prognoz.tc.mvc.controller.discussion;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.DiscussionDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Discussion;
import ru.prognoz.tc.mvc.business.forum.ForumService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.controller.editcourse.ForumBean;

@Controller
@Scope("request")
@RequestMapping(value = "/main/discussion")
public class DiscussionController {

	private static final Logger logger = LoggerFactory.getLogger(DiscussionController.class);
	private static final String FORUM_BEAN_ATTR_NAME = "forumData";
	private static final String FORUM_CHANGED_HANDLER = "DiscussionModule.updateDiscussionListPanel();";

	@Autowired
	private DiscussionBean discussionBean;
	@Autowired
	private CourseDao courseDao;
	@Autowired
	private DiscussionDao discussionDao;
	@Autowired
	private HandyObjectDao oidDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ForumService forumService;
	@Autowired
	private NotificationService notificationService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index(Locale locale, @RequestParam(value = "coid") final long courseOid) throws HandyCoreException {

		logger.info("Discussion page");
		discussionBean.clear();
		
		ModelAndView mav = new ModelAndView("discussion/index");
		mav.addObject("bean", discussionBean);
		discussionBean.clear();
		discussionBean.setEditCourseMode(false);
		Course course = courseDao.getByOid(courseOid);
		discussionBean.setCourse(course);
		discussionBean.setUserOid(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getObjectId().getId());
		if (course.getDiscussions().size() > 0) {
			for(Discussion discussion:course.getDiscussions()){
			    if(!discussion.isArchive()){
				discussionBean.setActiveDiscussion(course.getDiscussions().get(0));
        			break;
			    }
			}
			mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(course.getDiscussions().get(0).getObjectId().getId(), FORUM_CHANGED_HANDLER) );
		} else {
			mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(-1L, FORUM_CHANGED_HANDLER));
		}

		return mav;
	}
	
	/**
	 * Создание нового обсуждения.
	 * 
	 * @param attachmentOid
	 * @return
	 */
	@RequestMapping(value = { "/ajaxCreateTopic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<Long> ajaxCreateTopic(Locale locale) {
		logger.info("Discussion Edit Course : ajaxCreateTopic");
		Discussion discussion = discussionDao.newDomainInstatnce();
		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		discussion.setObjectId(oidDao.generate());
		discussion.setCreator(currentUser);
		discussion.setCreateDate(new Date());
		discussion.setModifiedDate(new Date());
		discussion.setCourse(discussionBean.getCourse());
		discussion.setTopic(messageSource.getMessage("DiscussionFace.newTopicTitle", null, locale));
		if (discussionBean.isEditCourseMode() == false) {
			discussionDao.save(discussion);
			notificationService.fireNewTopic(discussion);
		}
		
		discussionBean.getCourse().getDiscussions().add(discussion);

		DeferredResult<Long> result = new DeferredResult<Long>();
		result.setResult(discussion.getObjectId().getId());
		return result;
	}

	@RequestMapping(value = { "/ajaxGetTopicStat" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxGetTopicStat() {
		logger.info("Discussion Edit Course : ajaxGetTopicStat");

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxUpdateDiscListPanel" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> ajaxUpdateDiscListPanel() {
		logger.info("Discussion Edit Course : ajaxUpdateDiscListPanel");

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/discussion/discussionList");
		if (discussionBean.getActiveDiscussion() != null) {
			mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(discussionBean.getActiveDiscussion().getObjectId().getId(), FORUM_CHANGED_HANDLER));
		}
		result.setResult(mav);
		return result;
	}

	@RequestMapping(value = { "/ajaxActivateTopic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxActivateTopic(@RequestParam(value = "discussionOid") final Long discussionOid) {
		logger.info("Discussion Edit Course : ajaxActivateTopic");
		List<Discussion> discussionList = discussionBean.getCourse().getDiscussions();
		for (Discussion discussion : discussionList) {
			if (discussion.getObjectId().getId() == discussionOid) {
				discussionBean.setActiveDiscussion(discussion);
			}
		}
		discussionBean.setEditableDiscussion(false);
		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxStartEditTopic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxStartEditTopic() {
		logger.info("Discussion Edit Course : ajaxStartEditTopic");

		discussionBean.setEditableDiscussion(true);

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxCompleteEditTopic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxCompleteEditTopic(
			@RequestParam(value = "discussionTopic", defaultValue = "") final String discussionTopic) {
		logger.info("Discussion Edit Course : ajaxCompleteEditTopic");

		discussionBean.getActiveDiscussion().setTopic(discussionTopic);
		discussionBean.setEditableDiscussion(false);
		if (discussionBean.isEditCourseMode() == false) {
			discussionDao.save(discussionBean.getActiveDiscussion());
		}

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxCancelEditTopic" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxCancelEditTopic() {
		logger.info("Discussion Edit Course : ajaxCancelEditTopic");

		discussionBean.setEditableDiscussion(false);

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	/**
	 * Перерисовка всей панели активного обсуждения. (хедер и форум)
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/updateActiveDiscussionPanel" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> updateActiveDiscussionPanel() {
		logger.info("Discussion Edit Course : updateActiveDiscussionPanel");

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/discussion/activeDiscussionPanel");
		if (discussionBean.getActiveDiscussion() != null) {
			mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(discussionBean.getActiveDiscussion().getObjectId().getId(), FORUM_CHANGED_HANDLER));
		}
		result.setResult(mav);
		return result;
	}

	/**
	 * Перерисовка хедера панели активного обсуждения.
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/updateDiscussionContent" }, method = RequestMethod.POST)
	public DeferredResult<String> updateDiscussionContent() {
		logger.info("Discussion Edit Course : updateActiveDiscussionPanel");

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("/discussion/discussionContent");
		return result;
	}

	/*
	 * Обработчики форума.
	 */
	@RequestMapping(value = { "/ajaxOnAddComment" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxOnAddComment(@RequestParam(value = "param_parentOID") final Long parentOID,
			@RequestParam(value = "param_commentContent", defaultValue = "") final String content) {
		logger.info("Discussion Edit Course : ajaxOnAddComment");

		User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		forumService.createComment(currentUser, parentOID, content);

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxOnEditComment" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxOnEditComment(@RequestParam(value = "param_objectID") final Long commentOID,
			@RequestParam(value = "param_commentContent", defaultValue = "") final String content) {
		logger.info("Discussion Edit Course : ajaxOnEditComment");

		forumService.editComment(commentOID, content);

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	@RequestMapping(value = { "/ajaxOnRemoveComment" }, method = RequestMethod.POST)
	@ResponseBody
	public DeferredResult<String> ajaxOnRemoveComment(@RequestParam(value = "param_objectID") final Long commentOID) {
		logger.info("Discussion Edit Course : ajaxOnRemoveComment");

		forumService.removeComment(commentOID);

		DeferredResult<String> result = new DeferredResult<String>();
		result.setResult("ok");
		return result;
	}

	/**
	 * Перерисовка форума.
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/updateForumContent" }, method = RequestMethod.POST)
	public DeferredResult<ModelAndView> updateForumContent(@RequestParam(value = "forum_root") final Long forumRoot,
			@RequestParam(value = "forum_handler") final String forumHandler) {
		logger.info("Discussion Edit Course : updateForumContent");

		DeferredResult<ModelAndView> result = new DeferredResult<ModelAndView>();
		ModelAndView mav = new ModelAndView("/discussion/forumContent");
		mav.addObject(FORUM_BEAN_ATTR_NAME, populateForumBean(forumRoot, forumHandler));
		result.setResult(mav);
		return result;
	}

	private ForumBean populateForumBean(Long forumRootComponent, String forumHandler) {
		ForumBean forum = new ForumBean(forumRootComponent);
		forum.setOnChangeHandler(forumHandler);
		return forum;
	}
}
