package ru.prognoz.tc.mvc.business.contact.search;

/**
 * Элемент списка результатов поиска контакта.
 * 
 * @author Denis.V.Mikulich
 * 
 */
public class ResultItem {

	/**
	 * тип результата
	 */
	private ResultItemType type;
	/**
	 * ОИД контакта
	 */
	private Long objectID;
	/**
	 * имя пользователя.
	 */
	private String userName;
	/**
	 * емейл пользователя
	 */
	private String email;
	/**
	 * Аватар
	 */
	private String avatar;
	/**
	 * Релевантность контакта.
	 */
	private long relevo;

	public ResultItemType getType() {
		return type;
	}

	public void setType(ResultItemType type) {
		this.type = type;
	}

	public Long getObjectID() {
		return objectID;
	}

	public void setObjectID(Long objectID) {
		this.objectID = objectID;
	}

	public String getEmail() {
		return userName;
	}

	public void setEmail(String value) {
		this.userName = value;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public long getRelevo() {
		return relevo;
	}

	public void setRelevo(long l) {
		this.relevo = l;
	}

	@Override
	public String toString() {
		return "{ 'type' : '" + type.getStrValue() + "', 'oid' : '" + objectID + "', 'username' : '" + userName + "', 'email' : '" + email + "', 'avatar' : '" + avatar + "'}";
	}

}
