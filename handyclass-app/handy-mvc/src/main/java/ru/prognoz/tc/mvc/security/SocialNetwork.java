package ru.prognoz.tc.mvc.security;

public enum SocialNetwork {

	GOOGLE, FACEBOOK, TWITTER, VKONTAKTE
}
