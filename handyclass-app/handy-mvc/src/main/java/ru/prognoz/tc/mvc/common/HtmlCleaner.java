package ru.prognoz.tc.mvc.common;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class HtmlCleaner {

	// Тэги доступные в wysiwyg редакторе
	public static String cleanWysiwyg(String html) {
		Whitelist whitelist = new Whitelist();
		// Допустимыми являются элементы нумерованного и ненумерованного списков, подчеркнутый, курсивный и жирный стили текста, тэг абзаца
		// и переноса на следующую строку.
		whitelist.addTags("p", "ol", "ul", "li", "em", "strong", "span", "br", "b", "u", "i");
		whitelist.addAttributes("span", "style");
		return Jsoup.clean(html, whitelist);
	}

	// Возвращает только простой текст без какой-либо разметки.
	public static String cleanAll(String html) {
		return Jsoup.clean(html, Whitelist.none());
	}
}
