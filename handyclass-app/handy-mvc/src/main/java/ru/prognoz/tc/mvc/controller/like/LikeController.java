package ru.prognoz.tc.mvc.controller.like;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.ModelAndView;

import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.LikeDao;
import ru.prognoz.tc.domain.Like;
import ru.prognoz.tc.domain.User;

@Controller
public class LikeController {

	@Autowired
	private LikeDao likeDao;
	@Autowired
	private HandyObjectDao oidDao;

	@Scope("request")
	@RequestMapping(value = "/like/get/{id}", method = RequestMethod.POST)
	public DeferredResult<ModelAndView> get(@PathVariable("id") Long id) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView("like/like");
		model.addObject("id", id);
		model.addObject("count", likeDao.getCountLikes(id));

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication instanceof AnonymousAuthenticationToken)) {
			model.addObject("active", false); // GUEST
		} else {
			model.addObject("active", likeDao.isLiked(id, (User) authentication.getPrincipal()));
		}
		result.setResult(model);
		return result;
	}

	@Scope("request")
	@RequestMapping(value = "/like/push/{id}", method = RequestMethod.POST)
	public DeferredResult<ModelAndView> push(@PathVariable("id") Long id) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		ModelAndView model = new ModelAndView("like/like");
		likeDao.pushLike(id, (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		model.addObject("count", likeDao.getCountLikes(id));
		model.addObject("active", likeDao.isLiked(id, (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
		model.addObject("id", id);
		result.setResult(model);
		return result;
	}

	@Scope("request")
	@RequestMapping(value = "/like/all/{id}", method = RequestMethod.POST)
	public DeferredResult<ModelAndView> all(@PathVariable("id") Long id) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		List<UserItem> users = getAll(id);
		ModelAndView model = new ModelAndView("like/all");
		model.addObject("users", users);
		model.addObject("count", users.size());
		result.setResult(model);
		return result;
	}

	@Scope("request")
	@RequestMapping(value = "/like/last/{id}", method = RequestMethod.POST)
	public DeferredResult<ModelAndView> last(@PathVariable("id") Long id) {
		DeferredResult<ModelAndView> result = new DeferredResult<>();
		List<UserItem> users = getLast(id);
		ModelAndView model = new ModelAndView("like/last");
		model.addObject("users", users);
		if (users.size() * 32 + 16 < 70) {
			model.addObject("width", 70 + "px");
		} else {
			model.addObject("width", users.size() * 32 + 16 + "px");
		}
		result.setResult(model);
		return result;
	}

	private List<UserItem> getLast(Long id) {
		List<UserItem> users = new ArrayList<UserItem>();
		List<Like> likes = likeDao.getComponentLikes(id, 5L);
		for (Like like : likes) {
			UserItem item = new UserItem();
			item.setId(like.getOwner().getObjectId().getId());
			item.setImage(like.getOwner().getAvatarURL());
			users.add(item);
		}
		return users;
	}

	private List<UserItem> getAll(Long id) {
		List<UserItem> users = new ArrayList<UserItem>();
		List<Like> likes = likeDao.getComponentLikes(id);
		for (Like like : likes) {
			UserItem item = new UserItem();
			item.setId(like.getOwner().getObjectId().getId());
			item.setImage(like.getOwner().getAvatarURL());
			item.setName(like.getOwner().getName());
			item.setMail(like.getOwner().getLogin());
			users.add(item);
		}
		return users;
	}
}
