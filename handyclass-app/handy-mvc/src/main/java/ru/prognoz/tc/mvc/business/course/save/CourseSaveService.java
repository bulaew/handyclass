package ru.prognoz.tc.mvc.business.course.save;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ru.prognoz.tc.core.business.HandyCoreException;
import ru.prognoz.tc.core.utils.HcRandom;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.InviteDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Attachment;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.test.Answer;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.mvc.business.contact.ContactService;
import ru.prognoz.tc.mvc.business.coursereg.CourseRegistryService;
import ru.prognoz.tc.mvc.business.mail.MailService;
import ru.prognoz.tc.mvc.business.notification.NotificationService;
import ru.prognoz.tc.mvc.business.participants.IParticipant;
import ru.prognoz.tc.mvc.business.participants.InvitedParticipant;
import ru.prognoz.tc.mvc.business.participants.RegisteredParticipant;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseBean;
import ru.prognoz.tc.mvc.controller.editcourse.EditCourseVerificationMessage;
import ru.prognoz.tc.service.RollbackedSaver;

@Service
public class CourseSaveService {

	@Autowired
	private HandyObjectDao objectDao;
	@Autowired
	private InviteDao inviteDao;

	@Autowired
	private RollbackedSaver saveService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private MailService mailService;
	@Autowired
	private CourseRegistryService courseRegistryService;
	@Autowired
	private ContactService contactService;

	public EditCourseVerificationMessage verifyCourse(EditCourseBean courseBean) {
		Course course = courseBean.getCourse();
		if (StringUtils.isEmpty( course.getFullName()) ) {
			return EditCourseVerificationMessage.EMPTY_COURSE_TITLE;
		}
		return null;
	}
	
	public void storeCourse(EditCourseBean courseBean) throws HandyCoreException {
		try {
			List<Object> entities = new ArrayList<Object>();
			entities.add(courseBean.getCourse());

			// Сохранение прикреплений:
			for (Attachment attachment : courseBean.getAttachments()) {
				entities.add(attachment);
			}

			for (Slide slide : courseBean.getSlides()) {
				entities.add(slide);
				for (SlideComponent component : courseBean.getComponentsBySlide(slide)) {
					if(component.getType()=="marker" || component.getType()=="pointer") {
					    continue;
					}
				    	component.setActive(false);
					entities.add(component);
					try {
						for (ChildComponent child : courseBean.getChildComponents().get(component.getComponentOID().getId())) {
							entities.add(child);
						}
					} catch (NullPointerException e) {
						continue;
					}
				}
			}

			for (Question question : courseBean.getQuestions()) {
				question.setActive(false);
				entities.add(question);
				if (courseBean.getAnswersByQuestion(question) != null) {
					if (courseBean.getAnswersByQuestion(question) != null) {
						for (Answer answer : courseBean.getAnswersByQuestion(question)) {
							answer.setActive(false);
							entities.add(answer);
						}
					}
				}
			}

			entities.add(courseBean.getTest());
			saveService.merge(entities);
			
		} catch (Exception exc) {
			throw new HandyCoreException(exc);
		}
	}

	/**
	 * Сохранение участников курса.
	 * 
	 * @param courseBean
	 *            Данные курса
	 * @param locale
	 *            текущая локаль пользователя
	 * @throws HandyCoreException
	 */
	public void storeParticipants(final EditCourseBean courseBean, final Locale locale) throws HandyCoreException {
		// Сохранение участников.
		Set<IParticipant> participants = courseBean.getParticipants();
		for (IParticipant participant : participants) {
			if (participant instanceof InvitedParticipant) {
				Invite freshInvite;
				InvitedParticipant invitedParticipant = (InvitedParticipant) participant;
				freshInvite = inviteDao.getById(invitedParticipant.getInviteID());
				if (freshInvite == null) { // если приглашение еще не сохранено в базе.
					if (participant.isRemoved()) {
						continue; // Не сохраняем удаленных участников.
					}
					// Создание приглашения.
					freshInvite = inviteDao.newDomainInstatnce();
					freshInvite.setCourse(courseBean.getCourse());
					freshInvite.setUser(participant.getUser());
					freshInvite.setConfirm(false);
					freshInvite.setGuestEmail(participant.getGuestEmail());
					freshInvite.setInviteHashCode(HcRandom.random().toString());
				} else {
					if (participant.isRemoved()) { // Участник сохранен в базе, но его нужно удалить.
						notificationService.fireInviteDismiss(participant.getUser(), courseBean.getCourse());
						mailService.dismissInvite(locale, courseBean.getCourse(), participant.getUser(), participant.getGuestEmail());
						inviteDao.removeInvite(freshInvite);
						continue;
					}
				}
				freshInvite.setActive(CourseAccessType.INVITE.equals(courseBean.getCourse().getAccessType()));
				if (freshInvite.isActive() && !freshInvite.isInviteMsgSended()) {
					notificationService.fireInviteSended(freshInvite);
					mailService.sendInvite(locale, freshInvite);
					freshInvite.setInviteMsgSended(true);
					User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
					contactService.increaseRelevo(currentUser, freshInvite);
				}
				inviteDao.merge(freshInvite);
			}

			if (participant instanceof RegisteredParticipant) {
				if (participant.isRemoved()) { // нужно разрегистрировать пользователя.
					// Инициализация и выполнение бизнес процесса отчисления с курса.
					try {
						courseRegistryService.unregistration(courseBean.getCourse(), participant.getUser(), locale);
						// Инициализация бизнес процесса отправки сообщения об отчислении с курса.
						mailService.dismissCourseRegistry(locale, courseBean.getCourse(), participant.getUser());
					} catch (HandyCoreException exc) {

					}
				}
			}
		}
	}
}
