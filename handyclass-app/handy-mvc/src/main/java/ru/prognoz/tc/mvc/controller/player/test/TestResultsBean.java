package ru.prognoz.tc.mvc.controller.player.test;

import java.util.List;

import org.springframework.stereotype.Component;

import ru.prognoz.tc.domain.attempt.CourseAttempt;
import ru.prognoz.tc.domain.attempt.QuestionAttempt;
import ru.prognoz.tc.domain.attempt.TestAttempt;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;

@Component
public class TestResultsBean {
	private Course course;
	private Test test;
	private TestAttempt ta;
	private CourseAttempt ca;
	private List<QuestionAttempt> qa;
	private List<Question> questions;
	private long countRegistrations = 0;

	public long getCountRegistrations() {
		return countRegistrations;
	}

	public void setCountRegistrations(long countRegistrations) {
		this.countRegistrations = countRegistrations;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public TestAttempt getTa() {
		return ta;
	}

	public void setTa(TestAttempt ta) {
		this.ta = ta;
	}

	public CourseAttempt getCa() {
		return ca;
	}

	public void setCa(CourseAttempt ca) {
		this.ca = ca;
	}

	public List<QuestionAttempt> getQa() {
		return qa;
	}

	public void setQa(List<QuestionAttempt> qa) {
		this.qa = qa;
	}

	public String getTotalTime() {
		return timeFormat(ta.getTotalTime());
	}

	public String getLastTryTime() {
		return timeFormat(ta.getSpendedTime());
	}

	private String timeFormat(long timeInMillis) {
		long time = timeInMillis;
		String seconds = String.valueOf((time / 1000) % 60);
		String minutes = String.valueOf((time / (1000 * 60)) % 60);
		String hours = String.valueOf((time / (1000 * 60 * 60)) % 24);
		for (int i = 0; i < 2; i++) {
			if (seconds.length() < 2) {
				seconds = "0" + seconds;
			}
			if (minutes.length() < 2) {
				minutes = "0" + minutes;
			}
			if (hours.length() < 2) {
				hours = "0" + hours;
			}
		}
		return hours + ":" + minutes + ":" + seconds;
	}

	public String getStatus() {
		return ta.getStatus().name();
	}

	public String getQuestionText(QuestionAttempt qa) {
		for (Question q : questions) {
			if (qa.getQuestionOid().getId() == q.getObjectId().getId()) {
				return q.getText();
			}
		}
		return null;
	}

	public boolean isAttemptBetter() {
		return ta.getLastResult() >= ta.getBestResult();
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;

	}

	public List<Question> getQuestions() {
		return questions;
	}
}
