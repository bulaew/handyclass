package ru.prognoz.tc.mvc.controller.editcourse.slide;

import org.springframework.util.StringUtils;

import ru.prognoz.tc.core.config.HandyclassAppContext;
import ru.prognoz.tc.dao.slide.GraffitiDao;
import ru.prognoz.tc.dao.slide.HeaderDao;
import ru.prognoz.tc.dao.slide.ImageDao;
import ru.prognoz.tc.dao.slide.MarkerDao;
import ru.prognoz.tc.dao.slide.PointerDao;
import ru.prognoz.tc.dao.slide.TextDao;
import ru.prognoz.tc.dao.slide.VideoDao;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;

public class ComponentCreator {

	public static SlideComponent createComponent(String type, Slide slide) {
		if (StringUtils.isEmpty(type)) {
			return null;
		} else if (type.equals("header")) {
			return createHeader(slide);
		} else if (type.equals("text")) {
			return createText(slide);
		} else if (type.equals("video")) {
			return createVideo(slide);
		} else if (type.equals("image")) {
			return createImage(slide);
		} else if (type.equals("graffiti")) {
			return createGraffiti(slide);
		} else {
			return null;
		}
	}

	public static ChildComponent createChildComponent(ComponentPojo pojo, Slide slide) {
		if (pojo.getType().equals("pointer")) {
			return createPointer(pojo, slide);
		} else if (pojo.getType().equals("marker")) {
			return createMarker(pojo, slide);
		} else {
			return null;
		}
	}

	private static SlideComponent createHeader(Slide slide) {
		return HandyclassAppContext.getBean(HeaderDao.class).newDomainInstatnce(slide);
	}

	private static SlideComponent createText(Slide slide) {
		return HandyclassAppContext.getBean(TextDao.class).newDomainInstatnce(slide);
	}

	private static SlideComponent createImage(Slide slide) {
		return HandyclassAppContext.getBean(ImageDao.class).newDomainInstatnce(slide);
	}

	private static SlideComponent createVideo(Slide slide) {
		return HandyclassAppContext.getBean(VideoDao.class).newDomainInstatnce(slide);
	}

	private static SlideComponent createGraffiti(Slide slide) {
		return HandyclassAppContext.getBean(GraffitiDao.class).newDomainInstatnce(slide);
	}

	private static ChildComponent createPointer(ComponentPojo pojo, Slide slide) {
		return HandyclassAppContext.getBean(PointerDao.class).newDomainInstatnce(slide, pojo.getParent(), pojo.getX(), pojo.getY());
	}

	private static ChildComponent createMarker(ComponentPojo pojo, Slide slide) {
		return HandyclassAppContext.getBean(MarkerDao.class).newDomainInstatnce(slide, pojo.getParent(), pojo.getX(), pojo.getY(),
				pojo.getWidth(), pojo.getHeight());
	}
}
