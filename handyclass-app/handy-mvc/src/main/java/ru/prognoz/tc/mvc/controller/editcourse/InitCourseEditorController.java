package ru.prognoz.tc.mvc.controller.editcourse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ru.prognoz.tc.dao.AttachmentDao;
import ru.prognoz.tc.dao.CourseCategoryDao;
import ru.prognoz.tc.dao.CourseDao;
import ru.prognoz.tc.dao.CourseRegistrationDao;
import ru.prognoz.tc.dao.HandyObjectDao;
import ru.prognoz.tc.dao.SlideComponentDao;
import ru.prognoz.tc.dao.slide.SlideDao;
import ru.prognoz.tc.dao.test.AnswerDao;
import ru.prognoz.tc.dao.test.QuestionDao;
import ru.prognoz.tc.dao.test.TestDao;
import ru.prognoz.tc.data.CourseAccessType;
import ru.prognoz.tc.domain.CourseRegistration;
import ru.prognoz.tc.domain.User;
import ru.prognoz.tc.domain.course.Course;
import ru.prognoz.tc.domain.course.Invite;
import ru.prognoz.tc.domain.slide.ChildComponent;
import ru.prognoz.tc.domain.slide.Slide;
import ru.prognoz.tc.domain.slide.SlideComponent;
import ru.prognoz.tc.domain.test.Question;
import ru.prognoz.tc.domain.test.Test;
import ru.prognoz.tc.mvc.business.participants.IParticipant;
import ru.prognoz.tc.mvc.business.participants.InvitedParticipant;
import ru.prognoz.tc.mvc.business.participants.RegisteredParticipant;
import ru.prognoz.tc.mvc.business.participants.TeacherParticipant;

@Controller
@Scope("request")
@RequestMapping(value = "/main/editcourse")
public class InitCourseEditorController {

	private static final Logger logger = LoggerFactory
	    .getLogger(InitCourseEditorController.class);

    @Autowired
    private EditCourseBean courseBean;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private HandyObjectDao oidDao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private AttachmentDao attachmentDao;
    @Autowired
    private CourseRegistrationDao courseRegistrationDao;
    @Autowired
    private SlideDao slideDao;
    @Autowired
    private SlideComponentDao slideComponentDao;
    @Autowired
    private TestDao testDao;
    @Autowired
    private QuestionDao questionDao;
    @Autowired
    private AnswerDao answerDao;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String startEditing(
	    Locale locale,
	    Model model,
	    @RequestParam(value = "coid", defaultValue = "-1") final Long courseOid) {
	logger.info("Init Course Editor: ");
	EditCourseWizardUtils.cleanCourseBean(courseBean);
	if (courseOid == -1) { // Новый курс
	    courseBean.setCourseNew(true);
	    courseBean.setCourse(createNewCourse());
	} else { // редактирование заданного курса
	    loadCourseData(courseOid);
	}
	loadPageData(courseBean);
	initSlides();
	initTest();
	return "redirect:"
		+ EditCourseWizardUtils.getStepUrl(courseBean.getStep());
    }

    private void loadPageData(EditCourseBean courseBean2) {

    }

    private void loadCourseData(Long courseOid) {
	Course course = courseDao.getByOid(courseOid);
	courseBean.setCourse(course);
	courseBean.setAttachments(attachmentDao.getByCourse(course));
	courseBean.setParticipants(loadParticipants(course));
    }

    private Set<IParticipant> loadParticipants(Course currentCourse) {
	// создаем список пользователей приглашенных и/или зарегистрированных на
	// курс.
	Set<IParticipant> totalParticipants = new TreeSet<IParticipant>();
	/*
	 * Добавляем из списка приглашений.
	 */
	Set<Invite> invites = currentCourse.getInvites();
	for (Invite invite : invites) {
	    InvitedParticipant participant = new InvitedParticipant(invite);
	    if (participant.getUser() != null
		    && participant.getUser().getId() == currentCourse
			    .getOwner().getId()) {
		continue; // отфильтровываем учителя
	    }
	    totalParticipants.add(participant);
	}
	/*
	 * Добавляем из списка регистраций.
	 */
	List<CourseRegistration> courseRegistrations = courseRegistrationDao
		.listRegistrations(currentCourse);
	for (CourseRegistration courseRegistration : courseRegistrations) {
	    RegisteredParticipant participant = new RegisteredParticipant(
		    courseRegistration);
	    if (participant.getUser().getId() == currentCourse.getOwner()
		    .getId()) {
		continue; // отфильтровываем учителя
	    }
	    totalParticipants.add(participant);
	}
	/*
	 * Добавляем учителя.
	 */
	totalParticipants.add(new TeacherParticipant(currentCourse.getOwner()));
	return totalParticipants;
    }

    private Course createNewCourse() {
	Course course = courseDao.newDomainInstatnce();
	course.setObjectId(oidDao.generate());
	User currentUser = (User) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();
	course.setOwner(currentUser);
	course.setCreated(new Date());
	course.setModified(new Date());
	course.setAccessType(CourseAccessType.PRIVATE);
	// TODO: убрать магическую константу
	course.setAvatarUrl("/img/course/default/biology.png");
	course.setAviableFrom(new Date());
	return course;
    }

    // Инициализация редактора
    private void initSlides() {
	courseBean.setSlides(slideDao.getAllByCourse(courseBean.getCourse()
		.getObjectId().getId()));
	if (!courseBean.getSlides().isEmpty()) {
	    courseBean.setActiveSlide(courseBean.getVisibleSlides().get(0));
	}
	initSlideComponents();
    }

    private void initSlideComponents() {
	for (Slide slide : courseBean.getSlides()) {
	    List<SlideComponent> components = slideComponentDao
		    .getBySlide(slide);
	    courseBean.setSlideComponents(slide, components);
	    for (SlideComponent parent : components) {
		long parentOid = parent.getComponentOID().getId();
		List<ChildComponent> children = slideComponentDao
			.getChildren(parentOid);
		if (children == null) {
		    courseBean.getChildComponents().put(parentOid,
			    new ArrayList<ChildComponent>());
		} else {
		    courseBean.getChildComponents().put(parentOid, children);
		}
	    }
	}
    }

    private void initTest() {
	Test test = testDao.getByCourse(courseBean.getCourse());
	if (test == null) {
	    test = testDao.newDomainInstance(courseBean.getCourse());
	}
	courseBean.setTest(test);
	initQuestions();
    }

    private void initQuestions() {
	courseBean.setQuestions(questionDao.getQuestionsByTest(courseBean
		.getTest()));
	initAnswers();
    }

    private void initAnswers() {
	if (courseBean.getQuestions() == null) {
	    return;
	}
	for (Question q : courseBean.getQuestions()) {
	    courseBean.getAnswers().put(q.getObjectId().getId(),
		    answerDao.getAnswersByQuestion(q));
	}
    }
}
