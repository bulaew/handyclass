var UseCaseModule = {
	deferredResultAjax : function() {
		$.ajax({
			url : Util.getCurrentPath()+"/usecasedeferred",
			type : "POST",
			data : "daoTestPanelUpdate",
			cache : false,
			success : function(messages) {
				$("#newHandyObjectValue").html(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	callableAjax : function() {
		$.ajax({
			url : Util.getCurrentPath()+"/usecasecallable",
			type : "POST",
			data : "daoTestPanelUpdate",
			cache : false,
			success : function(messages) {
				$("#daoTestPanel").replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	}
}