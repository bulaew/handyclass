var Util = {
	getHandyclassPath : function() {
		var path = document.location.href.split('/');
		return path[0] + '//' + path[2] + '/' + path[3];
	},
	getCurrentPath : function() {
		var path = location.href.split('?')[0];
		if (path[path.length - 1] == '/') {
			path = path.substr(0, path.length - 2);
		}
		return path.substr(0, path.lastIndexOf('/')) + '/';
	},
	goTo : function(path) {
		var url = Util.getHandyclassPath();
		url = url + "/" + path;
		document.location = url;
	},
	goToPath : function(path) {
		location = Util.getCurrentPath() + path;
	},

	fireEvent : function(url, post, callback) {
		$.ajax({
			url : url,
			type : "POST",
			data : post,
			cache : false,
			success : function(jsonResult, status, xhr) {
				Util.onAjaxSuccess(jsonResult, status, xhr, callback);
			},
			error : Util.onAjaxError,
		});
	},

	syncFireEvent : function(url, post, callback) {
		$.ajax({
			url : url,
			type : "POST",
			data : post,
			cache : false,
			success : function(jsonResult, status, xhr) {
				Util.onAjaxSuccess(jsonResult, status, xhr, callback);
			},
			error : Util.onAjaxError,
			async : false
		});
	},

	onAjaxError : function(xhr) {
		if (xhr.statusText != "abort" && xhr.status != 503) {
			console.error("Unable to retrieve chat messages. Chat ended.");
		}
	},

	onAjaxSuccess : function(result, status, xhr, callback) {
		if (xhr.status == 200 && result.match('LOGIN_PAGE_UNIQUE_KEY')) {
			self.location = Util.getHandyclassPath() + "/login";
		} else {
			if (callback) {
				callback(result);
			}
		}
	},

	jsonFireEvent : function(url, post, callback) {
		$.ajax({
			url : url,
			type : "POST",
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(post), // Stringified Json Object
			async : true, // Cross-domain requests and dataType: "jsonp"
			cache : false, // This will force requested pages not to be cached
			processData : false, // To avoid making query String instead of
			// JSON
			success : function(jsonResult, status, xhr) {
				Util.onAjaxSuccess(jsonResult, status, xhr, callback);
			},
			error : Util.onAjaxError
		});
	},
	paging : function() {
		var active = parseInt($('.page.active').text());
		var pages = parseInt($('.page').size());
		if (pages > 15) {
			if (active == 1) {
				$('.page').each(function(index, value) {
					if ((index + 1) == 15) {
						$(value).find('a').replaceWith('<a>...</a>');
					} else if ((index + 1) > 15) {
						$(value).hide();
					}
				});
			} else if (active == pages) {
				$('.page').each(function(index, value) {
					if ((pages - index) == 15) {
						$(value).find('a').replaceWith('<a>...</a>');
					} else if ((pages - index) > 15) {
						$(value).hide();
					}
				});
			} else {
				$('.page').each(
						function(index, value) {
							if ((index - 2) == active || (index - 1) == active
									|| (index + 3) == active
									|| (index + 1) == active
									|| (index + 2) == active
									|| (index + 4) == active || index == active
									|| index < 3 || (pages - index) <= 3) {

							} else if ((index - 3) == active
									|| (index + 5) == active) {
								$(value).find('a').replaceWith('<a>...</a>');
							} else {
								$(value).hide();
							}
						})
			}
		}
	}
};