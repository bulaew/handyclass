var Wysiwyg = {
	// Инициализация tinymce с необходимыми параметрами
	init : function(id) {
		tinymce.init({
			selector : "#" + id,
			toolbar : " underline bold italic | bullist numlist ",
			// body_id: "body_id",
			// body_class: "bodyClass",
			// content_css: "contentClass",
			menubar : false,
			statusbar : false,
			dialog_type : "modal",
			paste_as_text : true,
			// Локализация
			language : getLang(),
			// Для валидации использовать Jsoup на стороне сервера.
			// valid_elements : "p, strong/b, ul,ol,li, i/em, u, span[style]",
			plugins : 'paste, autoresize',
			autoresize_min_height : 128,
			paste_auto_cleanup_on_paste : true,
			paste_remove_styles : true,
			paste_remove_styles_if_webkit : true,
			paste_strip_class_attributes : true,
		});
	},
	initQ : function(id) { // questions
		tinymce.init({
			// Локализация
			language : getLang(),
			selector : "#" + id,
			toolbar : " underline bold italic | bullist numlist ",
			menubar : false,
			statusbar : false,
			dialog_type : "modal",
			paste_as_text : true,
			plugins : 'paste',
			paste_auto_cleanup_on_paste : true,
			paste_remove_styles : true,
			paste_remove_styles_if_webkit : true,
			paste_strip_class_attributes : true,
			setup : function(ed) {
				ed.on('init', function(e) {
					e.target.selection.select(e.target.getBody(), true);
					e.target.focus();
				});
			}
		});
	},
	initF : function(id) { // forum on editor
		tinymce.init({
			// Локализация
			language : getLang(),
			selector : "#" + id,
			toolbar : " underline bold italic | bullist numlist ",
			// body_id: "body_id",
			// body_class: "bodyClass",
			// content_css: "contentClass",
			height : 150,
			menubar : false,
			statusbar : false,
			dialog_type : "modal",
			paste_as_text : true,
			// Для валидации использовать Jsoup на стороне сервера.
			// valid_elements : "p, strong/b, ul,ol,li, i/em, u, span[style]",
			resize : false,
			plugins : 'paste',
			paste_auto_cleanup_on_paste : true,
			paste_remove_styles : true,
			paste_remove_styles_if_webkit : true,
			paste_strip_class_attributes : true,
			setup : function(ed) {
				ed.on('init', function(e) {
					e.target.selection.select(e.target.getBody(), true);
					e.target.focus();
				})
			}
		});
	},
	// Инициализация всех tinymce редакторов на страницу
	initAll : function() {
		jQuery('textarea.hc-mce').each(function(index, value) {
			Wysiwyg.init(jQuery(value).attr('id'));
		});
	},
	initAllF : function() {
		jQuery('textarea.hc-mce').each(function(index, value) {
			Wysiwyg.initF(jQuery(value).attr('id'));
		});
	},
	// Получение отредактированного текста из редактора
	getContent : function(id) {
		return tinymce.get(id).getContent();
	},
	// Т.к. невозможно просто установить value элементу и тем самым установить
	// стартовое значение, приходится инициализировать элемент таким вот
	// образом.
	setContent : function(id, data) {
		tinymce.get(id).setContent(data);
	}
};

function getLang() {
	var lang = getCookie('org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE');
	if(lang){
		if(lang=='ru_RU') {
			lang = 'ru';
		} else {
			lang = 'en_GB';
		}
	} else {
		lang = 'en_GB';
	}
	return lang;
}

function getCookie(name) {
	var matches = document.cookie
			.match(new RegExp("(?:^|; )"
					+ name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')
					+ "=([^;]*)"));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}
