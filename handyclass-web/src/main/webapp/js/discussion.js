var DiscussionModule = {
	
	// Создание нового объекта обсуждения
	createNewTopic : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxCreateTopic",
			type : "POST",
			data : "",
			cache : false,
			success : function(result) {
				DiscussionModule.activateTopic(result);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateDiscussionListPanel : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxUpdateDiscListPanel",
			type : "POST",
			data : "",
			cache : false,
			success : function(panelContent) {
				$('#discListPanel').replaceWith(panelContent);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	activateTopic : function(oid) {
		var post = {};
		post.discussionOid = oid;
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxActivateTopic",
			type : "POST",
			data : post,
			cache : false,
			success : function(result) {
				DiscussionModule.updateDiscussionListPanel();
				DiscussionModule.updateActiveDiscussionPanel();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	startEditTopic : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxStartEditTopic",
			type : "POST",
			data : "",
			cache : false,
			success : function(result) {
				DiscussionModule.updateDiscussionContent();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	completeEditTopic : function() {
		var post = {};
		post.discussionTopic = $('#editableTopicTextfield').val();
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxCompleteEditTopic",
			type : "POST",
			data : post,
			cache : false,
			success : function(result) {
				DiscussionModule.updateDiscussionContent();
				DiscussionModule.updateDiscussionListPanel();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	cancelEditTopic : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxCancelEditTopic",
			type : "POST",
			data : "",
			cache : false,
			success : function(result) {
				DiscussionModule.updateDiscussionContent();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateActiveDiscussionPanel : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/updateActiveDiscussionPanel",
			type : "POST",
			data : "",
			cache : false,
			success : function(panelContent) {
				$('#actDiscPanel').replaceWith(panelContent);
				Wysiwyg.init('tiny');
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateDiscussionContent : function() {
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/updateDiscussionContent",
			type : "POST",
			data : "",
			cache : false,
			success : function(panelContent) {
				$('#discussionContent').replaceWith(panelContent);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	}
	
}