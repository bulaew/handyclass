var TestPlayer = {
	changeQuestion : function(oid) {
		this.save(function() {
			var data = {};
			data.oid = oid + "";
			var url = Util.getHandyclassPath() + "/main/testplayer/question";
			Util.fireEvent(url, data, function(response) {
				if (response == 'true') {
					TestPlayer.question();
					TestPlayer.paging();
				}
			});
		});
	},
	next : function() {
		this.save(function() {
			var data = {};
			var url = Util.getHandyclassPath() + "/main/testplayer/next";
			Util.fireEvent(url, data, function(response) {
				if (response == 'true') {
					TestPlayer.question();
					TestPlayer.paging();
				}
			});
		});
	},
	prev : function() {
		this.save(function() {
			var data = {};
			var url = Util.getHandyclassPath() + "/main/testplayer/prev";
			Util.fireEvent(url, data, function(response) {
				if (response == 'true') {
					TestPlayer.question();
					TestPlayer.paging();
				}
			});
		});
	},
	last : function() {
		this.save(function() {
			var data = {};
			var url = Util.getHandyclassPath() + "/main/testplayer/last";
			Util.fireEvent(url, data, function(response) {
				if (response == 'true') {
					TestPlayer.question();
					TestPlayer.paging();
				}
			});
		});
	},
	first : function() {
		this.save(function() {
			var data = {};
			var url = Util.getHandyclassPath() + "/main/testplayer/first";
			Util.fireEvent(url, data, function(response) {
				if (response == 'true') {
					TestPlayer.question();
					TestPlayer.paging();
				}
			});
		});
	},
	question : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/testplayer/content";
		Util.fireEvent(url, data, function(response) {
			$('#activeQuestion').hide(200);
			setTimeout(function() {
				$('#activeQuestion').replaceWith(response);
				$('#activeQuestion').show(200);
				var height = $('#questionContainer').height() - 100;
				$('.right-paging, .left-paging').animate({
					'height' : height,
					'line-height' : height + 'px'
				}, 200);
			}, 200);
		});
	},
	paging : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/testplayer/paging";
		Util.fireEvent(url, data, function(response) {
			$('#testQuestions').replaceWith(response);
		});
	},
	save : function(callback) {
		var data = {};
		data.answers = this.getAnswersData();
		data.oid = $('#activeQuestion').attr('data-oid');
		var url = Util.getHandyclassPath() + "/main/testplayer/save";
		Util.jsonFireEvent(url, data, function(response) {
			if (response == 'true') {
				callback();
			}
		});
	},
	getAnswersData : function() {
		var answerContainer = $('.hc-answer-container');
		var type = answerContainer.attr('data-type');
		if (type == 'single' || type == 'alternative' || type == 'multiple') {
			return this.gatherSingleData();
		} else if (type == 'sequence') {
			return this.gatherSequenceData();
		} else if (type == 'open') {
			return this.gatherOpenData();
		} else if (type == 'association') {
			return this.gatherAssociationData();
		}
		return answers;
	},
	gatherSingleData : function() {
		var answers = new Array();
		$(".hc-answer-container").find('li').each(function(index, value) {
			var answer = {};
			answer.oid = $(value).attr('id');
			answer.value = $(value).find('input').is(':checked');
			answers.push(answer);
		});
		return answers;
	},
	gatherSequenceData : function() {
		var answers = new Array();
		$(".hc-answer-container").find('li').each(function(index, value) {
			var answer = {};
			answer.oid = $(value).attr('id');
			answer.value = index + 1;
			answers.push(answer);
		});
		return answers;
	},
	gatherOpenData : function() {
		var answers = new Array();
		$(".hc-answer-container").find('li').each(function(index, value) {
			var answer = {};
			answer.oid = $(value).attr('id');
			answer.value = $(value).find('input').val();
			answers.push(answer);
		});
		return answers;
	},
	gatherAssociationData : function() {
		var answers = new Array();
		$(".hc-answer-container ").find('.titles-column').find('li').each(
				function(index, value) {
					var answer = {};
					answer.oid = $(value).attr('id');
					answer.value = $(value).find('.answerValue').html();
					answers.push(answer);
				});
		return answers;
	},
	initAssociation : function() {
		jQuery('.q-sequence.enabled.player').draggable({
			revert : "invalid",
			containment : "document",
		});
		jQuery('.q-sequence.place').each(function(i) {
			if (jQuery(this).children().length == 0) {
				TestPlayer.enableDroppablePlace(jQuery(this));
			}
		});
		jQuery('.hc-answer-container .answers.col-md-4').find('li').each(
				function(i) {
					if (jQuery(this).children().length == 0) {
						TestPlayer.enableDroppablePlace(jQuery(this));
					}
				});
	},
	enableDroppablePlace : function(item) {
		item.droppable({
			drop : function(event, ui) {
				var item = ui.draggable;
				var oldParent = item.parent();
				item.appendTo(this);
				item.removeAttr('style');
				item.attr("style", "position: relative;");
				jQuery(this).droppable('destroy');
				TestPlayer.enableDroppablePlace(oldParent);
				if (oldParent.is('li')) {
					oldParent.appendTo(oldParent.parent());
				}
			}
		});
	},
	initSequence : function() {
		jQuery('.q-sequence').closest('ul').sortable();
	},
	finishTest : function() {
		this.save(function() {
			var data = {};
			var url = Util.getHandyclassPath() + "/main/testplayer/calculate";
			Util.fireEvent(url, data, function(response) {
				var data = JSON.parse(response);
				if (data.value == 'true') {
					Util.goTo('main/testresult/' + data.oid);
				}
			});
		});
	},
};
$(document).ready(function() {
	var height = $('#questionContainer').height() - 100;
	$('.right-paging, .left-paging').css({
		'height' : height,
		'line-height' : height + 'px'
	});
});