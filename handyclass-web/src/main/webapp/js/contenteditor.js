(function() {

	function AddressBookModel() {
		var obj = {};
		/**
		 * все списки контактов пользователя.
		 */
		obj._lists = [];
		/**
		 * список всех пользователей и адресов, которым данный пользователь
		 * когда-либо отправлял приглашения.
		 */
		obj._totalList;
		/**
		 * Отображается список всех пользователей и адресов, выбранных при
		 * помощи разных фильтров.
		 */
		obj._checkedList;
		/**
		 * список адресов незарегистрированных пользователей, которым данный
		 * пользователь когда-либо отправлял приглашения.
		 */
		obj._unregList;
		obj._contactFilterValue = '';
		obj._additionalParam = '';
		/**
		 * 
		 */
		obj.__i18nAddBtn = 'Add';
		obj.__user = 'User';
		obj.__users = 'Users';
		obj.__registered = 'Selected';
		obj.__notRegistered = 'Not registered';
		return obj;
	}
	;

	if (handy)
		handy.addressBookModel = new AddressBookModel();
})();

var AddressBookModule = {

	ContactList : function(ID, title) {
		var obj = {};
		obj._ID = ID;
		obj._title = title;
		obj._active = false;
		obj._contacts = [];
		return obj;
	},

	Contact : function() {
		var obj = {};
		obj._contactID = '';
		obj._parentID = '';
		obj._state = 'unchecked';
		obj._avatarURL = '';
		obj._regUser = false;
		obj._name = 'name';
		obj._email = 'email';
		return obj;
	},
	/**
	 * Строковые константы для интернационализаии.
	 * 
	 * @returns
	 */
	i18nBook : function(i18nData) {
		if (i18nData == null) {
			return;
		}
		handy.addressBookModel.__i18nAddBtn = i18nData.addBtnTitle;
	},

	/**
	 * clData : Данные списков контактов. totalData список контактов когда либо
	 * приглашенных
	 */
	initBook : function(clData, totalData, additionalParam) {
		// Загрузка списков.
		handy.addressBookModel._lists = [];
		for (i in clData) {
			contactList = AddressBookModule.loadContactList(clData[i][0],
					clData[i][1], clData[i][2]);
			handy.addressBookModel._lists.push(contactList);
		}
		// Загрузка когда либо приглашенных.
		handy.addressBookModel._totalList = AddressBookModule.loadContactList(
				totalData[0], totalData[1], totalData[2]);
		// Загрузка выбранных
		handy.addressBookModel._checkedList = new AddressBookModule.ContactList(
				'checkedlist');
		var locale = getCookie('handy_i18n_lang');
		if (locale == 'ru') {
			handy.addressBookModel._checkedList._title = "Только выбранные";
			// Загрузка незарегистрированных.
		} else {
			handy.addressBookModel._checkedList._title = "Selected";
		}
		handy.addressBookModel._unregList = new AddressBookModule.ContactList(
				'unreglist');
		if (locale == 'ru') {
			handy.addressBookModel._unregList._title = "Незарегистрированные";
			// Загрузка незарегистрированных.
		} else {
			handy.addressBookModel._unregList._title = "Not registered";
		}
		// сбрасываем фильтр контактов.
		handy.addressBookModel._contactFilterValue = '';
		handy.addressBookModel._additionalParam = additionalParam;
		for (i in handy.addressBookModel._totalList._contacts) {
			var contact = handy.addressBookModel._totalList._contacts[i];
			if (contact._regUser == false) {
				handy.addressBookModel._unregList._contacts.push(contact);
			}
		}
		// после загрузки данных, активируем первый список.
		if (handy.addressBookModel._lists.length > 0) {
			handy.addressBookModel._lists[0]._active = true;
		} else {
			handy.addressBookModel._totalList._active = true;
		}

		AddressBookModule.renderAddressBook();
		AddressBookModule.initComboboxes('listFilter');
	},

	initComboboxes : function(id) {

		jQuery('#' + id + '_select').data('disabled', false);

		jQuery('#' + id).css({
			'position' : 'absolute',
			'left' : '0',
			'bottom' : '0',
			'width' : '100%',
			'opacity' : 0
		});

		jQuery('#' + id + '_input').css({
			'border' : 0,
			'width' : '100%'
		}).val(jQuery('#' + id).find('option:selected').html());

		jQuery('#' + id).change(function(event) {
			var self = jQuery(this);
			var input = jQuery('#' + id + '_input').eq(0);
			var disabled = self.data('disabled');
			if (disabled)
				return false;
			input.val(self.find('option:selected').html());
			AddressBookModule.onListFilterChange();
		});
	},

	loadContactList : function(listID, listTitle, contactsData) {
		var contactList = new AddressBookModule.ContactList(listID, listTitle);
		contactList._contacts = AddressBookModule.loadContacts(listID,
				contactsData);
		return contactList;
	},

	loadContacts : function(parentID, contactsData) {
		var contacts = [];
		for (i in contactsData) {
			var contact = new AddressBookModule.Contact();
			contact._contactID = contactsData[i][0];
			contact._parentID = parentID;
			contact._state = 'unchecked'; // может быть 'checked' /
			// 'unchecked';
			contact._avatarURL = contactsData[i][1];
			contact._name = contactsData[i][2];
			contact._email = contactsData[i][3];
			contact._regUser = contactsData[i][4];
			contacts.push(contact);
		}
		return contacts;
	},

	getContactList : function(id) {
		if (id == 'totallist') {
			return handy.addressBookModel._totalList;
		}

		if (id == 'checkedlist') {
			return handy.addressBookModel._checkedList;
		}

		if (id == 'unreglist') {
			return handy.addressBookModel._unregList;
		}

		for (i in handy.addressBookModel._lists) {
			contactList = handy.addressBookModel._lists[i];
			if (contactList._ID == id) {
				return contactList;
			}
		}
	},

	getActiveContactList : function() {
		for (i in handy.addressBookModel._lists) {
			contactList = handy.addressBookModel._lists[i];
			if (contactList._active == true) {
				return contactList;
			}
		}
		if (handy.addressBookModel._totalList._active == true) {
			return handy.addressBookModel._totalList;
		}
		if (handy.addressBookModel._checkedList._active == true) {
			return handy.addressBookModel._checkedList;
		}
		if (handy.addressBookModel._unregList._active == true) {
			return handy.addressBookModel._unregList;
		}

		return null;
	},

	/**
	 * Снять/установить галочку на контакте.
	 */
	checkContact : function(coid, isChecked) {
		// проходим по всем пользовательским спискам.
		for (i in handy.addressBookModel._lists) {
			contactList = handy.addressBookModel._lists[i];
			AddressBookModule.checkContactInList(contactList, coid, isChecked);
		}
		// проходим по списку "Все".
		contact = AddressBookModule.checkContactInList(
				handy.addressBookModel._totalList, coid, isChecked);
		// добавляем/удаялем контакт в списоке выбранных.
		if (contact == null) {
			return;
		}
		/*
		 * Обработка добавления/удаления в список выбранных.
		 */
		if (isChecked == true) {
			// проверяем нужно ли добавление:
			var bExist = false;
			for (i in handy.addressBookModel._checkedList._contacts) {
				checklistContact = handy.addressBookModel._checkedList._contacts[i];
				if (checklistContact._contactID == coid) {
					bExist = true;
					break;
				}
			}
			if (!bExist) {
				handy.addressBookModel._checkedList._contacts.push(contact);
			}
		} else {
			for (i in handy.addressBookModel._checkedList._contacts) {
				contact = handy.addressBookModel._checkedList._contacts[i];
				if (contact._contactID == coid) {
					handy.addressBookModel._checkedList._contacts.splice(i, 1);
					return;
				}
			}
		}
	},

	/**
	 * Снять/установить галочку на контакте списка.
	 */
	checkContactInList : function(contactList, coid, isChecked) {
		for (i in contactList._contacts) {
			contact = contactList._contacts[i];
			if (contact._contactID == coid) {
				if (isChecked == true) {
					contact._state = 'checked';
				} else {
					contact._state = 'unchecked';
				}
				return contact;
			}
		}
	},

	/**
	 * Снять/установить галочку на контакте списка.
	 */
	checkAllContacts : function(contactList, isChecked, usingFilter) {
		for (i in contactList._contacts) {
			contact = contactList._contacts[i];
			if (usingFilter != null && usingFilter == true) {
				// Проверка на фильтр.
				if (handy.addressBookModel._contactFilterValue != null
						&& handy.addressBookModel._contactFilterValue != '') {
					if (contact._name
							.indexOf(handy.addressBookModel._contactFilterValue) == -1
							&& contact._email
									.indexOf(handy.addressBookModel._contactFilterValue) == -1) {
						continue;
					}
				}
			}
			AddressBookModule.checkContact(contact._contactID, isChecked);
		}
	},

	getCountCheckedContactsInList : function(contactList) {
		if (contactList == null) {
			return 0;
		}

		var checkedCount = 0;
		for (i in contactList._contacts) {
			contact = contactList._contacts[i];
			if (contact._state == 'checked') {
				checkedCount++;
			}
		}
		return checkedCount;
	},

	findContact : function(parentID, contactID) {
		if (parentID == null || contactID == null) {
			return null;
		}
		contactList = AddressBookModule.getContactList(parentID);
		for (i in contactList._contacts) {
			contact = contactList._contacts[i];
			if (contact._contactID == contactID) {
				return contact;
			}
		}
		return null;
	},

	renderAddressBook : function() {
		var body = jQuery('#addressBook .modal-body');
		AddressBookModule.renderFiltersPanel();
		AddressBookModule.renderButtonsPanel();
		AddressBookModule.renderListContent(AddressBookModule
				.getActiveContactList());
	},

	renderFiltersPanel : function() {
		listFilter = jQuery('#listFilter');
		listFilter.find('option').remove();

		listFilter.append(AddressBookModule
				.renderListFilterOption(handy.addressBookModel._totalList));

		listFilter.append(AddressBookModule
				.renderListFilterOption(handy.addressBookModel._checkedList));

		listFilter.append(AddressBookModule
				.renderListFilterOption(handy.addressBookModel._unregList));

		for (i in handy.addressBookModel._lists) {
			listFilter.append(AddressBookModule
					.renderListFilterOption(handy.addressBookModel._lists[i]));

		}

		AddressBookModule.renderTotalCheckBtn(AddressBookModule
				.getActiveContactList());
	},

	// Создание/перерисовка элемента дропбокса.
	renderListFilterOption : function(contactList) {
		var option = jQuery("#listFilter option[loid=" + contactList._ID + "]");
		if (option.length == 0) {
			option = jQuery('<option/>');
			option.attr('loid', contactList._ID);
		}
		if (contactList._active == true) {
			option.attr('selected', 'selected');
		}
		option.html(contactList._title + ' : ' + contactList._contacts.length);
		return option;
	},

	/**
	 * Перерисовка содержимого списка контактов.
	 */
	renderListContent : function(contactList) {
		if (contactList == null) {
			return;
		}
		var listContentPanel = jQuery('#listContent');
		listContentPanel.html('');
		for (i in contactList._contacts) {
			var contact = contactList._contacts[i];
			// Проверка на фильтр.
			if (handy.addressBookModel._contactFilterValue != null
					&& handy.addressBookModel._contactFilterValue != '') {
				if (contact._name
						.indexOf(handy.addressBookModel._contactFilterValue) == -1
						&& contact._email
								.indexOf(handy.addressBookModel._contactFilterValue) == -1) {
					continue;
				}
			}
			listContentPanel.append(AddressBookModule.renderContact(contact));
		}
	},

	renderContact : function(contact) {
		contactElem = jQuery('<div/>').addClass('col-md-6').addClass(
				'hc-member-h dlg');

		// чекбокс
		var checkbox = jQuery('<input>', {
			type : 'checkbox'
		});
		if (contact._state == 'checked') {
			checkbox.attr('checked', 'checked');
		}
		checkbox.css({
			'margin-top' : '9px',
			'float' : 'left',
			'margin-right' : '9px'
		});
		checkbox.attr('loid', contact._parentID); // list object id.
		checkbox.attr('coid', contact._contactID); // contact object id.
		checkbox.change(function(event) {
			AddressBookModule.onCheckContact(jQuery(this));
		});
		checkbox.appendTo(contactElem);

		// аватарка
		avatarDiv = jQuery('<div/>');
		avatarDiv.addClass('address-avatar').appendTo(contactElem);

		avatarImg = jQuery('<img/>', {
			src : contact._avatarURL
		});
		avatarImg.appendTo(avatarDiv);
		// имя и емейл
		userAnchor = jQuery('<a/>');
		userAnchor.css({
			'height' : '32px',
			'line-height' : '16px',
			'padding-left' : '0px'
		});
		contactElem.append(userAnchor);
		if (contact._name.length != 0) {
			jQuery('<label/>').html(contact._name).appendTo(userAnchor);
			jQuery('<br>').appendTo(userAnchor);
		} else {
			userAnchor.css('line-height', '32px');
		}
		jQuery('<label/>').css({
			'color' : '#999999'
		}).html(contact._email).appendTo(userAnchor);
		return contactElem;
	},

	renderButtonsPanel : function() {
		buttonsPanel = jQuery('#buttonsPanel');
		buttonsPanel.html('');
		buttonsPanel
				.append('<a class="hc-btn-primary" data-dismiss="modal" onclick="AddressBookModule.onAddButtonClick();">'
						+ handy.addressBookModel.__i18nAddBtn + '</a>');
		buttonsPanel.append('<label id="checkedCounter"/>');
		AddressBookModule.renderCounterCheckedContacts();
	},

	renderCounterCheckedContacts : function() {
		checkedCounter = jQuery('#checkedCounter');
		if (checkedCounter == null) {
			return;
		}
		var locale = getCookie('handy_i18n_lang');
		var users = handy.addressBookModel._checkedList._contacts.length;
		var usersLang;
		if (locale == 'ru') {
			var usersString = users.toString();
			var n = users.toString().length;
			if ((usersString[n - 1] == 1 && (!usersString[n - 2] || usersString[n - 2] == 0))) {
				usersLang = 'пользователь';
			} else if (((usersString[n - 1] == 2 || usersString[n - 1] == 3 || usersString[n - 1] == 4) && (!usersString[n - 2] || usersString[n - 2] == 0))) {
				usersLang = 'пользователя';
			} else {
				usersLang = 'пользователей';
			}
		} else {
			var usersString = users.toString();
			var n = users.toString().length;
			if ((usersString[n - 1] == 1 && (!usersString[n - 2] || usersString[n - 2] == 0))) {
				usersLang = 'user';
			} else {
				usersLang = 'users';
			}
		}
		checkedCounter.html(users + " " + usersLang);
	},

	renderTotalCheckBtn : function(selectedContactList) {
		totalCheckbox = jQuery('#totalCheckbox');
		if (totalCheckbox == null) {
			return;
		}
		var checkedCount = AddressBookModule
				.getCountCheckedContactsInList(selectedContactList);
		var state;
		totalCheckbox.removeClass();
		if (checkedCount == 0) {
			state = 'unchecked';
			totalCheckbox.prop("checked", false);
		}
		if (checkedCount > 0
				&& checkedCount < selectedContactList._contacts.length) {
			state = 'halfchecked';
			totalCheckbox.prop("checked", true);
		}
		if (checkedCount > 0
				&& checkedCount == selectedContactList._contacts.length) {
			state = 'fullchecked';
			totalCheckbox.prop("checked", true);
		}
		totalCheckbox.addClass(state);

	},

	onAddButtonClick : function() {
		var url = handy.createPostUrl('ajaxSubmitContacts'); // ИД аякс
		// запроса
		var post = handy.createPostData(); // json объект с данными для сервера
		for (i in handy.addressBookModel._checkedList._contacts) {
			contact = handy.addressBookModel._checkedList._contacts[i];
			post[i] = contact._contactID;
		}
		var wrappost = handy.createPostData(); // json объект с данными для
		// сервера
		wrappost = {
			'datasize' : handy.addressBookModel._checkedList._contacts.length,
			'data' : post,
			'additionalParam' : handy.addressBookModel._additionalParam
		}

		handy.fireEvent(url, wrappost, function(response) { // отправка запроса
			var data = handy.getDataFromResponse(response); // обработка ответа
			if (data.param_groupContent != null) {
				jQuery('#groupContentPanel').replaceWith(
						data.param_groupContent); // перерисовываем контент
				// группы.
				setTimeout(GroupInfoModule.loadGroupInfo(data.group_oid, true),
						3000);
			}
			if (data.hidingContent != null) {
				jQuery('#ParticipantEditCourseActionaccessPanelID')
						.replaceWith(data.hidingContent); // перерисовываем
				// участников.
			}
			if (data.userContent != null) {
				jQuery('#ParticipantEditCourseActionuserPanelID').replaceWith(
						data.userContent);
			}
		});

	},

	onListFilterChange : function() {
		var contactListID = jQuery('#listFilter').find('option:selected').attr(
				'loid');
		// активируем новый список.
		currentActiveList = AddressBookModule.getActiveContactList();
		if (currentActiveList != null) {
			currentActiveList._active = false;
		}
		contactList = AddressBookModule.getContactList(contactListID);
		contactList._active = true;
		// сброс фильтра
		handy.addressBookModel._contactFilterValue = '';
		jQuery('#contactFilter').val('');
		// перерисовка содержимого списка.
		AddressBookModule.renderListContent(contactList);
		// перерисовка кнопки "выделить всё"
		AddressBookModule.renderTotalCheckBtn(contactList);

	},
	onCheckContact : function(checkbox) {
		if (checkbox) {
			var coid = parseInt(checkbox.attr('coid'));
			var loid = parseInt(checkbox.attr('loid'));
			var checked = checkbox.prop("checked");

			// если поставили галочку, то нужно поставить галочку на этом
			// контакте во всех списках.
			AddressBookModule.checkContact(coid, checked);
			// перерисовываем элемент дропбокса со списком выбранных контактов.
			AddressBookModule
					.renderListFilterOption(handy.addressBookModel._checkedList);
			// перерисовать счетчик
			AddressBookModule.renderCounterCheckedContacts();
			// перерисовка кнопки "выделить всё"
			activeContactlist = AddressBookModule.getActiveContactList();
			AddressBookModule.renderTotalCheckBtn(activeContactlist);

			if (activeContactlist._ID == 'checkedlist') {
				jQuery("#listFilter").val('checkedlist');
				// AddressBookModule.renderListContent(activeContactlist);
			}

		}
	},

	onTotalCheckbox : function(checkbox) {
		if (checkbox) {
			contactlist = AddressBookModule.getActiveContactList();
			var checkedCount = AddressBookModule
					.getCountCheckedContactsInList(contactlist);
			if (!checkbox.prop("checked")
					&& checkedCount < contactlist._contacts.length) {
				checkbox.prop("checked", true);
			}
			AddressBookModule.checkAllContacts(contactlist, checkbox
					.prop("checked"), true);
			// перерисовываем элемент дропбокса со списком выбранных контактов.
			AddressBookModule
					.renderListFilterOption(handy.addressBookModel._checkedList);
			// перерисовать счетчик
			AddressBookModule.renderCounterCheckedContacts();
			AddressBookModule.renderListContent(contactlist);
		}

	},

	onContactFilterChange : function(event) {
		if (!event)
			var event = window.event;
		if (event.target) {
			handy.addressBookModel._contactFilterValue = jQuery(event.target)
					.val();
			AddressBookModule.renderListContent(AddressBookModule
					.getActiveContactList());
		}
	}

}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i].trim();
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}