jQuery(function() {
	TestEditor.checkEmpty();
	function startDrag() {
		var area = jQuery('#questions_area');
		var height = area.height();
		area.addClass('active');
		area.css("height", (height + 300));
	}
	;
	function stopDrag() {
		var area = jQuery('#questions_area');
		area.removeClass('active');
		area.css("height", "auto");
	}
	;
	TestEditor.applySortComponents();
			$('#single').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q1'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			$('#multiple').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q2'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			$('#alternative').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q3'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			$('#open').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q4'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			$('#sequence').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q5'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			$('#association').draggable({
				helper : function(event) {
					return jQuery("<div class='draggable-question q6'></div>");
				},
				start : function(event, ui) {
					startDrag();
				},
				stop : function(event, ui) {
					stopDrag();
				}
			}),

			jQuery('#questions_area')
					.droppable(
							{
								accept : "#single, #multiple, #alternative, #open, #association, #sequence",
								drop : function(event, ui) {
									var type = ui.draggable.context.id;
									TestEditor.addQuestion(type);
								}
							});
});

var TestEditor = {
	checkEmpty: function() {
		if($('#id_questions_ul>li').size()>0){
			$('#emptyqmessage').hide(250);
		} else {
			$('#emptyqmessage').show(250);	
		}
	},
	addQuestion : function(type) {
		var data = {};
		data.type = type;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/question/add",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#id_questions_ul').append(messages);
				TestEditor.applySortComponents();
				TestEditor.checkEmpty();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	deleteQuestion : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/question/delete",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				var options = {};
				$('#' + messages).find('item-menu').remove();
				$('#'+messages).css('width','780px')
				$('#' + messages).effect('blind', options, 200, function() {
					$('#' + messages).remove();
					TestEditor.applySortComponents();
				});
				TestEditor.checkEmpty();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	editQuestion : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/question/edit",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
				TestEditor.applySortComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	cancelQuestion : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/question/cancel",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
				TestEditor.applySortComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	save : function(oid) {
		if ($('#answers' + oid + '>li.active').size() > 0) {
			TestEditor.saveAnswers(oid);
		} else {
			TestEditor.saveQuestion(oid);
			TestEditor.applySortComponents();
		}
	},
	saveQuestion : function(oid) {
		var data = TestEditor.getSaveData(oid);
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/test/question/save";
		Util.jsonFireEvent(url, data, function(response) {
			$('#' + oid).replaceWith(response);
			TestEditor.applySortComponents();
		});
	},
	saveAnswers : function(oid) {
		var data = TestEditor.getSaveData(oid);
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/test/answer/save";
		Util.jsonFireEvent(url, data, function(response) {
			$('#' + oid).replaceWith(response);
		});
	},
	getSaveData : function(oid) {
		var data = {};
		data.oid = oid;
		data.question = Wysiwyg.getContent('text' + oid);
		var type = $('#' + oid).attr('data-type');
		data.answers = TestEditor.getAnswersData(type, oid);
		return data;
	},
	// "#single, #multiple, #alternative, #open, #association, #sequence",
	getAnswersData : function(type, oid) {
		var answers = new Array();
		$('#' + oid + '>.answers>li').each(
				function(index, value) {
					var answer = {};
					if (type == 'single') {
						answer.oid = $(value).attr('id');
						if ($(value).hasClass('active')) {
							answer.text = Wysiwyg.getContent('text'
									+ answer.oid);
						} else {
							answer.value = $('#radiobutton' + answer.oid).is(
									":checked");
						}
						answers.push(answer);
					} else if (type == 'multiple') {
						answer.oid = $(value).attr('id');
						if ($(value).hasClass('active')) {
							answer.text = Wysiwyg.getContent('text'
									+ answer.oid);
						} else {
							answer.value = $('#checkbox' + answer.oid).is(
									":checked");
						}
						answers.push(answer);
					} else if (type == 'alternative') {
						answer.oid = $(value).attr('id');
						answer.value = $('#radiobutton' + answer.oid).is(
								":checked");
						answers.push(answer);
					} else if (type == 'open') {
						answer.oid = $(value).attr('id');
						if ($(value).hasClass('active')) {
							answer.text = $('#text' + answer.oid).val();
						}
						answers.push(answer);
					} else if (type == 'association') {
						answer.oid = $(value).attr('id');
						if ($(value).hasClass('active')) {
							answer.text = Wysiwyg.getContent('text'
									+ answer.oid);
							answer.value = Wysiwyg.getContent('value'
									+ answer.oid);
						}
						answers.push(answer);
					} else if (type == 'sequence') {
						answer.oid = $(value).attr('id');
						if ($(value).hasClass('active')) {
							answer.text = Wysiwyg.getContent('text'
									+ answer.oid);
						}
						answer.value = index + 1;
						answers.push(answer);
					}
				});
		return answers;
	},
	answerUp : function(oid) {
		var index = $('#' + oid).index();
		if ((index - 1) >= 0) {
			$('#' + oid).after($('#' + oid).parent().children().get(index - 1));
			$('#' + oid).closest('ul').find('.text.three').each(function(index, value){
				$(value).html((index+1) + '.');
			})
		}
	},
	answerDown : function(oid) {
		var index = $('#' + oid).index();
		var size = $('#' + oid).parent().children().size();
		if ((index + 1) < size) {
			$('#' + oid)
					.before($('#' + oid).parent().children().get(index + 1));
			$('#' + oid).closest('ul').find('.text.three').each(function(index, value){
				$(value).html((index+1) + '.');
			})
		}
	},
	initMove : function(oid) {
		if ($('#' + oid).parent().find('li:first-of-type').attr('id') == oid) {
			$(this).find('.top').remove();
		}
		;
		if ($('#' + oid).parent().find('li:last-of-type').attr('id') == oid) {
			$(this).find('.bottom').remove();
		}
		;
	},
	toogleTest : function() {
		$.ajax({
			url : Util.getHandyclassPath() + "/main/editcourse/test/toogle",
			type : "POST",
			cache : false,
			success : function(messages) {
				if (messages == 'ok') {
					$('.test-toogle-container').toggleClass("on");
				}
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	changePassingScore : function() {
		var data = {};
		data.score = $('#slider').val();
		Util.fireEvent(
				Util.getHandyclassPath() + "/main/editcourse/test/score", data,
				function() {
				});
	},
	initComponent : function(oid) {
		jQuery("#" + oid).click(
				function(e) {
					e.stopPropagation();
					var x = e.offsetX == undefined ? e.originalEvent.layerX
							: e.offsetX;
					var y = e.offsetY == undefined ? e.originalEvent.layerY
							: e.offsetY;
					jQuery("#ui" + oid).css({
						'top' : y,
						'left' : x
					});
					jQuery("#ui" + oid).addClass('on');
					jQuery("#ui" + oid).show();
					jQuery(this).addClass('active');
				}).mouseleave(function() {
			jQuery("#ui" + oid).hide();
			jQuery(this).removeClass('active');
		}).mouseover(function() {
			jQuery(this).addClass('active');
		});
	},
	addAnswer : function(oid) {
		var data = {};
		data.oid = oid;
		$
				.ajax({
					url : Util.getHandyclassPath()
							+ "/main/editcourse/test/answer/add",
					type : "POST",
					data : data,
					cache : false,
					success : function(messages) {
						$('#answers' + oid).append(messages);
					},
					error : function(xhr) {
						if (xhr.statusText != "abort" && xhr.status != 503) {
							console.error("Error!");
						}
					}
				});
	},
	deleteAnswer : function(oid) {
		var data = {};
		data.oid = oid;
		data.parentoid = $('#' + oid).closest('.hc-question-active').attr('id');
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/answer/delete",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				var options = {};
				$('#' + messages).effect('blind', options, 200, function() {
					$('#' + messages).remove();
				});
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	editAnswer : function(oid) {
		var data = {};
		data.oid = oid;
		data.parentoid = $('#' + oid).closest('.hc-question-active').attr('id');
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/answer/edit",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	cancelAnswer : function(oid) {
		var data = {};
		data.oid = oid;
		data.parentoid = $('#' + oid).closest('.hc-question-active').attr('id');
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/test/answer/cancel",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},

	// TODO перемещение вопросов.
	applySortComponents : function() {
		if (jQuery('#id_questions_ul').children()
				.hasClass('hc-question-active')) {
			jQuery('#id_questions_ul').sortable({
				disabled : true
			});
			return;
		}
		jQuery('#id_questions_ul').sortable({
			disabled : false,
			cursor : "move",
			scroll : true,
			update : function(event, ui) {
				TestEditor.move(event, ui);
			}
		});
	},
	move : function(event, ui) {
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/test/question/move";
		var data = {};
		data.position = jQuery('#' + ui.item.context.id).index(
				"#id_questions_ul>li");
		// data.prevPosition = ui.item.context.dataset.position;
		data.oid = ui.item.context.id;
		Util.fireEvent(url, data, function(response) {

		});
	},

}