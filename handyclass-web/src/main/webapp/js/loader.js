var Loader = {
	create : function(selector) {
		$(selector).html(this.loader());
	},
	remove : function() {
		$('#loader').remove();
	},
	loader : function() {
		return $("<div/>").attr('id', 'loader').addClass('loader')
				.append($('<div/>').addClass('dot1'))
				.append($('<div/>').addClass('dot2'));
	}
};