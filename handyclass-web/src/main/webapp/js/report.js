var Chart = {
	data : function(selector) {
		var array = new Array();
		var size = $(selector).size();
		$(selector).each(function data(index, value) {
			var item = new Array();
			item.push($(value).text());
			item.push(size - index - 1);
			array.push(item);
		});
		return array;

	},
	viewsData : function() {
		return this.data('.td.view');
	},
	likesData : function() {
		return this.data('.td.like');
	},
	widthAndHeight : function() {
		var height = $('.table.report tbody').height()
				+ $('.table.report thead').height();
		var width = $('#report').parent().width();
		$('#report').css('height', height);
		$('#report').css('width', width);
	},
	simpleReport : function() {
		var data = this.data('.td.report-data');
		this.widthAndHeight();
		this.simpleBarChart('#report', [ data ]);
	},
	activityAuthorsChart : function() {
		var data = this.data('.td.report-data');
		this.widthAndHeight();
		this.simpleBarChart('#report', [ data ]);
	},
	ratingChart : function() {
		this.widthAndHeight();
		var b = Chart.viewsData();
		var a = Chart.likesData();
		Chart.ratingViewsChart('#report', [ a, b ]);
	},
	ratingViewsChart : function(id, lines) {
		var height = $('.table.report thead').height();
		jQuery.plot(id, lines, {
			bars : {
				show : true,
				align : "center",
				horizontal : true,
				barWidth : 0.4,
				fill : true,
				order : 1,
				lineWidth : 0,
			},
			xaxis : {
				tickDecimals : 0,
				minTickSize : 1,
				show : true,
				position : "top",
				labelHeight : height,
				labelWidth : 30
			},
			yaxis : {
				show : false,
			},
			grid : {
				borderWidth : 0,
			},
			colors : [ "#c8503c", "#3C3C5A" ],
		});
		this.formatLabels();
	},
	formatLabels : function() {
		$('.flot-tick-label.tickLabel').css({
			'line-height' : '16px',
			'color' : '#b4b4be',
			'font-size' : '14px'
		});
		// $('.flot-tick-label.tickLabel').each(function(index,value){
		// var text = $(value).html().split('.')[0];
		// $(value).html(text);
		// });
	},
	simpleBarChart : function(id, lines) {
		var height = $('.table.report thead').height();
		jQuery.plot(id, lines, {
			bars : {
				show : true,
				align : "center",
				horizontal : true,
				barWidth : 0.7,
				fill : true,
				order : 1,
				lineWidth : 0,
			},
			xaxis : {
				tickDecimals : 0,
				minTickSize : 1,
				show : true,
				position : "top",
				labelHeight : height,
				labelWidth : 30
			},
			yaxis : {
				show : false,
			},
			grid : {
				borderWidth : 0,
			},
			colors : [ "#c8503c" ],
		});
		this.formatLabels();
	},
	activityChart : function(id, legend_id, lines) {
		jQuery.plot(id, lines, {
			lines : {
				show : true,
			},
			xaxis : {
				show : true,
				position : "bottom",
				mode : "time",
				timeformat : "%b",
				monthNames : [ "янв", "фев", "мар", "апр", "май", "июн", "июл",
						"авг", "сен", "окт", "ноя", "дек" ],
			},
			yaxis : {
				show : true,
			},
			grid : {
				borderWidth : 0,
			},
			colors : [ "#c8503c", "#3C3C5A" ],
			legend : {
				container : legend_id,
			}
		});
		legend_id.find('.legendColorBox').first('div').css({
			'border' : 'none',
			'padding' : '0'
		}).first('div').css({
			'border-left' : '8',
			'border-right' : '8',
			'border-top' : '2',
			'border-bottom' : '2'
		});
	},
};

function ReportViewModel() {
	var obj = {};
	obj.controller = '';
	obj.reportType = '';
	// ID категории
	obj.categoryFilter = '';
	// Статус прохождения курса
	obj.statusFilter = '';
	// Регистраций/Просмотров/Того и другого
	obj.userActivityFilter = '';
	// Параметр поиска
	obj.search = '';
	// Сортировка по колонке (смотреть в классе AuthorActivityItem).
	obj.sort = '';
	// Направление сортировки ASC и DSC
	obj.direction = '';
	// Параметры периода поиска
	obj.startDate = '';
	obj.endDate = '';
	return obj;
};

var Report = {
	getViewModel : function(reportType) {
		if (reportType == "MY_PROPENSITY") {
			return handy.myPropensityVM;
		}
		if (reportType == "MY_EDUCATION") {
			return handy.myEducationVM;
		}
		if (reportType == "MY_ACHIEVEMENT") {
			return handy.myAchievementsVM;
		}
		if (reportType == "MY_COURSES_LEARNING") {
			return handy.myCoursesLearningVM;
		}
		if (reportType == "MY_COURSE_RATING") {
			return handy.myCourseRatingVM;
		}
		if (reportType == "COURSE_RATING") {
			return handy.allCourseRatingVM;
		}
		return null;
	},

	loadReports : function() {
		// рейтинг всех курсов
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/courseRating/loadBillboard", {}, function(content) {
			$("#OverallCourseRating").replaceWith(content);
		});
		// рейтинг моих курсов
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/myCourseRating/loadBillboard", {},
				function(content) {
					$("#MyCourseRating").replaceWith(content);
				});
		// обучение по моим курсам
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/myCourseLearning/loadBillboard", {}, function(
				content) {
			$("#MyCourseLearning").replaceWith(content);
		});
		// Моё обучение
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/myEducation/loadBillboard", {}, function(content) {
			$("#MyEducation").replaceWith(content);
		});
		// Мои успехи
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/myAchievements/loadBillboard", {},
				function(content) {
					$("#MyAchievements").replaceWith(content);
				});
		// Мои наклонности
		Util.fireEvent(Util.getHandyclassPath()
				+ "/reports/myPropensity/loadBillboard", {}, function(content) {
			$("#MyPropensity").replaceWith(content);
		});

	},

	renderReport : function(jObj) {
		Util.jsonFireEvent(Util.getHandyclassPath()
				+ handy.reportViewModel.controller + "/renderReport",
				handy.reportViewModel, function(content) {
					container = jObj.attr('data-report-container');
					$("#" + container).replaceWith(content);
					jQuery('label').tooltip();
					eval(jQuery('.chart-container').attr('data-chart'));
				});
	},

	renderBillboard : function(jObj) {
		var vm = Report.getViewModel(jObj.attr('data-report-type'));
		Util.jsonFireEvent(Util.getHandyclassPath() + vm.controller
				+ "/renderBillboard", vm, function(content) {
			container = jObj.attr('data-report-container');
			$("#" + container).replaceWith(content);
			jQuery('label').tooltip();
			eval(jQuery('.chart-container').attr('data-chart'));
		});
	},

	sortBillboard : function(jObj) {
		vm = Report.getViewModel(jObj.attr('data-report-type'));
		vm.reportType = jObj.attr('data-report-type');
		vm.sort = jObj.attr('data-column-name');
		vm.direction = jObj.attr('data-sort');
	},

	sort : function(jObj) {
		handy.reportViewModel.reportType = jObj.attr('data-report-type');
		handy.reportViewModel.sort = jObj.attr('data-column-name');
		handy.reportViewModel.direction = jObj.attr('data-sort');
	},
	search : function(jObj) {
		handy.reportViewModel.search = jObj.val();
	},
	category : function(jObj) {
		handy.reportViewModel.categoryFilter = jObj.attr("data-category-id");
		jQuery('#categoryFilterLabel').html(jObj.html());
	},
	status : function(jObj) {
		handy.reportViewModel.statusFilter = jObj.attr("data-status-id");
		jQuery('#statusFilterLabel').html(jObj.html());
	}
}