var CommentModule = {
	/*
	 * Нажатие на кнопку добавления корневого коментария.
	 */
	onClickSendBtn : function(forumID) {
		var wysiwygID = forumID+"_wysiwyg";
		var parentOID = jQuery('#'+wysiwygID).attr('for');
		var content = Wysiwyg.getContent(wysiwygID);
		CommentModule.funcAddNewComment(forumID, parentOID, content);
	},
	
	/*
	 * Ответить на комментарий.
	 * Запуск модального диалогового окна, содержащего текст комментария, для последующего редактирования. 
	 */
	onClickReplyBtn : function(forumID, commentOID) {
		var container = jQuery('#'+commentOID+'_comment');
		var dlg = jQuery('#'+forumID+'_modal_reply');
		var wysiwyg = dlg.find('.wysiwyg');
		
		var submitBtn = dlg.find('a');
		submitBtn.attr('target-data', wysiwyg.attr('id'));
		submitBtn.attr('target-comment', container.attr('id'));
		dlg.modal('show');
	},
	/*
	 * Нажатие на кнопку отправить на модальном диалоге добавления комментария.
	 */
	onClickSubmitReplyComment : function(forumID, item) {
		var dlg = item.closest('.modal');
		dlg.modal('hide');
		var parentObjectID = item.attr('target-comment').replace('_comment', '');
		var commentContent = Wysiwyg.getContent(dlg.find('.hc-mce').attr('id'));
		Wysiwyg.setContent(dlg.find('.hc-mce').attr('id'),'');
		CommentModule.funcAddNewComment(forumID, parentObjectID, commentContent);
	},
	// редактировать комментарий
	onClickEditBtn : function(forumID, commentOID) {
		var container = jQuery('#'+commentOID+'_comment');
		var commentContent = container.find('.comment-content');
		var dlg = jQuery('#'+forumID+'_modal_edit');
		var wysiwyg = dlg.find('.wysiwyg');
		Wysiwyg.setContent(dlg.find('.hc-mce').attr('id'),commentContent.html());
		var submitBtn = dlg.find('a');
		submitBtn.attr('target-data', wysiwyg.attr('id'));
		submitBtn.attr('target-comment', container.attr('id'));
		dlg.modal('show');
	},
	/*
	 * Нажатие на кнопку отправить на модальном диалоге редактирования комментария.
	 */
	onClickSubmitEditComment : function(forumID, item) {
		var dlg = item.closest('.modal');
		dlg.modal('hide');
		var objectID = item.attr('target-comment').replace('_comment', '');
		var commentContent = Wysiwyg.getContent(dlg.find('.hc-mce').attr('id'));
		Wysiwyg.setContent(dlg.find('.hc-mce').attr('id'),'');
		CommentModule.funcEditComment(forumID, objectID, commentContent);
	},
	// удалить комментарий
	onClickRemoveBtn : function(forumID, commentOID) {
		CommentModule.funcRemoveComment(forumID, commentOID);
	},
	/**
	 * функционал
	 */
	funcAddNewComment : function(forumID, parentOID, content) {
		var post = {};
		post.param_parentOID = parentOID;
		post.param_commentContent = content;
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxOnAddComment",
			type : "POST",
			data : post,
			cache : false,
			success : function(result) {
				// перерисовываем весь форум
				CommentModule.updateForum(forumID);
				// выполняем пользовательский скрипт
				CommentModule.eventForumDataChanged(forumID);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	funcEditComment : function(forumID, objectID, content) {
		var post = {};
		post.param_objectID = objectID;
		post.param_commentContent = content;
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxOnEditComment",
			type : "POST",
			data : post,
			cache : false,
			success : function(result) {
				// перерисовываем весь форум
				CommentModule.updateForum(forumID);
				// выполняем пользовательский скрипт
				CommentModule.eventForumDataChanged(forumID);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	funcRemoveComment : function(forumID, objectID) {
		var post = {};
		post.param_objectID = objectID;
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/ajaxOnRemoveComment",
			type : "POST",
			data : post,
			cache : false,
			success : function(result) {
				// перерисовываем весь форум
				CommentModule.updateForum(forumID);
				// выполняем пользовательский скрипт
				CommentModule.eventForumDataChanged(forumID);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateForum : function(forumID) {
		var post = {};
		post.forum_root = $('#'+forumID).attr("forum-root");
		post.forum_handler = $('#'+forumID).attr("forum-data-changed");
		$.ajax({
			url : Util.getHandyclassPath()+"/main/discussion/updateForumContent",
			type : "POST",
			data: post,
			cache : false,
			success : function(forumContent) {
				// перерисовываем весь форум
				$('#'+forumID).replaceWith(forumContent);
				// активируем визивиг.
				Wysiwyg.initF(forumID+"_wysiwyg");
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	eventForumDataChanged : function(forumID) {
		var strScript = jQuery('#'+forumID).attr('forum-data-changed'); // получаем пользовательский код.
		eval(strScript);
	}
	
}