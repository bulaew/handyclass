var EditCourseWizard = {
	onChangeStep : function(oldStep, newStep) {
		if (oldStep == 1) {
			saveFieldsData(function() {
				location = location.href.split('?')[0] + '/changestep?step='
						+ newStep;
			});
		}
		location = location.href.split('?')[0] + '/changestep?step=' + newStep;
	}
};