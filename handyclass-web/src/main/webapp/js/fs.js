$(document).ready(function onReady(){
	$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function onFullscreenChange(){
		if(document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen) {
		
		} else {
			Fullscreen.slideDown();
		}	
	});
	if(getCookie('handy.fullscreen')=='true'){
		Fullscreen.slideUp();
//		Fullscreen.onFullscreen(document.documentElement);
	}
});

var Fullscreen = {
	slideUp : function() {
		$('.can-hide').hide();
		$('.wrapper>.container').css('padding-top', '0px');
		$('.full-size').addClass('active').switchClass("icon-sort-up",
				"icon-sort-down");
	},
	slideDown : function() {
		$('.wrapper>.container').css('padding-top', '64px');
		$('.can-hide').show();
		$('.full-size').removeClass('active').switchClass("icon-sort-down",
				"icon-sort-up");
	},
	toggleFullscreen : function() {
		if ($('.full-size').hasClass('active')) {
			deleteCookie('handy.fullscreen');
			setCookie('handy.fullscreen','false');
			this.slideDown();
//			this.exitFullscreen();
		} else {
			deleteCookie('handy.fullscreen');
			setCookie('handy.fullscreen','true');
//			this.onFullscreen(document.documentElement);
			this.slideUp();
		}
	},
	onFullscreen : function(element) {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		} 
	},
	exitFullscreen : function() {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	},
};

//возвращает cookie если есть или undefined
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
	  "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	))
	return matches ? decodeURIComponent(matches[1]) : undefined 
}

// уcтанавливает cookie
function setCookie(name, value, props) {
	props = props || {}
	var exp = props.expires
	if (typeof exp == "number" && exp) {
		var d = new Date()
		d.setTime(d.getTime() + exp*1000)
		exp = props.expires = d
	}
	if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

	value = encodeURIComponent(value)
	var updatedCookie = name + "=" + value;
	for(var propName in props){
		updatedCookie += "; " + propName
		var propValue = props[propName]
		if(propValue !== true){ updatedCookie += "=" + propValue }
	}
	document.cookie = updatedCookie + ";path=/";

}

// удаляет cookie
function deleteCookie(name) {
	setCookie(name, null, { expires: -1 })
}
