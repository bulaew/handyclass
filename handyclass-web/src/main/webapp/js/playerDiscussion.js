$(document).ready(function() {
	PlayerDiscussion.showAllTopics();
});

var topics = {};

var PlayerDiscussion = {
	init : function(oid) {
		topics.course = oid;
	},
	addTopic : function() {
		var data = {
			'course' : topics.course
		};
		var url = Util.getHandyclassPath()
				+ "/main/player/discussion/topic/add";
		Util.fireEvent(url, data, function onResponse(result) {
			PlayerDiscussion.showTopic(result);
		});
	},
	deleteTopic : function(oid) {
		var data = {
			'oid' : oid
		};
		var url = Util.getHandyclassPath()
				+ "/main/player/discussion/topic/delete";
		Util.fireEvent(url, data, function onResponse(result) {
			PlayerDiscussion.showAllTopics();
		});
	},
	editTopic : function(oid) {
		var data = {
			'oid' : oid
		};
		var url = Util.getHandyclassPath()
				+ "/main/player/discussion/topic/edit";
		Util.fireEvent(url, data, function onResponse(result) {
			$('#discussionContent').replaceWith(result);
		});
	},
	cancelTopic: function(oid) {
		this.getTopic(oid);
	},
	saveTopic : function(oid) {
		var name = $('#discussionContent').find('input').val();
		var data = {
			'oid' : oid,
			'name' : name
		};
		var url = Util.getHandyclassPath()
				+ "/main/player/discussion/topic/save";
		Util.fireEvent(url, data, function onResponse(result) {
			PlayerDiscussion.getTopic(oid);
		});
	},
	getTopic : function(oid) {
		var data = {
			"oid" : oid
		};
		var url = Util.getHandyclassPath()
				+ "/main/player/discussion/view/topic";
		Util.fireEvent(url, data, function onResponse(result) {
			$('#discussionContent').replaceWith(result);
		});
	},
	showTopic : function(oid) {
		PlayerDiscussion.getTopic(oid);
		$('.hc-discussion').addClass('active');
	},
	getAllTopics : function() {
		var data = {
			"oid" : topics.course
		};
		var url = Util.getHandyclassPath() + "/main/player/discussion/view/all";
		Util.fireEvent(url, data, function onResponse(result) {
			$('#discussionsList').replaceWith(result);
		});
	},
	showAllTopics : function() {
		PlayerDiscussion.getAllTopics();
		$('.hc-discussion').removeClass('active');

	},
};