var Graffiti = {
  init: function(id) {
    var canvas = this.__canvas = new fabric.Canvas(id, {
      isDrawingMode: true
    });
  },
  initImg: function(id, data) {
    var canvas = this.__canvas = new fabric.Canvas(id, {
      isDrawingMode: true,
      backgroundColor: "#fff",
    });
    var img = new Image;
    jQuery('#' + id + '_brush').val('10 px');
    canvas.freeDrawingBrush.width = 10;
    if (data) {
      img.src = data;
      var ctx = canvas.getContext('2d');
      var fabricImg = new fabric.Image(img);
      img.onload = function() {
        canvas.setBackgroundImage(fabricImg, canvas.renderAll
          .bind(canvas));
      };
    };
    jQuery('#' + id + '_clear').click(function() {
      canvas.clear();
    });
    jQuery('#' + id + '_color').simplecolorpicker({
      picker: true
    }).change(function() {
      jQuery('#' + id + '_eraser').removeClass('active');
      canvas.freeDrawingBrush.color = jQuery(this).val();
    });
    jQuery('#' + id + '_eraser').click(function() {
      canvas.freeDrawingBrush.color = '#fff';
      jQuery(this).addClass('active');
    });
    jQuery('#' + id + '_brush').change(function() {
      var width = parseInt(jQuery(this).val());
      if (width > 100) {
        width = 100;
      } else if (width < 0) {
        width = 1;
      }
      jQuery(this).val(width + ' px');
      canvas.freeDrawingBrush.width = width;
    });

  }
}