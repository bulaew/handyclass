(function() {

	function ContactsModel() {
		var obj = {};
		/**
		 * текущая группа
		 */
		obj._activeGroup = '';
		
		return obj;
	}
	;

	if (handy)
		handy.contactsModel = new ContactsModel();
})();

var ContactsModule = {
	deleteGroup : function(groupOid) {
		post = {};
		post.groupOid = groupOid;
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/deleteList", post,  
			function(newActiveGroupOID) {
				if (groupOid == handy.contactsModel._activeGroup) {
					ContactsModule.activateGroup( newActiveGroupOID );
				}
				ContactsModule.renderGroups();
			}
		);
	},
	createGroup : function() {
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/createList", "",  
			function(activeGroupOID) {
				ContactsModule.activateGroup( activeGroupOID );
				ContactsModule.renderGroups();
			}
		);
	},
	
	activateGroup : function(groupOid) {
		if (groupOid == null)
			return;
		var oldActiveGroup = jQuery('#' + handy.contactsModel._activeGroup);
		if (oldActiveGroup != null) { // деактивируем старую группу
			jQuery(oldActiveGroup).removeClass("active");
			handy.contactsModel._activeGroup = -1;
		}
		var newActiveGroup = jQuery('#' + groupOid);
		if (newActiveGroup != null) { // активируем новую группу
			jQuery(newActiveGroup).addClass("active");
			handy.contactsModel._activeGroup = groupOid;
		}
		ContactsModule.renderGroupContent();
	},
	
	/**
	 * Сохраниение названия группы.
	 */
	changeListTitle : function(groupOid) {
		if (groupOid == null)
			return;
		post = {};
		post.groupOid = groupOid;
		post.groupTitle = jQuery('#groupTitle').find('input').val();
		
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/updateListTitle", post,  
			function(msg) {
				ContactsModule.renderGroups();
				ContactsModule.renderGroupContent();
			}
		);
	},
	
	/*
	 * Поиск пользователя по заданному критерию. После поиска - перерисовка контента группы/списка.
	 */
	findContact : function(contactName) {
		post = {};
		post.groupOid = handy.contactsModel._activeGroup;
		post.contactName = contactName;
		
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/findContact", post,  
			function(groupContent) {
				$('#groupContentPanel').replaceWith(groupContent);
			}
		);
	},
	
	addContactByUser : function(groupOID, userOID) {
		post = {};
		post.groupOid = groupOID;
		post.userOid = userOID;
		
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/addContactByUser", post,  
			function(groupContent) {
				ContactsModule.renderGroups();
				ContactsModule.renderGroupContent();
			}
		);
	},
	
	addContactByEmail : function(groupOID, email) {
		post = {};
		post.groupOid = groupOID;
		post.email = email;
		
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/addContactByEmail", post,  
			function(groupContent) {
				ContactsModule.renderGroups();
				ContactsModule.renderGroupContent();
			}
		);
	},
	
	addContactByAddressbook : function(groupOID, contactsData) {
		post = {};
		post.contacts = contactsData;
		post.activeList = groupOID;
		$.ajax({ 
             url:Util.getHandyclassPath() + "/main/contacts/addContactByAddressbook",    
             type:"POST", 
             dataType: 'json', 
             mimeType: 'application/json',
             contentType: "application/json; charset=utf-8",
             data: JSON.stringify(post), //Stringified Json Object
             async: false,    //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
             cache: false,    //This will force requested pages not to be cached by the browser          
             processData:false, //To avoid making query String instead of JSON
             success: function(resposeJsonObject){  
            	ContactsModule.renderGroups();
 				ContactsModule.renderGroupContent();
        }});
	},
	
	deleteContact : function(groupOID, contactOID) {
		post = {};
		post.groupOid = groupOID;
		post.contactOid = contactOID;
		
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/deleteContact", post,  
			function(groupContent) {
				ContactsModule.renderGroups();
				ContactsModule.renderGroupContent();
			}
		);
	},
	
	renderGroups : function() {
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/renderGroups", handy.contactsModel,  
			function(messages) {
				$('#groups').replaceWith(messages);
			}
		);
	},
	
	renderGroupContent : function(editTitleMode) {
		post = {};
		post._activeGroup = handy.contactsModel._activeGroup;
		if (editTitleMode == null) {
			post.editTitleMode = false;
		} else {
			post.editTitleMode = editTitleMode;
		}
		Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/renderGroupContent", post,  
			function(messages) {
				$('#groupContentPanel').replaceWith(messages);
			}
		);
	},
	
	showAddressBook : function() {
		alert("init");
		jQuery('.modal').modal();
	}
}