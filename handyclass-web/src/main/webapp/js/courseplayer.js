var CoursePlayer = {
	changePage : function(oid) {
		var data = {};
		data.oid = oid + "";
		var url = Util.getHandyclassPath() + "/main/courseplayer/page";
		Util.fireEvent(url, data, function(response) {
			if (response == 'true') {
				CoursePlayer.getPageContent();
			}
		});
	},
	nextPage : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/courseplayer/next";
		Util.fireEvent(url, data, function(response) {
			if (response == 'true') {
				CoursePlayer.getPageContent();
			}
		});
	},
	prevPage : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/courseplayer/prev";
		Util.fireEvent(url, data, function(response) {
			if (response == 'true') {
				CoursePlayer.getPageContent();
			}
		});
	},
	firstPage : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/courseplayer/first";
		Util.fireEvent(url, data, function(response) {
			if (response == 'true') {
				CoursePlayer.getPageContent();
			}
		});
	},
	lastPage : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/courseplayer/last";
		Util.fireEvent(url, data, function(response) {
			if (response == 'true') {
				CoursePlayer.getPageContent();
			}
		});
	},
	getPageContent : function() {
		var data = {};
		var url = Util.getHandyclassPath() + "/main/courseplayer/content";
		Util.fireEvent(url, data, function(response) {
			$('#activePage').hide(200);
			setTimeout(function() {
				$('#activePage').replaceWith(response);
				$('#activePage').show();
			}, 200);
			CoursePlayer.getPagingContent();
		});
	},
	getPagingContent : function() {
		var data = {};
		var url = Util.getHandyclassPath()
				+ "/main/courseplayer/content/paging";
		Util.fireEvent(url, data, function(response) {
			$('.wizard-paging').replaceWith(response);
		});
	},
};

$(window).unload(function() {
	var data = {};
	var url = Util.getHandyclassPath() + "/main/courseplayer/unload";
	Util.fireEvent(url, data, function(response) {

	});
});