var Participant = {};
Participant.init = function() {
	// Устанавливаем тип поиска.
	Search.type = "participants";

	jQuery('#AccessModeSelectorID').data('disabled', false);
	jQuery('#AccessModeSelectorIDSelect').css({
		'position' : 'absolute',
		'left' : '0',
		'bottom' : '0',
		'width' : '100%',
		'opacity' : 0
	});
	jQuery('#AccessModeSelectorIDInput').css({
		'border' : 0,
		'width' : '100%'
	})
			.val(
					jQuery('#AccessModeSelectorIDSelect').find(
							'option:selected').html());
	jQuery('#AccessModeSelectorIDSelect').change(function(event) {
		var self = jQuery(this);
		var input = jQuery('#AccessModeSelectorIDInput').eq(0);
		var disabled = self.data('disabled');
		if (disabled)
			return false;
		input.val(self.find('option:selected').html());
		Participant.onChangeAccessType(event);
	});
}

/*
 * Поиск пользователя по заданному критерию. После поиска - перерисовка контента
 * группы/списка.
 */
Participant.findContact = function(contactName) {
	post = {};
	post.groupOid = handy.contactsModel._activeGroup;
	post.contactName = contactName;

	Util.fireEvent(Util.getHandyclassPath() + "/main/contacts/findContact",
			post, function(groupContent) {
				$('#groupContentPanel').replaceWith(groupContent);
			});
}

Participant.onChangeAccessType = function(event) {
	Participant.accesstype($(event.currentTarget).val())
}

Participant.accesstype = function(newValue) {
	post = {};
	post.accesstype = newValue;
	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/accesstype", post,
			function(result) {
		Participant.changeAccessView(newValue)
	});
};

Participant.changeAccessView = function(newValue) {
	if (newValue == 1) {
		$("#ParticipantEditCourseActionaccessPanelID").hide();
		$("#ParticipantEditCourseActionuserPanelID").hide();
	}
	if (newValue == 2) {
		$("#ParticipantEditCourseActionaccessPanelID").show();
		$("#ParticipantEditCourseActionuserPanelID").show();
	}
	if (newValue == 3) {
		$("#ParticipantEditCourseActionaccessPanelID").hide();
		$("#ParticipantEditCourseActionuserPanelID").show();
	}
}

Participant.searchParticipant = function(searchCriteria) {
	post = {};
	post.contactName = searchCriteria;

	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/searchParticipant", post, function(
			panelContent) {
		$('#ParticipantEditCourseActionaccessPanelID')
				.replaceWith(panelContent);
	});
}

Participant.removeParticipant = function(userOid, email) {
	post = {};
	if (userOid != null) {
		post.userOid = userOid;
	}
	post.email = email;

	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/removeParticipant", post, function(
			panelContent) {
		$('#ParticipantEditCourseActionuserPanelID').replaceWith(panelContent);
	});
}

Participant.inviteUser = function(userOid) {
	post = {};
	post.userOid = userOid;

	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/inviteUser", post, function(
			panelContent) {
		$('#ParticipantEditCourseActionaccessPanelID')
				.replaceWith(panelContent);
		Participant.renderListPanel();
	});
}

Participant.inviteEmail = function(email) {
	post = {};
	post.email = email;

	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/inviteEmail", post, function(
			panelContent) {
		$('#ParticipantEditCourseActionaccessPanelID')
				.replaceWith(panelContent);
		Participant.renderListPanel();
	});
}

Participant.inviteList = function(listOid) {
	post = {};
	post.listOid = listOid;

	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/inviteList", post, function(
			panelContent) {
		$('#ParticipantEditCourseActionaccessPanelID')
				.replaceWith(panelContent);
		Participant.renderListPanel();
	});
}

Participant.inviteAddressbook = function(contactsData) {
	post = {};
	post.contacts = contactsData;
	$.ajax({
		url : Util.getHandyclassPath()
				+ "/main/editcourse/participant/inviteAddressbook",
		type : "POST",
		dataType : 'json',
		mimeType : 'application/json',
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(post), // Stringified Json Object
		async : true, // Cross-domain requests and dataType: "jsonp"
		// requests do not support synchronous operation
		cache : false, // This will force requested pages not to be cached
		// by the browser
		processData : false, // To avoid making query String instead of JSON
		success : function(jsonResult) {
			Participant.renderAddParticipantPanel();
			Participant.renderListPanel();
		}
	});
}

Participant.renderListPanel = function() {
	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/renderListPanel", "", function(
			panelContent) {
		$('#ParticipantEditCourseActionuserPanelID').replaceWith(panelContent);
	});
}

Participant.renderAddParticipantPanel = function() {
	Util.fireEvent(Util.getHandyclassPath()
			+ "/main/editcourse/participant/renderAddParticipantPanel", "",
			function(panelContent) {
				$('#ParticipantEditCourseActionaccessPanelID').replaceWith(
						panelContent);
			});
}

Participant.ui = function(id) {
	$("#ui" + id).parent().mouseenter(function() {
		$("#ui" + id).show();
	}).mouseleave(function() {
		$("#ui" + id).hide();
	});
}