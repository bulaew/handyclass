var LikeModule = {
	get : function(id) {
		$.ajax({
			url : Util.getHandyclassPath() + "/like/get/" + id,
			type : "POST",
			cache : false,
			success : function(messages) {
				$('#likeContainer' + id).replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	push : function(id) {
		$.ajax({
			url : Util.getHandyclassPath() + "/like/push/" + id,
			type : "POST",
			cache : false,
			success : function(messages) {
				$('#likeContainer' + id).replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	all : function(id) {
		$.ajax({
			url : Util.getHandyclassPath() + "/like/all/" + id,
			type : "POST",
			cache : false,
			success : function(messages) {
				LikeModule.showAll(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	last : function(id) {
		$.ajax({
			url : Util.getHandyclassPath() + "/like/last/" + id,
			type : "POST",
			cache : false,
			success : function(messages) {
				LikeModule.showLast(id, messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	showLast : function(id, messages) {
		$('#lastLike' + id).html(messages).show();
		$('#lastLike' + id).mouseleave(function(){
			$('#lastLike' + id).hide();
		});
	},
	showAll : function(messages) {
		if ($('#allLike').size() > 0) {
			$('#allLike').replaceWith(messages).modal();
		} else {
			$('#body').append(messages);
			$('#allLike').modal();
		}
	}
}