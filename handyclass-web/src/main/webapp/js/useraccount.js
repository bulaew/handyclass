var Account = {
	collectFieldsData : function() {
		var post = {};
		// Собираем данные:
		post.firstName = jQuery('#firstName').val();
		post.lastName = jQuery('#lastName').val();
		post.about = jQuery('#about').val();
		post.email = jQuery('#email').val();
		post.cur_pass = jQuery('#cur_pass').val();
		post.new_pass = jQuery('#new_pass').val();
		post.repeat_pass = jQuery('#repeat_pass').val();
		return post;
	},

	deleteAvatarFunc : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/delete_avatar",
			type : "POST",
			cache : false,
			success : function(messages) {
				$("#avatarPanel").replaceWith(messages);
				Account.updateTopmenuUserAvatar();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},

	bindSocial : function(networkType, element) {
		Util.goTo("/auth/"+networkType+"bind");
	},
	
	unbindSocial : function(networkType, element) {
		Account.preSubmitAvatarFunc();

		var url = handy.createPostUrl('unbind_social'); // ИД аякс запроса
		var post = handy.createPostData();
		post.networkType = networkType;
		handy.fireEvent(url, post, function(response) {
			var data = handy.getDataFromResponse(response); // обработка ответа
			if (data.binderContent != null) {
				jQuery('#binderPanel').replaceWith(data.binderContent);
			}
		});
	},

	/**
	 * Обработчики событий.
	 */
	onSavePersonalClick : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/save_personal",
			type : "POST",
			data : Account.collectFieldsData(),
			cache : false,
			success : function(messages) {
				$("#personalPanel").replaceWith(messages);
				Account.updateTopmenuUserName();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateTopmenuUserName : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/updateTopmenuUserName",
			type : "POST",
			cache : false,
			success : function(messages) {
				$("#topUserPrincipal").text(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},
	
	updateTopmenuUserAvatar : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/updateTopmenuUserAvatar",
			type : "POST",
			cache : false,
			success : function(messages) {
				$("#topUserAvatar").attr("src", Util.getHandyclassPath() +"/"+ messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},

	onSaveEmailClick : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/save_email",
			type : "POST",
			data : Account.collectFieldsData(),
			cache : false,
			success : function(messages) {
				$("#emailPanel").replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},

	onSavePasswordClick : function() {
		$.ajax({
			url : Util.getCurrentPath()+"useraccount/save_password",
			type : "POST",
			data : Account.collectFieldsData(),
			cache : false,
			success : function(messages) {
				$("#passwordPanel").replaceWith(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Unable to retrieve chat messages. Chat ended.");
				}
			}
		});
	},

	onSocialBtnClick : function(event, isBind) {
		var networkType = jQuery(event.target).attr('networkType');
		if (isBind == true) {
			Account.bindSocial(networkType, event.target);
		} else {
			Account.unbindSocial(networkType, event.target);
		}
	}
}
