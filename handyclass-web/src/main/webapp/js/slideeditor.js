$(document).ready(function() {
	SlideEditor.applySortComponents();
	SlideEditor.sortablePages();
});

var SlideEditor = {
	// Page
	addPage : function() {
		Util.goTo("main/editcourse/content/page/add");
	},
	openModal : function(oid) {
		$('#deleteDlgBtn').attr('data-oid', oid);
		$('#deleteDlg').modal();
	},
	deletePage : function() {
		var oid = $('#deleteDlgBtn').attr('data-oid');
		Util.goTo("main/editcourse/content/page/delete/" + oid);
	},
	movePage : function(page, place) {

	},
	changePage : function(oid) {
		Util.goTo("main/editcourse/content/page/change/" + oid);
	},
	nextPage : function(oid) {
		Util.goTo("main/editcourse/content/page/next");
	},
	lastPage : function(oid) {
		Util.goTo("main/editcourse/content/page/last");
	},
	prevPage : function(oid) {
		Util.goTo("main/editcourse/content/page/prev");
	},
	firstPage : function(oid) {
		Util.goTo("main/editcourse/content/page/first");
	},

	// Page components
	addComponent : function(type) {
		var data = {};
		data.type = type;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/add",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('.hc-editor-area>ul').append(messages);
				if ($('.hc-editor-area>ul>li').size() > 0) {
					$('.empty-message').hide();
				}
				SlideEditor.applySortComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	deleteComponent : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/delete",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				var options = {};
				$('#' + messages).effect('blind', options, 200, function() {
					$('#' + messages).remove();
				});
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	saveComponent : function(oid, componentData) {
		var data;
		if (componentData) {
			data = componentData;
		} else {
			data = SlideEditor.getSaveData(oid);
		}
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/save",
			type : "POST",
			dataType : 'json',
			mimeType : 'application/json',
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(data),
			cache : false,
			success : function(messages) {
				SlideEditor.getSavedView(oid);
				SlideEditor.applySortComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	saveChild : function(oid, data) {
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/content/component/savechild";
		Util.jsonFireEvent(url, data, function(response) {
			SlideEditor.getSavedView(oid);
			SlideEditor.applySortComponents();
		});
	},
	deleteChild : function(oid, parentOid) {
		var data = {
			'oid' : oid,
			'parent' : parentOid
		};
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/content/component/deletechild";
		Util.fireEvent(url, data, function(messages) {
			$('#' + messages).remove();
		});
	},
	getSavedView : function(oid) {
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/view/" + oid,
			type : "POST",
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
				SlideEditor.applySortComponents();
				SlideEditor.droppableComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	saveImageByUrl : function(oid) {
		var data = {};
		data.oid = oid;
		data.url = jQuery('#' + oid + '_image_url_input').val();
		data.type = "image";
		data.urlType = "outer";
		data.width = 100;
		data.rotation = 0;
		data.description = "";
		jQuery("<img>").attr('src', data.url).attr('id', 'loading_' + data.id)
				.css('width', '100%').load(
						function() {
							data.height = jQuery('#' + oid).find('img').attr(
									'src', data.url).height();
							SlideEditor.saveComponent(oid, data);
						});
	},
	saveVideoByUrl : function(oid) {
		var data = {};
		data.oid = oid;
		data.embed = 'true';
		data.url = jQuery('#input' + oid).val();
		data.type = "video";
		SlideEditor.saveComponent(oid, data);
	},
	uploadComponent : function(oid) {
		$('#file' + oid).click();
	},
	submitComponent : function(oid) {
		$('#form' + oid).submit();
		Loader.create('#' + oid);
		$("*").unbind();
		$("[onclick]").removeAttr("onclick");
	},
	getSaveData : function(oid) {
		var component = $('#' + oid);
		var data = {};
		data.oid = oid;
		if (component.hasClass('header')) {
			data.content = jQuery('#' + oid).find('input').val();
			data.type = 'header';
			return data;
		} else if (component.hasClass('text')) {
			data.type = 'text';
			data.content = Wysiwyg.getContent('text' + oid);
			return data;
		} else if (component.hasClass('image')) {
			data.type = "image";
			// data.url = jQuery('#' + oid + '_image').attr('src');
			jQuery('#' + oid + '_image').width();
			jQuery('#' + oid + '_image').parent().hide();
			var width = jQuery('#' + oid + '_image').width();
			jQuery('#' + oid + '_image').parent().show();
			data.width = width;
			data.rotation = jQuery('#' + oid + '_image').attr('data-rotation');
			data.description = jQuery('#' + oid + '_image_description').val();
			var position = parseInt(data.rotation) / 90 % 2;
			var trueWidth = jQuery('#' + oid + '_image').width();
			var trueHeight = jQuery('#' + oid + '_image').height();
			if (position == 0) {
				data.height = trueHeight;
			} else {
				data.height = trueWidth;
			}
			return data;
		} else if (component.hasClass('video')) {

		} else if (component.hasClass('graffiti')) {
			data.type = "graffiti";
			var canvas = document.getElementById(oid + '_canvas');
			data.content = canvas.toDataURL();
			return data;
		}
	},
	declineComponent : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/decline",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
				SlideEditor.applySortComponents();
				SlideEditor.droppableComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	cancelChildEdit : function(oid, parent) {
		var data = {};
		data.oid = oid;
		data.parent = parent;
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/content/childcomponent/decline";
		Util.fireEvent(url, data, function onSuccess(messages) {
			$('#' + oid + "ui").remove();
			$('#' + oid).replaceWith(messages);
		});
	},
	editComponent : function(oid) {
		var data = {};
		data.oid = oid;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/component/edit",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#' + oid).replaceWith(messages);
				SlideEditor.applySortComponents();
				SlideEditor.droppableComponents();
				$('#' + oid).find('input').focus();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	editMarker : function(oid) {
		var data = {};
		data.oid = oid;
		data.parent = $('#' + oid).closest('li.hc-player-item').attr('id');
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/childcomponent/edit",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				if ($('#' + oid + 'ui').size() > 0) {
					return;
				} else {
					$('#' + oid).append(messages);
				}
				SlideEditor.applySortComponents();
				SlideEditor.droppableComponents();
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	changeValue : function(property, oid, comp) {
		var value = parseInt(comp.val());
		if (value < 0) {
			value = 0;
		}
		$('#' + oid).css(property, value);
		comp.val(value + ' px');
	},
	changeDataValue : function(property, oid, value) {
		$('#' + oid).attr(property, value);
		$('#' + oid + '>i').attr('class', 'icon-hand-' + value);
	},
	changeTimer : function(timer) {
		var data = {};
		data.timer = timer;
		$.ajax({
			url : Util.getHandyclassPath()
					+ "/main/editcourse/content/page/timer",
			type : "POST",
			data : data,
			cache : false,
			success : function(messages) {
				$('#SlideTimer').val(messages);
			},
			error : function(xhr) {
				if (xhr.statusText != "abort" && xhr.status != 503) {
					console.error("Error!");
				}
			}
		});
	},
	// Редактирование изображения
	rotateLeft : function(oid) {
		var image = jQuery('#' + oid + '_image');
		var angle = parseInt(image.attr('data-rotation'));
		angle = angle - 90;
		image.attr('data-rotation', angle);
		image.rotate(angle);
		var position = angle / 90 % 2;
		var width = image.width();
		var height = image.height();
		if (position == 0) {
			image.parent().css({
				'height' : height
			});
			image.css('margin-top', 0);
		} else {
			var marginTop = Math.abs((width - height) / 2);
			image.css('margin-top', marginTop);
			image.parent().css({
				'height' : width
			});
		}
	},
	rotateRight : function(oid) {
		var image = jQuery('#' + oid + '_image');
		var angle = parseInt(image.attr('data-rotation'));
		angle = angle + 90;
		image.attr('data-rotation', angle);
		image.rotate(angle);
		var position = angle / 90 % 2;
		var width = image.width();
		var height = image.height();
		if (position == 0) {
			image.parent().css({
				'height' : height
			});
			image.css('margin-top', 0);
		} else {
			var marginTop = Math.abs((width - height) / 2);
			image.css('margin-top', marginTop);
			image.parent().css({
				'height' : width
			});
		}
	},
	changeSize : function(oid, obj) {
		var image = jQuery('#' + oid + '_image');
		image.parent().hide();
		// var currentWidth = image.width();
		image.parent().show();
		var width = parseInt(obj.val());
		if (width > 100 || width < 0) {
			image.css({
				'width' : '100%'
			});
			obj.val(100 + ' %');
			width = 100;
		} else {
			image.css({
				'width' : width + '%'
			});
			obj.val(width + ' %');
		}
		SlideEditor.changeContainerSize(oid);
	},
	changeContainerSize : function(oid) {
		var image = jQuery('#' + oid + '_image');
		var angle = parseInt(image.attr('data-rotation'));
		var position = angle / 90 % 2;
		var width = image.width();
		var height = image.height();
		if (position == 0) {
			image.parent().css({
				'height' : height
			});
			image.css('margin-top', 0);
		} else {
			var marginTop = Math.abs((width - height) / 2);
			image.css('margin-top', marginTop);
			image.parent().css({
				'height' : width
			});
		}
	},
	init : function() {
		// Возможность создавать компоненты с помощью перетаскивания
		SlideEditor.droppableComponents();
		SlideEditor.applySortComponents();
		jQuery('#header').draggable({
			helper : function(event) {
				return jQuery("<div class='draggable-header'></div>");
			},
			start : function(event, ui) {
				// TODO highlight
			},
			stop : function(event, ui) {
				// TODO highlight
			}
		});
		jQuery('#text').draggable({
			helper : function(event) {
				return jQuery("<div class='draggable-text'></div>");
			},
			start : function(event, ui) {
				// TODO highlight
			},
			stop : function(event, ui) {
				// TODO highlight
			}
		});
		jQuery(' #image').draggable({
			helper : function(event) {
				return jQuery("<div class='draggable-image'></div>");
			},
			start : function(event, ui) {
				// TODO highlight
			},
			stop : function(event, ui) {
				// TODO highlight
			}
		});
		jQuery('#video').draggable({
			helper : function(event) {
				return jQuery("<div class='draggable-video'></div>");
			},
			start : function(event, ui) {
				// TODO highlight
			},
			stop : function(event, ui) {
				// TODO highlight
			}
		});
		jQuery('#graffiti').draggable({
			helper : function(event) {
				return jQuery("<div class='draggable-painter'></div>");
			},
			start : function(event, ui) {
				// TODO highlight
			},
			stop : function(event, ui) {
				// TODO highlight
			}
		});
		jQuery('.hc-editor-area').droppable({
			accept : "#header, #text, #image, #video, #graffiti",
			drop : function(event, ui) {
				var type = ui.draggable.context.id;
				SlideEditor.addComponent(type);
			}
		});
		jQuery('#marker')
				.draggable(
						{
							helper : function(event) {
								return jQuery("<div class='hc-player-item marker' style='height:30px; width:60px;'></div>");
							},
							start : function(event, ui) {
								jQuery(
										'.hc-player-item.header, .hc-player-item.text')
										.not('activated')
										.addClass('for-linked');
							},
							stop : function(event, ui) {
								jQuery(
										'.hc-player-item.header, .hc-player-item.text')
										.not('activated').removeClass(
												'for-linked');
							}
						});
		jQuery('#pointer')
				.draggable(
						{
							helper : function(event) {
								return jQuery("<i class='hc-player-item pointer icon-hand-up'></i>");
							},
							start : function(event, ui) {
								jQuery(
										'.hc-player-item.header, .hc-player-item.text')
										.not('activated').not('row').addClass(
												'for-linked');
							},
							stop : function(event, ui) {
								jQuery(
										'.hc-player-item.header, .hc-player-item.text')
										.not('activated').removeClass(
												'for-linked');
							}
						});
		SlideEditor.droppableComponents();

	},
	droppableComponents : function() {
		jQuery(
				'.hc-player-item.header, .hc-player-item.text, .hc-player-item.image, .hc-player-item.painter')
				.not('.activated').droppable({
					accept : "#pointer, #marker",
					drop : function(event, ui) {
						var obj = jQuery(this);
						SlideEditor.createComponentWithParent(event, ui, obj);
					}
				});
	},
	createComponentWithParent : function(event, ui, obj) {
		var type = ui.draggable.context.id;
		if (type == 'marker') {
			SlideEditor.createMarker(event, ui, obj);
		} else if (type == 'pointer') {
			var data = SlideEditor.createPointer(event, ui, obj);
			SlideEditor.create("pointer", data);
		}
	},
	// Создаем первоначальные данные для указателя
	createPointer : function(event, ui, obj) {
		var data = {};
		data.parent = obj.attr("id");
		data.pointerType = 1;
		data.type = 'pointer';
		data.x = parseInt(ui.offset.left - obj.offset().left);
		data.y = parseInt(ui.offset.top - obj.offset().top);
		return data;
	},
	createMarker : function(event, ui, obj) {
		var data = {};
		data.parent = obj.attr("id");
		data.type = 'marker';
		SlideEditor.createMarkerInitParameters(ui, obj, data);
	},
	createMarkerInitParameters : function(ui, obj, data) {
		var marker = jQuery("<div class='hc-player-item marker'></div>");
		var x = parseInt(ui.offset.left - obj.offset().left);
		var y = parseInt(ui.offset.top - obj.offset().top);
		data.x = x;
		data.y = y;
		marker.css({
			'position' : 'absolute',
			'width' : 60,
			'height' : 30,
			'left' : x,
			'top' : y
		});
		obj.append(marker);
		obj.mousemove(function(event) {
			marker.css({
				'width' : parseInt(event.pageX - ui.offset.left),
				'height' : parseInt(event.pageY - ui.offset.top)
			});
		}).click(function(event) {
			obj.off('click');
			var width = parseInt(marker.css('width'));
			var height = parseInt(marker.css('height'));
			data.width = width;
			data.height = height;
			marker.remove();
			SlideEditor.create(data.type, data);
			SlideEditor.initComponent(obj.attr('id'));
		});
	},
	create : function(type, data) {
		if (type == 'marker') {
			var url = Util.getHandyclassPath()
					+ "/main/editcourse/content/component/add/marker";
			Util.jsonFireEvent(url, data, function(response) {
				$('#' + data.parent).append(response);
			});
		} else if (type == 'pointer') {
			var url = Util.getHandyclassPath()
					+ "/main/editcourse/content/component/add/pointer";
			Util.jsonFireEvent(url, data, function(response) {
				$('#' + data.parent).append(response);
			});
		}
	},
	initComponent : function(oid) {
		jQuery("#" + oid).click(
				function(e) {
//					e.stopPropagation();
					var x = e.offsetX == undefined ? e.originalEvent.layerX
							: e.offsetX;
					var y = e.offsetY == undefined ? e.originalEvent.layerY
							: e.offsetY;
					if ((x + 100) > jQuery(e.target).width()) {
						x -= 100;
					}
					jQuery("#ui" + oid).css({
						'top' : y,
						'left' : x
					});
					jQuery("#ui" + oid).show();
//					jQuery(this).addClass('active');
				}).mouseleave(function() {
			jQuery("#ui" + oid).hide();
			jQuery(this).removeClass('active');
		}).mouseover(function() {
			jQuery(this).addClass('active');
		});
	},
	resizableMarker : function(id, parent) {
		jQuery('#' + id).resizable({
			containment : "parent",
			handles : "n,e,s,w,ne,nw,se,sw",
			stop : function() {
				SlideEditor.saveMarker(id, parent);
			},
		});
		jQuery('#' + id).find('.ui-resizable-se').removeClass('ui-icon')
				.removeClass('ui-icon-gripsmall-diagonal-se');
		jQuery('.ui-resizable-handle').css('display', '');
	},
	// Возможность перетаскивать маркер
	draggableMarker : function(id, parent) {
		jQuery('#' + id).draggable({
			containment : "parent",
			start : function() {
				$(this).find('div').remove();
			},
			scroll : false,
			stop : function() {
				SlideEditor.saveMarker(id, parent);
			},
		});
	},
	draggablePointer : function(id, parent) {
		jQuery('#' + id).draggable({
			start : function() {
				$(this).find('.item-menu').remove();
			},
			containment : "parent",
			scroll : false,
			stop : function() {
				SlideEditor.savePointer(id, parent);
			},
		});
	},
	saveMarker : function(id, parent) {
		var data = {};
		data.type = 'marker';
		data.oid = id;
		data.parent = $('#' + id).closest('li.editor').attr('id');
		data.x = parseInt($('#' + id).css('left'));
		data.y = parseInt($('#' + id).css('top'));
		data.width = parseInt($('#' + id).css('width'));
		data.height = parseInt($('#' + id).css('height'));
		SlideEditor.saveChild(id, data);
	},
	savePointer : function(id, parent) {
		var data = {};
		data.oid = id;
		data.parent = $('#' + id).closest('li.editor').attr('id');
		data.type = 'pointer';
		data.x = parseInt($('#' + id).css('left'));
		data.y = parseInt($('#' + id).css('top'));
		data.pointerType = $('#' + id).attr('data-type');
		SlideEditor.saveChild(id, data);
	},
	applySortComponents : function() {
		if (jQuery('.hc-editor-area>ul').children().hasClass('activated')) {
			jQuery('.hc-editor-area>ul').sortable({
				disabled : true
			});
			return;
		}
		jQuery('.hc-editor-area>ul').sortable({
			disabled : false,
			cursor : "move",
			scroll : true,
			update : function(event, ui) {
				SlideEditor.move(event, ui);
			}
		});
	},
	move : function(event, ui) {
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/content/component/move";
		var data = {};
		data.position = jQuery('#' + ui.item.context.id).index(
				".hc-editor-area>ul>li");
		// data.prevPosition = ui.item.context.dataset.position;
		data.oid = ui.item.context.id;
		Util.fireEvent(url, data, function(response) {

		});
	},
	sortablePages : function() {
		$('.wizard-paging').sortable({
			items : '.page',
			update : function(event, ui) {
				SlideEditor.moveSlide(event, ui);
			}
		});
	},
	moveSlide : function(event, ui) {
		var url = Util.getHandyclassPath()
				+ "/main/editcourse/content/page/move";
		var data = {};
		data.position = ui.item.index(".wizard-paging>li") - 2;
		data.pageNumber = parseInt(ui.item.find('a').html());
		Util.fireEvent(url, data, function(response) {
			document.location = document.location;
		});
	},
};