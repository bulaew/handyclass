var Search = {};
// Тип поиска: contacts / participants;
Search.type = "contacts";

Search.init = function(id) {
	jQuery('#' + id + '_inp').focus(
			function() {
				if (jQuery('#' + id + '_ul>li').size() == 0
						&& jQuery('#' + id + '_inp').val() == '') {
					
				}
			});
	jQuery('#' + id + '_inp').blur(function() {
		jQuery('#' + id + '_ul').slideUp();
	});
	jQuery('#' + id + '_inp').keyup(function(event) {
		Search.search(id)
	});
	jQuery('#' + id + '_c').tooltip();
	jQuery('#' + id + '_a').tooltip();
}

Search.search = function(id) {
	typewatch(
			function() {
				var data = {};
				data.searchCriteria = jQuery('#' + id + '_inp').val();
				Util.fireEvent( Util.getHandyclassPath() + "/search/onSearch" , data, function(ulContent) {
					$("#"+id+"_ul").replaceWith(ulContent);
					$("#"+id+"_ul").show();
				})
			}, 750);
	
}

Search.check = function() {
	var contactName = jQuery('#searchComponent_inp').val();
	
	if (Search.type == "contacts") {
		ContactsModule.findContact(contactName);
	} else {
		Participant.searchParticipant(contactName);
	}
}

Search.clickMail = function(event, data) {
	var url = handy.createPostUrl('ajaxFindMember');
	var post = handy.createPostData();
	post.group_oid = handy.groupInfoModel._activeGroup;
	post.param_memberCriteria = data.username;
	handy.fireEvent(url, post, function(response) {
		var data = handy.getDataFromResponse(response); // обработка ответа
		if (data.param_groupControlPanel != null) {
			jQuery('#groupControlPanel').replaceWith(
					data.param_groupControlPanel); // перерисовываем панель
			// поиска.
		}
		Search.init(jQuery('.hc-adv-search').attr('id'));
	});
}

Search.clickUser = function(event, data) {
	var url = handy.createPostUrl('ajaxFindMember');
	var post = handy.createPostData();
	post.group_oid = handy.groupInfoModel._activeGroup;
	post.param_memberCriteria = data.username;
	handy.fireEvent(url, post, function(response) {
		var data = handy.getDataFromResponse(response); // обработка ответа
		if (data.param_groupControlPanel != null) {
			jQuery('#groupControlPanel').replaceWith(
					data.param_groupControlPanel); // перерисовываем панель
			// поиска.
		}
		Search.init(jQuery('.hc-adv-search').attr('id'));
	});
}

Search.clickHint = function(event) {
	var value = jQuery(event.target).html();
	jQuery('#searchComponent_inp').val(value);	
}

Search.clearResults = function(id) {
	jQuery('#' + id + '_ul>li').not('.active').remove();
	jQuery('#' + id + '_ul>li.empty').show();
}

var typewatch = (function() {
	var timer = 0;
	return function(callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	}
})();
