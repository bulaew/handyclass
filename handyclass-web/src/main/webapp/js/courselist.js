var CourseListModule = {};

/*
 * Сменить режим учитель/ученик
 */
CourseListModule.changeUserMode = function (newUserMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/changeUserMode", newUserMode, function(content) {
		CourseListModule.renderUserModeUl();
		CourseListModule.renderNotifyPanel();
		CourseListModule.renderCoursesPanel();
	});
	
}

/*
 * Сменить режим показа курсов иконки/список
 */
CourseListModule.changeViewMode = function (newViewMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/changeViewMode", newViewMode, function(content) {
		CourseListModule.renderViewModeUl();
		CourseListModule.renderCoursesPanel();
	});
}

/*
 * Изменить фильтр категорий
 */
CourseListModule.changeCategoryFilter = function (newCategoryValue) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/changeCategoryFilter", newCategoryValue, function(content) {
		CourseListModule.renderCategoryFilter();
		CourseListModule.renderCoursesPanel();
	});
}

/*
 * Сменить режим сортировки курсов
 */
CourseListModule.changeSortingFilter = function (newSortMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/changeSortingFilter", newSortMode, function(content) {
		CourseListModule.renderSortingFilter();
		CourseListModule.renderCoursesPanel();
	});
}

/*
 * Изменить фильтр регистраций.
 */
CourseListModule.changeRegFilter = function (newRegMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/changeRegFilter", newRegMode, function(content) {
		CourseListModule.renderRegFilter();
		CourseListModule.renderCoursesPanel();
	});
}

/*
 * Изменить режим оповещений.
 */
CourseListModule.changeNotifyMode = function (notifyMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/switchNotifyMode", notifyMode, function(content) {
		CourseListModule.renderNotifyPanel();
	});
}
/*
 * Удалить оповещения пользователя. 
 */
CourseListModule.removeNotifies = function(notifyMode) {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/removeNotifies", null, function(content) {
		$("#idNotifyPanel").replaceWith(content);
	});
}


CourseListModule.renderUserModeUl = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderUserModeUl", null, function(content) {
		$("#userModeUl").replaceWith(content);
	});
}

CourseListModule.renderViewModeUl = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderViewModeUl", null, function(content) {
		$("#viewModeUl").replaceWith(content);
	});
}

CourseListModule.renderNotifyPanel = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderNotifyPanel", null, function(content) {
		$("#idNotifyPanel").replaceWith(content);
	});
}

CourseListModule.renderCoursesPanel = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderCoursesPanel", null, function(content) {
		$("#coursesPanel").replaceWith(content);
	});
}

CourseListModule.renderCategoryFilter = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderCategoryFilter", null, function(content) {
		$("#categoryFilter").replaceWith(content);
	});
}

CourseListModule.renderSortingFilter = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderSortingFilter", null, function(content) {
		$("#sortingFilter").replaceWith(content);
	});
}

CourseListModule.renderRegFilter = function () {
	Util.jsonFireEvent(Util.getHandyclassPath()+ "/main/courselist/renderRegFilter", null, function(content) {
		$("#regFilter").replaceWith(content);
	});
}
