package ru.prognoz.handy.web.config;

import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import ru.prognoz.tc.mvc.common.HtmlBuilder;

@Component
public class VelocityHtmlBuilder implements HtmlBuilder {
	
	@Autowired(required = false)
	@Qualifier(value = "myVelocityEngine")
	private VelocityEngine velocityEngine;

	@Override
	public String build(final String viewName, Map<String, Object> model) {
		VelocityContext context = new VelocityContext(model);
		StringWriter writer = new StringWriter();
		velocityEngine.mergeTemplate("../views/"+viewName+".vm", "UTF-8", context, writer);
		return writer.toString();
	}

}
