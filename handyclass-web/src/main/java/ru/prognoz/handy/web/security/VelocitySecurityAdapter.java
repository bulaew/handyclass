package ru.prognoz.handy.web.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import ru.prognoz.tc.domain.User;

@Component(value="velocitySecurityAdapter")
public class VelocitySecurityAdapter {
	
	public static boolean isAnnonymus() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (obj instanceof UserDetails) {
			return false;
		} else {
						return true;
		}
	}

	public static String getPrincipal() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (obj instanceof UserDetails) {
			return ((UserDetails) obj).getUsername();
		} else {
			return "Guest";
		}
	}
	
	public static String getAvatarUrl() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String url = "/img/avatar/avatar_big.png";
		if (obj instanceof UserDetails) {
			User user = (User)obj;
			if (!StringUtils.isEmpty(user.getAvatarURL())) {
				url = user.getAvatarURL();
			}
		} 
		return url;
	}
	
	public static long getCurrentUserID() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		long userID = -1;
		if (obj instanceof UserDetails) {
			User user = (User)obj;
			return user.getId();
		} 
		return userID;
	}
}
